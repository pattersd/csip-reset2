/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m.seasonal;

import csip.ModelDataService;
import csip.api.server.ServiceException;
import csip.annotations.Description;
import csip.annotations.Name;
import csip.annotations.Resource;
import static csip.annotations.ResourceType.ARCHIVE;
import csip.annotations.Resources;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.Path;
import m.reset.V2;

/**
 *
 * @author pattersd
 */
@Name("AgroET")
@Description("AgroET image processing for a season.")
@Path("m/seasonal/1.0")
@Resources({
    @Resource(file = "/python/automatic.zip", type = ARCHIVE),})
public class seasonal_v1 extends V2 {

    boolean showProgress = false;

    @Override
    protected void doProcess() throws Exception {
        // Save the request to the workspace so it can be picked up by the web server.
        File req = new File(workspace().getDir()+ "/request.json");
        FileWriter writer = new FileWriter(req);
        request().getRequest().write(writer);
        writer.close();
        results().put("request.json", req);

        String driver = "seasonal/AgroET_seasonal_automatic.py";

        File f = new File("/tmp/csip/python/" + driver);

        List<String> args = new ArrayList<>();

        addStringArgument(args, "start_date", "");
        addStringArgument(args, "end_date", "");
        addStringArgument(args, "aoi", "--aoi");
        addStringArgument(args, "project", "--project");
        // 1 = use gridded if no climate data, 2 = use weather data only, 3 = use gridded only
        //  default is 1
        addIntArgument(args, "weather-data", "--weather-data");
        addIntArgument(args, "max-cloud-perc", "--max-cloud-perc");
        addBooleanArgument(args, "NDVI", "--ndvi");

        writeAgroET_input();

        String error_log = runPython(f, args, workspace().getDir());

        if (!error_log.equalsIgnoreCase("")) {
            throw new ServiceException(error_log);
        }
    }

    @Override
    protected void postProcess() throws Exception {
        File reset_log = workspace().getExistingFile("progress.log");
        results().put(reset_log, "Log of run");
        
        // Look for any files that start with ET_seasonal_exact_dates_
        String project = parameter().getString("project", "");
        if (!project.isEmpty()) {
            File projDir = new File("/reset/seasonal_projects/" + project);

            List<String> rasters = Arrays.asList("ET_seasonal_exact_dates_");
            LOG.info("Checking " + projDir + " for seasonal files");
            for (File file : projDir.listFiles()) {
                for (String test : rasters) {
                    if (file.getName().startsWith(test)) {
                        results().put(file);
                    }
                }
            }
        }
        
        emailResults(parameter().getString("user", "no_user"), parameter().getString("project"), parameter().getString("email", "pattersd@colostate.edu"));
    }
}
