/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m.reset;

// import gisobjects.raster.GISRaster;
import csip.ModelDataService;
import csip.api.server.ServiceException;
import static csip.annotations.ResourceType.ARCHIVE;
//import oms3.annotations.*;
import csip.annotations.*;
//import csip.utils.Services;
// import gisobjects.GISObjectException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.ws.rs.Path;

/**
 *
 * @author pattersd
 */
@Name("AgroET")
@Description("AgroET image processing")
@Path("m/process/1.0")
@Resources({
    @Resource(file = "/python/automatic.zip", type = ARCHIVE),})
public class V1 extends ModelDataService {

    @Override
    protected void doProcess() throws Exception {
        // Save the request to the workspace so it can be picked up by the web server.
        File req = new File(workspace().getDir()+ "/request.json");
        try (FileWriter writer = new FileWriter(req)) {
          request().getRequest().write(writer);
        }
        results().put("request.json", req);

        // Download landsat image
        String scene_id = parameter().getString("scene_id");

        String driver = "AgroET_WEB_LS8.py";
        saveFile(scene_id);

        writeAgroET_input();
        
        File f = new File("/tmp/csip/python/" + driver);
        String error_log = runPython(f);

        if (!error_log.equalsIgnoreCase("")) {
            throw new ServiceException(error_log);
        }
    }

    @Override
    protected void postProcess() throws Exception {
        File reset_log = workspace().getExistingFile("progress.log");
        results().put(reset_log, "Log of run");

        List<String> rasters = Arrays.asList("et_24_raster.img", "et_24_raster_uncal.img", "et_inst.img", "evapo_fr.img", "composite.tif", "ndvi.img", "et_seasonal.img");
        for (File file : workspace().getDir().listFiles()) {
            for (String test : rasters) {
                if (file.getName().toLowerCase().endsWith(test)) {
                    results().put(file);
                    LOG.info(file.toString());
                }
            }
        }

        List<String> shapefiles = Arrays.asList("cold_aoi", "hot_aoi", "cold_pts_rn", "hot_pts");
        List<String> exts = Arrays.asList(".shp", ".dbf", ".cpg", ".shx", ".prj");
        for (File file : workspace().getDir().listFiles()) {
            for (String shapefile : shapefiles) {
                for (String ext : exts) {
                    String path = shapefile + ext;
                    if (file.getName().toLowerCase().endsWith(path)) {
                        results().put(file);
                        LOG.info(file.toString());
                    }
                }
            }
        }
        
        
        
        emailResults(parameter().getString("user", "<no user>"), parameter().getString("project", "no project"), parameter().getString("email", ""));

        // Open the comp file to make sure that is copied since there seems to be lag when
        //   writing the file.
//        File resultsDir = Services.getResultsDir(getSUID());
//        if (!compFileName.isEmpty()) {
//            int cnt = 0;
//            String compFilePath = resultsDir.getPath().concat("/" + compFileName);
//            while (cnt < 5) {
//                try {
//                    GISRaster rast = new GISRaster(compFilePath);
//                    HashMap<Double, Integer> hist = rast.createRasterHistogram();
//                    cnt = 1000;
//                } catch(GISObjectException | IOException e) {
//                    LOG.info("Failed to load file " + compFilePath);
//                    cnt += 1;
//                    Thread.sleep(5000);
//                }
//            }
//        }
    }

    // run with no arguments
    public String runPython(File f) throws IOException, InterruptedException, ServiceException {
        return runPython(f, Arrays.asList(), workspace().getDir());
    }

    public String runPython(File driver, List<String> args, File cwd) throws IOException, InterruptedException, ServiceException {
        List<String> pbargs = new ArrayList<String>();
        pbargs.add("python3");
        pbargs.add(driver.getAbsolutePath());
        pbargs.addAll(args);
        LOG.info("Running " + String.join(" ", pbargs));
        ProcessBuilder pb = new ProcessBuilder(pbargs);
        // pb.redirectErrorStream();
        Map<String, String> env = pb.environment();
        env.put("PYTHONPATH", "/tmp/csip/python/");
        env.put("LOGLEVEL", parameter().getString("LOGLEVEL", "DEBUG"));

        pb.directory(cwd);
        Process p = pb.start();

        Logger logger = Logger.getLogger("reset");
        logger.log(Level.INFO, "Running {0}", String.join(" ", pbargs));

        File log = workspace().getFile("process.log");
        log.delete();
        FileHandler fh = new FileHandler(log.getPath());
        fh.setFormatter(new SimpleFormatter());
        logger.addHandler(fh);
        StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(), logger::info);
        StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(), logger::severe);
        Thread gobbler = new Thread(outputGobbler);
        Thread errorgobbler = new Thread(errorGobbler);
        gobbler.start();
        errorgobbler.start();

        while (p.isAlive()) {
            Thread.sleep(1000);
            File reset_log = workspace().getFile("progress.log");
            if (reset_log.exists()) {
                List<String> lines = Files.readAllLines(reset_log.toPath());
                setProgress(String.join("\n", lines));
            }
        }
        // This worked fine
        int exitCode = p.waitFor();
        LOG.info("Exit Code : " + exitCode);

        fh.close();
        List<String> lines = Files.readAllLines(log.toPath());
        lines.removeIf(s -> s.contains("Iterator"));
        lines.removeIf(s -> !s.contains("SEVERE"));
        String errorLine = "";
        if (exitCode > 0) {
            // Return the last line
            if (!lines.isEmpty()) {
                errorLine = lines.get(lines.size() - 1);
            }
            if (errorLine.isEmpty()) {
                errorLine = "Exit.";
            }
        }
        return errorLine;
    }

    public void saveFile(String scene_id) throws IOException, ServiceException, InterruptedException {
        File f = new File("/tmp/csip/python/download.py");
        String errors = runPython(f, Arrays.asList(scene_id, workspace().getDir().toString()), f.getParentFile());
        if (!errors.isEmpty()) {
            throw new ServiceException(errors);
        }
    }

    public void writeAgroET_input() throws FileNotFoundException, ServiceException {
        File agroET_input = new File(workspace().getDir(), "/AgroET_input.txt");
        PrintWriter writer = new PrintWriter(agroET_input.getPath());
        writer.println("DATA_DIRECTORY=.");
        writer.println("LS_IMAGE_DIR=image");
        writer.println("DEM_FILE=dem.tif");
        writer.println("climate_windrun_mile_day=climate_windrun_mile_day.tif");
        writer.println("climate_etr_hourly=climate_etr_hourly.tif");
        writer.println("climate_etr=climate_etr.tif");
        writer.println("WS_FILE=ws.shp");
        writer.println("AOI_FILE=aoi.shp");
        List<String> options = Arrays.asList(
                "USER_CLOUD_BUFFER", "USER_COLD_BUFFER", "USER_COLD_AOI_FILE",
                "USER_COLD_PTS_FILE", "USER_HOT_AOI_FILE", "USER_HOT_PTS_FILE",
                "LS7_FILL_GAP", "USER_UNCALIB", "DEBUG_FLAG", "USER_CONL", "DEM_FLAT",
                "USER_MAX_CLOUD_PERC");
        for (String option : options) {
            String val = parameter().getString(option, "");
            if (!val.isEmpty()) {
                writer.println(option + "=" + val);
            }
        }
        writer.close();
    }
  
    public void emailResults(String user, String project, String to_email) throws Exception {
        String driver = "email_results.py";

        File f = new File("/tmp/csip/python/" + driver);

        String host = request().getHost();
        if (host.startsWith("172")) {
            // This is localhost
            host = "localhost";
        } else {
            // assume production
            host = "agroet.westus.cloudapp.azure.com";
        }
                
        List<String> pbargs = new ArrayList();
        pbargs.add("python3");
        pbargs.add(f.getAbsolutePath());
        pbargs.add(user);
        pbargs.add(project);
        pbargs.add(host);
        pbargs.add(parameter().getString("email_to", "pattersd@colostate.edu"));
        LOG.info("Running " + String.join(" ", pbargs) + " in directory " + workspace().getDir());
        ProcessBuilder pb = new ProcessBuilder(pbargs);
        pb.directory(workspace().getDir());
        
        // pb.redirectErrorStream();
        Map<String, String> env = pb.environment();
        env.put("PYTHONPATH", "/tmp/csip/python/");

        Process p = pb.start();
    }
}
