/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m.reset;

import csip.annotations.Description;
import csip.annotations.Name;
import csip.annotations.Resource;
import static csip.annotations.ResourceType.ARCHIVE;
import csip.annotations.Resources;
import csip.api.server.ServiceException;
import java.io.File;
import java.util.*;
import javax.ws.rs.Path;

/**
 *
 * @author pattersd
 */
@Name("AgroET")
@Description("AgroET image processing")
@Path("m/process/2.0")
@Resources({
  @Resource(file = "/python/automatic.zip", type = ARCHIVE, id = "AgroET_automatic"),})
public class V2 extends V1 {

  @Override
  protected void doProcess() throws Exception {
    List<String> args = new ArrayList<>();

    addStringArgument(args, "date", "");
    addStringArgument(args, "aoi", "--aoi");
    addStringArgument(args, "row", "--row");
    addStringArgument(args, "path", "--path");
    addIntArgument(args, "cold_buffer", "--cold-buffer");
    addIntArgument(args, "cloud_buffer", "--cloud-buffer");
    addStringArgument(args, "cold_aoi", "--cold-aoi");
    addStringArgument(args, "cold_pts", "--cold-pts");
    addStringArgument(args, "hot_aoi", "--hot-aoi");
    addStringArgument(args, "hot_pts", "--hot-pts");
    addStringArgument(args, "run_name", "--prefix");
    addStringArgument(args, "email", "--user");
    addBooleanArgument(args, "debug", "--debug");
    addBooleanArgument(args, "ls7-disable-fill-gap", "--ls7-disable-fill-gap");
    addBooleanArgument(args, "user-uncalib", "--user-uncalib");
    addIntArgument(args, "user-conl", "--user-conl");
    addStringArgument(args, "max-cloud-perc", "--max-cloud-perc");
    addBooleanArgument(args, "no-flat-dem", "--no-flat-dem");
    // 1 = use gridded if no climate data, 2 = use weather data only, 3 = use gridded only
    //  default is 1
    addIntArgument(args, "weather-data", "--weather-data");
    boolean testClimate = parameter().getBoolean("test_climate", false);
    if (testClimate) {
      args.add("--test-climate");
    }
    
    File f = new File("/tmp/csip/python/AgroET_automatic.py");
    String error_log = runPython(f, args, workspace().getDir());

    if (parameter().getBoolean("download-ws", false)) {
      // Don't trigger an error if it didn't work
      results().put(workspace().getDir(), "workspace.zip");
    } else if (!error_log.equalsIgnoreCase("")) {
      throw new ServiceException(error_log);
    }
  }

  protected void addStringArgument(List<String> args, String csip_arg, String model_param) throws ServiceException {
    String missingVal = "__missing__";
    String csip_val = parameter().getString(csip_arg, missingVal);
    if (!csip_val.equals(missingVal)) {
      // not sure if this is useful? Why isn't an empty string okay?
      if (!model_param.isEmpty()) {
        args.add(model_param);
      }
      args.add(csip_val);
    }
  }

  protected void addIntArgument(List<String> args, String csip_arg, String model_param) throws ServiceException {
    Integer csip_val = parameter().getInt(csip_arg, -999);
    if (csip_val != -999) {
      if (!model_param.isEmpty()) {
        args.add(model_param);
      }
      args.add(csip_val.toString());
    }
  }

  protected void addBooleanArgument(List<String> args, String csip_arg, String model_param) throws ServiceException {
    if (parameter().has(csip_arg)) {
      boolean csip_val = parameter().getBoolean(csip_arg);
      // All boolean parameters are store_true but check the arg anyway
      if (csip_val) {
        args.add(model_param);
      }
    }
  }
}
