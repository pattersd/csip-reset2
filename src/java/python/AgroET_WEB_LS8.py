from __future__ import print_function

# This must be the first statement before other statements.
# You may only put a quoted or triple quoted string,
# Python comments, other future statements, or blank lines before the __future__ line.

try:
    import __builtin__
except ImportError:
    import builtins as __builtin__

from osgeo import gdal
from osgeo import osr
from osgeo import ogr
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
import csip
import numpy as np
import subprocess
import rasterio
import rasterio.features
import json, logging, re, os, sys, glob, requests, shutil, subprocess, urllib
import xml.etree.ElementTree as ET
from rasterstats.utils import stats_to_csv
from rasterstats import zonal_stats
import osgeo.ogr
import shapely.geometry
from shapely.wkt import dumps, loads
from os import path
import math
import struct
import gzip
import csv
from operator import itemgetter
from datetime import datetime
from AgroET_library import write_log 

# !/usr/bin/python
import sys, getopt

# Store input and output file names
ifile = ""
ofile = ""

# Read command line args
myopts, args = getopt.getopt(sys.argv[1:], "i:o:")
AgroET_file = ""
###############################
# o == option
# a == argument passed to the o
###############################
for o, a in myopts:
    if o == "-i":
        AgroET_file = a
#    elif o == '-o':
#        ofile = a

if AgroET_file == "":
    AgroET_file = "AgroET_input.txt"

# Display input and output file name passed as the args
write_log("Input file is ", AgroET_file)

# End Input File Name

GFSAD_file_orig = "/tmp/csip/GFSAD/GFSAD30.tif"
GFSAD_file1 = "GFSAD30_clip1.tif"
GFSAD_file = "GFSAD30_clip.tif"
GFSAD_mask_file = "GFSAD30_mask.tif"
loglevel = os.environ.get("LOGLEVEL", "DEBUG").upper()
logging.basicConfig(filename="AgroET.log", level=loglevel)

pf = open("progress.log", "a+")

# Redefine print so that it sends results to logger.
_print = print


def print(*args):
    msg = " ".join([str(a) for a in args])
    logging.debug(msg)
    _print(msg)


# redefine exit so that triggers an exception
def exit(exit_code):
    raise Exception("Exit code {}".format(exit_code))


def gdal_calc_func(outfile_name, gdal_calc):
    delete_file_w_path(basedir + "/" + outfile_name)
#    write_log("gdal_calc_func ", gdal_calc)
    if DEBUGFLAG == 2:
        write_log(gdal_calc)
    os.system(gdal_calc)


# register all of the drivers
gdal.AllRegister()
# this allows GDAL to throw Python Exceptions
gdal.UseExceptions()

USER_COLD_AOI_FILE = 0
USER_HOT_AOI_FILE = 0
USER_COLD_PTS_FILE = 0
USER_HOT_PTS_FILE = 0
USER_CLOUD_BUFFER = 0
USER_CLOUD_PERCENT = 0
max_cloud_percent = 50.0
USER_COLD_BUFFER = 0
LS7_DISABLE_FILL_GAP = 0
USER_DEF_CONL = 0
USER_UNCALIB = 0
AgroET_uncalib = 0
USER_DEM = 0
DEM_FLAT = 1
seasonal = 0

# Raster layers cell size
cellSize = 30
# Initial Search Radius for cold points around Weather Stations (km)
sr = 20
# Maximum Search Radius for cold points around Weather Stations (km)
sr_m = 40
# number of minimum adjacent pixels that will not be removed
threshold = 50
# adjacent pixels are only considered in four directions (-4) vs eight directions (-8)
connected = 8

# DEBUGFLAG sets a flag that prints the different commands that are being executed (mainly gdal_calc).
# 0 produces only output that GDAL generates and send to the console.
# 1 prints the most output to the console (all gdal_calc commands and more).
# 2 prints only some variable values.
DEBUGFLAG = 0
# DEBUGCODE sets a flag that copies intermediate output layers.
# 1 copy the output intermediate output layers
DEBUGCODE = 0

# LAG 12-17-19 Input File Now is defined at the beginning with -is AgroET_file = 'AgroET_input.txt'
if not os.path.exists(AgroET_file):
    write_log(
        "AgroET input file", AgroET_file, "does not exist.  You need to create it !!!!"
    )


def copyFile(src, dest):
    try:
        shutil.copyfile(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        write_log("Error source and destination are the same : %s" % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        write_log("Error source file does not exist: ", e.strerror, " source file is ", src)


def copy_shp_file(base_name, new_base_name):
    delete_shp_file(new_base_name)
    itemList = os.listdir(basedir)
    for item in itemList:
        if base_name[:-4] == item[:-4]:
            # write_log('file copied is ', basedir + '/' + new_base_name[:-4] + item[-4:])
            try:
                os.remove(basedir + "/" + new_base_name[:-4] + item[-4:])
            except OSError:
                # LAG 8-12                if DEBUGFLAG == 1:
                # LAG 8-12                write_log('!!!!!!!! could not remove file ', basedir + '/' + new_base_name[:-4] + item[-4:])
                pass
            copyFile(
                basedir + "/" + item, basedir + "/" + new_base_name[:-4] + item[-4:]
            )


def delete_shp_file(shp_name):
    if os.path.exists(shp_name):
        driver = ogr.GetDriverByName("ESRI Shapefile")
        driver.DeleteDataSource(shp_name)


def delete_files_w_pattern(dirpath, pattern):
    # Get a list of all the file paths that ends with .txt from in specified directory
    fileList = glob.glob(os.path.join(dirpath, pattern))

    # Iterate over the list of filepaths & remove each file.
    for filePath in fileList:
        try:
            os.remove(filePath)
        except:
            if DEBUGFLAG == 1:
                write_log("!!!!!!!! could not remove file ", filePath)
            pass


def delete_file(filename):
    full_name = basedir + "/" + filename
    if os.path.isfile(full_name):
        try:
            os.remove(full_name)
        except OSError:
            # LAG 8-12            if DEBUGFLAG == 1:
            # LAG 8-12                write_log('!!!!!!!! could not remove file ', full_name)
            pass


def delete_file_w_path(filename):
    full_name = filename
    if os.path.isfile(full_name):
        try:
            os.remove(full_name)
        except OSError:
            # LAG 8-12            if DEBUGFLAG == 1:
            # LAG 8-12                write_log('!!!!!!!! could not remove file ', full_name)
            pass


def check_utm_prj(ls_utm_z, proj_file, file_name, f_type):
    if f_type == "shapefile":
        check_file_exists(
            file_name[:-4] + ".prj",
            " ",
            0,
            "Projection file (.prj) for " + file_name + " is missing.",
        )
        fproj = open(proj_file, "r")

        ln = fproj.readline()
        fproj.close()
        s2 = "Zone"
        start = ln.find(s2)
        if start == -1:
            reproj_shp(ls_utm_z, file_name)
            write_log(
                file_name,
                "Shapefile was not in UTM Coordinates.  \nIt was projected into the same UTM coordinates as the Landsat image!!!!",
            )
        utm_z = ln[start + 5 : start + 7]
        if DEBUGFLAG == 1:
            write_log("Filename is ", file_name)
            write_log("Projection file line ", ln)
            write_log("start of UTM Zone ID ", start)
            write_log("Shapefile UTM ZONE is ", utm_z, " Landsat UTM ZONE is ", ls_utm_z)
        fproj.close()
        if utm_z != ls_utm_z:
            reproj_shp(ls_utm_z, file_name)
            write_log(
                file_name,
                "Shapefile is in a different UTM zone than the Landsat image.\nIt was projected into the same UTM zone as the Landsat image!!!!",
            )

    if f_type == "raster":
        rast_utm_z = raster_utm_prj(basedir + "/" + file_name)
        if rast_utm_z != ls_utm_z:
            reproj_rast(ls_utm_z, file_name)


def check_file_exists(file_name, f_type, ls_utm_zone, error_text):
    file_exists = os.path.isfile(basedir + "/" + file_name)
    if file_exists == 0:
        write_log(error_text, basedir + "/" + file_name, " does not exist !!!!")
        exit(0)
    if f_type == "raster":
        check_utm_prj(ls_utm_zone, " ", file_name, "raster")
    if f_type == "shapefile":
        check_utm_prj(
            ls_utm_zone, basedir + "/" + file_name[:-4] + ".prj", file_name, "shapefile"
        )


def check_directory_exists(directory_path, error_text):
    dir_exists = path.exists(directory_path)
    if dir_exists == 0:
        write_log(error_text, directory_path, " does not exist !!!!")
        exit(0)


def get_band_stats_original(layer):
    # open the image
    ds = gdal.Open(basedir + "/" + layer, GA_ReadOnly)
    # get raster band 1
    band = ds.GetRasterBand(1)
    # set NoData value
    band.SetNoDataValue(-9999)
    # Calculate and write_log Statistics

    try:
        stats = band.GetStatistics(0, 1)
    except:
        write_log("Failed to compute statistics, no valid pixels found in sampling.")
        return (-999, -999, -999, -999)

    if DEBUGFLAG == 1:
        write_log(
            layer,
            " Statistics min, max and avg std ",
            stats[0],
            stats[1],
            stats[2],
            stats[3],
        )
    del ds
    return (stats[0], stats[1], stats[2], stats[3])


def CheckMinValue(in_image):
    min_v, max_v, avg_v, std_v = get_band_stats_original(in_image)
    if min_v <= 0.0:
        write_log(
            "The ",
            in_image,
            " layer has values less than or equal to zero.  This should not happen. Therefore, there must be an error in this layer.  \nPlease check and re-generate this layer and re-run the model.",
        )
        exit(0)


def CheckPixelResolution(in_image, cell_size):
    delete_file_w_path(basedir + "/" + "gdalinfo_output.txt")
    gdalinfo = (
        "gdalinfo -nomd -norat -noct "
        + in_image
        + " > "
        + basedir
        + "/gdalinfo_output.txt"
    )
    if DEBUGFLAG == 1:
        write_log(gdalinfo)
    os.system(gdalinfo)
    xres, yres = PixelResolution(basedir + "/gdalinfo_output.txt")
    if xres < cell_size - 0.05 or yres < cell_size - 0.05:
        write_log("The X & Y resolution of file ", in_image)
        write_log("X resolution = % 6.2f, Y resolution is = % 6.2f " % (xres, yres))
        write_log(
            "The resolution of this file is less than ",
            cell_size,
            " meters and therefore it is not compatible with the LandSat image. Recreate this file with a ",
            cell_size,
            " meter resolution.",
        )
        reproj_rast(utm_zone, in_image)
    # 12-1-20        exit(1)
    if xres > cell_size + 0.05 or yres > cell_size + 0.05:
        write_log("The X & Y resolution of file ", in_image)
        write_log("X resolution = % 6.2f, Y resolution is = % 6.2f " % (xres, yres))
        write_log(
            "The resolution of this file is greater than ",
            cell_size,
            " meters and therefore it is not compatible with the LandSat image. Recreate this file with a ",
            cell_size,
            " meter resolution.",
        )
        reproj_rast(utm_zone, in_image)
    # 12-1-20        exit(1)
    delete_file_w_path(basedir + "/" + "gdalinfo_output.txt")



def raster_utm_prj(prj_tif):
    # tif with projections I want
    tif = gdal.Open(prj_tif, GA_ReadOnly)

    # set spatial reference and transformation
    targetprj1 = osr.SpatialReference(wkt=tif.GetProjection())
    targetprj = r"{}".format(targetprj1)
    start_prj = targetprj.find("zone")
    tif = None

    if start_prj == -1:
        write_log(
            "The UTM zone for ",
            prj_tif,
            "was not found. Therefore, it will try to be reprojected to the Landsat UTM zone!!!!",
        )
        utm_prj = -1
    else:
        utm_prj = targetprj[start_prj + 5 : start_prj + 7]
    if DEBUGFLAG == 1:
        write_log("utm_prj string is ", utm_prj)
    return utm_prj


def reproj_shp(ls_utm_z, shp_name):
    orig_shp = shp_name[:-4] + "_orig.shp"
    copy_shp_file(shp_name, orig_shp)
    delete_shp_file(shp_name)
    os.system(
        "ogr2ogr -t_srs EPSG:326"
        + ls_utm_z
        + " "
        + basedir
        + "/"
        + shp_name
        + " "
        + basedir
        + "/"
        + orig_shp
    )


def ls7_fill_scan_gap(ls_name1):
    if DEBUGFLAG == 1:
        write_log(
            "LS7_fill_scan_gap basedir ",
            basedir,
            "ls_image_dir",
            ls_image_dir,
            "ls_name1 ",
            ls_name1,
        )
    ls_name = basedir + "/" + ls_image_dir + "/" + ls_name1
    ET1 = gdal.Open(ls_name, GA_Update)
    ETband = ET1.GetRasterBand(1)
    gdal.FillNodata(
        targetBand=ETband, maskBand=None, maxSearchDist=10, smoothingIterations=0
    )
    ET1 = None


def reproj_rast(ls_utm_z, rast_name):
    orig_rast = rast_name[:-4] + "_orig." + rast_name[-3:]
    copyFile(basedir + "/" + rast_name, basedir + "/" + orig_rast)
    delete_file_w_path(basedir + "/" + rast_name)
    str_cellSize = str(cellSize)
    os.system(
        "gdalwarp -t_srs EPSG:326"
        + ls_utm_z
        + " -tr "
        + str_cellSize
        + " "
        + str_cellSize
        + " "
        + basedir
        + "/"
        + orig_rast
        + " "
        + basedir
        + "/"
        + rast_name
    )


def align_rast(rast_name):
    orig_rast = rast_name[:-4] + "_orig." + rast_name[-3:]
    copyFile(basedir + "/" + rast_name, basedir + "/" + orig_rast)
    delete_file_w_path(basedir + "/" + rast_name)
    str_cellSize = str(cellSize)
    str_LL_x = str(LL_x)
    str_UR_x = str(UR_x)
    str_LL_y = str(LL_y)
    str_UR_y = str(UR_y)
    os.system(
        "gdalwarp -tr "
        + str_cellSize
        + " "
        + str_cellSize
        + " -te "
        + str_LL_x
        + " "
        + str_LL_y
        + " "
        + str_UR_x
        + " "
        + str_UR_y
        + " "
        + basedir
        + "/"
        + orig_rast
        + " "
        + basedir
        + "/"
        + rast_name
    )
    if DEBUGFLAG == 1:
        write_log(
            "gdalwarp -tr "
            + str_cellSize
            + " "
            + str_cellSize
            + " -te "
            + str_LL_x
            + " "
            + str_LL_y
            + " "
            + str_UR_x
            + " "
            + str_UR_y
            + " "
            + basedir
            + "/"
            + orig_rast
            + " "
            + basedir
            + "/"
            + rast_name
        )


def PixelResolution(fp_path):
    fp = open(fp_path)
    for line in fp:
        if "Pixel Size " in line:
            line2 = line.split("=" or ",")[-1].strip()
            line3 = line2[1:-1]
            xres, yres = map(real, line3.split(","))
            if DEBUGFLAG == 1:
                write_log(
                    "Pixel Size String %s - X resolution % 6.2f, Y resolution % 6.2f"
                    % (line3, xres, yres)
                )
            break
    fp.close()
    return (abs(float(xres)), abs(float(yres)))


def get_band_stats(in_image):
    delete_file_w_path(basedir + "/" + "gdalinfo_raster_output.txt")
    gdalinfo = (
        "gdalinfo -stats -nomd -norat -noct "
        + in_image
        + " > "
        + basedir
        + "/gdalinfo_raster_output.txt"
    )
    if DEBUGFLAG == 1:
        write_log(gdalinfo)
    os.system(gdalinfo)
    fp = open(basedir + "/gdalinfo_raster_output.txt")
    for line in fp:
        # if the word Minimum is the gdalinfo_unfirm_output.txt file it means the file is not uniform (all zero values) and therefore the Uniform value is not true = 0
        if "Minimum=" in line:
            line0 = line.split(", " or "=")
            line1 = line0[0].split("=")
            Min_val = line1[1]
            line2 = line0[1].split("=")
            Max_val = line2[1]
            line3 = line0[2].split("=")
            Avg_val = line3[1]
            line4 = line0[3].split("=")
            Std_val = line4[1]
            break
    fp.close()
    delete_file_w_path(basedir + "/" + "gdalinfo_raster_output.txt")
    # 5-6-20    write_log('Min, Max, Avg, Std',Min_val, Max_val, Avg_val, Std_val)
    return (Min_val, Max_val, Avg_val, Std_val)


def Totally_cloudy(in_image):
    Min_val, Max_val, Avg_v, STD_d = get_band_stats_original(in_image)
    with rasterio.open(basedir + "/" + in_image, "r") as src:
        meta = src.meta.copy()
        data = src.read(1)
    pct_valid = 100 * (data != meta["nodata"]).sum() / (meta["width"] * meta["height"])
    valid_pixels = meta["width"] * meta["height"] * pct_valid / 100.0
#    write_log(
#        "Percent of valid data is ",
#        pct_valid,
#        " Number of valid pixels are ",
#        valid_pixels,
#    )
    return 100.0 - pct_valid


def Uniform_Raster(in_image):
    no_nass_categories = 1
    Min_val, Max_val, Avg_v, STD_d = get_band_stats_original(in_image)
    with rasterio.open(basedir + "/" + in_image, "r") as src:
        meta = src.meta.copy()
        data = src.read(1)
    pct_valid = 100 * (data != meta["nodata"]).sum() / (meta["width"] * meta["height"])
    valid_pixels = meta["width"] * meta["height"] * pct_valid / 100.0
    write_log(
        "Percent of valid data is ",
        pct_valid,
        " Number of valid pixels are ",
        valid_pixels,
    )
    src = None
    if valid_pixels > MIN_COLD_RASTERS:
        float_Min_val = float(Min_val)
        if Min_val != Max_val:
            no_nass_categories = 0
        elif Min_val == Max_val and float_Min_val == 1.0:
            no_nass_categories = 0
    return no_nass_categories


def Read_Raster_Extent(in_image):
    delete_file_w_path(basedir + "/" + "gdalinfo_output.txt")
    gdalinfo = (
        "gdalinfo -nomd -norat -noct "
        + in_image
        + " > "
        + basedir
        + "/gdalinfo_output.txt"
    )
    os.system(gdalinfo)
    fp = open(basedir + "/gdalinfo_output.txt")
    for line in fp:
        if "Upper Left " in line:
            line2 = line.split(" " or ")")
            UL_x = line2[5][:-1]
            UL_y = line2[6][:-1]
            if DEBUGFLAG == 1:
                write_log("Upper Left ", line2, "X ", UL_x, "Y ", UL_y)
        if "Lower Left " in line:
            line2 = line.split(" " or ")")
            LL_x = line2[5][:-1]
            LL_y = line2[6][:-1]
            if DEBUGFLAG == 1:
                write_log("Lower Left ", line2, "X ", LL_x, "Y ", LL_y)
        if "Upper Right " in line:
            line2 = line.split(" " or ")")
            UR_x = line2[4][:-1]
            UR_y = line2[5][:-1]
            if DEBUGFLAG == 1:
                write_log("Upper Right ", line2, "X ", UR_x, "Y ", UR_y)
        if "Lower Right " in line:
            line2 = line.split(" " or ")")
            LR_x = line2[4][:-1]
            LR_y = line2[5][:-1]
            if DEBUGFLAG == 1:
                write_log("Lower Right ", line2, "X ", LR_x, "Y ", LR_y)
            break
    fp.close()
    delete_file_w_path(basedir + "/" + "gdalinfo_output.txt")
    return (
        abs(float(UL_x)),
        abs(float(UL_y)),
        abs(float(LL_x)),
        abs(float(LL_y)),
        abs(float(UR_x)),
        abs(float(UR_y)),
        abs(float(LR_x)),
        abs(float(LR_y)),
    )


def del_tmp_files(basedir1, ls_basedir1):
    outf = [
        "go_*.*",
        "inPoint*.*",
        "ls_*",
        "*mem.img",
        "*temp.img",
        "*_mask*.*",
        "RF*.*",
        "u_star*.*",
        "*_alb.*",
        "c_h_pts*.*",
        "*_buff.*",
        "*_ZZ*.*",
        "s_t_hot*.*",
        "s_t_cold*.*",
        "sin_*.*",
        "*aux.xml",
        "nass_cold*.*",
        "nass_hot*.*",
        "air_d_*.*",
        "*cloud_cold*.*",
        "*cloud_mask*.*",
        "dt_cold*.*",
        "dt_hot*.*",
        "h_*.*",
        "intercept.img",
        "*_extent.*",
        "R_?.img",
        "n?_mem.img",
        "n??_memory.img",
        "n??_mem_?.img",
        "n??_mem.img",
        "n??_temp_?.img",
        "rah*.*",
        "slop*.*",
        "trans.img",
        "t_one.img",
        "st_hot1.img",
        "st_cold1.img",
        "SecT.img",
        "aoi_cloud_*.img",
        "aspect.img",
        "c_one.img",
        "aspect.img",
        "h_cold.img",
        "le.img",
        "et_ins_mod.img",
        "lambda.img",
        "slope.img",
        "i3.img",
        "h_hot.img",
        "st_hot.img",
        "rn_hot*.img",
        "cos_*.*",
        "costheta*.*",
        "dem_asp.img",
        "emissivity.img",
        "enb.img",
        "lai_*.*",
        "n8_memory*.*",
        "ra24.*",
        "rahone.img",
        "savi.img",
        "surf_alb_ndvi*.*",
        "surface_albedo*.*",
        "u200.*",
        "zo.img",
        "nhm.img",
        "rn.img.aux.xml",
        "mean_elev.TIF",
        "dem_slp.img",
        "albedo_toa.img",
        "rn_cold.img",
        "rm_0.img",
        "st_dem_c.img",
        "st_cold.img",
        "n5_temp*.*",
#        "nass_hot*.*",
        "air_d*.*",
        "*_cloud_cold*.*",
        "*_cloud_mask*.*",
        "*_cloud_plus*.*",
        "nass_orig.img",
        "c_h_pts*.*",
        "cold_pts?.*",
        "go.img",
        "intercept.img",
        "intercept?.img",
        "n??_temp?.img",
        "nass_clip_orig.img",
        "st_hot?.img",
        "cold_pts_out*.*",
        "aoi1.*",
        "cold2_mask*.*",
        "*_extent.*",
        "inPointFeature*.*",
        "ls_mask*.*",
        "nass_cold_mask*.*",
        "nass_hot_mask*.*",
        "DEM_1.*",
        "*_extent.*",
    ]
    for x in outf:
        delete_files_w_pattern(basedir1, x)
    delete_files_w_pattern(ls_basedir1, "*_CLIP.TIF")
#    delete_files_w_pattern(ls_basedir1, "*.*")
#    delete_files_w_pattern(ls_basedir1 + "/gap_mask", "*.*")


def del_out_files(basedir1, ls_basedir1, seasonal):
    outf = [
        "et_*.*",
        "hot_pts*.*",
        "cold_pts*.*",
        "st_dem*.*",
        "st*.*",
        "rn_*.*",
        "nass.img",
        "nass_clip.img",
        "aoi_cloud_cold_plus*.*",
        "aoi_cloud_cold_plus_buff*.*",
        "aoi_cloud_mask_buff*.*",
        "aoi_cloud_mask22*.*",
        "aoi_cloud_plus_buff*.*" "aoi_hot_mask*.*",
        "aoi_cold_mask*.*",
        "aoi1*.*",
    ]
    for x in outf:
        delete_files_w_pattern(basedir1, x)
    if seasonal:
        outf = ("climate_etr_hourly1*.*", "climate_etr2*.*")
        for x in outf:
            delete_files_w_pattern(basedir1, x)

    delete_files_w_pattern(ls_basedir1, "*_CLIP.TIF")


def ls7_gap_mask(basedir1, ls_basedir1):
    for dirName, subdirList, fileList in os.walk(ls_basedir1):
        str = dirName
        os.chdir(dirName)
        suffix = "gap_mask"
        i = 0
        if str.endswith(suffix):
            for fname in fileList:
                file_z = dirName + "/" + fname
                os.chdir(dirName)
                suff = ".gz"
                strr = file_z
                if strr.endswith("_B8.TIF.gz"):
                    delete_file_w_path(strr)
                if strr.endswith("_VCID_2.TIF.gz"):
                    delete_file_w_path(strr)
    for dirName, subdirList, fileList in os.walk(ls_basedir1):
        str = dirName
        os.chdir(dirName)
        suffix = "gap_mask"
        i = 0
        if str.endswith(suffix):
            for fname in fileList:
                file_z = dirName + "/" + fname
                # If _GM_ exists in the name of the Gap Map file remove it to be consistent with the name of the file
                if "_GM_" in file_z:
                    file_z_old = dirName + "/" + fname
                    file_z_new = file_z.replace("_GM_", "_")
                    file_z_new_exists = os.path.isfile(file_z_new)
                    if file_z_new_exists == 0:
                        os.rename(file_z_old, file_z_new)
                    file_z = file_z_new
                os.chdir(dirName)
                suff = ".gz"
                strr = file_z
                if strr.endswith(suff):
                    inF = gzip.GzipFile(file_z, "rb")
                    s = inF.read()
                    inF.close()
                    outf = str + "/" + fname[0:-3]
                    outF = open(outf, "wb")
                    outF.write(s)
                    outF.close()
            index = 0
            fnames = []
            for fname in fileList:
                outf = [
                    "B1.TIF",
                    "B2.TIF",
                    "B3.TIF",
                    "B4.TIF",
                    "B5.TIF",
                    "VCID_1.TIF",
                    "B7.TIF",
                ]
                for fn in outf:
                    if fname.endswith(fn):

                        # IN THIS CODE WE ARE TRYING TO SET THE NO DATA TO -9999 FOR COLLECTION 2 OF LS7 WHICH HAS THE NODATA SET TO 0
#                        ET1 = gdal.Open(ls_basedir1 + "/" + fname, GA_Update)
#                        band = ET1.GetRasterBand(1)
#                        band.SetNoDataValue(-9999)
#                        band.SetNoDataValue(0)
#                        ET1 = None

                        fnames.append(fname)
                        index += 1
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + fnames[0]
        + " -B "
        + fnames[1]
        + " -C "
        + fnames[2]
        + " -D "
        + fnames[3]
        + " -E "
        + fnames[4]
        + " -F "
        + fnames[5]
        + " -G  "
        + fnames[6]
        + " --outfile="
        + basedir1
        + "/ls7_gap_mask.img"
        + " --calc="
        + '"( A * B * C *D * E * F * G )"'
        + " --type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func("ls7_gap_mask.img", gdal_calc)
    inputfile = "ls7_gap_mask.img"
    outputfile = "ls7_gap_mask_clip.img"
    str_len = len(basedir1)
    clip_img_wo_align(basedir + "/" + inputfile, outputfile, aoi)
    delete_file_w_path(outputfile[:-4] + "_ZZ" + outputfile[-4:])
    return "ls7_gap_mask_clip.img"


def calc_convergence(max_rah_old, max_dt_hot_u_old, cnt):
    # compare hot points
    rah_conv = []
    dt_hot_u_conv = []
    list_x = []
    list_y = []
    list_x, list_y = checkP("inPointFeature_H.shp", "inPointFeature_h_one.shp", "rah")
    if DEBUGFLAG == 1:
        write_log("list_x = ", list_x)
        write_log("list_y = ", list_y)

    # compare cold points
    list_xc = []
    list_yc = []
    list_xc, list_yc = checkP(
        "inPointFeature_H.shp", "inPointFeature_h_one.shp", "dt_hot_u"
    )
    if DEBUGFLAG == 1:
        write_log("list_xc = ", list_xc)
        write_log("list_yc = ", list_yc)

    ii = 0
    # while ii<10:
    for x, y in zip(list_x, list_y):
        if x > 0.0:
            rah_conv.append(abs(100 * (x - y) / x))
        else:
            rah_conv.append(0.0)
    max_rah = max(rah_conv)
    for xc, yc in zip(list_xc, list_yc):
        if xc > 0.0:
            dt_hot_u_conv.append(abs(100 * (xc - yc) / xc))
        else:
            dt_hot_u_conv.append(0.0)
    max_dt_hot_u = max(dt_hot_u_conv)
    cnt += 1  # 6
    while abs(max_rah) > CONL or ii > 6:
        if DEBUGFLAG == 1:
            write_log("Model is now in iteration #", cnt)
            write_log(
                "Inside the while loop - rah_conv ",
                rah_conv,
                "dt_hot_u_conv",
                dt_hot_u_conv,
                "iteration number ",
                ii,
            )
        copy_shp_file("inPointFeature_H.shp", "inPointFeature_h_one.shp")
        copy_shp_file("inPointFeature_C.shp", "inPointFeature_c_one.shp")
        interp(air_d_img, rah_img, "X", "X")
        cnt += 1
        F13si(cnt)
        F10it(cnt)
        rah(cnt)
        F12_4(air_d_img, h_img, rah_img, dt_hot_u_img, "NONE")
        cnt += 1
        list_x = []
        list_y = []
        rah_conv = []
        dt_hot_u_conv = []
        list_x, list_y = checkP(
            "inPointFeature_H.shp", "inPointFeature_h_one.shp", "rah"
        )
        for x, y in zip(list_x, list_y):
            if x > 0.0:
                rah_conv.append(abs(100 * (x - y) / x))
            else:
                rah_conv.append(0.0)
        max_rah = max(rah_conv)
        list_xc = []
        list_yc = []
        list_xc, list_yc = checkP(
            "inPointFeature_H.shp", "inPointFeature_h_one.shp", "dt_hot_u"
        )
        for xc, yc in zip(list_xc, list_yc):
            if xc > 0.0:
                dt_hot_u_conv.append(abs(100 * (xc - yc) / xc))
            else:
                dt_hot_u_conv.append(0.0)
        max_dt_hot_u = max(dt_hot_u_conv)
        ii += 1

        if max_rah > max_rah_old or max_dt_hot_u > max_dt_hot_u_old:
            write_log(
                "code stop converging - calculated ET but ET at hot points might not be zero"
            )
            break
        max_rah_old = max_rah
        max_dt_hot_u_old = max_dt_hot_u
    return (max_rah, max_dt_hot_u)


def final_ET_calcs():
    F10it(cnt)

    ## Final AgroET 24 ET Calculation
    # Process: Raster Calculator
    n24_mem_img = "n24_mem.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + rn_img
        + " -B "
        + basedir
        + "/"
        + h_img
        + " -C "
        + basedir
        + "/"
        + go_img
        + " --outfile="
        + basedir
        + "/n24_mem.img"
        + " --calc="
        + '"((A-B-C)/(A-C))" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(n24_mem_img, gdal_calc)

    # Process: Raster Calculator (2)
    evapo_fr_img = "evapo_fr.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + n24_mem_img
        + " --outfile="
        + basedir
        + "/evapo_fr.img"
        + " --calc="
        + '"where(A<0,0,A)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(evapo_fr_img, gdal_calc)

    # Process: Raster Calculator (3)
    n17_temp_img = "n17_temp.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + rn_img
        + " -B "
        + basedir
        + "/"
        + h_img
        + " -C "
        + basedir
        + "/"
        + go_img
        + " -D "
        + basedir
        + "/"
        + st_cold1_img
        + " --outfile="
        + basedir
        + "/n17_temp.img"
        + " --calc="
        + '"(((A-B-C)*3600.0)/((2.501-0.00236*(D-273.16))*1000000.0))" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(n17_temp_img, gdal_calc)
    # Process: Raster Calculator (4)
    et_inst_img = "et_inst.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + n17_temp_img
        + " --outfile="
        + basedir
        + "/et_inst.img"
        + " --calc="
        + '"where(A<0,0,A)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(et_inst_img, gdal_calc)

    if USER_UNCALIB == 0:
        # Process: Raster Calculator (5)
        n15_mem_img = "n15_mem.img"
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + basedir
            + "/"
            + et_inst_img
            + " -B "
            + basedir
            + "/"
            + etinst_ws_img
            + " -C "
            + basedir
            + "/"
            + et24_ws_img
            + " --outfile="
            + n15_mem_img
            + " --calc="
            + '"((A/B)*C)" '
            + "--type=Float64"
            + " --overwrite --quiet --NoDataValue=-9999"
        )
        gdal_calc_func(n15_mem_img, gdal_calc)

        # Process: Raster Calculator (6)
        et_24_raster1_img = "et_24_raster1.img"
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + basedir
            + "/"
            + n15_mem_img
            + " --outfile="
            + et_24_raster1_img
            + " --calc="
            + '"where(A<0,0,A)" '
            + "--type=Float64"
            + " --overwrite --quiet --NoDataValue=-9999"
        )
        gdal_calc_func(et_24_raster1_img, gdal_calc)
        et_24_raster_img = "et_24_raster.img"
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + basedir
            + "/"
            + et_24_raster1_img
            + " --outfile="
            + et_24_raster_img
            + " --calc="
            + '"where(A<20,A,-9999)" '
            + "--type=Float64"
            + " --overwrite --quiet --NoDataValue=-9999"
        )
        gdal_calc_func(et_24_raster_img, gdal_calc)
        delete_file_w_path(basedir + "/" + et_24_raster1_img)

        # Calculate the ETa_to_ETr layer
        et_24_raster_img = "et_24_raster.img"
        ETa_to_ETr_img = "ETa_to_ETr.img"
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + basedir
            + "/"
            + et_24_raster_img
            + " -B "
            + basedir
            + "/"
            + et24_ws_img
            + " --outfile="
            + ETa_to_ETr_img
            + " --calc="
            + '"(A/B)" '
            + "--type=Float64"
            + " --overwrite --quiet --NoDataValue=-9999"
        )
        gdal_calc_func(ETa_to_ETr_img, gdal_calc)

    else:
        # Process: Raster Calculator (3)
        n003temp_img = "n003temp.img"
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + basedir
            + "/"
            + surface_albedo_img
            + " -B "
            + basedir
            + "/"
            + ra24_img
            + " -C "
            + basedir
            + "/"
            + cos_slope_img
            + " -D "
            + basedir
            + "/"
            + trans_img
            + " --outfile="
            + basedir
            + "/n003temp.img"
            + " --calc="
            + '"(1.0 - A) * (B / C) * D - (110.0 * D)" '
            + "--type=Float64"
            + " --overwrite --quiet --NoDataValue=-9999"
        )
        gdal_calc_func(n003temp_img, gdal_calc)
        n0023mem_img = "n0023mem.img"
        delete_file_w_path(basedir + "/" + n0023mem_img)
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + basedir
            + "/"
            + ndvi_img
            + " -B "
            + basedir
            + "/"
            + n003temp_img
            + " --outfile="
            + basedir
            + "/n0023mem.img"
            + " --calc="
            + '"where(A<0,('
            + str_s3
            + " * B - "
            + str_s4
            + '),0)" '
            + "--type=Float64"
            + " --overwrite --quiet --NoDataValue=-9999"
        )
        gdal_calc_func(n0023mem_img, gdal_calc)
        n0007mem_img = "n0007mem.img"
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + basedir
            + "/"
            + st_img
            + " --outfile="
            + basedir
            + "/n0007mem.img"
            + " --calc="
            + '"(2.501 - 0.002361 * (A - 273)) * 1000000.0" '
            + "--type=Float64"
            + " --overwrite --quiet --NoDataValue=-9999"
        )
        gdal_calc_func(n0007mem_img, gdal_calc)
        et_24_raster_uncal_t1_img = "et_24_raster_uncal_t1.img"
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + basedir
            + "/"
            + evapo_fr_img
            + " -B "
            + basedir
            + "/"
            + n003temp_img
            + " -C "
            + basedir
            + "/"
            + n0023mem_img
            + " -D "
            + basedir
            + "/"
            + n0007mem_img
            + " --outfile="
            + basedir
            + "/et_24_raster_uncal_t1.img"
            + " --calc="
            + '"(A * ((B - C) * 86400)) / (D)" '
            + "--type=Float64"
            + " --overwrite --quiet --NoDataValue=-9999"
        )
        gdal_calc_func(et_24_raster_uncal_t1_img, gdal_calc)
        et_24_raster_uncal0_img = "et_24_raster_uncal0.img"
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + basedir
            + "/"
            + et_24_raster_uncal_t1_img
            + " --outfile="
            + basedir
            + "/et_24_raster_uncal0.img"
            + " --calc="
            + '"where(A>0,A,0)" '
            + "--type=Float64"
            + " --overwrite --quiet --NoDataValue=-9999"
        )
        gdal_calc_func(et_24_raster_uncal0_img, gdal_calc)
        et_24_raster_uncal_img = "et_24_raster_uncal.img"
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + basedir
            + "/"
            + et_24_raster_uncal0_img
            + " --outfile="
            + et_24_raster_uncal_img
            + " --calc="
            + '"where(A<20,A,-9999)" '
            + "--type=Float64"
            + " --overwrite --quiet --NoDataValue=-9999"
        )
        gdal_calc_func(et_24_raster_uncal_img, gdal_calc)
        delete_file_w_path(basedir + "/" + et_24_raster_uncal0_img)
        # Calculate the ETa_to_ETr layer
        et_24_raster_uncal_img = "et_24_raster_uncal.img"
        ETa_to_ETr_img = "ETa_to_ETr_uncal.img"
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + basedir
            + "/"
            + et_24_raster_uncal_img
            + " -B "
            + basedir
            + "/"
            + et24_ws_img
            + " --outfile="
            + ETa_to_ETr_img
            + " --calc="
            + '"(A/B)" '
            + "--type=Float64"
            + " --overwrite --quiet --NoDataValue=-9999"
        )
        gdal_calc_func(ETa_to_ETr_img, gdal_calc)


def check_hot_ET():
    max_hot_pnt = -99
    delete_shp_file("hot_pts_check.shp")
    copy_shp_file(hot_pts, "hot_pts_check.shp")
    if USER_UNCALIB == 1:
        pnt_val_from_rast("et_24_raster_uncal.img", "hot_pts_check.shp", "NONE")
    else:
        pnt_val_from_rast("et_24_raster.img", "hot_pts_check.shp", "NONE")
    shp_to_csv("hot_pts_check.shp")
    sort_points(basedir + "/hot_pts_check.csv", "et_24_rast", "DESCENDING")
    nlines = 0
    f = open(basedir + "/hot_pts_check_sorted.csv", "r+")
    lines = f.readlines()
    f.seek(0)
    for line in lines:
        # Inside of a while the nws is increased at the top of the while.  The first value of nws is 0.  So checking for nws == 0 is checking for the first time through the loop.
        if nlines == 1:
            line1 = line.strip().split(",")
            max_hot_pnt = float(line1[3])
            break
        nlines += 1
    f.close()
    if DEBUGCODE == 0:
        delete_shp_file("hot_pts_check.shp")
    return max_hot_pnt


def shape_lyr_area(in_shp):
    shp_area = 0.0
    shp = ogr.Open(in_shp)
    drv = shp.GetDriver()
    lyr1 = shp.GetLayer(0)
    feature = lyr1.GetNextFeature()
    while feature is not None:
        geom = feature.GetGeometryRef()
        shp_area = +geom.GetArea()
        if DEBUGFLAG == 1:
            write_log("Cumulative Area of Shapefile is ", shp_area)
        feature = lyr1.GetNextFeature()
    return shp_area


def raster_lyr_pixels(rasterfile):
    r = gdal.Open(rasterfile)
    band = 1
    raster_arr = np.array(r.GetRasterBand(band).ReadAsArray())
    for cover in np.unique(raster_arr):
        tot_num_pixels = np.sum(raster_arr == cover)
        if DEBUGFLAG == 1 and cover == 1:
            write_log("Cover is ", cover, "Areas is ", tot_num_pixels)
    return tot_num_pixels


def calc_time():
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")


write_log("AgroET Version 2.17")
write_log("Starting calculation")
input = open(AgroET_file, "r")
basedir = os.getcwd()
#DEBUGFLAG = 1
for line in input:
    line1 = line.split("=")[0].strip()
    if DEBUGFLAG == 1:
        write_log("Line in AgroET_input.txt file is ", line1)
    if "DATA_DIRECTORY" == line1:
        basedir = line.split("=")[-1].strip()
        if DEBUGFLAG == 1:
            write_log("basedir ", basedir)
    elif "LS_IMAGE_DIR" == line1:
        ls_image_dir = line.split("=")[-1].strip()
        if DEBUGFLAG == 1:
            write_log("landsat image directory ", ls_image_dir)
    elif "DEM_FILE" == line1:
        USER_DEM = 1
        dem_img = line.split("=")[-1].strip()
        if DEBUGFLAG == 1:
            write_log("DEM file ", dem_img)
    elif "climate_windrun_mile_day" == line1:
        wind_img1 = line.split("=")[-1].strip()
        if DEBUGFLAG == 1:
            write_log("climate_windrun_mile_day file ", wind_img1)
    elif "climate_etr_hourly" == line1:
        etinst_ws_img1 = line.split("=")[-1].strip()
        if DEBUGFLAG == 1:
            write_log("climate_etr_hourly file ", etinst_ws_img1)
    elif "climate_etr" == line1:
        et24_ws_img1 = line.split("=")[-1].strip()
        if DEBUGFLAG == 1:
            write_log("climate_etr file ", et24_ws_img1)
    elif "WS_FILE" == line1:
        ws = line.split("=")[-1].strip()
        if DEBUGFLAG == 1:
            write_log("Weather Station File ", ws)
    elif "AOI_FILE" == line1:
        aoi_file_orig = line.split("=")[-1].strip()
        aoi_file = line.split("=")[-1].strip()
        if DEBUGFLAG == 1:
            write_log("Area of Interest (AOI) file ", aoi_file)
    elif "USER_COLD_AOI_FILE" == line1:
        aoi_cold = line.split("=")[-1].strip()
        USER_COLD_AOI_FILE = 1
        write_log("User defined cold AOI " + aoi_cold)
    elif "USER_HOT_AOI_FILE" == line1:
        aoi_hot = line.split("=")[-1].strip()
        USER_HOT_AOI_FILE = 1
        write_log("User defined hot AOI " + aoi_hot)
    elif "USER_COLD_PTS_FILE" == line1:
        cold_pts = line.split("=")[-1].strip()
        USER_COLD_PTS_FILE = 1
        write_log("User defined cold points " + cold_pts)
    elif "USER_HOT_PTS_FILE" == line1:
        hot_pts = line.split("=")[-1].strip()
        USER_HOT_PTS_FILE = 1
        write_log("User defined hot points " + hot_pts)
    elif "USER_CLOUD_BUFFER" == line1:
        cloud_buffer = float(line.split("=")[-1].strip())
        USER_CLOUD_BUFFER = 1
        write_log("User defined cloud buffer {0}".format(cloud_buffer))
    elif "USER_MAX_CLOUD_PERC" == line1:
        max_cloud_percent = float(line.split("=")[-1].strip())
        USER_CLOUD_PERCENT = 1
        write_log(
            "User defined the maximum cloud cover as {0}".format(max_cloud_percent)
        )
#    elif "WEATHER_DATA" == line1:
#        weather_data = int(line.split("=")[-1].strip())
#        write_log(
#            "1. Use either WS or gridded data (default); 2. Use WS data ONLY; 3. Use gridded data ONLY {0}".format(weather_data)
#        )
    elif "USER_COLD_BUFFER" == line1:
        cold_buffer = float(line.split("=")[-1].strip())
        USER_COLD_BUFFER = 1
        write_log("User defined cold buffer {0}".format(cold_buffer))
    elif "DEM_FLAT" == line1:
        DEM_FLAT = float(line.split("=")[-1].strip())
        if DEM_FLAT == 1:
            write_log("User selected to USE a flat DEM")
        else:
            write_log("User selected to NOT to use a flat dem")
    elif "LS7_DISABLE_FILL_GAP" == line1:
        LS7_DISABLE_FILL_GAP = float(line.split("=")[-1].strip())
        if DEBUGFLAG == 1 and LS7_DISABLE_FILL_GAP == 1:
            write_log("For Landsat 7 images user selects to NOT fill the scan line gaps")
            write_log("For Landsat 7 images user selects to NOT fill the scan line gaps")
        if DEBUGFLAG == 1 and LS7_DISABLE_FILL_GAP == 0:
            write_log("For Landsat 7 images user selects to FILL the scan line gaps")
            write_log("For Landsat 7 images user selects to FILL the scan line gaps")
    elif "USER_UNCALIB" == line1:
        USER_UNCALIB = int(line.split("=")[-1].strip())
        if DEBUGFLAG == 1 and USER_UNCALIB == 1:
            write_log("Running the model in uncalibrated mode")
    elif "DEBUG_FLAG" == line1:
        DEBUGFLAG = int(line.split("=")[-1].strip())
        if DEBUGFLAG == 1:
            write_log("DEBUG flag set to ", DEBUGFLAG)
    elif "USER_CONL" == line1:
        CONL = float(line.split("=")[-1].strip())
        USER_DEF_CONL = 1
        if DEBUGFLAG == 1:
            write_log("User Defined CONL is ", CONL)
    elif "seasonal" == line1:
        seasonal = 1
        write_log("Running model as part of a seasonal run")
    elif "LOCAL_NASS_DIR" == line1:
        local_nass_dir = line.split("=")[-1].strip()
        if DEBUGFLAG == 1:
            write_log(
                "The local directory where the NASS layers are stored is ",
                local_nass_dir,
            )
    elif "LOCAL_DEM_DIR" == line1:
        local_dem_dir = line.split("=")[-1].strip()
        if DEBUGFLAG == 1:
            write_log(
                "The local directory where the DEM layers are stored is ", local_dem_dir
            )
    elif "#" == line1:
        if DEBUGFLAG == 1:
            write_log("End of input file ")
        break
    else:
        write_log(
            "The following line: ",
            line,
            "in the file AgroET_input.txt does not contain a valid token!!!",
        )
        exit(1)

input.close()
# Checking if the base directory exists
basedir = os.path.abspath(basedir)
len_basedir = len(basedir)
check_directory_exists(basedir, "Base directory ")
# Checking if the image directory exists
check_directory_exists(basedir + "/" + ls_image_dir, "Image directory ")
ls_basedir = basedir + "/" + ls_image_dir
del_tmp_files(
    basedir, ls_basedir
)  # delete the temporary files that might have created from a previous run
del_out_files(
    basedir, ls_basedir, seasonal
)  # delete output files that might have been created from previous runs
# Finding the root name of the landsat image (use B1.TIF given that this band should always be there).
FileList = glob.glob(os.path.join(ls_basedir, "*B1.TIF"))
len_ls_basedir = len(ls_basedir)
landsat_img = FileList[0][len_ls_basedir + 1 : -7]
# Finding the UTM zone for the Landsat images
utm_zone = raster_utm_prj(ls_basedir + "/" + landsat_img + "_B1.TIF")
if USER_DEM == 1:
    # Checking if the DEM file exists and is in the correct projection
    check_file_exists(dem_img, "raster", utm_zone, "DEM raster ")
    CheckPixelResolution(basedir + "/" + dem_img, cellSize)
# Checking if the Wind file exists and is in the correct projection
check_file_exists(wind_img1, "raster", utm_zone, "Wind raster ")
CheckPixelResolution(basedir + "/" + wind_img1, cellSize)
if USER_UNCALIB == 0:  # Means that the run is CALIBRATED
    # Checking if the ETr hourly raster file exists and is in the correct projection
    check_file_exists(etinst_ws_img1, "raster", utm_zone, "ETr hourly raster ")
    CheckPixelResolution(basedir + "/" + etinst_ws_img1, cellSize)
    # Checking if the ET24 raster file exists and is in the correct projection
    check_file_exists(et24_ws_img1, "raster", utm_zone, "ET24 raster ")
    CheckPixelResolution(basedir + "/" + et24_ws_img1, cellSize)
# Checking if the Weather Station file exists and is in the correct projection
check_file_exists(ws, "shapefile", utm_zone, "Weather Station shapefile ")
# Checking if the AOI file exists and is in the correct projection
check_file_exists(aoi_file, "shapefile", utm_zone, "Area of Interest (AOI) shapefile ")
if USER_COLD_AOI_FILE == 1:
    check_file_exists(
        aoi_cold, "shapefile", utm_zone, "User defined COLD AOI shapefile "
    )
if USER_HOT_AOI_FILE == 1:
    check_file_exists(aoi_hot, "shapefile", utm_zone, "User defined HOT AOI shapefile ")
if USER_COLD_PTS_FILE == 1:
    check_file_exists(
        cold_pts, "shapefile", utm_zone, "User defined COLD POINTS shapefile "
    )
if USER_HOT_PTS_FILE == 1:
    check_file_exists(
        hot_pts, "shapefile", utm_zone, "User defined HOT POINTS shapefile "
    )
py_script_dir = r"C:\Users\lag\AppData\Local\Continuum\anaconda3\envs\AgroET\Scripts"
py1_script_dir = r"C:\OSGeo4W64\bin"
if not os.path.exists(py_script_dir):
    py_script_dir = "/usr/bin"
if not os.path.exists(os.path.join(py_script_dir, "gdal_calc.py")):
    py_script_dir = "/usr/local/bin/"
# LAG set the working directory to basedir
os.chdir(basedir)
delete_shp_file(aoi_file[:-4] + "1.shp")
copy_shp_file(aoi_file, aoi_file[:-4] + "1.shp")
aoi_file1 = aoi_file[:-4] + "1.shp"
aoi_file = aoi_file1
area_shp = basedir + "/" + aoi_file
# Checking if the AOI file exists
aoi_exists = os.path.isfile(area_shp)
if aoi_exists == 0:
    write_log("Area of Interest shapefile ", area_shp, " does not exist !!!!")
    exit(1)
aoi = area_shp
workspace = basedir
# Change directory to the workspace
os.chdir(workspace)
cwd = os.getcwd()


def strip_xyz_NoDataPoints(filename, EndData):
    f = open("{}.xyz".format(filename), "r+")
    try:
        os.remove("{}.csv".format(filename))
    except OSError:
        pass
    fone = open("{}.csv".format(filename), "w+")
    lines = f.readlines()
    f.seek(0)
    fone.write("X Y Z" + "\n")
    search_list = ["nan", "IND", "-9999"]
    for line in lines:
        # Writes lines that do not end with NoData.
        if not re.compile("|".join(search_list), re.IGNORECASE).search(
            line
        ):  # re.IGNORECASE is used to ignore case
            fone.write(line)
    fone.close()
    f.close()
    try:
        os.remove("{}.xyz".format(filename))
    except OSError:
        pass


def strip_csv_NoDataPoints(filename, EndData):
    copyFile(filename, filename + "1")
    try:
        os.remove("{}".format(filename))
    except OSError:
        pass
    f = open("{}1".format(filename), "r+")
    fone = open("{}".format(filename), "w+")
    lines = f.readlines()
    f.seek(0)
    search_list = ["nan", "IND", "-9999"]
    for line in lines:
        # Writes lines that do not end with NoData.
        if not re.compile("|".join(search_list), re.IGNORECASE).search(
            line
        ):  # re.IGNORECASE is used to ignore case
            fone.write(line)
    fone.close()
    f.close()
    try:
        os.remove("{}1".format(filename))
    except OSError:
        pass


# Counts and sorts (ascending or decending) the points in a csv file and writes a new file with _sorted name at the end.
# uses the python sorted command to sort the points based on the Z argument from low to high.  This is used to find the
# points with the lowest or highest values (for cold points reverse is false (0) ascending order (cold on top) and for hot points
# reverse is true (1)) - descending (hot on top). This function is used as part of finding the coldest and hottest set of points.
def sort_points(filename, field, sort_order):
    if sort_order == "ASCENDING":
        order = 0
    elif sort_order == "DESCENDING":
        order = 1
    else:
        write_log("Sort order can only be DESCENDING or ASCENDING not ", sort_order)
        exit(1)
    delete_file_w_path(basedir + "/" + filename[:-4] + "_sorted.csv")
    output = open(filename[:-4] + "_sorted.csv", "w")

    numlines = 0
    # Read in the data creating a label list and list of one tuple per row
    source = open(filename, "r")
    reader = csv.reader(source, delimiter=" ")
    row_count = 0
    data = []
    for row in reader:
        row_count += 1
        # Place the first row into the header
        if row_count == 1:
            header = row
            continue
        # Append all non-header rows into a list of data as a tuple of cells
        data.append(tuple(row))

    # Sort is stable as of Python 2.2. As such, we can break down the
    # complex sort into a series of simpler sorts. We just need to remember
    # to REVERSE the order of the sorts.
    field_pos = header.index(field)
    data = sorted(data, key=itemgetter(field_pos), reverse=order)

    # Now write all of this out to the new file
    writer = csv.writer(output, lineterminator="\n")
    writer.writerow(header)  # Write the header in CSV format
    for sorted_row in data:  # Write the sorted data, converting to CSV format
        writer.writerow(sorted_row)
    # Be good and close the files
    source.close()
    output.close()
    if DEBUGFLAG == 1:
        write_log("Number of cold points found is ", row_count)
    return row_count - 1


# This function is used when there is only 1 weather station and a grid of a constant value is created for the hot or cold surface.
# It returns the single value of the cold or hot pixel that is used as the constant value for the grid.
def fnd_val_single_pt(pts, f_name):
    pts_shp = basedir + "/" + pts
    pts_source = osgeo.ogr.Open(pts_shp, update=0)
    if pts_source is None:
        write_log("Open Point File ", pts, " failed.")
        exit(1)
    # pts_source = driver.Open(pts_shp)
    pts_layer = pts_source.GetLayer()
    field_names = [field.name for field in pts_layer.schema]
    if DEBUGFLAG == 1:
        write_log("field names are =", field_names)
        write_log("f_name is ", f_name)
    field_pos = field_names.index(f_name)
    feature = pts_layer.GetFeature(0)
    field_val = feature.GetField(field_pos)
    if DEBUGFLAG == 1:
        write_log("Single value point \nfield names = ", field_names)
        write_log("print f_name", f_name, "field_value = ", field_val)
    del pts_source
    return field_val


# This function calculates the distance from a set of cold points to each weather station.  Then it determines which of those
# points are within a certain distance (srml) of each of the weather stations (ws) and writes that distance as an attribute to
# each point and determines the coldest point.
def fnd_pt_by_loc(ws, pts, pts1, srm1_init, sort_field, sort_order):
    srm1 = srm1_init
    ws_shp = basedir + "/" + ws
    if DEBUGFLAG == 1:
        write_log("Weather Station Name ", ws_shp)
    pts_shp = basedir + "/" + pts
    if DEBUGFLAG == 1:
        write_log("Find Coldest Pnt Points Name ", pts_shp)
    ws_source = osgeo.ogr.Open(ws_shp)
    if ws_source is None:
        write_log("Open of Weather Station failed. ", ws_shp, " file not found.")
        exit(1)
    pts_source = osgeo.ogr.Open(pts_shp, update=1)
    if pts_source is None:
        write_log("Open Point File failed. ", pts, "file not found.")
        exit(1)
    ws_layer = ws_source.GetLayer()
    wsfield_names = [field.name for field in ws_layer.schema]
    if DEBUGFLAG == 1:
        write_log("Weather Station field_names ", wsfield_names)
    pts_layer = pts_source.GetLayer()
    ptsfield_names = [field.name for field in pts_layer.schema]
    if DEBUGFLAG == 1:
        write_log("Point Layer field_names ", ptsfield_names)
    ws_fields = []
    for i in range(0, ws_layer.GetFeature(0).GetFieldCount()):
        field = ws_layer.GetFeature(0).GetDefnRef().GetFieldDefn(i).GetName()
        ws_fields.append(field)
    if DEBUGFLAG == 1:
        write_log(" WS field ", ws_fields)
    pts_fields = []
    if DEBUGFLAG == 1:
        write_log(
            "Number of cold points in cold_pts_set.shp ",
            pts_layer.GetFeature(0).GetFieldCount(),
        )
    if pts_layer.GetFeature(0).GetFieldCount() == 0:
        write_log(
            "There are NO cold points to select from.  The number of cold points in cold_pt.shp is ZERO."
        )
        write_log("Check to see if the cold AOI is not too restrictive.")
        exit(1)
    for i in range(0, pts_layer.GetFeature(0).GetFieldCount()):
        field1 = pts_layer.GetFeature(0).GetDefnRef().GetFieldDefn(i).GetName()
        pts_fields.append(field1)
    if DEBUGFLAG == 1:
        write_log(" Pts field ", pts_fields)
    num_pts = pts_layer.GetFeatureCount()
    ws = []
    for index in range(ws_layer.GetFeatureCount()):
        feature = ws_layer.GetFeature(index)
        geometry = feature.GetGeometryRef()
        ws.append((geometry.GetX(), geometry.GetY()))
        if DEBUGFLAG == 1:
            write_log("Weather Station ", index, geometry.GetX(), geometry.GetY())
    num_ws = ws_layer.GetFeatureCount()
    if DEBUGFLAG == 1:
        write_log(
            "Number of Weather Stations ",
            num_ws,
            " If only 1 then bypass finding coldest point for each.",
        )
        write_log(
            "there will be a constant value field.  One WS point only no interpolation needed."
        )
    if num_ws > 0:
        schema = []
        ldefn = pts_layer.GetLayerDefn()
        ldefn_range = ldefn.GetFieldCount()
        for n in range(ldefn_range):
            fdefn = ldefn.GetFieldDefn(n)
            schema.append(fdefn.name)
        num_wsint = num_ws + 2
        WS_id_found = [0] * num_wsint
        count = 0
        while count < num_ws:
            count = count + 1
            for n in range(ldefn_range):
                if schema[n] == "WS_Dist" + str(count):
                    WS_id_found[count] = 1
            if WS_id_found[count] == 0:
                pts_layer.CreateField(
                    ogr.FieldDefn("WS_Dist" + str(count), ogr.OFTReal)
                )
        points = []
        for index in range(pts_layer.GetFeatureCount()):
            feature = pts_layer.GetFeature(index)
            geometry = feature.GetGeometryRef()
            points.append((geometry.GetX(), geometry.GetY()))
        # initializing the cooldest_feature1 array
        cooldest_feature1 = [0 for i in range(num_pts)]
        num_pts = [0 for i in range(num_ws)]
        for index in range(ws_layer.GetFeatureCount()):
            num_pts_within_srm1 = 0
            while num_pts_within_srm1 == 0:
                if DEBUGFLAG == 1:
                    write_log("Weather station being evaluation for cold points ", index)
                ws_coord = np.array(ws[index], dtype="float64")
                pnt_coord = np.array(points, dtype="float64")
                dists1 = np.sqrt(np.sum((ws_coord - pnt_coord) ** 2, axis=1))
                pts_layer.ResetReading()
                if DEBUGFLAG == 1:
                    write_log("Points layer count ", pts_layer.GetFeatureCount())
                c = 0
                feature = pts_layer.GetNextFeature()
                for i in range(pts_layer.GetFeatureCount()):
                    feature.SetField("WS_Dist" + str(index + 1), dists1[i])
                    pts_layer.SetFeature(feature)
                    if dists1[i] < srm1:
                        cooldest_feature1[c] = pts_layer.GetFeature(i)
                        c += 1
                        num_pts_within_srm1 += 1
                    feature = pts_layer.GetNextFeature()
                num_pts[index] = c
                # check if there are cold points within the srm1 distance (c > 0).  If there are create c_h_pts_ws#.shp and .csv files
                if c > 0:
                    ws_pts_shp = "c_h_pts_" + str(index) + ".shp"
                    ws_pts_csv = "c_h_pts_" + str(index) + ".csv"
                    ws_path_pts_shp = basedir + "/" + ws_pts_shp
                    pts_shp = pts_source.GetLayerByIndex(0)
                    delete_shp_file("c_h_pts_" + str(index) + ".shp")
                    delete_file_w_path(basedir + "/" + ws_pts_csv)
                    driver_name = "ESRI Shapefile"
                    drv = ogr.GetDriverByName(driver_name)
                    out_ds = drv.CreateDataSource(ws_path_pts_shp)
                    proj = pts_shp.GetSpatialRef()
                    coldest_pt_lyr = out_ds.CreateLayer(
                        ws_path_pts_shp.split(".")[0], proj, ogr.wkbPoint
                    )
                    # copy the schema of the original shapefile to the destination shapefile
                    lyr_def = pts_shp.GetLayerDefn()
                    for ij in range(lyr_def.GetFieldCount()):
                        coldest_pt_lyr.CreateField(lyr_def.GetFieldDefn(ij))
                    ik = 0
                    while ik < c:
                        coldest_pt_lyr.CreateFeature(cooldest_feature1[ik])
                        ik += 1
                    del out_ds, coldest_pt_lyr
                    # Writing the features
                    shp_to_csv(ws_pts_shp)
                    num_points = sort_points(ws_pts_csv, sort_field, sort_order)
                #               if after checking all the points none are within the srm1 search radius then expand the radius by 5000 mts
                if num_pts_within_srm1 == 0:
                    if DEBUGFLAG == 1:
                        write_log(
                            "No cold points found withing a radius of ",
                            srm1,
                            ". Expanding the radius by 5000 mts",
                        )
                    srm1 += 5000
                    #                   The maximum search radius is srm_max mts.  If no points are found within this radius inform the user and stop.
                    if srm1 > srm_max:
                        write_log(
                            "Weather station",
                            index,
                            "has NO cold points within the search radius of 40 kms.\nThe search radius has already been increase to the maximum.",
                        )
                        srm1 = srm1_init
                        break
        del pts_source, ws_source
        delete_file_w_path(basedir + "/" + pts1[:-4] + ".csv")
        fp_w = open(pts1[:-4] + ".csv", "w")
        j = 0
        nws = 0
        while j < num_ws:
            if num_pts[j] > 0:
                ws_pts = "c_h_pts_" + str(j) + "_sorted.csv"
                # read the first two lines
                N = 2
                nlines = 0
                f = open(ws_pts, "r+")
                lines = f.readlines()
                f.seek(0)
                for line in lines:
                    # Inside of a while the nws is increased at the top of the while.  The first value of nws is 0.  So checking for nws == 0 is checking for the first time throught the loop.
                    if nws == 0 and nlines == 0:
                        fp_w.write(line)
                    if nlines == 1:
                        fp_w.write(line)
                    nlines += 1
                    if nlines >= N:
                        break
                f.close()
                nws += 1
            j += 1
        if DEBUGFLAG == 1:
            write_log("Number of weather stations with cold points ", nws)
        fp_w.close()
        delete_file_w_path(basedir + "/" + pts1)
        # This statement creates the cold_points.shp file (pts1)
        os.system(
            'ogr2ogr -f "ESRI Shapefile" -a_srs EPSG:326'
            + utm_zone
            + " -oo X_POSSIBLE_NAMES=X* -oo Y_POSSIBLE_NAMES=Y* -oo KEEP_GEOM_COLUMNS=NO {0}.shp {0}.csv".format(
                basedir + "/" + pts1[:-4]
            )
        )
    return num_ws


# This function reads the value of the points in inPointFeature_C.shp and inPointFeature_H.shp from multiple
# surfaces (file1, file2, file3 and file4) and appends a field with the grid layer name to the inPointFeature shapefile
# and assigns the number it reads from the surface at each of the points.
def F12_4(file1, file2, file3, file4, extract_option):
    copy_shp_file(cold_pts, "inPointFeature_C.shp")
    copy_shp_file(hot_pts, "inPointFeature_H.shp")
    pnt_val_from_rast(file1, "inPointFeature_C.shp", extract_option)
    pnt_val_from_rast(file2, "inPointFeature_C.shp", extract_option)
    if file3 != "X":
        pnt_val_from_rast(file3, "inPointFeature_C.shp", extract_option)
    # for uncalibrated option do not extract values for dt_hot_u.img
    if file4 != "X":
        if USER_UNCALIB == 0:
            pnt_val_from_rast(file4, "inPointFeature_C.shp", extract_option)
        elif USER_UNCALIB == 1 and file4 != "dt_hot_u.img":
            pnt_val_from_rast(file4, "inPointFeature_C.shp", extract_option)
    pnt_val_from_rast(file1, "inPointFeature_H.shp", extract_option)
    pnt_val_from_rast(file2, "inPointFeature_H.shp", extract_option)
    if file3 != "X":
        pnt_val_from_rast(file3, "inPointFeature_H.shp", extract_option)
    if file4 != "X":
        pnt_val_from_rast(file4, "inPointFeature_H.shp", extract_option)


# This function creates interpolated raster layers using an inverse distance method based on the values of the files (file1, file2, file3 and/or file4)
# at the location of the cold points (inPointFeature_C.shp) and the hot point (inPointFeature_H.shp) for
def interp(file1, file2, file3, file4):
    field1 = file1[:-4]
    field2 = file2[:-4]
    if file3 != "X":
        field3 = file3[:-4]
    else:
        field3 = file3
    if file4 != "X":
        field4 = file4[:-4]
    else:
        field4 = file4
    in_C_pnt_source = osgeo.ogr.Open(basedir + "/" + "inPointFeature_C.shp")
    in_C_layer = in_C_pnt_source.GetLayer()
    C_field_names = []
    for i in range(0, in_C_layer.GetFeature(0).GetFieldCount()):
        C_field_name = in_C_layer.GetFeature(0).GetDefnRef().GetFieldDefn(i).GetName()
        C_field_names.append(C_field_name)
    del in_C_pnt_source
    for C_field in C_field_names:
        if (
            C_field == field1
            or C_field == field2
            or C_field == field3
            or C_field == field4
        ):
            create_csv_vrt("inPointFeature_C.shp", C_field)
            if DEBUGFLAG == 1:
                write_log("C_field is ", C_field)
            C_out = basedir + "/" + C_field + "_cold.img"
            C_out1 = basedir + "/" + C_field + "_cold7.img"
            if DEBUGFLAG == 1:
                write_log("C_out is ", C_out)
            count_C = find_n_rows_shp(basedir + "/inPointFeature_C.shp")
            if count_C >= 2:
                delete_file_w_path(basedir + "/" + C_field + "_cold7.img")
                gdal_grid_text = str(
                    "gdal_grid -a invdist:power={0} -txe {1} {2} -tye {3} {4} -outsize {5} {6} -a_srs EPSG:326{7} -zfield {8} -of GTiff -l {9} -ot Float64 {10} {11}".format(
                        idw_power,
                        clip_x1,
                        clip_x2,
                        clip_y2,
                        clip_y1,
                        x_cells,
                        y_cells,
                        utm_zone,
                        C_field,
                        "inPointFeature_C",
                        basedir + "/inPointFeature_C.vrt",
                        C_out1,
                    )
                )
                if DEBUGFLAG == 1:
                    write_log("gdal_grid_text ", gdal_grid_text)
                os.system(gdal_grid_text)
                clip_img(basedir + "/" + C_field + "_cold7.img", C_field + "_cold.img", aoi)
                delete_file_w_path(basedir + "/" + C_field + "_cold7.img")
            else:
                field_val = fnd_val_single_pt("inPointFeature_C.shp", C_field)
                if DEBUGFLAG == 1:
                    write_log("field val ", field_val)
                str_field_val = str(field_val)
                gdal_calc = (
                    "python "
                    + py_script_dir
                    + "/gdal_calc.py -A "
                    + dem_img
                    + " --outfile="
                    + C_out1
                    + " --calc="
                    + '"('
                    + str_field_val
                    + ')"'
                    + " --type=Float64"
                    + " --overwrite --NoDataValue=-9999 --quiet"
                )
                gdal_calc_func(C_field + "_cold.img", gdal_calc)
                clip_img(basedir + "/" + C_field + "_cold7.img", C_field + "_cold.img", aoi)
                delete_file_w_path(basedir + "/" + C_field + "_cold7.img")
    del C_field_names
    #   ExtractMultiValuesToPoints("inPointFeature_H.shp", inRasterList ,"BILINEAR")
    field1 = file1[:-4]
    field2 = file2[:-4]
    if file3 != "X":
        field3 = file3[:-4]
    else:
        field3 = file3
    if file4 != "X":
        field4 = file4[:-4]
    else:
        field4 = file4
    # Interpolating raster from Hot points
    in_H_pnt_source = osgeo.ogr.Open(basedir + "/" + "inPointFeature_H.shp")
    in_H_layer = in_H_pnt_source.GetLayer()
    H_field_names = []
    for i in range(0, in_H_layer.GetFeature(0).GetFieldCount()):
        H_field_name = in_H_layer.GetFeature(0).GetDefnRef().GetFieldDefn(i).GetName()
        H_field_names.append(H_field_name)
    for H_field in H_field_names:
        if (
            H_field == field1
            or H_field == field2
            or H_field == field3
            or H_field == field4
        ):
            create_csv_vrt("inPointFeature_H.shp", H_field)
            if DEBUGFLAG == 1:
                write_log("H_field is ", H_field)
            H_out = basedir + "/" + H_field + "_hot.img"
            H_out1 = basedir + "/" + H_field + "_hot7.img"
            if DEBUGFLAG == 1:
                write_log("H_out is ", H_out)
            count_C = find_n_rows_shp(basedir + "/inPointFeature_H.shp")
            if count_C >= 2:
                delete_file_w_path(basedir + "/" + H_field + "_hot7.img")
                gdal_grid_text = str(
                    "gdal_grid -a invdist:power={0} -txe {1} {2} -tye {3} {4} -outsize {5} {6} -a_srs EPSG:326{7} -zfield {8} -of GTiff -l {9} -ot Float64 {10} {11}".format(
                        idw_power,
                        clip_x1,
                        clip_x2,
                        clip_y2,
                        clip_y1,
                        x_cells,
                        y_cells,
                        utm_zone,
                        H_field,
                        "inPointFeature_H",
                        basedir + "/inPointFeature_H.vrt",
                        H_out1,
                    )
                )
                if DEBUGFLAG == 1:
                    write_log("gdal_grid_text ", gdal_grid_text)
                os.system(gdal_grid_text)
                clip_img(basedir + "/" + H_field + "_hot7.img", H_field + "_hot.img", aoi)
                delete_file_w_path(basedir + "/" + C_field + "_hot7.img")
            else:
                field_val = fnd_val_single_pt("inPointFeature_H.shp", H_field)
                if DEBUGFLAG == 1:
                    write_log("field val ", field_val)
                str_field_val = str(field_val)
                gdal_calc = (
                    "python "
                    + py_script_dir
                    + "/gdal_calc.py -A "
                    + dem_img
                    + " --outfile="
                    + H_out1
                    + " --calc="
                    + '"('
                    + str_field_val
                    + ')"'
                    + " --type=Float64"
                    + " --overwrite --NoDataValue=-9999 --quiet"
                )
                gdal_calc_func(H_field + "_hot.img", gdal_calc)
                clip_img(basedir + "/" + H_field + "_hot7.img", H_field + "_hot.img", aoi)
                delete_file_w_path(basedir + "/" + C_field + "_hot7.img")
    del H_field_names


# This features gets the value of a certain attribute (col_name) from two shapefiles and returns a list of the values of
# that attribute from each of the files.
def checkP(pts1, pts2, col_name):
    pts1_shp = basedir + "/" + pts1
    if DEBUGFLAG == 1:
        write_log("Points Name ", pts1_shp)
    pts1_source = osgeo.ogr.Open(pts1_shp, update=0)
    if pts1_source is None:
        write_log("Open Point File failed. ", pts1, "file not found.")
        exit(1)
    pts1_layer = pts1_source.GetLayer()
    field1_names = [field.name for field in pts1_layer.schema]
    if DEBUGFLAG == 1:
        write_log("field names are ", field1_names)
    pts1_fields = []
    for i in range(0, pts1_layer.GetFeature(0).GetFieldCount()):
        field1 = pts1_layer.GetFeature(0).GetDefnRef().GetFieldDefn(i).GetName()
        if field1 == col_name:
            field1_id = i
    list1 = []
    for index in range(pts1_layer.GetFeatureCount()):
        feature1 = pts1_layer.GetFeature(index)
        list1.append(feature1.GetField(field1_id))
    del pts1_source
    pts2_shp = basedir + "/" + pts2
    if DEBUGFLAG == 1:
        write_log("Points Name ", pts2_shp)
    pts2_source = osgeo.ogr.Open(pts2_shp, update=0)
    if pts2_source is None:
        write_log("Open Point File failed. ", pts2, "file not found.")
        exit(1)
    pts2_layer = pts2_source.GetLayer()
    field2_names = [field.name for field in pts2_layer.schema]
    if DEBUGFLAG == 1:
        write_log("field2_names are ", field2_names)
    pts2_fields = []
    for i in range(0, pts2_layer.GetFeature(0).GetFieldCount()):
        field2 = pts2_layer.GetFeature(0).GetDefnRef().GetFieldDefn(i).GetName()
        if field2 == col_name:
            field2_id = i
    list2 = []
    for index in range(pts2_layer.GetFeatureCount()):
        feature2 = pts2_layer.GetFeature(index)
        list2.append(feature2.GetField(field2_id))
    del pts2_source
    return (list1, list2)


# Calculating the aerodynamic resistance Rah
def rah(im):
    i = str(im)
    n5_temp_img = "n5_temp.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + h_img
        + " -B "
        + air_d_img
        + " -C "
        + u_star_img
        + " -D "
        + st_img
        + " --outfile="
        + basedir
        + "/n5_temp.img"
        + " --calc="
        + '"where(A != 0,((- '
        + str_Cp
        + '* B * (pow(C,3)) * D) / (0.41 * 9.81 * A)),1000)" '
        + "--type=Float64"
        + " --quiet --overwrite --NoDataValue=-9999"
    )
    gdal_calc_func(n5_temp_img, gdal_calc)
    if DEBUGCODE == 1:
        copyFile(n5_temp_img, "n5_temp" + i + ".img")
    # Process: Raster Calculator (4)
    n12_z1_mem_img = "n12_z1_mem.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + n5_temp_img
        + " --outfile="
        + basedir
        + "/n12_z1_mem.img"
        + " --calc="
        + '"(where(A<0,(pow((1.0-16.0*('
        + str_z1
        + '/A)),0.5)),1))" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(n12_z1_mem_img, gdal_calc)
    if DEBUGCODE == 1:
        copyFile(n12_z1_mem_img, "n12_z1_mem" + i + ".img")

    # Process: Raster Calculator (4)
    n12_z2_mem_img = "n12_z2_mem.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + n5_temp_img
        + " --outfile="
        + basedir
        + "/n12_z2_mem.img"
        + " --calc="
        + '"(where(A<0,(pow((1.0-16.0*('
        + str_z2
        + '/A)),0.5)),1))" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(n12_z2_mem_img, gdal_calc)
    if DEBUGCODE == 1:
        copyFile(n12_z2_mem_img, "n12_z2_mem" + i + ".img")

    # Process: Raster Calculator (3)
    n11_z1_mem_img = "n11_z1_mem.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + n5_temp_img
        + " -B "
        + basedir
        + "/"
        + n12_z1_mem_img
        + " --outfile="
        + basedir
        + "/n11_z1_mem.img"
        + " --calc="
        + '"(where(A<0,(2.0*log((1.0+B)/2.0)),(-5.0*'
        + str_z1
        + '/A)))" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(n11_z1_mem_img, gdal_calc)
    if DEBUGCODE == 1:
        copyFile(n11_z1_mem_img, "n11_z1_mem" + i + ".img")

    # Process: Raster Calculator (3)
    n11_z2_mem_img = "n11_z2_mem.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + n5_temp_img
        + " -B "
        + basedir
        + "/"
        + n12_z2_mem_img
        + " --outfile="
        + basedir
        + "/n11_z2_mem.img"
        + " --calc="
        + '"(where(A<0,(2.0*log((1.0+B)/2.0)),(-5.0*'
        + str_z2
        + '/A)))" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(n11_z2_mem_img, gdal_calc)
    if DEBUGCODE == 1:
        copyFile(n11_z2_mem_img, "n11_z2_mem" + i + ".img")
    # Process: Raster Calculator (2)
    n25_mem_img = "n25_mem.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + n5_temp_img
        + " --outfile="
        + basedir
        + "/n25_mem.img"
        + " --calc="
        + '"where(A<0,(pow((1.0-16.0*(200.0/A)),0.25)),1.0)" '
        + "--type=Float64"
        + " --quiet --overwrite --NoDataValue=-9999"
    )
    gdal_calc_func(n25_mem_img, gdal_calc)
    if DEBUGCODE == 1:
        copyFile(n25_mem_img, "n25_mem" + i + ".img")
    # Process: Raster Calculator (6)
    n10_memory_img = "n10_memory.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + n5_temp_img
        + " -B "
        + n25_mem_img
        + " --outfile="
        + basedir
        + "/n10_memory.img"
        + " --calc="
        + '"where(A< 0,(2.0*log((1.0+B)/2) + log((1.0+ pow(B,2.0))/2.0)-2.0* arctan(B)+0.5*math.pi),(-5.0*2.0/A))" '
        + "--type=Float64"
        + " --quiet --overwrite --NoDataValue=-9999"
    )
    gdal_calc_func(n10_memory_img, gdal_calc)
    if DEBUGCODE == 1:
        copyFile(n10_memory_img, "n10_memory" + i + ".img")
    # Process: Raster Calculator (5)
    u_star_o1_img = "u_star_o1.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + u200_img
        + " -B "
        + zo_img
        + " -C "
        + n10_memory_img
        + " --outfile="
        + basedir
        + "/u_star_o1.img"
        + " --calc="
        + '"(A*0.41)/(log(200.0/B)-C)" '
        + "--type=Float64"
        + " --quiet --overwrite --NoDataValue=-9999"
    )
    gdal_calc_func(u_star_o1_img, gdal_calc)
    copy_shp_file("u_star_o1.img", "u_star.img")
    if DEBUGCODE == 1:
        copyFile(u_star_o1_img, "u_star_o1" + i + ".img")
    # Process: Raster Calculator (8)
    rah_img = "rah.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + n11_z1_mem_img
        + " -B "
        + basedir
        + "/"
        + n11_z2_mem_img
        + " -C "
        + basedir
        + "/"
        + u_star_o1_img
        + " --outfile="
        + basedir
        + "/rah.img"
        + " --calc="
        + '"('
        + str_aerod_res
        + '-B+A)/(0.41*C)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(rah_img, gdal_calc)
    if DEBUGCODE == 1:
        copyFile(rah_img, "rah" + i + ".img")


def kmToM(val):
    return val * 1000


# Create a vrt file based on the shapefile name and the zfield that contains the values to be used for and csv file
def create_csv_vrt(file_name_full, zfield):
    file_name = file_name_full[:-4]
    try:
        os.remove(basedir + "/" + file_name + ".vrt")
    except OSError:
        pass
    out_vrt = open(basedir + "/" + file_name + ".vrt", "w")
    out_vrt.write("<OGRVRTDataSource>\n")
    out_vrt.write('    <OGRVRTLayer name="' + file_name + '" >\n')
    out_vrt.write(
        "        <SrcDataSource>" + basedir + "/" + file_name + ".csv</SrcDataSource>\n"
    )
    out_vrt.write("        <GeometryType>wkbPoint</GeometryType>\n")
    out_vrt.write("        <LayerSRS>EPSG:326" + utm_zone + "</LayerSRS>\n")
    out_vrt.write(
        '        <GeometryField encoding="PointFromColumns" x="X" y="Y" z="'
        + zfield
        + '"/>\n'
    )
    out_vrt.write("    </OGRVRTLayer>\n")
    out_vrt.write("</OGRVRTDataSource>")
    out_vrt.close()
    shp_to_csv(file_name_full)


def F13si(ix):
    i = str(ix)
    st_img_hot = "st_hot.img"
    h_img_hot = "h_hot_I.img"
    h_img_cold = "h_cold_I.img"
    rah_img_hot = "rah_hot.img"
    rah_img_cold = "rah_cold.img"
    air_d_img_hot = "air_d_hot.img"
    air_d_img_cold = "air_d_cold.img"
    dt_hot_u_img = "dt_hot_u.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + h_img_hot
        + " -B "
        + rah_img_hot
        + " -C "
        + air_d_img_hot
        + " --outfile="
        + basedir
        + "/dt_hot_u.img"
        + " --calc="
        + '"A*B/(C*'
        + str_Cp
        + ')" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(dt_hot_u_img, gdal_calc)
    if DEBUGCODE == 1:
        copyFile(dt_hot_u_img, "dt_hot_u" + i + ".img")
        copyFile(h_img_hot, "h_hot_dt_h" + i + ".img")

    if USER_UNCALIB == 0:
        dt_cold_u_img = "dt_cold_u.img"
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + h_img_cold
            + " -B "
            + rah_img_cold
            + " -C "
            + air_d_img_cold
            + " --outfile="
            + basedir
            + "/dt_cold_u.img"
            + " --calc="
            + '"A*B/(C*'
            + str_Cp
            + ')" '
            + "--type=Float64"
            + " --overwrite --quiet --NoDataValue=-9999"
        )
        gdal_calc_func(dt_cold_u_img, gdal_calc)
        if DEBUGCODE == 1:
            copyFile(dt_cold_u_img, "dt_cold_u" + i + ".img")
        slop_img = "slop.img"
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + dt_hot_u_img
            + " -B "
            + dt_cold_u_img
            + " -C "
            + st_img_hot
            + " -D "
            + st_img_cold
            + " --outfile="
            + basedir
            + "/slop.img"
            + " --calc="
            + '"(A-B)/(C-D)" '
            + "--type=Float64"
            + " --overwrite --quiet --NoDataValue=-9999"
        )
        gdal_calc_func(slop_img, gdal_calc)
        if DEBUGCODE == 1:
            copyFile(slop_img, "slop" + i + ".img")
    else:
        slop_img = "slop.img"
        delete_file_w_path(basedir + "/" + slop_img)
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + dt_hot_u_img
            + " -B "
            + st_img_hot
            + " -C "
            + st_img_cold
            + " --outfile="
            + basedir
            + "/slop.img"
            + " --calc="
            + '"where((B-C)!=0,A/(B-C),0)" '
            + "--type=Float64"
            + " --overwrite --quiet --NoDataValue=-9999"
        )
        if DEBUGFLAG == 1:
            write_log(gdal_calc)
        os.system(gdal_calc)
        if DEBUGCODE == 1:
            copyFile(slop_img, "slop" + i + ".img")

    intercept_img = "intercept.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + dt_hot_u_img
        + " -B "
        + st_img_hot
        + " -C "
        + slop_img
        + " --outfile="
        + basedir
        + "/intercept.img"
        + " --calc="
        + '"(A-B*C)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(intercept_img, gdal_calc)
    if DEBUGCODE == 1:
        copyFile(intercept_img, "intercept" + i + ".img")


def F10it(ii):
    i = str(ii)
    n10_temp_img = "n10_temp.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + slop_img
        + " -B "
        + st_dem_img
        + " -C "
        + intercept_img
        + " --outfile="
        + basedir
        + "/n10_temp.img"
        + " --calc="
        + '"(A * B + C)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(n10_temp_img, gdal_calc)
    if DEBUGCODE == 1:
        copyFile(n10_temp_img, "n10_temp" + i + ".img")
    n9_mem_img = "n9_mem.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + st_img
        + " -B "
        + n10_temp_img
        + " --outfile="
        + basedir
        + "/n9_mem.img"
        + " --calc="
        + '"(A-B)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(n9_mem_img, gdal_calc)
    if DEBUGCODE == 1:
        copyFile(n9_mem_img, "n9_mem" + i + ".img")
    air_d_img = "air_d.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + n9_mem_img
        + " -B "
        + basedir
        + "/"
        + dem_img
        + " -C "
        + basedir
        + "/"
        + st_img
        + " --outfile="
        + basedir
        + "/air_d.img"
        + " --calc="
        + '"(349.467 * (pow(((A-0.0065*B)/A),5.26)/C))" '
        + "--type=Float64"
        + " --overwrite --quiet  --NoDataValue=-9999"
    )
    gdal_calc_func(air_d_img, gdal_calc)
    if DEBUGCODE == 1:
        copyFile(air_d_img, "air_d" + i + ".img")
    h_img = "h.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + air_d_img
        + " -B "
        + n10_temp_img
        + " -C "
        + rah_img
        + " --outfile="
        + basedir
        + "/h.img"
        + " --calc="
        + '"(A*'
        + str_Cp
        + '*B/C)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(h_img, gdal_calc)
    if DEBUGCODE == 1:
        copyFile(h_img, "h" + i + ".img")
        copyFile(rah_img, "rah_h" + i + ".img")


# This function returns the number of features in a shapefile
def find_n_rows_shp(shp_name):
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(shp_name, 0)  # 0 means read-only. 1 means writeable.
    # Check to see if shapefile is found.
    if dataSource is None:
        write_log("Could not open %s" % (shp_name))
    else:
        layer = dataSource.GetLayer()
        featureCount = layer.GetFeatureCount()
        if DEBUGFLAG == 1:
            write_log(
                "Number of features in %s: %d"
                % (os.path.basename(shp_name), featureCount)
            )
    del dataSource
    return featureCount


# This features converts a point shapefile at comma separated file
def shp_to_csv(pts):
    pts_shp = basedir + "/" + pts
    if DEBUGFLAG > 1:
        write_log("Shape to CSV # 4 Points Name ", pts_shp)
    pts_source = osgeo.ogr.Open(pts_shp, update=1)
    if pts_source is None:
        write_log(
            "Open Point File failed.\nThe file ",
            pts_shp,
            " did not contain any points.",
        )
        exit(1)
    pts_layer = pts_source.GetLayer()
    pts_field_names = [field.name for field in pts_layer.schema]
    if DEBUGFLAG > 1:
        write_log("Point Field Names ", pts_field_names)
    delete_file_w_path(basedir + "/" + pts[:-4] + ".csv")
    out_csv = open(basedir + "/" + pts[:-4] + ".csv", "w")
    pts_fields = []
    pts_fields.append("X")
    pts_fields.append("Y")
    if pts_layer.GetFeature(0).GetFieldCount() > 0:
        for i in range(0, pts_layer.GetFeature(0).GetFieldCount()):
            field1 = pts_layer.GetFeature(0).GetDefnRef().GetFieldDefn(i).GetName()
            pts_fields.append(field1)
        out_csv.write(" ".join(str(x) for x in pts_fields))
        out_csv.write("\n")
        for index in range(pts_layer.GetFeatureCount()):
            feature = pts_layer.GetFeature(index)
            geometry = feature.GetGeometryRef()
            out_csv.write("{0} {1} ".format(geometry.GetX(), geometry.GetY()))
            for i in range(0, pts_layer.GetFeature(0).GetFieldCount()):
                if feature.GetField(i) == None:
                    out_csv.write("0.0")
                else:
                    out_csv.write(str(feature.GetField(i)))
                out_csv.write(" ")
            out_csv.write("\n")
        out_csv.close()
    del pts_source
    strip_csv_NoDataPoints(basedir + "/" + pts[:-4] + ".csv", -9999)


# This functions clips a raster image (in_image) based on a shapefile (aoi_lyr)
def clip_img(in_image, out_image, aoi_lyr):
    clip_img_wo_align(in_image, out_image, aoi_lyr)
    align_rast(out_image)


# This functions clips a raster image (in_image) based on a shapefile (aoi_lyr)
def clip_img_wo_align(in_image, out_image, aoi_lyr):
    out_image1 = out_image[:-4] + "_ZZ" + out_image[-4:]
    delete_file_w_path(basedir + "/" + out_image)
    delete_file_w_path(basedir + "/" + out_image1)
    # GENERATION A 0.0 BUFFER IS NEEDED TO FIX A GEOMETRY FATAL ERROR IN GDALWARP
    shp_buffer(aoi_lyr, 0.0, aoi_lyr[:-4] + "_buff.shp")
    gdal_warp = [
        "gdalwarp",
        "-dstnodata",
        "-9999",
        "-overwrite",
        "-tr",
        str(cellSize),
        str(cellSize),
        "-cutline",
        aoi_lyr[:-4] + "_buff.shp",
# 01-03-23        basedir + "/" + in_image,
        in_image,
        basedir + "/" + out_image1,
    ]
    if DEBUGFLAG == 2:
        write_log(" ".join(gdal_warp))
    out = subprocess.check_output(gdal_warp)
    ds = gdal.Open(basedir + "/" + out_image1)
    ds = gdal.Translate(
        basedir + "/" + out_image, ds, projWin=[clip_x1, clip_y1, clip_x2, clip_y2]
    )
    ds = None
    delete_file_w_path(basedir + "/" + out_image1)


# This function adds a buffer (buff) to a shapefile (in_shp) and returns a new shapefile (out_shp).
def shp_buffer(in_shp, buff, out_shp):
    # LAG1    aoi_cold = 'aoi_cold6.shp'
    delete_shp_file(out_shp)
    shp = ogr.Open(in_shp)
    drv = shp.GetDriver()
    drv.CopyDataSource(shp, out_shp)
    shp.Destroy()
    buf1 = ogr.Open(out_shp, 1)
    lyr1 = buf1.GetLayer(0)
    for i in range(0, lyr1.GetFeatureCount()):
        feat = lyr1.GetFeature(i)
        lyr1.DeleteFeature(i)
        geom1 = feat.GetGeometryRef()
        feat.SetGeometry(geom1.Buffer(buff))
        lyr1.CreateFeature(feat)
    # This code adds an area attribute to the aoi buffered file.  Once you buffer
    # some of the polygons are empty and therefore have no area.  This code
    # determines if a polygon has no area and deletes it.  If we do not delete
    # empty polygons the code crashes later in gdalwarp.
    feature = lyr1.GetNextFeature()
    while feature is not None:
        geom = feature.GetGeometryRef()
        try:
            area = geom.GetArea()
            if area < cellSize * cellSize * 1.1:
                lyr1.DeleteFeature(feature.GetFID())
        except:
            lyr1.DeleteFeature(feature.GetFID())
        feature = lyr1.GetNextFeature()
    buf1.Destroy()


# This function is used to set the OBJECTID for the aoi.shp file to 1.  That will be used in the
# mask to determine the areas that are inside and outside the AOI.
def set_aoi_objectid(aoi_full_path):
    aoi_source = ogr.Open(aoi_full_path, update=1)
    if aoi_source is None:
        write_log("Open AOI File failed ", aoi_full_path)
        exit(1)
    aoi_layer = aoi_source.GetLayer()
    if aoi_layer is None:
        write_log("AOI GetLayer failed for file ", aoi_full_path)
        exit(1)
    # for index in range(aoi_layer.GetFeatureCount()):
    schema = []
    ldefn = aoi_layer.GetLayerDefn()
    ldefn_range = ldefn.GetFieldCount()
    for n in range(ldefn_range):
        fdefn = ldefn.GetFieldDefn(n)
        schema.append(fdefn.name)
    objectid_found = 0
    for n in range(ldefn_range):
        if schema[n] != "OBJECTID":
            aoi_layer.DeleteField(n)
        if schema[n] == "OBJECTID":
            objectid_found = 1
    aoi_source.Destroy()
    aoi_source = ogr.Open(aoi_full_path, update=1)
    if aoi_source is None:
        write_log("Open AOI File failed ", aoi_full_path)
        exit(1)
    aoi_layer = aoi_source.GetLayer()
    if objectid_found == 0:
        if DEBUGFLAG == 1:
            write_log(
                "OBJECTID was not found.  Add field OBJECTID",
                "aoi_full_path ",
                aoi_full_path,
            )
        fldDef = ogr.FieldDefn("OBJECTID", ogr.OFTReal)
        aoi_layer.CreateField(fldDef)
    schema1 = []
    ldefn1 = aoi_layer.GetLayerDefn()
    for n in range(ldefn1.GetFieldCount()):
        fdefn1 = ldefn1.GetFieldDefn(n)
        schema1.append(fdefn1.name)
    featureCount = aoi_layer.GetFeatureCount()
    if featureCount > 0:
        feature = aoi_layer.GetFeature(0)
        if feature is None:
            write_file("AOI GetFeature failed for file ", aoi_full_path)
            exit(1)
        while feature:
            feature.SetField("OBJECTID", 1)
            aoi_layer.SetFeature(feature)
            feature = aoi_layer.GetNextFeature()
    aoi_source.Destroy()


# This function finds the the number of rows in a file that represent the top percent (perc) of the file.
def getTopXPercentCount(row_cnt, perc):
    cnt1 = int(row_cnt * int(perc) / 100.0)
    if cnt1 <= 2:
        cnt1 = 2
    # NOTE in GIS the number was 10 but it started counting at 0 so it ended up with 11 points.  I modified
    # the minimum to be 11 in GDAL so it would match.
    return min(cnt1, 11)


# This function writes to a new file (outfile) only the top (num_lines) from an existing file (filename)
def WriteTopXPercent(filename, basedir, outfile, num_lines):
    infile1 = open(filename, "r")
    outfile1 = open(basedir + "/" + outfile, "w")
    for i in range(0, num_lines + 1):
        line = infile1.readline()
        outfile1.write(line)
    infile1.close()
    outfile1.close()


# This function determines the four corners of the AOI extent (North, South, East, West).  From these points
# a rectangle can be created that will encompass the AOI
def aoi_extent(aoi):
    inDriver = ogr.GetDriverByName("ESRI Shapefile")
    inDataSource = inDriver.Open(aoi, 0)
    inLayer = inDataSource.GetLayer()
    extent = inLayer.GetExtent()
    del inDataSource
    clip_x1 = extent[0]
    clip_y1 = extent[3]
    clip_x2 = extent[1]
    clip_y2 = extent[2]
    x_cells = round((clip_x2 - clip_x1) / cellSize)
    y_cells = round((clip_y1 - clip_y2) / cellSize)
    if DEBUGFLAG > 1:
        write_log("clip_x1 ", extent[0])
        write_log("clip_y1 ", extent[3])
        write_log("clip_x2 ", extent[1])
        write_log("clip_y2 ", extent[2])
        write_log("x_cells = ", x_cells, "y_cells = ", y_cells)
    return (clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells)


# This function delete small polygons in a shapefile.  The function deletes polygons that have less than 110% the cellsize*cellzize (slightly more than one cell).
def check_shp_areas(in_shp):
    ioShpFile = ogr.Open(in_shp, update=1)
    lyr = ioShpFile.GetLayerByIndex(0)
    # This code adds an area attribute to the aoi buffered file.  Once you buffer
    # some of the polygons are empty and therefore have no area.  This code
    # determines if a polygon has no area and deletes it.  If we do not delete
    # empty polygons the code crashes later in gdalwarp.
    lyr.ResetReading()
    feature = lyr.GetNextFeature()
    while feature is not None:
        geom = feature.GetGeometryRef()
        try:
            area = geom.GetArea()
            if DEBUGFLAG == 1:
                write_log(
                    "out_shp",
                    out_shp,
                    "area of polygon ",
                    area,
                    "min area is ",
                    cellSize * cellSize * 1.1,
                    "cell size ",
                    cellSize,
                )
            if area < cellSize * cellSize * 1.1:
                lyr.DeleteFeature(feature.GetFID())
        except:
            lyr.DeleteFeature(feature.GetFID())
        feature = lyr.GetNextFeature()
    ioShpFile = None


# This function converts a raster file to a shapefile
def img_to_shp(rasterTemp, outShp):
    sourceRaster = gdal.Open(rasterTemp)
    band = sourceRaster.GetRasterBand(1)
    driver = ogr.GetDriverByName("ESRI Shapefile")
    # If shapefile already exist, delete it
    if os.path.exists(outShp):
        driver.DeleteDataSource(outShp)
    outDatasource = driver.CreateDataSource(outShp)
    # get proj from raster
    srs = osr.SpatialReference()
    srs.ImportFromWkt(sourceRaster.GetProjectionRef())
    # create layer with proj
    outLayer = outDatasource.CreateLayer(outShp, srs)
    # Add class column (1,2...) to shapefile
    newField = ogr.FieldDefn("Class", ogr.OFTInteger)
    outLayer.CreateField(newField)
    gdal.Polygonize(band, None, outLayer, 0, [], callback=None)
    outDatasource.Destroy()
    sourceRaster = None
    band = None
    ioShpFile = ogr.Open(outShp, update=1)
    lyr = ioShpFile.GetLayerByIndex(0)
    lyr.ResetReading()
    feat = lyr.GetNextFeature()
    while feat is not None:
        if feat.GetField("Class") != 1:
            lyr.DeleteFeature(feat.GetFID())
        feat = lyr.GetNextFeature()
    ioShpFile = None
    return outShp


# This function clips the AOI shapefile to the extent of the Landsat image (to avoid processing areas where we do not have imagery for)
def clip_aoi_to_ls_extent(aoi_file, ls_file, wf2):
    for g in wf2:
        if g.find(ls_image_dir) != -1:
            gdir = os.path.join(cwd, g)

            ls_basedir1 = gdir
            for t in os.listdir(ls_basedir):
                if t.endswith(ls_file):
                    ff = ls_basedir1 + "/" + t
                    if ls == 7:
                        ff0 = ls_basedir1 + "/gap_mask/" + t
                        ff1 = ls_basedir1 + "/gap_mask/" + t[:-4] + "_MASK.TIF"
                        dst_layername = basedir + "/ls_mask1.shp"
                        delete_file_w_path(ff1)
                        delete_file_w_path(dst_layername)
                        gdal_grid_text = (
                            "gdalwarp -dstnodata 0 -dstalpha -of GTiff "
                            + ff0
                            + " "
                            + ff1
                        )
                        if DEBUGFLAG == 2:
                            write_log(gdal_grid_text)
                        os.system(gdal_grid_text)
                        gdal_calc = (
                            "python "
                            + py_script_dir
                            + "/gdal_polygonize.py "
                            + ff1
                            + ' -b 2 -f "ESRI Shapefile" '
                            + dst_layername
                            + " "
                            + "ls_mask1 OBJECTID"
                        )
                        if DEBUGFLAG == 1:
                            write_log(gdal_calc)
                        os.system(gdal_calc)
                        ds = ogr.Open(dst_layername, update=1)
                        if ds is None:
                            write_log("Open shapefile ", dst_layername, " failed.")
                            exit(1)
                        dst_layer = ds.GetLayer()
                    else:
                        gdal_calc = (
                            "python "
                            + py_script_dir
                            + "/gdal_calc.py -A "
                            + ff
                            + " --outfile="
                            + basedir
                            + "/ls_mask1.img"
                            + " --calc="
                            + '"where(A>0,1,-9999)" '
                            + "--type=Float64"
                            + " --NoDataValue=-9999 --quiet"
                        )
                        gdal_calc_func("ls_mask1.img", gdal_calc)
                        src_ds = gdal.Open(basedir + "/ls_mask1.img")
                        if src_ds is None:
                            write_log("Unable to open %s" % src_filename)
                            exit(1)
                        proj = osr.SpatialReference()
                        proj.ImportFromWkt(src_ds.GetProjection())
                        try:
                            srcband = src_ds.GetRasterBand(1)
                        except RuntimeError as e:
                            write_log(
                                "Band ( %i ) not found in source file ( %s) "
                                % (1, src_filename)
                            )
                            exit(1)
                        #  create output datasource
                        delete_shp_file("ls_mask.shp")
                        delete_shp_file("ls_mask1.shp")
                        dst_layername = basedir + "/ls_mask1"
                        drv = ogr.GetDriverByName("ESRI Shapefile")
                        dst_ds = drv.CreateDataSource(dst_layername + ".shp")
                        dst_layer = dst_ds.CreateLayer(
                            dst_layername, proj, geom_type=ogr.wkbMultiPolygon
                        )
                        fd = ogr.FieldDefn("OBJECTID", ogr.OFTReal)
                        dst_layer.CreateField(fd)
                        dst_field = 0
                        gdal.Polygonize(
                            srcband, None, dst_layer, dst_field, [], callback=None
                        )

                    feat = dst_layer.GetNextFeature()
                    while feat is not None:
                        if (
                            feat.GetField("OBJECTID") == 0
                            or feat.GetField("OBJECTID") == -9999
                        ):
                            dst_layer.DeleteFeature(feat.GetFID())
                        feat = dst_layer.GetNextFeature()
                    srcband = None
                    dst_ds = None
                    if ls == 8 or ls == 5 or ls == 9:
                        shp_buffer(dst_layername + ".shp", -500.0, "ls_mask.shp")
                    elif ls == 7:
                        copy_shp_file("ls_mask1.shp", "ls_mask.shp")
                    delete_file_w_path(basedir + "/" + "aoi_file1.img")
                    gdal_grid_text = str(
                        "gdal_rasterize -a OBJECTID -te {0} {1} {2} {3} -tr {4} {4} -a_srs EPSG:326{5} -l {6} {7} {8}".format(
                            clip_x1,
                            clip_y2,
                            clip_x2,
                            clip_y1,
                            cellSize,
                            utm_zone,
                            aoi_file[:-4],
                            basedir + "/" + aoi_file,
                            basedir + "/aoi_file1.img",
                        )
                    )
                    if DEBUGFLAG == 1:
                        write_log(gdal_grid_text)
                    os.system(gdal_grid_text)
                    delete_file_w_path(basedir + "/aoi_file2.img")
                    clip_img(basedir + "/aoi_file1.img", "aoi_file2.img", "ls_mask.shp")
                    delete_shp_file(aoi_file[:-4] + "2.shp")
                    img_to_shp(
                        basedir + "/aoi_file2.img",
                        basedir + "/" + aoi_file[:-4] + "2.shp",
                    )
                    aoi_file2 = aoi_file[:-4] + "2.shp"
                    delete_file_w_path(basedir + "/ls_mask1.img")
                    delete_file_w_path(basedir + "/aoi_file1.img")
                    delete_file_w_path(basedir + "/aoi_file2.img")
                    if ls == 7:
                        ds = None
                    return aoi_file2


# This function returns the projection of a shapefile
def get_projection_shapefile(file_path):
    source = osgeo.ogr.Open(file_path)
    layer = source.GetLayerByIndex(0)
    proj = layer.GetSpatialRef()
    return proj


# This function returns the extent of a shapefile
def get_extent_shapefile(file_path, projection=None):
    reproj_file_path = file_path
    if projection:
        reproj_file_path = project_shapefile(file_path, projection)
    source = osgeo.ogr.Open(reproj_file_path)
    layer = source.GetLayerByIndex(0)
    return layer.GetExtent()


def get_geojson(aoi_file):
    # reproject aoi to epsg:4326 and convert to geojson
    basename, ext = os.path.splitext(aoi_file)
    aoi_file_4326 = basename + "_4326.shp"
    gdaltransform_txt = str(
        "ogr2ogr -s_srs EPSG:326"
        + utm_zone
        + " -t_srs EPSG:4326 "
        + basedir
        + "/"
        + aoi_file_4326
        + " "
        + basedir
        + "/"
        + aoi_file
    )
    if DEBUGFLAG == 1:
        write_log(gdaltransform_txt)
    os.system(gdaltransform_txt)
    aoi_geojson = basename + "_4326.geojson"
    delete_file_w_path(basedir + "/" + aoi_geojson)
    check = subprocess.check_output(
        "ogr2ogr -f GeoJSON {0} {1}".format(aoi_geojson, aoi_file_4326).split()
    )
    if b"Error" in check:
        write_log("Failed to reproject aoi to 4326: {0}".format(check))
        exit(1)
    with open(aoi_geojson) as fp:
        boundary = json.load(fp)
    del boundary["crs"]
    return boundary


def match_projection(shp_path, raster_path):
    # reproject to AOI projection
    aoi_proj = get_projection_shapefile(shp_path)
    reproj_file_name = reproject(raster_path, aoi_proj.ExportToProj4())
    return reproj_file_name


def get_wkt(aoi_file_full_extent):
    aoi_geojson = get_geojson(aoi_file_full_extent)
    s = shapely.geometry.shape(aoi_geojson["features"][0]["geometry"])
    return s.wkt


def reproject(raster_path, projection, cell_size=None):
    basename, ext = os.path.splitext(raster_path)
    reproj_path = basename + "_reproj" + ext
    args = ["gdalwarp", "-t_srs", projection]
    if cell_size:
        args += ["-tr", str(cell_size), str(cell_size)]
    args += [raster_path, reproj_path]
    delete_file_w_path(reproj_path)
    check = subprocess.check_output(args)
    if b"Error" in check:
        write_log("Failed to reproject NLCD to aoi projection: {0}".format(check))
        exit(1)
    return reproj_path


def set_projection(raster_path, proj_str):
    # Assign a projection.
    basename, ext = os.path.splitext(raster_path)
    reproj_path = basename + "_reproj" + ext
    args = [
        "gdalwarp",
        "-t_srs",
        proj_str,
        "-s_srs",
        proj_str,
        raster_path,
        reproj_path,
    ]
    delete_file_w_path(reproj_path)
    check = subprocess.check_output(args)
    if b"Error" in check:
        write_log("Failed to assign raster to projection: {0}".format(check))
        exit(1)
    return reproj_path


def project_shapefile(shp_path, projection):
    basename, ext = os.path.splitext(shp_path)
    reproj_path = basename + "_reproj" + ext
    args = ["ogr2ogr", "-t_srs", projection, reproj_path, shp_path]
    check = subprocess.check_output(args)
    if b"Error" in check:
        write_log("Failed to reproject NLCD to aoi projection: {0}".format(check))
        exit(1)
    return reproj_path


def extract_dem(aoi_file_full_ext, dem_imgx):
    boundary = get_wkt(aoi_file_full_ext)
    req = csip.Client()
    req.add_data("bound_wkt", boundary)
    url = "http://csip.engr.colostate.edu:8088/csip-watershed/m/extract_DEM/1.0"
    dem = req.execute(url)
    if dem.get_error():
        if DEBUGFLAG == 1:
            write_log("Extracting DEM Failed trying local version !!!!!!!!")
        local_dem_layer = local_dem_dir + "/US_DEM_all2.tif"
        # check if the local dem layer exists
        file_exists = os.path.isfile(local_dem_layer)
        if file_exists != 0:
            dem_path = dem_img
            check = subprocess.run(
                [
                    "gdalwarp",
                    "-overwrite",
                    "-ot",
                    "Float32",
                    "-of",
                    "GTiff",
                    "-cutline",
                    aoi,
                    "-crop_to_cutline",
                    "-dstalpha",
                    local_dem_layer,
                    basedir + "/" + dem_path,
                ]
            )
        else:
            write_log(dem.get_error())
            exit(1)
    else:
        files = dem.get_data_files()
        dem.download_data_files(files=files)
        for f in files:
            if "DEM" in f:
                dem_path = f
    reproj_rast(utm_zone, dem_path)
    align_rast(dem_path)
    copyFile(basedir + "/" + dem_path, basedir + "/" + dem_imgx)
    return


def extract_gfsad(aoi1):
    file_exists = os.path.isfile(GFSAD_file_orig)
    if file_exists != 0:
        gfsad_path = GFSAD_file_orig
        check = subprocess.run(
            [
                "gdalwarp",
                "-overwrite",
                "-ot",
                "Float32",
                "-of",
                "GTiff",
                "-cutline",
                aoi1,
                "-crop_to_cutline",
                "-dstalpha",
                GFSAD_file_orig,
                basedir + "/" + GFSAD_file1,
            ]
        )
    else:
        write_log("GFSAD does not exist", gfsad_path)
        exit(10)

    reproj_rast(utm_zone, GFSAD_file1)
    align_rast(GFSAD_file1)
    copyFile(basedir + "/" + GFSAD_file1, basedir + "/" + GFSAD_file)
    return


def extract_nlcd(aoi_file, year):
    if year == 2019:
        ncld_year = year - 1
    else:
        ncld_year = year
    boundary = get_geojson(aoi_file)
    req = csip.Client()
    req.add_data("boundary", [boundary])
    req.add_data("year", ncld_year)
    nlcd = req.execute("http://csip.engr.colostate.edu:8088/csip-nlcd/m/clip/1.0")
    if nlcd.get_error():
        write_log(nlcd.get_error())
        exit(1)
    files = nlcd.get_data_files()
    nlcd.download_data_files(files=files)
    nlcd_path = files[0]
    nlcd_reproj_path = match_projection(aoi_file, nlcd_path)
    return nlcd_reproj_path


def gfsad_mask():
    # first check for cold point in Alfalfa (NASS category 36), corn (NASS category 1), Sweet Corn (NASS category 12) or
    # Pop Corn (NASS category 13)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/aoi_cloud_cold_mask1.img"
        + " -B "
        + basedir
        + "/"
        + GFSAD_file
        + " --outfile="
        + GFSAD_mask_file
        + " --calc="
        + '"where((A*B)==2,1,-9999)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(GFSAD_mask_file, gdal_calc)
    return

def extract_nass(year):
    nass_proj = "EPSG:5070"
    # NOTE THAT IF THE IMAGE IS FOR THE CURRENT YEAR WE LOOK AT THE PREVIOUS YEAR FOR THE NASS LAYER (CURRENT_YEAR - 1)
    current_year = int(datetime.now().year)
    current_month = int(datetime.now().month)
    # NASS FOR THE PREVIOUS YEAR IS RELEASED AROUND FEBRUARY OR MARCH.  IF THE CURRENT MONTH IS MORE THAN MARCH THEN ASSUME NASS IS AVAILABLE FOR THE PREVIOUS YEAR AND
    # SET THE YEAR AS CURRENT - 1 OTHERWISE SET THE YEAR AS CURRENT - 2.
    if current_month > 3:
        nass_year = min(current_year - 1, year)
    else:
        nass_year = min(current_year - 2, year)
    if DEBUGFLAG == 1:
        write_log("The NASS year used is ", nass_year)
    (xmin, xmax, ymin, ymax) = get_extent_shapefile(basedir + "/" + aoi_file, nass_proj)
    data = {
        "year": nass_year,
        "bbox": ",".join([str(x) for x in [xmin, ymin, xmax, ymax]]),
        "srs": nass_proj,
    }
    success = True
    try:
        req = requests.get(
            "https://nassgeodata.gmu.edu/axis2/services/CDLService/GetCDLFile", data
        )
    except:
        success = False
    nass_cold_mask1_img = basedir + "/nass_cold_mask1.img"
    nass_cold_mask1a_img = basedir + "/nass_cold_mask1a.img"
    nass_hot_mask4_img = basedir + "/nass_hot_mask4.img"
    if not success or req.status_code >= 500:
        local_nass_layer = local_nass_dir + "/" + str(nass_year) + "_30m_cdls.img"
        if DEBUGFLAG == 1:
            write_log("NASS Failed trying local version !!!!!!!!", local_nass_layer)
        # check if the local nass layer exists - if it does then extract the aoi area otherwise use the cloud mask
        file_exists = os.path.isfile(local_nass_layer)
        write_log("Check to see if local NASS layer exists return a value of (0 - no; 1 - yes ", file_exists)
        # defining check as None so that if the local NASS file does not exist the if for checking the return value of the gdalwarp has a value to check against
        check_code = None
        if file_exists != 0:
            nass_img1 = nass_img[:-4] + "_1.img"
            check_code = subprocess.run(
                [
                    "gdalwarp",
                    "-overwrite",
                    "-ot",
                    "Float32",
                    "-of",
                    "GTiff",
                    "-te",
                    str(xmin),
                    str(ymin),
                    str(xmax),
                    str(ymax),
                    "-dstalpha",
                    local_nass_layer,
                    basedir + "/" + nass_img1,
                ]
            )
            reproj_rast(utm_zone, nass_img1)
            align_rast(nass_img1)
            clip_img(basedir + "/" + nass_img1, nass_img, aoi)
#            rasterfile = basedir + "/" + nass_img
#            write_log("Before calling Sieve",rasterfile)
#            ET1 = gdal.Open(rasterfile, GA_Update)
#            ETband = ET1.GetRasterBand(1)
#            gdal.SieveFilter(
#                srcBand=ETband, maskBand=None, dstBand=ETband, threshold=20, connectedness=4,
#                callback=gdal.TermProgress_nocb)
#            ET1 = None
            ii = 2
            while ii < 11:
                Sieve_Raster(nass_img, ii, 4)
                ii += 2

            Sieve_Raster(nass_img, threshold, connected)
            delete_file_w_path(basedir + "/" + nass_img1)
#        check_code = check.check_returncode()
        if check_code is not None or file_exists == 0:
            write_log(
                "No NASS layer was found in year",
                year,
                "on the web or locally. "
                "The aoi_cloud_mask.img was set as the NASS mask which is all the valid AOI",
            )
            copyFile("aoi_cloud_mask.img", "nass_cold_mask1.img")
            copyFile("aoi_cloud_mask.img", "nass_cold_mask1a.img")
            copyFile("aoi_cloud_mask.img", "nass_hot_mask4.img")
            copyFile("aoi_cloud_mask.img", "nass_clip.img")
        else:
            if DEBUGFLAG == 1:
                write_log("Local NASS worked !!!!")
            (
                nass_cold_mask1_img,
                nass_cold_mask1a_img,
                nass_hot_mask4_img,
            ) = nass_cold_hot_mask()
    else:
        xml = ET.fromstring(req.content)
        raster_url = xml.find("returnURL").text
        ret_path = "nass.img"
        no_nass = 0
        if raster_url:
            r = requests.get(raster_url, stream=True, verify=False)
            with open(ret_path, "wb") as out_file:
                shutil.copyfileobj(r.raw, out_file)
            reproj_rast(utm_zone, ret_path)
            #        nass_img = 'nass_clip.img'
            align_rast(ret_path)
            clip_img(basedir + "/" + ret_path, nass_img, aoi)
#            rasterfile = basedir + "/" + nass_img
#            write_log("Before calling Sieve",rasterfile)
#            ET1 = gdal.Open(rasterfile, GA_Update)
#            ETband = ET1.GetRasterBand(1)
#            gdal.SieveFilter(
#                srcBand=ETband, maskBand=None, dstBand=ETband, threshold=20, connectedness=4,
#                callback=gdal.TermProgress_nocb)
#            ET1 = None
            ii = 2
            while ii < 11:
                Sieve_Raster(nass_img, ii, 4)
                ii += 2

            Sieve_Raster(nass_img, threshold, connected)
            try:
                nass_min, nass_max, nass_avg, nass_std_d = get_band_stats_original(
                    nass_img
                )
            except RuntimeError:  # <- Check first what exception is being thrown
                create_nass_dummy_mask()
                no_nass = 1
            if no_nass == 0:
                (
                    nass_cold_mask1_img,
                    nass_cold_mask1a_img,
                    nass_hot_mask4_img,
                ) = nass_cold_hot_mask()
    if Uniform_Raster(nass_img) == 1:
        write_log(
            "The NASS layer for year",
            year,
            "was found to have no data. "
            "The aoi_cloud_mask.img was set as the NASS mask which is all the valid AOI",
        )
        copyFile("aoi_cloud_mask.img", "nass_cold_mask1.img")
        copyFile("aoi_cloud_mask.img", "nass_cold_mask1a.img")
        copyFile("aoi_cloud_mask.img", "nass_hot_mask4.img")
        copyFile("aoi_cloud_mask.img", "nass_clip.img")

    return (nass_cold_mask1_img, nass_cold_mask1a_img, nass_hot_mask4_img)


def nass_cold_hot_mask():
    nass_cold_mask1_img = basedir + "/nass_cold_mask1.img"
    nass_hot_mask4_img = basedir + "/nass_hot_mask4.img"
    align_rast(nass_img)
    nass_in_img = basedir + "/" + nass_img
    nass_cold_mask0_img = basedir + "/nass_cold_mask0.img"
    nass_cold_mask0a_img = basedir + "/nass_cold_mask0a.img"
    nass_cold_mask0b_img = basedir + "/nass_cold_mask0b.img"
    nass_cold_mask1a_img = basedir + "/nass_cold_mask1a.img"
    # first check for cold point in Alfalfa (NASS category 36), corn (NASS category 1), Sweet Corn (NASS category 12) or
    # Pop Corn (NASS category 13)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + nass_in_img
        + " --outfile="
        + nass_cold_mask0_img
        + " --calc="
        + '"where(logical_or(A==1, A==36),1,0)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_cold_mask0_img, gdal_calc)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + nass_in_img
        + " --outfile="
        + nass_cold_mask0a_img
        + " --calc="
        + '"where(logical_or(A==12, A==13),1,0)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_cold_mask0a_img, gdal_calc)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + GFSAD_file
        + " --outfile="
        + nass_cold_mask0b_img
        + " --calc="
        + '"where(A==2,1,0)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_cold_mask0b_img, gdal_calc)
    # note that if you do not have any values in nass_cold_mask0 or nass_cold_mask0a then the values should be 0 not -9999 in the gdal_calc in the gdal_calc.
    # when you sum two layers then no values should be zero that way when you sum two layers anything other than zero has values and anything that has zero's can be set to no data (-9999)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + nass_cold_mask0_img
        + " -B "
        + nass_cold_mask0a_img
        + " -C "
        + nass_cold_mask0b_img
        + " --outfile="
        + nass_cold_mask1a_img
        + " --calc="
        + '"((A+B)*C)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_cold_mask1a_img, gdal_calc)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + nass_cold_mask1a_img
        + " --outfile="
        + nass_cold_mask1_img
        + " --calc="
        + '"where(A==1,1,-9999)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_cold_mask1_img, gdal_calc)
    delete_file_w_path(nass_cold_mask0_img)
    delete_file_w_path(nass_cold_mask0a_img)
    delete_file_w_path(nass_cold_mask0b_img)
    # first check for hot point in NASS catogories Barren [65 and 131] and Fallow/Idle cropland [61] )
#    nass_hot_mask1_img = basedir + "/nass_hot_mask1.img"
#    nass_hot_mask2_img = basedir + "/nass_hot_mask2.img"
#    nass_hot_mask2a_img = basedir + "/nass_hot_mask2a.img"
#    nass_hot_mask3_img = basedir + "/nass_hot_mask3.img"
#    gdal_calc = (
#        "python "
#        + py_script_dir
#        + "/gdal_calc.py -A "
#        + nass_in_img
#        + " --outfile="
#        + nass_hot_mask1_img
#        + " --calc="
#        + '"where(logical_or(A==65, A==131),1,0)" '
#        + "--type=Float64"
#        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
#    )
#    gdal_calc_func(nass_hot_mask1_img, gdal_calc)
#    gdal_calc = (
#        "python "
#        + py_script_dir
#        + "/gdal_calc.py -A "
#        + nass_in_img
#        + " --outfile="
#        + nass_hot_mask2_img
#        + " --calc="
#        + '"where(A==61,1,0)" '
#        + "--type=Float64"
#        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
#    )
#    gdal_calc_func(nass_hot_mask2_img, gdal_calc)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + GFSAD_file
        + " --outfile="
        + nass_hot_mask4_img
        + " --calc="
        + '"where(A==2,1,0)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_hot_mask4_img, gdal_calc)
    # note that if you do not have any values in nass_hot_mask1 or nass_hot_mask2 then the values should be 0 not -9999 in the gdal_calc in the gdal_calc.
    # when you sum two layers then no values should be zero that way when you sum two layers anything other than zero has values and anything that has zero's can be set to no data (-9999)
#    gdal_calc = (
#        "python "
#        + py_script_dir
#        + "/gdal_calc.py -A "
#        + nass_hot_mask1_img
#        + " -B "
#        + nass_hot_mask2_img
#        + " -C "
#        + nass_hot_mask2a_img
#        + " --outfile="
#        + nass_hot_mask3_img
#        + " --calc="
#        + '"((A+B)*C)" '
#        + "--type=Float64"
#        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
#    )
#    gdal_calc_func(nass_hot_mask3_img, gdal_calc)
#    gdal_calc = (
#        "python "
#        + py_script_dir
#        + "/gdal_calc.py -A "
#        + nass_hot_mask3_img
#        + " --outfile="
#        + nass_hot_mask4_img
#        + " --calc="
#        + '"where(A>=1,1,-9999)" '
#        + "--type=Float64"
#        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
#    )
#    gdal_calc_func(nass_hot_mask4_img, gdal_calc)
#    delete_file_w_path(nass_hot_mask1_img)
#    delete_file_w_path(nass_hot_mask2_img)
#    delete_file_w_path(nass_hot_mask2a_img)
#    delete_file_w_path(nass_hot_mask3_img)
    return (nass_cold_mask1_img, nass_cold_mask1a_img, nass_hot_mask4_img)


# If no cold point are found in Alfalfa fields (NASS category 36) or No Alfalfa fields are present
# expand the NASS categories to look at.
def nass_second_cold_mask():
    nass_in_img = basedir + "/" + nass_img
    nass_cold_mask0_img = basedir + "/nass_cold_mask0.img"
#    nass_cold_mask1_img = basedir + "/nass_cold_mask1.img"
    nass_cold_mask4_img = basedir + "/nass_cold_mask4.img"
    # look for cold points in NASS categories
#    gdal_calc = (
#        "python "
#        + py_script_dir
#        + "/gdal_calc.py -A "
#        + nass_in_img
#        + " --outfile="
#        + nass_cold_mask0_img
#        + " --calc="
#        + '"where(logical_and(A<39, A>0),1,0)" '
#        + "--type=Float64"
#        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
#    )
#    gdal_calc_func(nass_cold_mask0_img, gdal_calc)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + GFSAD_file
        + " --outfile="
        + nass_cold_mask0_img
        + " --calc="
        + '"where(A==2,1,0)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_cold_mask0_img, gdal_calc)
    copyFile(nass_cold_mask0_img, nass_cold_mask4_img)
#    gdal_calc = (
#        "python "
#        + py_script_dir
#        + "/gdal_calc.py -A "
#        + GFSAD_file
#        + " --outfile="
#        + nass_cold_mask4_img
#        + " --calc="
#        + '"where(A==2,1,-9999)" '
#        + "--type=Float64"
#        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
#    )
#    gdal_calc_func(nass_cold_mask4_img, gdal_calc)
#    gdal_calc = (
#        "python "
#        + py_script_dir
#        + "/gdal_calc.py -A "
#        + nass_cold_mask0_img
#        + " -B "
#        + nass_cold_mask1_img
#        + " --outfile="
#        + nass_cold_mask4_img
#        + " --calc="
#        + '"where((A*B)>=1,1,-9999)" '
#        + "--type=Float64"
#        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
#    )
#    gdal_calc_func(nass_cold_mask4_img, gdal_calc)
#    delete_file_w_path(nass_cold_mask0_img)
#    delete_file_w_path(nass_cold_mask1_img)
    return (nass_cold_mask4_img, nass_cold_mask0_img)


# If no cold point are found in Alfalfa fields (NASS category 36) or No Alfalfa fields are present
# expand the NASS categories to look at.
def nass_third_cold_mask():
    nass_in_img = basedir + "/" + nass_img
    nass_cold_mask0_img = basedir + "/nass_cold_mask0.img"
    nass_cold_mask0a_img = basedir + "/nass_cold_mask0a.img"
    nass_cold_mask1_img = basedir + "/nass_cold_mask1.img"
    nass_cold_mask2_img = basedir + "/nass_cold_mask2.img"
    nass_cold_mask3_img = basedir + "/nass_cold_mask3.img"
    nass_cold_mask3a_img = basedir + "/nass_cold_mask3a.img"
    nass_cold_mask4_img = basedir + "/nass_cold_mask4.img"
    # look for cold points in NASS categories
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + nass_in_img
        + " --outfile="
        + nass_cold_mask0_img
        + " --calc="
        + '"where(logical_and(A<61, A>0),1,0)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_cold_mask0_img, gdal_calc)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + nass_in_img
        + " --outfile="
        + nass_cold_mask0a_img
        + " --calc="
        + '"where(A>203,1,0)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_cold_mask0a_img, gdal_calc)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + nass_in_img
        + " --outfile="
        + nass_cold_mask1_img
        + " --calc="
        + '"where(logical_and(A<77, A>65),1,0)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_cold_mask1_img, gdal_calc)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + nass_in_img
        + " --outfile="
        + nass_cold_mask2_img
        + " --calc="
        + '"where(logical_or(A==152, A==176),1,0)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_cold_mask2_img, gdal_calc)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + nass_cold_mask0_img
        + " -B "
        + nass_cold_mask0a_img
        + " -C "
        + nass_cold_mask1_img
        + " -D "
        + nass_cold_mask2_img
        + " --outfile="
        + nass_cold_mask3_img
        + " --calc="
        + '"(A+B+C+D)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_cold_mask3_img, gdal_calc)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + GFSAD_file
        + " --outfile="
        + nass_cold_mask3a_img
        + " --calc="
        + '"where(A==2,1,0)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_cold_mask3a_img, gdal_calc)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + nass_cold_mask3_img
        + " -B "
        + nass_cold_mask3a_img
        + " --outfile="
        + nass_cold_mask4_img
        + " --calc="
        + '"where((A*B)>=1,1,-9999)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_cold_mask4_img, gdal_calc)
    delete_file_w_path(nass_cold_mask0_img)
    delete_file_w_path(nass_cold_mask0a_img)
    delete_file_w_path(nass_cold_mask1_img)
    delete_file_w_path(nass_cold_mask2_img)
    delete_file_w_path(nass_cold_mask3a_img)
    return (nass_cold_mask4_img, nass_cold_mask3_img)


def nass_second_hot_mask():
    nass_in_img = basedir + "/" + nass_img
    nass_hot_mask1_img = basedir + "/nass_hot_mask1.img"
    nass_hot_mask2_img = basedir + "/nass_hot_mask2.img"
    nass_hot_mask2a_img = basedir + "/nass_hot_mask2a.img"
    nass_hot_mask3_img = basedir + "/nass_hot_mask3.img"
    nass_hot_mask4_img = basedir + "/nass_hot_mask4.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + nass_in_img
        + " --outfile="
        + nass_hot_mask1_img
        + " --calc="
        + '"where(logical_or(A==82,A==122),-9999,1)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_hot_mask1_img, gdal_calc)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + nass_in_img
        + " --outfile="
        + nass_hot_mask2_img
        + " --calc="
        + '"where(logical_or(A==123, A==124),-9999,1)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_hot_mask2_img, gdal_calc)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + GFSAD_file
        + " --outfile="
        + nass_hot_mask2a_img
        + " --calc="
        + '"where(A==2,1,0)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_hot_mask2a_img, gdal_calc)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + nass_hot_mask1_img
        + " -B "
        + nass_hot_mask2_img
        + " -C "
        + nass_hot_mask2a_img
        + " --outfile="
        + nass_hot_mask4_img
        + " --calc="
        + '"(((A+B)*C)==1,1,-9999)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nass_hot_mask4_img, gdal_calc)
#    gdal_calc = (
#        "python "
#        + py_script_dir
#        + "/gdal_calc.py -A "
#        + nass_hot_mask3_img
#        + " --outfile="
#        + nass_hot_mask4_img
#        + " --calc="
#        + '"where(A==1,1,-9999)" '
#        + "--type=Float64"
#        + " --overwrite --quiet --NoDataValue=-9999 --quiet"
#    )
#    gdal_calc_func(nass_hot_mask4_img, gdal_calc)
    delete_file_w_path(nass_hot_mask1_img)
    delete_file_w_path(nass_hot_mask2_img)
    delete_file_w_path(nass_hot_mask2a_img)
    delete_file_w_path(nass_hot_mask3_img)
    return nass_hot_mask4_img


def create_nass_dummy_mask():
    nass_cold_dummy_mask_img = basedir + "/nass_cold_mask1.img"
    delete_file_w_path(nass_cold_dummy_mask_img)
    gdal_grid_text = str(
        "gdal_rasterize -a OBJECTID -te {0} {1} {2} {3} -tr {4} {4} -a_srs EPSG:326{5} -l {6} {7} {8}".format(
            clip_x1,
            clip_y2,
            clip_x2,
            clip_y1,
            cellSize,
            utm_zone,
            aoi_file[:-4],
            basedir + "/" + aoi_file,
            nass_cold_dummy_mask_img,
        )
    )
    if DEBUGFLAG == 1:
        write_log(gdal_grid_text)
    os.system(gdal_grid_text)
    nass_hot_dummy_mask_img = basedir + "/nass_hot_mask4.img"
    delete_file_w_path(nass_hot_dummy_mask_img)
    gdal_grid_text = str(
        "gdal_rasterize -a OBJECTID -te {0} {1} {2} {3} -tr {4} {4} -a_srs EPSG:326{5} -l {6} {7} {8}".format(
            clip_x1,
            clip_y2,
            clip_x2,
            clip_y1,
            cellSize,
            utm_zone,
            aoi_file[:-4],
            basedir + "/" + aoi_file,
            nass_hot_dummy_mask_img,
        )
    )
    if DEBUGFLAG == 1:
        write_log(gdal_grid_text)
    os.system(gdal_grid_text)
    return ()


def imagt(MTL):
    fp = open(MTL, "r")
    ln = fp.readline()
    for ln in fp:
        if "LANDSAT_7" in ln:
            ls = 7
            return ls
        elif "LANDSAT_5" in ln:
            ls = 5
            return ls
        elif "LANDSAT_8" in ln:
            ls = 8
            return ls
        elif "LANDSAT_9" in ln:
            ls = 9
            return ls
        else:
            ls = 10
    fp.close()
    return ls


### READ LANDSAT 5 IMAGE
def readLansat5(ls_dir, ls_image, workspace):
    for i in range(1, 8):
        shutil.copy2(
            ls_dir + "/" + ls_image + "_B" + str(i) + "_CLIP.TIF",
            workspace + "/R_" + str(i) + ".img",
        )


### READ LANDSAT 9 IMAGE
def readLansat9(ls_dir, ls_image, workspace):
    for i in range(1, 8):
        if i < 6:
            shutil.copy2(
                ls_dir + "/" + ls_image + "_B" + str(i + 1) + "_CLIP.TIF",
                workspace + "/R_" + str(i) + ".img",
            )
        elif i == 6:
            shutil.copy2(
                ls_dir + "/" + ls_image + "_B" + str(10) + "_CLIP.TIF",
                workspace + "/R_" + str(i) + ".img",
            )
        else:
            shutil.copy2(
                ls_dir + "/" + ls_image + "_B" + str(i) + "_CLIP.TIF",
                workspace + "/R_" + str(i) + ".img",
            )


### READ LANDSAT 8 IMAGE
def readLansat8(ls_dir, ls_image, workspace):
    for i in range(1, 8):
        if i < 6:
            shutil.copy2(
                ls_dir + "/" + ls_image + "_B" + str(i + 1) + "_CLIP.TIF",
                workspace + "/R_" + str(i) + ".img",
            )
        elif i == 6:
            shutil.copy2(
                ls_dir + "/" + ls_image + "_B" + str(10) + "_CLIP.TIF",
                workspace + "/R_" + str(i) + ".img",
            )
        else:
            shutil.copy2(
                ls_dir + "/" + ls_image + "_B" + str(i) + "_CLIP.TIF",
                workspace + "/R_" + str(i) + ".img",
            )


### READ LANDSAT 7 IMAGE
def readLansat7(ls_dir, ls_image, workspace):
    for i in range(1, 8):
        filename = workspace + "/R_" + str(i) + ".img"
        delete_file(filename)
        if i == 6:
            copyFile(ls_dir + "/" + ls_image + "_B6_VCID_1_CLIP.TIF", filename)
        else:
            copyFile(ls_dir + "/" + ls_image + "_B" + str(i) + "_CLIP.TIF", filename)
    return ()


def read_acq_date(fp_path):
    re_date = re.compile("DATE_ACQUIRED = (.+)")
    re_time = re.compile("SCENE_CENTER_TIME = (.+)")
    fp = open(fp_path, "r")
    ln = fp.readline()
    while ln:
        s = re_date.search(ln)
        if s:
            dt_str0 = s.group(1)

        s = re_time.search(ln)
        if s:
            time_str = s.group(1)
        ln = fp.readline()
    Y, M, D = dt_str0.split("-")
    h, m, s = time_str.split(":")
    h1 = h[1:3]
    h = int(h1)
    s = int(math.floor(float(s[:-2])))  # trim trailing 'Z'
    mer = "AM"
    if h > 12:
        h -= 12
        mer = "PM"
    fp.close()
    # Format is 7/7/2001 5:26:00 PM
    return "{M}/{D}/{Y} {h}:{m}:{s} {mer}".format(M=M, D=D, Y=Y, h=h, m=m, s=s, mer=mer)


def readFile(fp_path):
    fp = open(fp_path)
    ln = fp.readline()
    corner_re = re.compile("CORNER_(..)_(...)_PRODUCT = (.*)")
    corners = {}
    while ln:
        search = corner_re.search(ln)
        if search:
            corner = search.group(1)
            coord = search.group(2)
            val = float(search.group(3))
            # generate variables of the form UL_LAT, LR_LON, etc
            corners["{0}_{1}".format(corner, coord)] = val
        ln = fp.readline()
    # Calculate centroid by averaging the average points
    UL_LR = (corners["UL_LAT"] + corners["LR_LAT"]) / 2.0
    LL_UR = (corners["LL_LAT"] + corners["UR_LAT"]) / 2.0
    center_lat = (UL_LR + LL_UR) / 2.0
    UL_LR = (corners["UL_LON"] + corners["LR_LON"]) / 2.0
    LL_UR = (corners["LL_LON"] + corners["UR_LON"]) / 2.0
    center_lon = (UL_LR + LL_UR) / 2.0
    fp.close()
    return (str(center_lat), str(center_lon))


def calculateRegion(lon):
    # """Get the region from the longitude"""
    tz_lon = int(round(math.fabs(-104) / 15.0, 0) * 15)
    while tz_lon >= 360:
        tz_lon -= 360
    while tz_lon < 0:
        tz_lon += 360
    return tz_lon


def calcDate(useGMT_str, region, dt_str):
    # Converts GMT to local time.
    from datetime import datetime, timedelta

    useGMT = useGMT_str == "true"
    dt = datetime.strptime(dt_str, "%m/%d/%Y %I:%M:%S %p")
    if useGMT:
        region = int(region)
        if region < 180:
            add_hours = -region / 15
        else:
            add_hours = (360 - region) / 15
        dt += timedelta(hours=add_hours)
    return dt


def readFile_ls5(fp_path):
    fp = open(fp_path)
    ln = fp.readline()
    values_min = []
    values_max = []
    while ln:
        if ln.find("MIN_MAX_RADIANCE") >= 0:
            # Read block of limits
            ln = fp.readline()
            while ln.find("END_GROUP") < 0:
                tokens = ln.split()
                if tokens[0].find("MIN") >= 0:
                    values_min.append(float(tokens[2]))  # column N
                elif tokens[0].find("MAX") >= 0:
                    values_max.append(float(tokens[2]))  # column N

                ln = fp.readline()
            break
        ln = fp.readline()
    return (values_min, values_max)


def readFile_ls7(fp_path):
    fp = open(fp_path)
    ln = fp.readline()
    values_min = []
    values_max = []
    while ln:
        if ln.find("MIN_MAX_RADIANCE") >= 0:
            # Read block of limits
            ln = fp.readline()
            while ln.find("END_GROUP") < 0:
                tokens = ln.split()

                # Skip band 62 and 8
                if tokens[0].find("BAND_6_VCID_2") >= 0:
                    pass
                elif tokens[0].find("BAND_8") >= 0:
                    pass

                # Any other line can just be added
                elif tokens[0].find("MIN") >= 0:
                    values_min.append(float(tokens[2]))  # column N
                elif tokens[0].find("MAX") >= 0:
                    values_max.append(float(tokens[2]))  # column N

                ln = fp.readline()
            break
        ln = fp.readline()
    return (values_min, values_max)


def readFile_ls8(fp_path):
    fp = open(fp_path)
    ln = fp.readline()
    values_min = []
    values_max = []
    while ln:
        if ln.find("RADIOMETRIC_RESCALING") >= 0:
            # Read block of limits
            ln = fp.readline()
            while ln.find("END_GROUP") < 0:
                tokens = ln.split()
                # Skip band 62 and 8
                if tokens[0].find("BAND_6_VCID_2") >= 0:
                    pass
                elif tokens[0].find("BAND_8") >= 0:
                    pass
                # Any other line can just be added
                elif tokens[0].find("REFLECTANCE_ADD") >= 0:
                    values_min.append(float(tokens[2]))  # column N
                elif tokens[0].find("REFLECTANCE_MULT") >= 0:
                    values_max.append(float(tokens[2]))  # column N
                ln = fp.readline()
            break
        ln = fp.readline()
    fp.close()
    return (values_min, values_max)


def readFile_ls9(fp_path):
    fp = open(fp_path)
    ln = fp.readline()
    values_min = []
    values_max = []
    while ln:
        if ln.find("RADIOMETRIC_RESCALING") >= 0:
            # Read block of limits
            ln = fp.readline()
            while ln.find("END_GROUP") < 0:
                tokens = ln.split()
                # Skip band 62 and 8
                if tokens[0].find("BAND_6_VCID_2") >= 0:
                    pass
                elif tokens[0].find("BAND_8") >= 0:
                    pass
                # Any other line can just be added
                elif tokens[0].find("REFLECTANCE_ADD") >= 0:
                    values_min.append(float(tokens[2]))  # column N
                elif tokens[0].find("REFLECTANCE_MULT") >= 0:
                    values_max.append(float(tokens[2]))  # column N
                ln = fp.readline()
            break
        ln = fp.readline()
    fp.close()
    return (values_min, values_max)


def pnt_val_from_rast(src_filename, shp_filename, extract_option):
    src_ds = gdal.Open(basedir + "/" + src_filename)
    if src_ds is None:
        write_log("Open Grid File ", src_filename, " failed./n")
        exit(1)
    gt = src_ds.GetGeoTransform()
    rb = src_ds.GetRasterBand(1)

    ds = ogr.Open(basedir + "/" + shp_filename, update=1)
    if ds is None:
        write_log("Open Point File ", shp_filename, " failed./n")
        exit(1)
    lyr = ds.GetLayer()
    field_name_len = len(src_filename)
    if field_name_len > 14:
        field_name = src_filename[:10]
    else:
        field_name = src_filename[:-4]
    shp_ldef = lyr.GetLayerDefn()
    field_loc = shp_ldef.GetFieldIndex(field_name)
    #   checking if the field exists
    if field_loc != -1:
        lyr.DeleteField(field_loc)
    field_defn = ogr.FieldDefn(field_name, ogr.OFTReal)
    lyr.CreateField(field_defn)
    i = 0
    feature = lyr.GetNextFeature()
    while feature:
        geom = feature.GetGeometryRef()
        mx, my = geom.GetX(), geom.GetY()  # coord in map units
        # Convert from map to pixel coordinates.
        # Only works for geotransforms with no rotation.
        px = int((mx - gt[0]) / gt[1])  # x pixel
        py = int((my - gt[3]) / gt[5])  # y pixel
        fsum = 0
        cnt = 0
        if extract_option == "BILINEAR":
            structval = rb.ReadAsArray(px, py, 1, 1, buf_type=gdal.GDT_Float64)
            fval0 = struct.unpack(
                "d", structval
            )  # use the 'float' format code (4 bytes) not double 'd' (8 bytes)
            structval = rb.ReadAsArray(px - 1, py, 1, 1, buf_type=gdal.GDT_Float64)
            fval1 = struct.unpack(
                "d", structval
            )  # use the 'float' format code (4 bytes) not double 'd' (8 bytes)
            structval = rb.ReadAsArray(px + 1, py, 1, 1, buf_type=gdal.GDT_Float64)
            fval2 = struct.unpack(
                "d", structval
            )  # use the 'float' format code (4 bytes) not double 'd' (8 bytes)
            structval = rb.ReadAsArray(px, py - 1, 1, 1, buf_type=gdal.GDT_Float64)
            fval3 = struct.unpack(
                "d", structval
            )  # use the 'float' format code (4 bytes) not double 'd' (8 bytes)
            structval = rb.ReadAsArray(px, py + 1, 1, 1, buf_type=gdal.GDT_Float64)
            fval4 = struct.unpack(
                "d", structval
            )  # use the 'float' format code (4 bytes) not double 'd' (8 bytes)
            if fval0[0] != -9999:
                fsum = fsum + fval0[0]
                cnt += 1
            if fval1[0] != -9999:
                fsum = fsum + fval0[0]
                cnt += 1
            if fval2[0] != -9999:
                fsum = fsum + fval0[0]
                cnt += 1
            if fval3[0] != -9999:
                fsum = fsum + fval0[0]
                cnt += 1
            if fval4[0] != -9999:
                fsum = fsum + fval0[0]
                cnt += 1
            fval5 = fsum / cnt
            if DEBUGFLAG > 1:
                write_log(
                    "mid cell ",
                    fval0[0],
                    "right cell ",
                    fval1[0],
                    "left cell ",
                    fval2[0],
                    "below cell ",
                    fval3[0],
                    "above cell",
                    fval4[0],
                    " Average ",
                    fval5,
                )
        else:
            structval = rb.ReadAsArray(px, py, 1, 1, buf_type=gdal.GDT_Float64)
            fval0 = struct.unpack(
                "d", structval
            )  # use the 'float' format code (4 bytes) not double 'd' (8 bytes)
            fval5 = fval0[0]

        if i < 2:
            i = i + 1
        feature.SetField(field_name, fval5)
        lyr.SetFeature(feature)
        feature = lyr.GetNextFeature()
    del ds, src_ds


# Set NDVI and RN values combined into a new field rn_ndvi
def rn_ndvi_pnt_val(shp_filename):
    ds = ogr.Open(basedir + "/" + shp_filename, update=1)
    if ds is None:
        write_log("Open Point File ", shp_filename, " failed./n")
        exit(1)
    lyr = ds.GetLayer()
    shp_ldef = lyr.GetLayerDefn()
    rn_field_loc = shp_ldef.GetFieldIndex("rn")
    #   checking if the rn_img field exists
    if rn_field_loc == -1:
        write_log(
            "The attribute field rn does not exist.  Please check the",
            shp_filename,
            "shapefile!!!!",
        )
        exit(1)
    ndvi_field_loc = shp_ldef.GetFieldIndex("ndvi")
    #   checking if the ndvi field exists
    if rn_field_loc == -1:
        write_log(
            "The attribute field ndvi does not exist.  Please check the",
            shp_filename,
            "shapefile!!!!",
        )
        exit(1)
    rn_ndvi_field_loc = shp_ldef.GetFieldIndex("rn_ndvi")
    #   checking if the field exists
    if rn_ndvi_field_loc != -1:
        lyr.DeleteField("rn_ndvi")
    rn_ndvi_field_defn = ogr.FieldDefn("rn_ndvi", ogr.OFTReal)
    lyr.CreateField(rn_ndvi_field_defn)
    feature = lyr.GetNextFeature()
    while feature:
        rn_field_val = feature.GetField(rn_field_loc) / 1000.0
        ndvi_field_val = feature.GetField(ndvi_field_loc)
        rn_ndvi_sum = rn_field_val + ndvi_field_val
        feature.SetField("rn_ndvi", rn_ndvi_sum)
        lyr.SetFeature(feature)
        feature = lyr.GetNextFeature()
    del ds


# Read down to MIN_MAX_RADIANCE
def calcS(dt_str):
    from datetime import datetime, date

    dt = datetime.strptime(dt_str, "%m/%d/%Y %I:%M:%S %p")
    if dt.month < 7:
        return [0.9, 40, 0.9, 50]
    return [1, 90, 1, 100]


def getYearFrac(dt_str):
    from datetime import date, datetime

    dt = datetime.strptime(dt_str, "%m/%d/%Y %I:%M:%S %p")
    DOY = dt.toordinal() - date(dt.year, 1, 1).toordinal() + 1
    if DEBUGFLAG > 1:
        write_log("DOY - Julian Day ", DOY)

    import calendar

    days_in_year = 366 if calendar.isleap(dt.year) else 365
    return float(DOY) / days_in_year


# Acqu_time
def calc(dt_str):
    # from datetime import date, datetime
    dt1 = dt_str
    return dt1.hour + dt1.minute / 60.0 + dt1.second / 3600.0


##DOY
def getDOY(dt_str):
    from datetime import date, datetime

    dt = datetime.strptime(dt_str, "%m/%d/%Y %I:%M:%S %p")
    DOY = dt.toordinal() - date(dt.year, 1, 1).toordinal() + 1
    return DOY


## Albedo calculations
####################
def Alb_calc(albedo_coeff, albedo, min_range, max_range):
    if DEBUGFLAG > 1:
        write_log(
            "Albedo Coefficient is {3}, {0} < {1} < {2}".format(
                min_range, albedo, max_range, albedo_coeff
            )
        )

    rng = max_range - min_range
    inc = 0.01
    if rng < 0.099:
        inc = 0.005

    if albedo < min_range:
        albedo_coeff -= inc
        return albedo_coeff

    if albedo > max_range:
        albedo_coeff += inc
        return albedo_coeff
    return albedo_coeff


# NASS COLD POINT MASK
def nass_cold_pt_mask():
    s_t_cold_img = "s_t_cold.img"
    s_t_cold3_img = "s_t_cold3.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + nass_cold_mask
        + " -B "
        + basedir
        + "/"
        + s_t_cold3_img
        + " --outfile="
        + basedir
        + "/"
        + s_t_cold_img
        + " --calc="
        + '"A*B"'
        + " --type=Float64 --overwrite --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(s_t_cold_img, gdal_calc)


# NASS HOT POINT MASK
def nass_hot_pt_mask():
    s_t_hot_img = "s_t_hot.img"
    ##    copyFile("s_t_hot1.img", "s_t_hot.img")
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + nass_hot_mask
        + " -B "
        + basedir
        + "/"
        + s_t_hot1_img
        + " --outfile="
        + basedir
        + "/"
        + s_t_hot_img
        + " --calc="
        + '"A*B"'
        + " --type=Float64 --overwrite --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(s_t_hot_img, gdal_calc)


def finding_cold_pts_set():
    delete_shp_file("cold_pts_set.shp")
    filename = basedir + "/s_t_cold"
    gdal_translate_txt = str(
        'gdal_translate -a_nodata -9999 -of "XYZ" {0}.img {0}.xyz'.format(filename)
    )
    if DEBUGFLAG == 1:
        write_log(gdal_translate_txt)
    os.system(gdal_translate_txt)
    strip_xyz_NoDataPoints(filename, -9999.0)
    delete_file_w_path(basedir + "/" + "cold_pts_set.csv")
    # os.rename('{}.csv'.format(filename), basedir + '/cold_point_set.csv')
    copyFile("s_t_cold.csv", "cold_pts_set.csv")
    cold_num_pts = sort_points(basedir + "/cold_pts_set.csv", "Z", "ASCENDING")
    return cold_num_pts


def finding_hot_pts_set():
    filename = basedir + "/s_t_hot"
    gdal_translate_txt = str(
        'gdal_translate -a_nodata -9999 -of "XYZ" {0}.img {0}.xyz'.format(filename)
    )
    if DEBUGFLAG == 1:
        write_log(gdal_translate_txt)
    os.system(gdal_translate_txt)
    strip_xyz_NoDataPoints(filename, -9999)
    delete_file_w_path(basedir + "/" + "hot_pts_set.csv")
    copyFile("s_t_hot.csv", "hot_pts_set.csv")
    hot_num_points = sort_points(basedir + "/hot_pts_set.csv", "Z", "DESCENDING")
    return hot_num_points


def check_min_max(file_name, min_val, max_val):
    str_min_v = str(min_val)
    str_max_v = str(max_val)
    temp_file = "temp_file.img"
    delete_file_w_path(basedir + "/" + temp_file)
    os.rename(basedir + "/" + file_name, basedir + "/" + temp_file)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + temp_file
        + " --outfile="
        + basedir
        + "/"
        + file_name
        + " --calc="
        + '"where(logical_and(A>='
        + str_min_v
        + ",A<="
        + str_max_v
        + '),A,-9999)" '
        + " --NoDataValue=-9999 --type=Float64"
        + " --quiet"
    )
    gdal_calc_func(file_name, gdal_calc)
    delete_file_w_path(basedir + "/" + temp_file)


# we are NOT using the NASS Mask.  We are changing to using the GFSAD mask instead
#def calc_s_t_cold_mask(nass_mask_img):
def calc_s_t_cold_mask():
    ### Process: Raster Calculator (4)
    s_t_cold1_img = "s_t_cold1.img"
#    no_nass_cat_found = Uniform_Raster(nass_mask_img
    no_nass_cat_found = Uniform_Raster(GFSAD_mask_file)
    if no_nass_cat_found == 1:
        write_log("No NASS categories found therefore set the s_t_cold1.img to zero")
    else:
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + basedir
            + "/"
            + ncm_img
            + " -B "
# we are NOT using the NASS Mask.  We are changing to using the GFSAD mask instead
#            + nass_mask_img
            + GFSAD_mask_file
            + " -C "
            + basedir
            + "/"
            + st_dem_c_img
            + " --outfile="
            + basedir
            + "/s_t_cold1.img"
            + " --calc="
            + '"where(logical_and(A == 1,B == 1),C,-9999)" '
            + "--type=Float64"
            + " --overwrite --NoDataValue=-9999 --quiet"
        )
        gdal_calc_func(s_t_cold1_img, gdal_calc)
        # Checking if after applying the NDVI filter (ncm.img and the st_dem_c_img) there are no usable areas.
        no_nass_cat_found = Uniform_Raster(s_t_cold1_img)
        write_log("s_t_cold1_img Uniform Raster result is ", s_t_cold1_img)
        #        result_histogram,total_count_histogram = calc_histogram(basedir + '/' + s_t_cold1_img)
        #        write_log('The total count of cells in the histogram for s_t_cold1_img is ',total_count_histogram)
        #        write_log('The histogram for s_t_cold1_img is ',result_histogram)

        if no_nass_cat_found == 0:
            # If using LS7 make sure that the s_t_cold1.img is only for areas not filled (ls7_gap_mask_clip.img).  This is done by
            # clipping s_t_cold1.img into s_t_cold1a.img and then renaming it back to s_t_cold1.img for checking on the AOI
            if ls == 7:
                s_t_cold1a_img = "s_t_cold1a.img"
                gdal_calc = (
                    "python "
                    + py_script_dir
                    + "/gdal_calc.py -A "
                    + basedir
                    + "/ls7_gap_mask_clip.img"
                    + " -B "
                    + basedir
                    + "/"
                    + s_t_cold1_img
                    + " --outfile="
                    + basedir
                    + "/s_t_cold1a.img"
                    + " --calc="
                    + '"where(A==0, -9999,B)" '
                    + " --type=Float64"
                    + " --overwrite --NoDataValue=-9999 --quiet"
                )
                gdal_calc_func(s_t_cold1a_img, gdal_calc)
                delete_file_w_path(basedir + "/" + s_t_cold1_img)
                os.rename(basedir + "/" + s_t_cold1a_img, basedir + "/" + s_t_cold1_img)

            s_t_cold2_img = "s_t_cold2.img"
            if USER_COLD_AOI_FILE == 0:
                clip_img(basedir + "/" + s_t_cold1_img, s_t_cold2_img, basedir + "/" + aoi_cold)
            if USER_COLD_AOI_FILE == 1:
                s_t_cold2a_img = "s_t_cold2a.img"
                clip_img(
                    basedir + "/" + s_t_cold1_img, s_t_cold2a_img, basedir + "/" + aoi_cloud_cold_file
                )
                clip_img(basedir + "/" + s_t_cold2a_img, s_t_cold2_img, basedir + "/" + aoi_cold)
                delete_file_w_path(basedir + "/" + s_t_cold2a_img)

            # LAG3 clip_img(s_t_cold1_img, s_t_cold2_img, aoi_cold)
            cold2_mask_img = "cold2_mask.img"
            gdal_calc = (
                "python "
                + py_script_dir
                + "/gdal_calc.py -A "
                + basedir
                + "/"
                + s_t_cold2_img
                + " --outfile="
                + basedir
                + "/cold2_mask.img"
                + " --calc="
                + '"where(A<=0,-9999,1)" '
                + "--type=Float64"
                + " --overwrite --NoDataValue=-9999 --quiet"
            )
            gdal_calc_func(cold2_mask_img, gdal_calc)

            s_t_cold2_mask_img = "s_t_cold2_mask.img"
            gdal_calc = (
                "python "
                + py_script_dir
                + "/gdal_calc.py -A "
                + basedir
                + "/"
                + cold2_mask_img
                + " -B "
                + basedir
                + "/"
                + s_t_cold2_img
                + " --outfile="
                + basedir
                + "/"
                + s_t_cold2_mask_img
                + " --calc="
                + '"A*B"'
                + " --type=Float64 --overwrite --NoDataValue=-9999 --quiet"
            )
            gdal_calc_func(s_t_cold2_mask_img, gdal_calc)

            min_t_c, max_t_c, avg_v_c, std_d_c = get_band_stats_original(
                s_t_cold2_mask_img
            )

            # 5-2-20 var2 = float(avg_v_c) - 2.0*float(std_d_c)

            var2 = float(avg_v_c) - 1.75 * float(std_d_c)
            if var2 < min_t_c:
                var2 = float(avg_v_c) - 1.25 * float(std_d_c)

            write_log(
                "\n\n\n\nInformation on COLD SURFACE \n",
                "min_t_c",
                min_t_c,
                " max_t_c",
                max_t_c,
                " std_d_c",
                std_d_c,
                "\nvar2 var2 = float(avg_v_c) - 1.75 * float(std_d_c)",
                var2
            )


            if DEBUGFLAG > 1:
                write_log(
                    "If s_t_cold2.img is greater than ",
                    var2,
                    " then -9999, otherwise s_t_cold2.img",
                )
                write_log(
                    "min_t_c",
                    min_t_c,
                    "max_t_c",
                    max_t_c,
                    "std_d_c",
                    std_d_c,
                    "Cold_min_temp_perc",
                    Cold_min_temp_perc,
                )
            # LAG 8-17str_var2 = str(var2)
            str_var2 = str("%.4f" % var2)
            s_t_cold3_img = "s_t_cold3.img"
            gdal_calc = (
                "python "
                + py_script_dir
                + "/gdal_calc.py -A "
                + basedir
                + "/"
                + s_t_cold2_img
                + " --outfile="
                + basedir
                + "/s_t_cold3.img"
                + " --calc="
                + '"where(A > '
                + str_var2
                + ',-9999,A)" '
                + "--type=Float64"
                + " --overwrite --NoDataValue=-9999 --quiet"
            )
            gdal_calc_func(s_t_cold3_img, gdal_calc)
    return no_nass_cat_found


def check_aoi_ls_footprint(aoi_shapefile, ls_image, ls_dir):
    """If the AOI crosses any part of the images, even the collar, then return True"""
    # create mask of image that is not the collar
    ls_mask = ls_dir + "/ls_mask.img"
    # 5-6-20    ls_mask = os.path.join(os.path.dirname(ls_image), 'ls_mask.img')
    gdal_calc = (
        "python3 "
        + py_script_dir
        + "/gdal_calc.py -A "
        + ls_image
        + " --outfile="
        + ls_mask
        + ' --calc="A>1" --quiet --overwrite'
    )
    subprocess.check_output(gdal_calc, shell=True)
    if DEBUGFLAG > 1:
        write_log("AOI shapefile ", aoi_shapefile, "Landsat Mask ", ls_mask)
    stats = zonal_stats(aoi_shapefile, ls_mask)
    if stats[0]["count"] == 0:
        write_log(
            "The footprint of the AOI shapefile is NOT inside the Landsat image!!! Either select a new Landsat image or modify the location of the AOI."
        )
        exit(0)


def calc_histogram(raster_path):
    dataset = gdal.Open(raster_path)
    #    dataset = gdal.Open(raster_path, gdalconst.GA_ReadOnly)
    bucket_class_count = 10
    raster_min, raster_max, raster_mean, raster_sd = get_band_stats(raster_path)
    write_log("Inside Histogram ", raster_min, raster_max, raster_mean, raster_sd)
    #    histogram = dataset.GetRasterBand(1).GetHistogram(raster_min, raster_max, bucket_class_count)
    histogram = dataset.GetRasterBand(1).GetHistogram()
    #    size = (float(raster_max) - float(raster_min)) / bucket_class_count
    #    bucket_range = [float(raster_min),] + [float(raster_min) + (i+1)*size for i in range(bucket_class_count)]
    ####    if classification_type == NATURAL_BREAKS:
    # Get the total number of histogram values
    total_count = sum(histogram)
    write_log("Total count of cells in ", raster_path, "layer is ", total_count)
    write_log("Histogram ", histogram)
    return total_count


#    if(total_count > 0):
#        block = total_count / float(bucket_class_count)
#        low = float(raster_min)
#        result = [low]
#        for i in range(0, bucket_class_count):
#            # Find the high value by adding each histogram item until we reach the block count.
#            cnt = 0
#            for j in range(len(histogram)):
#                bucket_cnt = histogram[j]
#                cnt += bucket_cnt
#                if cnt > block*(i+1):
#                    upper = bucket_range[j]
#                    if upper > low:
#                        result.append(upper)
#                    low = upper
#                    break
#            # Add max point
#            result.append(bucket_range[-1])
#        return (result, total_count)


######## result_histogram, total_count_histogram = calc_histogram(basedir + '/climate_etr.tif')
######## min_v, max_v, avg_v, std_d = get_band_stats_original('test_error.img')

# Uniform_Raster(test_s_t_cold1_test.img')


def calc_s_t_hot1(nhm_min1, nhm_max1):

    str_nhm_max = str(nhm_max1)
    str_nhm_min = str(nhm_min1)
    str_albedo_h_max = str(albedo_h_max)
    str_albedo_h_min = str(albedo_h_min)
    nhm_img = "nhm.img"
    delete_file_w_path(basedir + "/" + nhm_img)

    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + ndvi_img
        + " -B "
        + basedir
        + "/"
        + surface_albedo_img
        + " --outfile="
        + basedir
        + "/"
        + nhm_img
        + " --calc="
        + '"where(A>'
        + str_nhm_max
        + ",-9999,(where(A<"
        + str_nhm_min
        + ",-9999,(where(B>"
        + str_albedo_h_max
        + ",-9999,(where(B<"
        + str_albedo_h_min
        + ',-9999,1)))))))" '
        + "--type=Int32"
        + " --overwrite --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(nhm_img, gdal_calc)

    # Process: Raster Calculator (5)
    s_t_hot1_img = "s_t_hot1.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + nhm_img
        + " -B "
        + basedir
        + "/"
        + st_dem_c_img
        + " --outfile="
        + basedir
        + "/"
        + s_t_hot1_img
        + " --calc="
        + '"where(A==0, -9999,B)" '
        + " --type=Float64"
        + " --overwrite --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(s_t_hot1_img, gdal_calc)

    # If using LS7 make sure that the s_t_hot1.img is only for areas not filled (ls7_gap_mask_clip.img).  This is done by
    # clipping s_t_hot1.img into s_t_hot1a.img and then renaming it back to s_t_hot1.img for checking on the AOI
    if ls == 7:
        s_t_hot1a_img = "s_t_hot1a.img"
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + basedir
            + "/ls7_gap_mask_clip.img"
            + " -B "
            + basedir
            + "/"
            + s_t_hot1_img
            + " --outfile="
            + basedir
            + "/"
            + s_t_hot1a_img
            + " --calc="
            + '"where(A==0, -9999,B)" '
            + " --type=Float64"
            + " --overwrite --NoDataValue=-9999 --quiet"
        )
        gdal_calc_func(s_t_hot1a_img, gdal_calc)
        delete_file_w_path(basedir + "/" + s_t_hot1_img)
        os.rename(basedir + "/" + s_t_hot1a_img, basedir + "/" + s_t_hot1_img)

    ### Process: Extract by Mask (3)
    s_t_hot2_img = "s_t_hot2.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + aoi_hot_mask
        + " -B "
        + basedir
        + "/"
        + s_t_hot1_img
        + " --outfile="
        + basedir
        + "/"
        + s_t_hot2_img
        + " --calc="
        + '"where(A==1,B,-9999)" '
        + " --type=Float64"
        + " --overwrite --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(s_t_hot2_img, gdal_calc)

    # Process: Raster Calculator (24)
    str_min_v = str(2)
    hot2_mask_img = "hot2_mask.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + s_t_hot2_img
        + " --outfile="
        + basedir
        + "/"
        + hot2_mask_img
        + " --calc="
        + '"where(A<=0,-9999,1)" '
        + "--type=Int32"
        + " --overwrite --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(hot2_mask_img, gdal_calc)

    # Process: Zonal Statistics as Table getting the max value of ground temp in the hot area
    ##WARNING  #T_Hot_dbf = "T_Hot.dbf"

    s_t_hot2_mask_img = "s_t_hot2_mask.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + hot2_mask_img
        + " -B "
        + basedir
        + "/"
        + s_t_hot2_img
        + " --outfile="
        + basedir
        + "/"
        + s_t_hot2_mask_img
        + " --calc="
        + '"(A*B)"'
        + " --type=Float64 --overwrite --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(s_t_hot2_mask_img, gdal_calc)

    T_H_MIN, T_H_MAX, avg_v, std_d = get_band_stats_original(s_t_hot2_mask_img)

    var1 = float(T_H_MAX) - (
        (float(T_H_MAX) - float(T_H_MIN)) * float(hot_max_temp_perc) / 100
    )
    str_var1 = str(var1)
    if DEBUGFLAG > 1:
        write_log(
            "Max Surf_Temp for mask ",
            var1,
            "T_H_MIN ",
            T_H_MIN,
            " T_H_MAX ",
            T_H_MAX,
            " hot_max_temp_perc ",
            hot_max_temp_perc,
        )
        write_log(
            "surf_temp_hot threshold value ",
            (
                float(T_H_MAX)
                - ((float(T_H_MAX) - float(T_H_MIN)) * float(hot_max_temp_perc) / 100)
            ),
        )
    s_t_hot1_img = "s_t_hot1.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + s_t_hot2_mask_img
        + " --outfile="
        + basedir
        + "/"
        + s_t_hot1_img
        + " --calc="
        + '"where(A < '
        + str_var1
        + ',-9999,A)" '
        + "--type=Float64"
        + " --overwrite --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(s_t_hot1_img, gdal_calc)
    return s_t_hot1_img


def Sieve_Raster(layer, min_cells, connected):
    rasterfile = basedir + "/" + layer
    ET1 = gdal.Open(rasterfile, GA_Update)
    ETband = ET1.GetRasterBand(1)
    gdal.SieveFilter(
        srcBand=ETband, maskBand=None, dstBand=ETband, threshold=min_cells, connectedness=connected,
        callback=gdal.TermProgress_nocb)
    ET1 = None

#    str_threshold = str(threshold)
#    str_connectedness = str(connectedness)
#    ds = gdal.Open(basedir + "/" + layer, GA_ReadOnly)
    # get raster band 1
#    band = ds.GetRasterBand(1)
    # https://manpages.debian.org/testing/python-gdal/gdal_sieve.1.en.html
    # gdal_sieve.py [-q] [-st threshold] [-4] [-8] [-o name=value] srcfile [-nomask] [-mask filename] [-of format] [dstfile]
    # Additional details on the algorithm are available in the GDALSieveFilter() docs.
    # -q:               The script runs in quiet mode. The progress monitor is suppressed and routine messages are not displayed.
    # -st threshold:    Set the size threshold in pixels. Only raster polygons smaller than this size will be removed.
    # -o name=value:    Specify a special argument to the algorithm. Currently none are supported.
    # -4:               Four connectedness should be used when determining polygons. That is diagonal pixels are not considered directly connected. This is the default.
    # -8:               Eight connectedness should be used when determining polygons. That is diagonal pixels are considered directly connected.
    # srcfile           The source raster file used to identify target pixels. Only the first band is used.
    # -nomask:          Do not use the default validity mask for the input band (such as nodata, or alpha masks).
    # -mask filename:   Use the first band of the specified file as a validity mask (zero is invalid, non-zero is valid).
    # dstfile           The new file to create with the filtered result. If not provided, the source band is updated in place.
    # -of format:       Select the output format. Starting with GDAL 2.3, if not specified, the format is guessed from the extension (previously was GTiff). Use the short format name.
#    gdal_calc = (
#        "python "
#        + py_script_dir
#        + "/gdal_sieve.py -st "
#        + str_threshold
#        + " "
#        + str_connectedness
#        + " "
#        + basedir
#        + "/"
#        + layer
#    )
#    if DEBUGFLAG == 1:
#        write_log(gdal_calc)
#    os.system(gdal_calc)


# Setting parameters for the model
Cold_min_temp_perc = 10.0
hot_max_temp_perc = 10.0
fp_path = basedir + "/MTL1.txt"
ac = 0.025
# LAG 8-27 CONL = 10.0
if USER_DEF_CONL == 0:
    CONL = 2.0
albedo_max_range = 0.32
albedo_min_range = 0.15
# NDVI_MAX = 0.85
NDVI_MIN = 0.5
# The ncm_ variable should be the same as the NDVI_ variables
ncm_max = 1.0
ncm_min = 0.5
L_SAVI = 0.1
albedo_h_max = 0.3
albedo_h_min = 0.1
# nhm stands for Ndvi Hot Max = nhm
nhm_min = 0.1
nhm_max = 0.2
z1 = 0.1
str_z1 = str(z1)
z2 = 2.0
str_z2 = str(z2)
# Cp is the specific Heat (1004 J/kg/K)
Cp = 1004.0
str_Cp = str(Cp)
# The aerodynamic resistance
aerod_res = log(z2 / z1)
str_aerod_res = str(aerod_res)
# LS 8 Cloud threshold
# https://www.usgs.gov/landsat-missions/landsat-collection-1-level-1-quality-assessment-band?qt-science_support_page_related_con=0#qt-science_support_page_related_con
#LAG 7-12-22 2752 is the medium value for clouds - 2732 is the low confidence value so more conservative
# LS 9 Cloud threshold - NOTE THESE ARE COPIES OF LS8 - ERROR!!!!!!!!!!!!!!!
ls9_cloud_value = 21825
#ls9_cloud_value = 55052
ls9_clear_value = 21824
# LS 8 Cloud threshold
# NO more BQA ls8_cloud_value = 2732
# NO more BQA ls8_clear_value = 2720
ls8_cloud_value = 21825
#ls8_cloud_value = 55052
ls8_clear_value = 21824
# LS 7 Cloud threshold
#LAG 7-18-22 704 is the medium value for clouds -  is the low confidence value so more conservative
#ls5_7_cloud_value = 684
#ls5_7_clear_value = 672
#ls5_7_clear_value1 = 1
ls5_7_cloud_value = 5896
ls5_7_clear_value = 5440
# Minimum number of rasters in s_t_cold1.img to use in order to select the cold points for the run
MIN_COLD_RASTERS = 50
wf1 = list(filter(os.path.isdir, os.listdir(os.getcwd())))
##Selecting landsat type
check_file_exists(
    "/" + ls_image_dir + "/" + landsat_img + "_MTL.txt",
    " ",
    0,
    "MTL file for " + landsat_img + " is missing!!!!!\n",
)
copyFile(ls_basedir + "/" + landsat_img + "_MTL.txt", basedir + "/MTL1.txt")
ls = imagt(fp_path)
write_log("Generating cloud mask", ls8_cloud_value, ls5_7_cloud_value)
if ls == 9:
    cloud_val = str(ls9_cloud_value)
    clear_val = str(ls9_clear_value)
if ls == 8:
    cloud_val = str(ls8_cloud_value)
    clear_val = str(ls8_clear_value)
if ls == 7 or ls == 5:
    cloud_val = str(ls5_7_cloud_value)
    clear_val = str(ls5_7_clear_value)
#if ls == 7:
#    clear_val1 = str(ls5_7_clear_value1)
date_acquire = 0
# Need to buffer OUT from the AOI boundary so that when the buffer for the clouds and the buffer for the cold mask are generated they
# do no buffer in from the edge of the AOI.
if USER_CLOUD_BUFFER == 0:
    shp_buffer(aoi_file, +60.0, "aoi_cloud_plus.shp")
else:
    shp_buffer(aoi_file, +1.0 * cloud_buffer, "aoi_cloud_plus.shp")
if USER_COLD_BUFFER == 0:
    if ls == 8 or ls == 5 or ls == 9:
        shp_buffer(aoi_file, +600.0, "aoi_cloud_cold_plus.shp")
    elif ls == 7:
        copy_shp_file(aoi_file, "aoi_cloud_cold_plus.shp")
else:
    shp_buffer(aoi_file, +1.0 * cold_buffer, "aoi_cloud_cold_plus.shp")
clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells = aoi_extent(
    "aoi_cloud_cold_plus.shp"
)

if abs(clip_x1 - clip_x2) == 0 or abs(clip_y1 - clip_y2) == 0:
    write_log(
        "The area of the AOI file", aoi_file, "is zero. This AOI file is INVALID."
    )
    exit(0)
set_aoi_objectid(aoi)

for g in wf1:
    # if os.path.isdir(os.path.join(g)):
    # arcpy.env.workspace=g
    # LAG1    if g.find("image") != -1 :
    if g.find(ls_image_dir) != -1:
        gdir = os.path.join(cwd, g)
        # LAG ls_basedir=g
        ls_basedir1 = gdir
        for t in os.listdir(ls_basedir1):
            if t.find("MTL.txt") >= 0:
                ls_image_name = t[:-8]
                ff_txt = ls_basedir1 + "/" + t
                ff1_txt = ls_basedir1 + "/" + t[:-4] + ".1txt"
                copyFile(ff_txt, ff1_txt)
                dest = basedir + "/" + "MTL1.txt"
                copyFile(ff1_txt, dest)
                shakes = open(dest, "r")
                for line in shakes:
                    if "DATE_ACQUIRED" in line:
                        date = line.split("=")[-1].strip()
                        date_acquire = 1
                        ls_year = int(date[:4])
                        if DEBUGFLAG > 1:
                            write_log(
                                "LandSat Image Date is ",
                                date,
                                "LandSat Image Year is ",
                                ls_year,
                            )
                        break
                shakes.close()
        # AUTOMATIC CLOUD MASK CODE
        QA_PIXEL_layer = 0
        for t in os.listdir(ls_basedir1):
#LAG - NO C1 anymore remove BQA            if t.endswith("BQA.TIF") or t.endswith("QA_PIXEL.TIF"):
            if t.endswith("QA_PIXEL.TIF"):
                QA_PIXEL_layer = 1
                ff = t
                ff1 = t[:-4] + "1.TIF"
                ffc = t
                ffc1 = t[:-4] + "1.TIF"
                str_len = len(basedir)
                # check that the AOI footprint is inside the Landsat footprint
                check_aoi_ls_footprint(aoi, ls_image_dir + "/" + ff, ls_basedir)
                # Creating the cloud buffer around all the clouds with either 60 mts or cloud_buffer size for it.
                clip_img_wo_align(
                    basedir + "/" + ls_image_dir + "/" + ff,
                    ls_image_dir + "/" + ff1,
                    "aoi_cloud_plus.shp",
                )
                delete_file_w_path(ff1[:-4] + "_ZZ" + ff1[-4:])
                gdal_calc = (
                    "python "
                    + py_script_dir
                    + "/gdal_calc.py -A "
                    + ls_basedir1
                    + "/"
                    + ff1
                    + " --outfile="
                    + basedir
                    + "/aoi_cloud_mask2.img"
                    + " --calc="
                    + '"A" '
                    + "--type=Float64"
                    + " --NoDataValue=-9999 --quiet"
                )
                gdal_calc_func(basedir + "/aoi_cloud_mask2.img", gdal_calc)
                # This identifies all the areas with values LESS THAN cloud_val as not being clouds
                gdal_calc = (
                    "python "
                    + py_script_dir
                    + "/gdal_calc.py -A "
                    + basedir
                    + "/aoi_cloud_mask2.img"
                    + " --outfile="
                    + basedir
                    + "/aoi_cloud_mask1.img"
                    + " --calc="
                    + '"where(A<'
                    + cloud_val
                    + ',1,-9999)" '
                    + "--type=Float64"
                    + " --NoDataValue=-9999 --quiet"
                )
                gdal_calc_func(basedir + "/aoi_cloud_mask1.img", gdal_calc)
                delete_file_w_path(basedir + "/aoi_cloud_mask2.img")
                src_ds = gdal.Open(basedir + "/aoi_cloud_mask1.img")
                if src_ds is None:
                    write_log("Unable to open %s" % src_filename)
                    exit(1)
                proj = osr.SpatialReference()
                proj.ImportFromWkt(src_ds.GetProjection())
                try:
                    srcband = src_ds.GetRasterBand(1)
                except RuntimeError as e:
                    # for example, try GetRasterBand(10)
                    write_log(
                        "Band ( %i ) not found in source file ( %s) "
                        % (1, src_filename)
                    )
                    exit(1)
                #  create output datasource
                dst_layername = basedir + "/aoi_cloud_mask1"
                drv = ogr.GetDriverByName("ESRI Shapefile")
                dst_ds = drv.CreateDataSource(dst_layername + ".shp")
                dst_layer = dst_ds.CreateLayer(
                    dst_layername, proj, geom_type=ogr.wkbMultiPolygon
                )
                fd = ogr.FieldDefn("OBJECTID", ogr.OFTInteger)
                dst_layer.CreateField(fd)
                dst_field = 0
                gdal.Polygonize(srcband, None, dst_layer, dst_field, [], callback=None)
                #               Delete the features that are clouds
                #   Fix for python3
                feat = dst_layer.GetNextFeature()
                while feat is not None:
                    if (
                        feat.GetField("OBJECTID") == 0
                        or feat.GetField("OBJECTID") == -9999
                    ):
                        dst_layer.DeleteFeature(feat.GetFID())
                    feat = dst_layer.GetNextFeature()
                srcband = None
                dst_ds = None
                # Creating the cold buffer around all the clouds with either 600 mts or cold_buffer size for it.
                clip_img_wo_align(
                    basedir + "/" + ls_image_dir + "/" + ffc,
                    ls_image_dir + "/" + ffc1,
                    "aoi_cloud_cold_plus.shp",
                )
                delete_file_w_path(ff1[:-4] + "_ZZ" + ff1[-4:])
                if ls == 7:
                    gdal_calc = (
                        "python "
                        + py_script_dir
                        + "/gdal_calc.py -A "
                        + ls_basedir1
                        + "/"
                        + ffc1
                        + " --outfile="
                        + basedir
                        + "/aoi_cloud_cold_mask1.img"
                        + " --calc="
                        + '"where(A =='
                        + clear_val
#                        + '"where(logical_or(A =='
#                        + clear_val
#                        + ",A =="
#                        + clear_val1
#                        + '),1,-9999)" '
                        + ',1,-9999)" '
                        + "--type=Float64"
                        + " --NoDataValue=-9999 --quiet"
                    )
                else:
                    gdal_calc = (
                        "python "
                        + py_script_dir
                        + "/gdal_calc.py -A "
                        + ls_basedir1
                        + "/"
                        + ffc1
                        + " --outfile="
                        + basedir
                        + "/aoi_cloud_cold_mask1.img"
                        + " --calc="
                        + '"where(A=='
                        + clear_val
                        + ',1,-9999)" '
                        + "--type=Float64"
                        + " --NoDataValue=-9999 --quiet"
                    )
                gdal_calc_func(basedir + "/aoi_cloud_cold_mask1.img", gdal_calc)
                delete_file_w_path(basedir + "/" + ff1)
                src_ds_cold = gdal.Open(basedir + "/aoi_cloud_cold_mask1.img")
                if src_ds_cold is None:
                    write_log("Unable to open %s" % src_filename)
                    exit(1)
                proj_cold = osr.SpatialReference()
                proj_cold.ImportFromWkt(src_ds_cold.GetProjection())
                try:
                    srcband_cold = src_ds_cold.GetRasterBand(1)
                except RuntimeError as e:
                    # for example, try GetRasterBand(10)
                    write_log(
                        "Band ( %i ) not found in source file ( %s) "
                        % (1, src_filename)
                    )
                    exit(1)
                #  create output datasource
                dst_layername_cold = basedir + "/aoi_cloud_cold_mask1"
                drv = ogr.GetDriverByName("ESRI Shapefile")
                dst_ds_cold = drv.CreateDataSource(dst_layername_cold + ".shp")
                dst_layer_cold = dst_ds_cold.CreateLayer(
                    dst_layername_cold, proj_cold, geom_type=ogr.wkbMultiPolygon
                )
                fd_cold = ogr.FieldDefn("OBJECTID", ogr.OFTInteger)
                dst_layer_cold.CreateField(fd)
                dst_field_cold = 0
                gdal.Polygonize(
                    srcband_cold,
                    None,
                    dst_layer_cold,
                    dst_field_cold,
                    [],
                    callback=None,
                )
                #               Delete the features that are clouds
                #   Fix for python3
                feat_cold = dst_layer_cold.GetNextFeature()
                while feat_cold is not None:
                    if (
                        feat_cold.GetField("OBJECTID") == 0
                        or feat_cold.GetField("OBJECTID") == -9999
                    ):
                        dst_layer_cold.DeleteFeature(feat_cold.GetFID())
                    feat_cold = dst_layer_cold.GetNextFeature()
                srcband_cold = None
                dst_ds_cold = None
        if QA_PIXEL_layer == 0:
            write_log(
                "QA_PIXEL layer for landsat image ",
                landsat_img,
                " was not detected.\n Check directory ",
                basedir + "/" + ls_image_dir,
            )
            exit(1)
#        import pdb;pdb.set_trace()
        if USER_CLOUD_BUFFER == 0:
            shp_buffer(dst_layername + ".shp", -60.0, "aoi_cloud_mask.shp")
        else:
            shp_buffer(
                dst_layername + ".shp", -1.0 * cloud_buffer, "aoi_cloud_mask.shp"
            )
        if USER_COLD_BUFFER == 0:
            if ls == 8 or ls == 5 or ls == 9:
                shp_buffer(dst_layername_cold + ".shp", -600.0, "aoi_cloud_cold_mask.shp")
            elif ls == 7:
                shp_buffer(dst_layername_cold + ".shp", -100.0, "aoi_cloud_cold_mask.shp")
        # LAG 4-1-20                copy_shp_file('aoi_cloud_cold_mask1.shp', 'aoi_cloud_cold_mask.shp')
        else:
            shp_buffer(
                dst_layername + ".shp", -1.0 * cold_buffer, "aoi_cloud_cold_mask.shp"
            )
        # Redefine the aoi as the buffered (outward by 60 meters) of the cloud mask
        aoi_file = "aoi_cloud_mask.shp"
        area_shp = basedir + "/" + aoi_file
        aoi = area_shp
        # Redefine the aoi_cold as the buffered (outward by 600 meters) of the cloud mask
        aoi_cloud_cold_file = "aoi_cloud_cold_mask.shp"
        area_cold_shp = basedir + "/" + aoi_cloud_cold_file
copyFile(ff1_txt, ff_txt)
# delete_file_w_path(ff1_txt)

if seasonal:
    delete_files_w_pattern(basedir, ls_image_name +"*.*")

if date_acquire == 0:
    write_log(
        "Landsat Image Acquisition Date not determined.  \nCheck that the ",
        landsat_img,
        "_MTL.txt file exists in the project image directory.",
    )
    exit(1)
# LAG 8-12 clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells = aoi_extent(aoi)
# LAG 8-12 set_aoi_objectid(aoi)
perc_cloud_area = Totally_cloudy("aoi_cloud_mask1.img")
#write_log("The area of the aoi_cloud_mask1.img that has data is ", perc_cloud_area)
if(perc_cloud_area > 90.0):
    write_log(
        "The percent cloud cover is ",perc_cloud_area," which means this image is not usable."
        )
    exit(5)

clip_img_wo_align(basedir + "/aoi_cloud_mask1.img", "aoi_cloud_mask.img", aoi_file)

# Check if the user defined a maximum cloud cover for the image and check if it meets this criteria
aoi_shp_area = shape_lyr_area(basedir + "/" + aoi_file1)
aoi_shp_area_pixels = aoi_shp_area / (cellSize * cellSize)
rast_area_pixels = raster_lyr_pixels(basedir + "/aoi_cloud_mask.img")
perc_clouds = 100.0 - ((rast_area_pixels / aoi_shp_area_pixels) * 100.0)
if USER_CLOUD_PERCENT == 1 and max_cloud_percent < perc_clouds:
    write_log(
        "The percent cloud cover is % 5.2f which exceeds the user defined maximum of % 5.2f"
        % (perc_clouds, max_cloud_percent)
    )
    exit(5)
elif max_cloud_percent < perc_clouds:
    write_log(
        "The percent cloud cover is % 5.2f which exceeds the default maximum of % 5.2f"
        % (perc_clouds, max_cloud_percent)
    )
    write_log(
        "If you want to increase the maximum allowable percent cloud cover use the USER_MAX_CLOUD_PERC token to increase it from the default maximum of 50%"
    )
    exit(5)
# If user does not define the aoi_cold then set it to area_shp
if USER_COLD_AOI_FILE == 0:
    # LAG1    aoi_cold = aoi_file
    copy_shp_file(aoi_cloud_cold_file, "aoi_cold.shp")
    aoi_cold = "aoi_cold.shp"
# If user does not define the aoi_hot then set it to area_shp
if USER_HOT_AOI_FILE == 0:
    # LAG1    aoi_hot = aoi_file
    copy_shp_file(aoi_file, "aoi_hot.shp")
    aoi_hot = "aoi_hot.shp"
# If LandSat7 then generate a master gap mask to create a consistent gap in all the input bands
if ls == 7:
    ls7_gap_file = ls7_gap_mask(basedir, ls_basedir)
    # Change directory to the workspace
    os.chdir(workspace)
    cwd = os.getcwd()
write_log("Clipping the Landsat image to the AOI ~5% of run completed.")
for g in wf1:
    # if os.path.isdir(os.path.join(g)):
    # arcpy.env.workspace=g
    if g.find(ls_image_dir) != -1:
        gdir = os.path.join(cwd, g)
        ls_basedir1 = gdir
        if DEBUGFLAG > 1:
            write_log(
                "gdir ",
                gdir,
                " ls_basedir1 ",
                ls_basedir1,
                "ls_image_dir",
                ls_image_dir,
                "landsat_img",
                landsat_img,
            )
        #        landsat_img = g
        # AUTOMATIC CLOUD MASK CODE
        # CHANGED THE LOOP TO ONLY ADD CLIP TO A BAND FILES.
        for i in range(1, 8):
            if ls == 9:
                # LAG 5-19-20                outf = ["2", "3", "4", "5", "6", "10", "7"]
                outf = ["1", "2", "3", "4", "5", "6", "7"]
                inputfile = landsat_img + "_B" + str(i) + ".TIF"
                try:
                    os.remove(
                        ls_basedir1 + "/" + landsat_img + "_B" + str(i) + "_CLIP.TIF"
                    )
                except OSError:
                    pass
            elif ls == 8:
                # LAG 5-19-20                outf = ["2", "3", "4", "5", "6", "10", "7"]
                outf = ["1", "2", "3", "4", "5", "6", "7"]
                inputfile = landsat_img + "_B" + str(i) + ".TIF"
                try:
                    os.remove(
                        ls_basedir1 + "/" + landsat_img + "_B" + str(i) + "_CLIP.TIF"
                    )
                except OSError:
                    pass
            elif ls == 7:
                outf = ["1", "2", "3", "4", "5", "6_VCID_1", "7"]
                if i == 6:
                    inputfile = landsat_img + "_B6_VCID_1.TIF"
                    try:
                        os.remove(
                            ls_basedir1 + "/" + landsat_img + "_B6_VCID_1_CLIP.TIF"
                        )
                    except OSError:
                        pass
                else:
                    inputfile = landsat_img + "_B" + str(i) + ".TIF"
                    try:
                        os.remove(
                            ls_basedir1
                            + "/"
                            + landsat_img
                            + "_B"
                            + str(i)
                            + "_CLIP.TIF"
                        )
                    except OSError:
                        pass
            elif ls == 5:
                outf = ["1", "2", "3", "4", "5", "6", "7"]
                inputfile = landsat_img + "_B" + str(i) + ".TIF"
                try:
                    os.remove(
                        ls_basedir1 + "/" + landsat_img + "_B" + str(i) + "_CLIP.TIF"
                    )
                except OSError:
                    pass

            outputfile = inputfile[:-4] + "_CLIP.TIF"
            str_len = len(basedir)
            # Name of clip raster file(s)
            #            delete_file(ls_image_dir + '/' + landsat_img + "_B" + str(i) + "_CLIP.TIF")
            check_file_exists(
                "/" + ls_image_dir + "/" + inputfile,
                "raster",
                utm_zone,
                "Landsat image ",
            )
            clip_img_wo_align(
                basedir + "/" + ls_image_dir + "/" + inputfile, ls_image_dir + "/" + outputfile, aoi
            )
#            if ls == 7:
                # IN THIS CODE WE ARE TRYING TO SET THE NO DATA TO -9999 FOR COLLECTION 2 OF LS7 WHICH HAS THE NODATA SET TO 0
#                ET1 = gdal.Open(ls_image_dir + "/" + outputfile, GA_Update)
#                band = ET1.GetRasterBand(1)
#                band.SetNoDataValue(-9999)
#                ET1 = None

#            import pdb;pdb.set_trace()
            if LS7_DISABLE_FILL_GAP == 0 and ls == 7:
                ls7_fill_scan_gap(outputfile)
                if DEBUGFLAG > 1:
                    write_log("LS7_FILL_GAP Filling GAP for file ", outputfile)
            delete_file_w_path(
                ls_basedir1 + "/" + outputfile[:-4] + "_ZZ" + outputfile[-4:]
            )
        # for ls8 clip band 10 since it is not used in the composite but is needed later
        if ls == 8:
            inputfile = landsat_img + "_B" + str(10) + ".TIF"
            outputfile = inputfile[:-4] + "_CLIP.TIF"
            check_file_exists(
                "/" + ls_image_dir + "/" + inputfile,
                "raster",
                utm_zone,
                "Landsat image ",
            )
            clip_img_wo_align(
                basedir + "/" + ls_image_dir + "/" + inputfile, ls_image_dir + "/" + outputfile, aoi
            )

        # for ls8 clip band 10 since it is not used in the composite but is needed later
        if ls == 9:
            inputfile = landsat_img + "_B" + str(10) + ".TIF"
            outputfile = inputfile[:-4] + "_CLIP.TIF"
            check_file_exists(
                "/" + ls_image_dir + "/" + inputfile,
                "raster",
                utm_zone,
                "Landsat image ",
            )
            clip_img_wo_align(
                basedir + "/" + ls_image_dir + "/" + inputfile, ls_image_dir + "/" + outputfile, aoi
            )

        file_list = glob.glob(os.path.join(gdir, "*_CLIP.TIF"))
        new_list = list()
        for x in outf:
            new_list.append(gdir + "/" + landsat_img + "_B" + x + "_CLIP.TIF")
        with rasterio.open(file_list[0]) as src0:
            meta = src0.meta
        # Update meta to reflect the number of layers
        meta.update(count=len(new_list))
        src0 = None
        out_raster1 = basedir + "/" + ls_image_name + "_Composite1.TIF"
        out_raster = basedir + "/" + ls_image_name + "_Composite.TIF"
        # Read each layer and write it to stack
        ls_band_layer = []
        if ls == 9:
            ii = 1
            while ii < 12:
                ls_band_layer.append(ls_image_name + "_B" + str(ii) + ".TIF")
                ii += 1
        elif ls == 8:
            ii = 1
            while ii < 12:
                ls_band_layer.append(ls_image_name + "_B" + str(ii) + ".TIF")
                ii += 1
        elif ls == 7:
            ii = 1
            while ii < 9:
                if ii == 6:
                    ls_band_layer.append(ls_image_name + "_B" + str(ii) + "_VCID_1.TIF")
                else:
                    ls_band_layer.append(ls_image_name + "_B" + str(ii) + ".TIF")
                ii += 1
        elif ls == 5:
            ii = 1
            while ii < 8:
                ls_band_layer.append(ls_image_name + "_B" + str(ii) + ".TIF")
                ii += 1
        # Read metadata of first file
        if DEBUGFLAG > 1:
            write_log(
                basedir + "/" + ls_image_dir + "/" + ls_image_name + "_B1.TIF",
                "  ",
                ls_band_layer[1],
            )
        with rasterio.open(
            basedir + "/" + ls_image_dir + "/" + ls_image_name + "_B1.TIF"
        ) as src0:
            meta = src0.meta
        # Update meta to reflect the number of layers
        meta.update(count=len(ls_band_layer))
        with rasterio.open(out_raster1, "w", **meta) as dst:
            for id, layer in enumerate(ls_band_layer, start=1):
                if id != 8:
                    with rasterio.open(
                        basedir + "/" + ls_image_dir + "/" + layer
                    ) as src1:
                        dst.write_band(id, src1.read(1))
        srcl = None
        clip_img_wo_align(
            basedir + "/" + ls_image_name + "_Composite1.TIF", ls_image_name + "_Composite.TIF", aoi
        )
        delete_file_w_path(out_raster1)
        lsimage = out_raster
copyFile(ff1_txt, ff_txt)
delete_file_w_path(ff1_txt)
# If procesing Landsat 9 images
if ls == 9:
    UL_x, UL_y, LL_x, LL_y, UR_x, UR_y, LR_x, LR_y = Read_Raster_Extent(
        ls_basedir + "/" + landsat_img + "_B10_CLIP.TIF"
    )
    aoi_file2 = clip_aoi_to_ls_extent(aoi_file, "_B10.TIF", wf1)
    set_aoi_objectid(basedir + "/" + aoi_file2)
    aoi_file3 = clip_aoi_to_ls_extent(aoi_file2, "_B2.TIF", wf1)
    aoi_file = aoi_file3
# If procesing Landsat 8 images
if ls == 8:
    UL_x, UL_y, LL_x, LL_y, UR_x, UR_y, LR_x, LR_y = Read_Raster_Extent(
        ls_basedir + "/" + landsat_img + "_B10_CLIP.TIF"
    )
    aoi_file2 = clip_aoi_to_ls_extent(aoi_file, "_B10.TIF", wf1)
    set_aoi_objectid(basedir + "/" + aoi_file2)
    aoi_file3 = clip_aoi_to_ls_extent(aoi_file2, "_B2.TIF", wf1)
    aoi_file = aoi_file3
# If procesing Landsat 7 images
elif ls == 7:
    UL_x, UL_y, LL_x, LL_y, UR_x, UR_y, LR_x, LR_y = Read_Raster_Extent(
        ls_basedir + "/" + landsat_img + "_B6_VCID_1_CLIP.TIF"
    )
    aoi_file2 = clip_aoi_to_ls_extent(aoi_file, "_B6_VCID_1.TIF", wf1)
    set_aoi_objectid(basedir + "/" + aoi_file2)
    aoi_file3 = clip_aoi_to_ls_extent(aoi_file2, "_B2.TIF", wf1)
    aoi_file = aoi_file3
# If procesing Landsat 5 images
elif ls == 5:
    UL_x, UL_y, LL_x, LL_y, UR_x, UR_y, LR_x, LR_y = Read_Raster_Extent(
        ls_basedir + "/" + landsat_img + "_B6_CLIP.TIF"
    )
    aoi_file2 = clip_aoi_to_ls_extent(aoi_file, "_B6.TIF", wf1)
    set_aoi_objectid(basedir + "/" + aoi_file2)
    aoi_file3 = clip_aoi_to_ls_extent(aoi_file2, "_B2.TIF", wf1)
    aoi_file = aoi_file3

area_shp = basedir + "/" + aoi_file
aoi = area_shp
set_aoi_objectid(aoi)
aoi_cold_mask1 = "aoi_cold_mask1.img"
aoi_cold_mask = "aoi_cold_mask.img"
delete_file_w_path(basedir + "/" + aoi_cold_mask1)
gdal_grid_text = str(
    "gdal_rasterize -a OBJECTID -te {0} {1} {2} {3} -tr {4} {4} -a_srs EPSG:326{5} -l {6} {7} {8}".format(
        clip_x1,
        clip_y2,
        clip_x2,
        clip_y1,
        cellSize,
        utm_zone,
        aoi_cloud_cold_file[:-4],
        basedir + "/" + aoi_cloud_cold_file,
        basedir + "/aoi_cold_mask1.img",
    )
)
if DEBUGFLAG == 1:
    write_log("gdal_grid_text ", gdal_grid_text)
os.system(gdal_grid_text)
clip_img(basedir + "/" + aoi_cold_mask1, aoi_cold_mask, basedir + "/" + aoi_cold)
#delete_file_w_path(basedir + "/" + aoi_cold_mask1)
# !!!!!!! NOTE - check that aoi.shp has a 1 as the attribute for the inside of the shape !!!!!!!!!!
aoi_hot_mask1 = "aoi_hot_mask1.img"
aoi_hot_mask = "aoi_hot_mask.img"
delete_file_w_path(basedir + "/" + aoi_hot_mask1)
gdal_grid_text = str(
    "gdal_rasterize -a OBJECTID -te {0} {1} {2} {3} -tr {4} {4} -a_srs EPSG:326{5} -l {6} {7} {8}".format(
        clip_x1,
        clip_y2,
        clip_x2,
        clip_y1,
        cellSize,
        utm_zone,
        aoi_file[:-4],
        basedir + "/" + aoi_file,
        basedir + "/aoi_hot_mask1.img",
    )
)
#We purposely set the aoi_hot_mask1.img to the aoi_cold_mask so that we would exclude the scan line error areas
#for selecting hot points as well as any cloud areas.
aoi_hot_mask1 = "aoi_cold_mask1.img"
if DEBUGFLAG == 1:
    write_log("gdal_grid_text ", gdal_grid_text)
os.system(gdal_grid_text)
clip_img(basedir + "/" + aoi_hot_mask1, aoi_hot_mask, basedir + "/" + aoi_hot)
delete_file_w_path(basedir + "/" + aoi_hot_mask1)
if USER_UNCALIB == 0:
    # tinst_ws_img2="climate_etr_hourly2.TIF"
    etinst_ws_img = "climate_etr_hourly1.TIF"
    # Name of clip raster file(s)
    clip_img(basedir + "/" + etinst_ws_img1, etinst_ws_img, aoi)
    align_rast(etinst_ws_img)
    CheckMinValue(etinst_ws_img)
    # arcpy.gp.ExtractByMask_sa(etinst_ws_img1, aoi, etinst_ws_img)
    # LAG1 et24_ws_img1 = "climate_etr.TIF"
    # t24_ws_img2 = "climate_etr1.TIF"
    et24_ws_img = "climate_etr2.TIF"
    et24_ws_exists = os.path.isfile(basedir + "/" + et24_ws_img1)
    if et24_ws_exists == 0:
        write_log(
            "Weather station ET shapefile ",
            basedir + "/" + et24_ws_img1,
            " does not exist !!!!",
        )
        exit(1)
    # Name of clip raster file(s)
    clip_img(basedir + "/" + et24_ws_img1, et24_ws_img, aoi)
    align_rast(et24_ws_img)
    CheckMinValue(et24_ws_img)
# arcpy.gp.ExtractByMask_sa(et24_ws_img1, aoi, et24_ws_img)
dem_img1 = "DEM_1X.TIF"
if USER_DEM == 0:
    dem_img = "DEM_1.TIF"
    extract_dem(aoi_file_orig, dem_img)
dem_cold_img = "DEM_cold.TIF"
mean_elev = "mean_elev.TIF"
mean_elev1 = "mean_elev1.TIF"
# 6-25-20 dem_exists = os.path.isfile(basedir + '/' + dem_img1)
dem_exists = os.path.isfile(basedir + "/" + dem_img)
if dem_exists == 0:
    write_log("DEM shapefile ", basedir + "/" + dem_img, " does not exist !!!!")
    exit(1)
# Clip the GFSAD raster file(s)
#import pdb;pdb.set_trace()
extract_gfsad(basedir + "/" + aoi_file_orig)
#clip_img(GFSAD_file_orig, GFSAD_file, aoi)
# Name of clip raster file(s)
clip_img(basedir + "/" + dem_img, dem_img1, aoi)
delete_file(dem_img)
copyFile(dem_img1, dem_img)
# Name of clip raster file(s) to determine the average elevation
clip_img(basedir + "/" + dem_img, dem_cold_img, basedir + "/" + aoi_cold)
min_v, max_v, avg_v, std_d = get_band_stats_original(dem_cold_img)
delete_file_w_path(basedir + "/DEM_cold.TIF")
delete_file_w_path(basedir + "/DEM_cold_orig.TIF")
str_mean_elev = str(avg_v)
if DEBUGFLAG > 1:
    write_log("DEM values are MIN, MAX AVG, STD", min_v, max_v, avg_v, std_d)
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + dem_img
    + " --outfile="
    + basedir
    + "/"
    + "mean_elev1.TIF"
    + " --calc="
    + '"(where(A>0,'
    + str_mean_elev
    + ","
    + str_mean_elev
    + '))"'
    + " --type=Float64"
    + " --overwrite --NoDataValue=-9999 --quiet"
)
gdal_calc_func(basedir + "/mean_elev1.TIF", gdal_calc)
clip_img(basedir + "/mean_elev1.TIF", mean_elev, aoi)
delete_file_w_path(basedir + "/" + "mean_elev1.TIF")
# if the user selects to use a flat dem (DEM_FLAT == 1) then use the mean_elev.TIF coverage as the dem
if DEM_FLAT == 1:
    delete_file_w_path(basedir + "/" + dem_img)
    copyFile(mean_elev, dem_img)
# arcpy.gp.ExtractByMask_sa(dem_img1, aoi, dem_img)
# LAG1 wind_img1 = "climate_windrun_mile_day.TIF"
wind_img2 = "climate_windrun_mile_day1.TIF"
wind_img = "climate_windrun_mile_day2.TIF"
windrun_exists = os.path.isfile(basedir + "/" + wind_img1)
if windrun_exists == 0:
    write_log("Wind run shapefile ", basedir + "/" + wind_img1, " does not exist !!!!")
    exit(1)
# Name of clip raster file(s)
clip_img(basedir + "/" + wind_img1, wind_img, aoi)
align_rast(wind_img)
CheckMinValue(wind_img)
# add full path to wind_img
fp_wind_img = basedir + "/" + wind_img
u200_img = "u200.TIF"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + fp_wind_img
    + " --outfile="
    + basedir
    + "/u200.TIF "
    + "--calc="
    + '"((((((A* 1609.344)/(24.*60.*60.)) * 0.41)/(3.506557897))/0.41)* 8.111728083 )" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(basedir + "/u200.TIF", gdal_calc)
if ls == 7:  ##Landsat 7
    c_t1 = 1282.71
    c_t2 = 66.609
    alb = [0.293, 0.274, 0.231, 0.156, 0.034, 0, 0.012]
    esun = [1997, 1812, 1533, 1039, 230.8, 0, 84.9]
if ls == 5:  ##Landsat 5
    c_t1 = 1260.56
    c_t2 = 60.7
    alb = [0.293, 0.274, 0.233, 0.157, 0.033, 0, 0.011]
    esun = [1983, 1796, 1536, 1031, 220, 0, 83.44]
if ls == 8:  ##Landsat 8
    c_t1 = 1321.08
    c_t2 = 774.89
    alb = [0.293, 0.274, 0.233, 0.157, 0.033, 0, 0.011]
    esun = [1983, 1796, 1536, 1031, 220, 0, 83.44]
if ls == 9:  ##Landsat 9
    c_t1 = 1329.2405
    c_t2 = 799.0284
    alb = [0.293, 0.274, 0.233, 0.157, 0.033, 0, 0.011]
    esun = [1983, 1796, 1536, 1031, 220, 0, 83.44]
if ls == 10:  ## MODIS
    c_t1 = 1282.71
    c_t2 = 66.609
    alb = [0.215, 0.215, 0.242, 0.129, 0.101, 0.062, 0.036]
    esun = [1983, 1796, 1536, 1031, 220, 0, 83.44]

dt_str = read_acq_date(fp_path)

latlon = readFile(fp_path)
lat = latlon[0]
lon = abs(float(latlon[1]))
region = calculateRegion(lat[1])
dt = calcDate("true", region, dt_str)

if ls == 7:
    lmin_band = readFile_ls7(fp_path)[0]
    lmax_band = readFile_ls7(fp_path)[1]
elif ls == 8:
    lmin_band = readFile_ls8(fp_path)[0]
    lmax_band = readFile_ls8(fp_path)[1]
elif ls == 9:
    lmin_band = readFile_ls9(fp_path)[0]
    lmax_band = readFile_ls9(fp_path)[1]
elif ls == 5:
    lmin_band = readFile_ls5(fp_path)[0]
    lmax_band = readFile_ls5(fp_path)[1]

s1 = float(calcS(dt_str)[0])
s2 = float(calcS(dt_str)[1])
s3 = float(calcS(dt_str)[2])
str_s3 = str(s3)
s4 = float(calcS(dt_str)[3])
str_s4 = str(s4)

## Calculating Coef's input lat lon Equations date time region
write_log(
    "Calculating the coefficients based on the lat, long and date ~20% of run completed."
)
FAI = (math.pi / 180) * float(lat)
if DEBUGFLAG > 1:
    write_log("FAI", FAI)

yearfr = getYearFrac(dt_str)
if DEBUGFLAG > 1:
    write_log("yearfr", yearfr)
sigma = 0.409 * math.sin((2 * math.pi * yearfr) - 1.39)
if DEBUGFLAG > 1:
    write_log("sigma", sigma)

Acqu_time = calc(dt)
if DEBUGFLAG > 1:
    write_log("Acquisition Time ", Acqu_time)

##Omega_sunset
omega_sunset = math.acos(-math.tan(FAI) * math.tan(sigma))
if DEBUGFLAG > 1:
    write_log("omega_sunset", omega_sunset)

DOY = getDOY(dt_str)
if DEBUGFLAG > 1:
    write_log("DOY", DOY)
B = 2 * math.pi * (DOY - 81) / 364.0
if DEBUGFLAG > 1:
    write_log("B", B)
Sc = (
    float(0.1645 * math.sin(2 * B))
    - float(0.1255 * math.cos(B))
    - float(0.025 * math.sin(B))
)
if DEBUGFLAG > 1:
    write_log("Sc", Sc)
omegaup_sunrise = (omega_sunset) * (-1.0)
if DEBUGFLAG > 1:
    write_log("omegaup_sunrise", omegaup_sunrise)
omega_midpoint = float(math.pi / 12) * float(
    (Acqu_time + 0.06667 * (float(region) - float(lon)) + Sc) - 12
)
if DEBUGFLAG > 1:
    write_log("omega_midpoint", omega_midpoint)
omega2up = omegaup_sunrise + math.pi * 0.16666 / 24.0
if DEBUGFLAG > 1:
    write_log("omega2up", omega2up)
omega_1set = omega_sunset - math.pi * 0.166666 / 24.0
if DEBUGFLAG > 1:
    write_log("omega_1set", omega_1set)
# Remove output shapefile if it already exists
path_dem_extent = workspace + "/dem_extent.shp"
try:
    os.remove(path_dem_extent)
except OSError:
    pass
# LAG Generate a polygon with the extent of the DEM tif
commnd = "gdaltindex " + path_dem_extent + " " + basedir + "/" + dem_img
os.system(commnd)
dem_extent = workspace + "/dem_extent.shp"
dem_asp1 = "dem_asp1.img"
commnd = (
    "gdaldem aspect "
    + basedir
    + "/"
    + dem_img
    + " "
    + basedir
    + "/"
    + dem_asp1
    + " -zero_for_flat"
)
os.system(commnd)

dem_asp = "dem_asp.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + dem_asp1
    + " --outfile="
    + basedir
    + "/"
    + dem_asp
    + " --calc="
    + '"where(A==0,-1.0,A)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(dem_asp, gdal_calc)
delete_file_w_path(basedir + "/" + dem_asp1)

aspect = "aspect.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + dem_asp
    + " --outfile="
    + basedir
    + "/aspect.img "
    + "--calc="
    + '"((A - 180.0)*0.01745329251)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(aspect, gdal_calc)

dem_slp1 = "dem_slp1.img"
dem_slp = "dem_slp.img"
delete_file_w_path(basedir + "/" + dem_slp)
commnd = (
    "gdaldem slope " + basedir + "/" + dem_img + " " + basedir + "/" + dem_slp1 + " -p"
)
os.system(commnd)
clip_img(basedir + "/" + dem_slp1, dem_slp, aoi)
align_rast(dem_slp)
delete_file_w_path(basedir + "/" + dem_slp1)
delete_file_w_path(basedir + "/dem_slp_orig.img")

slope = "slope.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + dem_slp
    + " --outfile="
    + basedir
    + "/slope.img "
    + "--calc="
    + '"(A*0.01745329251 )" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(slope, gdal_calc)

sin_slope_img = "sin_slope.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + slope
    + " --outfile="
    + basedir
    + "/sin_slope.img "
    + "--calc="
    + '"(sin(A))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(sin_slope_img, gdal_calc)

cos_slope_img = "cos_slope.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + slope
    + " --outfile="
    + basedir
    + "/cos_slope.img "
    + "--calc="
    + '"(cos(A))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(cos_slope_img, gdal_calc)

sin_aspect_img = "sin_aspect.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + aspect
    + " --outfile="
    + basedir
    + "/sin_aspect.img "
    + "--calc="
    + '"(sin(A))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(sin_aspect_img, gdal_calc)

cos_aspect_img = "cos_aspect.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + aspect
    + " --outfile="
    + basedir
    + "/cos_aspect.img "
    + "--calc="
    + '"(cos(A))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(cos_aspect_img, gdal_calc)

# Process: R1_SIN_SIGMA_times_SIN_FAI
str1 = str(math.sin(sigma) * math.sin(FAI))
# Process: R2_SIN_SIGMA_times_COS_FAI
str2 = str(math.sin(sigma) * math.cos(FAI))
# Process: R3_COS_SIGMA_times_COS_FAI
str3 = str(math.cos(sigma) * math.cos(FAI))
# Process: R4_COS_SIGMA_SIN_FAI
str4 = str(math.cos(sigma) * math.sin(FAI))
# Process: R5_COS_SIGMA
str5 = str(math.cos(sigma))
# Process: R6_RISE_COS
str6 = str(math.cos(omega2up))
# Process: R7_RISE_SIN
str7 = str(math.sin(omega2up))
# Process: R8_SET_COS
str8 = str(math.cos(omega_1set))
# Process: R9_SET_SIN
str9 = str(math.sin(omega_1set))
# Process: R10_OMEGA_UP
str10 = str(omegaup_sunrise)
# Process: R11_OMEGA_SET
str11 = str(omega_sunset)
# Process: R12_GSC_21
str12 = str(
    (12 * 60 / math.pi) * 0.94932 * (1 + 0.033 * math.cos(DOY * 2 * math.pi / 365))
)
# Process: R13_COS_OMEGA
str13 = str(math.cos(omega_midpoint))
# Process: R14_SIN_OMEGA
str14 = str(math.sin(omega_midpoint))
if DEBUGFLAG > 1:
    write_log(
        "R1 through R14 ",
        str1,
        str2,
        str3,
        str4,
        str5,
        str6,
        str7,
        str8,
        str9,
        str10,
        str11,
        str12,
        str13,
        str14,
    )

# Process: C1_SIN_SIGMA_X_SIN_FAI
stc1 = str(math.sin(sigma) * math.sin(FAI))
# Process: C2_SIN_SIGMA_COS_FAI
stc2 = str(math.sin(sigma) * math.cos(FAI))
# Process: C3_COS_SIGMA_COS_FAI
stc3 = str(math.cos(sigma) * math.cos(FAI))
# Process: C4_COS_OMEGA
stc4 = str(math.cos(omega_midpoint))
# Process: C5_COS_SIGMA_SIN_FAI
stc5 = str(math.cos(sigma) * math.sin(FAI))
# Process: C6_COS_OMEGA
stc6 = str(math.cos(omega_midpoint))
# Process: C7_COS_SIGMA
stc7 = str(math.cos(sigma))
# Process: C8_SIN_OMEGA
stc8 = str(math.sin(omega_midpoint))
if DEBUGFLAG > 1:
    write_log("c1 through c8 ", stc1, stc2, stc3, stc4, stc5, stc6, stc7, stc8)

n16_temp_img = "n16_temp.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + cos_slope_img
    + " --outfile="
    + basedir
    + "/n16_temp.img "
    + "--calc="
    + '"(A * '
    + str1
    + ')" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n16_temp_img, gdal_calc)

# Process: Raster Calculator (4)
n15_temp_img = "n15_temp.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + sin_slope_img
    + " -B "
    + basedir
    + "/"
    + cos_aspect_img
    + " --outfile="
    + basedir
    + "/n15_temp.img "
    + "--calc="
    + '"(A * B * '
    + str2
    + ')" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n15_temp_img, gdal_calc)

# Process: Raster Calculator (5)
# n14_temp_img = cos_slope_img *R3
n14_temp_img = "n14_temp.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + cos_slope_img
    + " --outfile="
    + basedir
    + "/n14_temp.img "
    + "--calc="
    + '"(A * '
    + str3
    + ')" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n14_temp_img, gdal_calc)

# Process: Raster Calculator (6)
# n13_temp_img= sin_slope_img * cos_aspect_img * R4
n13_temp_img = "n13_temp.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + sin_slope_img
    + " -B "
    + basedir
    + "/"
    + cos_aspect_img
    + " --outfile="
    + basedir
    + "/n13_temp.img "
    + "--calc="
    + '"(A * B * '
    + str4
    + ')" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n13_temp_img, gdal_calc)

# Process: Raster Calculator (7)
# n12_temp_img= sin_slope_img * sin_aspect_img * R5
n12_temp_img = "n12_temp.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + sin_slope_img
    + " -B "
    + basedir
    + "/"
    + sin_aspect_img
    + " --outfile="
    + basedir
    + "/n12_temp.img"
    + " --calc="
    + '"(A * B * '
    + str5
    + ')" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n12_temp_img, gdal_calc)

# Process: Raster Calculator (10)
# n36_memory_img = n16_temp_img - n15_temp_img + n14_temp_img *R8+ n13_temp_img * R8 + n12_temp_img *R9
n36_mem_img = "n36_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n16_temp_img
    + " -B "
    + basedir
    + "/"
    + n15_temp_img
    + " -C "
    + basedir
    + "/"
    + n14_temp_img
    + " -D "
    + basedir
    + "/"
    + n13_temp_img
    + " -E "
    + basedir
    + "/"
    + n12_temp_img
    + " --outfile="
    + basedir
    + "/n36_mem.img"
    + " --calc="
    + '"(A - B + C * '
    + str8
    + " + D * "
    + str8
    + " + E * "
    + str9
    + ')" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n36_mem_img, gdal_calc)

# Process: Raster Calculator (8)
# n19_temp_img = n15_temp_img - n16_temp_img\
n19_temp_img = "n19_temp.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n15_temp_img
    + " -B "
    + basedir
    + "/"
    + n16_temp_img
    + " --outfile="
    + basedir
    + "/n19_temp.img"
    + " --calc="
    + '"(A - B)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n19_temp_img, gdal_calc)

# Process: Raster Calculator (9)
# n20_temp_img = n13_temp_img + n14_temp_img
n20_temp_img = "n20_temp.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n13_temp_img
    + " -B "
    + basedir
    + "/"
    + n14_temp_img
    + " --outfile="
    + basedir
    + "/n20_temp.img"
    + " --calc="
    + '"(A + B)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n20_temp_img, gdal_calc)

# Process: Raster Calculator (13)
# n22_memory_img=  (((2 * n19_temp_img*n12_temp_img/ ((n20_temp_img)** 2)) ** 2) - 4 * (1 + ((n12_temp_img)** 2) / ((n20_temp_img) ** 2)) * (((n19_temp_img)** 2) / ((n20_temp_img)** 2) - 1) )
n22_mem_img = "n22_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n19_temp_img
    + " -B "
    + basedir
    + "/"
    + n12_temp_img
    + " -C "
    + basedir
    + "/"
    + n20_temp_img
    + " --outfile="
    + basedir
    + "/n22_mem.img"
    + " --calc="
    + '"((pow((2.0 * A * B / (pow(C,2.0))),2.0)) - 4.0 * (1.0 + (pow(B,2.0)) / (pow(C,2.0))) * (pow(A,2.0) / (pow(C,2.0)) - 1.0) )" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n22_mem_img, gdal_calc)

# Process: Raster Calculator (14)
# n24_temp_img=  arcpy.sa.Con( n22_memory_img > 0.0001 , n22_memory_img, 0.0001 )
n24_temp_img = "n24_temp.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n22_mem_img
    + " --outfile="
    + basedir
    + "/n24_temp.img"
    + " --calc="
    + '"where(A>0.0001,A,0.0001)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n24_temp_img, gdal_calc)

# Process: Raster Calculator (15)
# n27_memory_img=(2 *n19_temp_img * n12_temp_img / ( (n20_temp_img)** 2) + ((n24_temp_img)**0.5))/(2*(1+( (n12_temp_img)**2)/((n20_temp_img)**2)))
n27_mem_img = "n27_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n19_temp_img
    + " -B "
    + basedir
    + "/"
    + n12_temp_img
    + " -C "
    + basedir
    + "/"
    + n20_temp_img
    + " -D "
    + basedir
    + "/"
    + n24_temp_img
    + " --outfile="
    + basedir
    + "/n27_mem.img"
    + " --calc="
    + '"(2.0*A*B/(pow(C,2.0))+(pow(D,0.5)))/(2.0*(1.0+(B*B)/(C*C)))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n27_mem_img, gdal_calc)

# Process: Raster Calculator (34)
# n31_temp_img = Con(n27_memory_img < -1,-1.57, Con (n27_memory_img > 1,1.57,ASin(n27_memory_img)))
n31_temp_img = "n31_temp.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n27_mem_img
    + " --outfile="
    + basedir
    + "/n31_temp.img"
    + " --calc="
    + '"where(A<-1,-1.57,(where(A > 1,1.57,arcsin(A))))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n31_temp_img, gdal_calc)

# Process: Raster Calculator (12)
# n46_memory_img= n16_temp_img - n15_temp_img + n14_temp_img * Cos (n31_temp_img - 0.0218166 ) + n13_temp_img *Cos (n31_temp_img -.0218166) + n12_temp_img  * Sin (  n31_temp_img -.0218166 )
n46_mem_img = "n46_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n16_temp_img
    + " -B "
    + basedir
    + "/"
    + n15_temp_img
    + " -C "
    + basedir
    + "/"
    + n14_temp_img
    + " -D "
    + basedir
    + "/"
    + n31_temp_img
    + " -E "
    + basedir
    + "/"
    + n13_temp_img
    + " -F "
    + basedir
    + "/"
    + n12_temp_img
    + " --outfile="
    + basedir
    + "/n46_mem.img"
    + " --calc="
    + '"A-B+C*cos(D - 0.0218166) + E*cos(D - 0.0218166) + F *sin(D - 0.0218166)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n46_mem_img, gdal_calc)

# Process: Raster Calculator (16)
# n28_memory_img=  (2 * n19_temp_img* n12_temp_img /( (n20_temp_img)** 2) - ((n24_temp_img)** .5))/(2 * (1 + ((n12_temp_img)** 2) / ((n20_temp_img)** 2)))
n28_mem_img = "n28_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n19_temp_img
    + " -B "
    + basedir
    + "/"
    + n12_temp_img
    + " -C "
    + basedir
    + "/"
    + n20_temp_img
    + " -D "
    + basedir
    + "/"
    + n24_temp_img
    + " --outfile="
    + basedir
    + "/n28_mem.img"
    + " --calc="
    + '"(2.0*A*B/(pow(C,2.0))-(pow(D,0.5)))/(2.0*(1.0+(pow(B,2.0))/(pow(C,2.0))))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n28_mem_img, gdal_calc)

# Process: Raster Calculator (33)
# n32_temp_img = Con( n28_memory_img < -1,-1.57, Con ( n28_memory_img > 1,1.57,ASin(n28_memory_img)))
n32_temp_img = "n32_temp.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n28_mem_img
    + " --outfile="
    + basedir
    + "/n32_temp.img"
    + " --calc="
    + '"where(A<-1.0,-1.57,(where(A > 1,1.57,arcsin(A))))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n32_temp_img, gdal_calc)

# Process: Raster Calculator (35)
# n52_memory_img = Con(n36_memory_img >= 0,float(R11),Con(n46_memory_img > 0,n31_temp_img,n32_temp_img))
n52_mem_img = "n52_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n36_mem_img
    + " -B "
    + basedir
    + "/"
    + n46_mem_img
    + " -C "
    + basedir
    + "/"
    + n31_temp_img
    + " -D "
    + basedir
    + "/"
    + n32_temp_img
    + " --outfile="
    + basedir
    + "/n52_mem.img"
    + " --calc="
    + '"where(A>=0,'
    + str11
    + ',(where(B>0,C,D)))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n52_mem_img, gdal_calc)

# Process: Raster Calculator (11)
# n34_memory_img = n16_temp_img- n15_temp_img+ n14_temp_img* float(R6) +n13_temp_img*float(R6) +n12_temp_img*float(R7)
n34_mem_img = "n34_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n16_temp_img
    + " -B "
    + basedir
    + "/"
    + n15_temp_img
    + " -C "
    + basedir
    + "/"
    + n14_temp_img
    + " -D "
    + basedir
    + "/"
    + n13_temp_img
    + " -E "
    + basedir
    + "/"
    + n12_temp_img
    + " --outfile="
    + basedir
    + "/n34_mem.img"
    + " --calc="
    + '"(A - B + C * '
    + str6
    + " + D * "
    + str6
    + " + E * "
    + str7
    + ')" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n34_mem_img, gdal_calc)

# Process: Raster Calculator (32)
# n45_memory_img = (n16_temp_img - n15_temp_img + n14_temp_img * Cos (n32_temp_img +.0218166 )  + n13_temp_img *Cos ( n32_temp_img+.0218166 ) + n12_temp_img * Sin ( n32_temp_img +.0218166 ) )
n45_mem_img = "n45_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n16_temp_img
    + " -B "
    + basedir
    + "/"
    + n15_temp_img
    + " -C "
    + basedir
    + "/"
    + n14_temp_img
    + " -D "
    + basedir
    + "/"
    + n32_temp_img
    + " -E "
    + basedir
    + "/"
    + n13_temp_img
    + " -F "
    + basedir
    + "/"
    + n12_temp_img
    + " --outfile="
    + basedir
    + "/n45_mem.img"
    + " --calc="
    + '"(A - B + C * cos(D + 0.0218166 )  + E * cos(D + 0.0218166 ) + F * sin(D + 0.0218166 ))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n45_mem_img, gdal_calc)

# Process: Raster Calculator (31)
# n51_temp_img = (Con(n34_memory_img > 0,float(R10),Con(n45_memory_img> 0,n32_temp_img,n31_temp_img)))
n51_temp_img = "n51_temp.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n34_mem_img
    + " -B "
    + basedir
    + "/"
    + n45_mem_img
    + " -C "
    + basedir
    + "/"
    + n32_temp_img
    + " -D "
    + basedir
    + "/"
    + n31_temp_img
    + " --outfile="
    + basedir
    + "/n51_temp.img"
    + " --calc="
    + '"where(A>=0,'
    + str10
    + ',(where(B>0,C,D)))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n51_temp_img, gdal_calc)

# Process: Raster Calculator (36)
# n53_memory_img= (Con(n52_memory_img> n51_temp_img,n52_memory_img,n51_temp_img))
n53_mem_img = "n53_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n52_mem_img
    + " -B "
    + basedir
    + "/"
    + n51_temp_img
    + " --outfile="
    + basedir
    + "/n53_mem.img"
    + " --calc="
    + '"where(A>B,A,B)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n53_mem_img, gdal_calc)

# Process: Raster Calculator (37)
# ra24_img = ((n16_temp_img *( n53_memory_img - n51_temp_img)-n15_temp_img* (n53_memory_img - n51_temp_img) + n14_temp_img * (Sin(n53_memory_img) -Sin(n51_temp_img) + n13_temp_img* (Sin(n53_memory_img)-Sin(n51_temp_img))- n12_temp_img * (Cos(n53_memory_img)-Cos(n51_temp_img)))) * float(R12) )
ra24_img = "ra24.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n16_temp_img
    + " -B "
    + basedir
    + "/"
    + n53_mem_img
    + " -C "
    + basedir
    + "/"
    + n51_temp_img
    + " -D "
    + basedir
    + "/"
    + n15_temp_img
    + " -E "
    + basedir
    + "/"
    + n14_temp_img
    + " -F "
    + basedir
    + "/"
    + n13_temp_img
    + " -G "
    + basedir
    + "/"
    + n12_temp_img
    + " --outfile="
    + basedir
    + "/ra24.img"
    + " --calc="
    + '"((A * (B - C) - D * (B - C) + E * (sin(B) - sin(C) + F * ( sin(B) - sin(C) )- G * (cos(B) - cos(C)))) * '
    + str12
    + ' )" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(ra24_img, gdal_calc)

# Process: Raster Calculator
# n8_memory_img= (c1*cos_slope_img - c2* sin_slope_img * cos_aspect_img + c3 * c4 * cos_slope_img + c5 * sin_slope_img * cos_aspect_img * c6 + c7 * sin_slope_img * sin_aspect_img * c8  )
n8_memory_img = "n8_memory.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + cos_slope_img
    + " -B "
    + basedir
    + "/"
    + sin_slope_img
    + " -C "
    + basedir
    + "/"
    + cos_aspect_img
    + " -D "
    + basedir
    + "/"
    + sin_aspect_img
    + " --outfile="
    + basedir
    + "/n8_memory.img"
    + " --calc="
    + '"( '
    + stc1
    + " * A - "
    + stc2
    + " * B * C + "
    + stc3
    + " * "
    + stc4
    + " * A + "
    + stc5
    + " * B * C * "
    + stc6
    + " + "
    + stc7
    + " * B * D * "
    + stc8
    + ')"'
    + " --type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n8_memory_img, gdal_calc)

# Process: Raster Calculator (2)
# costheta0815_img =  (n8_memory_img / cos_slope_img )
costheta0815_img = "costheta0815.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n8_memory_img
    + " -B "
    + basedir
    + "/"
    + cos_slope_img
    + " --outfile="
    + basedir
    + "/costheta0815.img"
    + " --calc="
    + '"(A / B)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(costheta0815_img, gdal_calc)

# Process: Calculate Value (12)
dr = 1 + 0.033 * math.cos(DOY * 2 * math.pi / 365)
str_dr = str(dr)
if DEBUGFLAG > 1:
    write_log(" dr {0}".format(dr))

write_log("Reading the Landsat images")
if ls == 9:
    readLansat9(ls_basedir, landsat_img, workspace)
elif ls == 8:
    readLansat8(ls_basedir, landsat_img, workspace)
elif ls == 7:
    readLansat7(ls_basedir, landsat_img, workspace)
elif ls == 5:
    readLansat5(ls_basedir, landsat_img, workspace)

# Process: Raster Calculator (17)
if ls == 5:
    for i in range(0, 7):
        if i != 5:
            bnd = "R_" + str(i + 1) + ".img"
            str_lmin_band_i = str(lmin_band[i])
            str_lmax_band_i = str(lmax_band[i])
            val1 = lmax_band[i] - lmin_band[i]
            str_val1 = str(val1)
            str_esun_i = str(esun[i])
            REF = "RF_" + str(i + 1) + ".img"
            delete_file_w_path(basedir + "/" + REF)
            gdal_calc = (
                "python "
                + py_script_dir
                + "/gdal_calc.py -A "
                + basedir
                + "/"
                + bnd
                + " --A_band=1 -B "
                + basedir
                + "/"
                + costheta0815_img
                + " --outfile="
                + basedir
                + "/RF_"
                + str(i + 1)
                + ".img"
                + " --calc="
                + '"(((('
                + str_lmin_band_i
                + " + "
                + str_val1
                + " * A / 255.0)) * math.pi) / ("
                + str_esun_i
                + " * B * "
                + str_dr
                + '))" '
                + " --type=Float64"
                + " --overwrite --quiet --NoDataValue=-9999"
            )
            if DEBUGFLAG == 1:
                write_log(gdal_calc)
            ret = os.system(gdal_calc)
        else:
            bnd = "R_" + str(i + 1) + ".img"
            str_lmin_band_i = str(lmin_band[i])
            str_lmax_band_i = str(lmax_band[i])
            REF = "RF_" + str(i + 1) + ".img"
            delete_file_w_path(basedir + "/" + REF)
            #                                                REF=(float(lmin_band[i])/10)+(((float(lmax_band[i])/10)-(float(lmin_band[i])/10))*(Raster(bnd) / 255.0))
            gdal_calc = (
                "python "
                + py_script_dir
                + "/gdal_calc.py -A "
                + basedir
                + "/"
                + bnd
                + " --outfile="
                + basedir
                + "/RF_"
                + str(i + 1)
                + ".img"
                + " --calc="
                + '"( '
                + str_lmin_band_i
                + " /10.0) + ((( "
                + str_lmax_band_i
                + "/10.0) - ("
                + str_lmin_band_i
                + '/10.0)) * (A / 255.0))" '
                + "--type=Float64"
                + " --overwrite --quiet --NoDataValue=-9999"
            )
            if DEBUGFLAG == 1:
                write_log(gdal_calc)
            os.system(gdal_calc)

if ls == 7:
    for i in range(0, 7):
        if i != 5:
            bnd = "R_" + str(i + 1) + ".img"
            str_lmin_band_i = str(lmin_band[i])
            str_lmax_band_i = str(lmax_band[i])
            val1 = lmax_band[i] - lmin_band[i]
            str_val1 = str(val1)
            str_esun_i = str(esun[i])
            REF = "RF_" + str(i + 1) + ".img"
            delete_file_w_path(basedir + "/" + REF)
            gdal_calc = (
                "python "
                + py_script_dir
                + "/gdal_calc.py -A "
                + basedir
                + "/"
                + bnd
                + " --A_band=1 -B "
                + basedir
                + "/"
                + costheta0815_img
                + " --outfile="
                + basedir
                + "/RF_"
                + str(i + 1)
                + ".img"
                + " --calc="
                + '"(((('
                + str_lmin_band_i
                + " + "
                + str_val1
                + " * A / 255.0)) * math.pi) / ("
                + str_esun_i
                + " * B * "
                + str_dr
                + '))" '
                + " --type=Float64"
                + " --overwrite --quiet --NoDataValue=-9999"
            )
            if DEBUGFLAG == 1:
                write_log(gdal_calc)
            ret = os.system(gdal_calc)
        else:
            bnd = "R_" + str(i + 1) + ".img"
            str_lmin_band_i = str(lmin_band[i])
            str_lmax_band_i = str(lmax_band[i])
            REF = "RF_" + str(i + 1) + ".img"
            delete_file_w_path(basedir + "/" + REF)
            #                                                REF=(float(lmin_band[i])/10)+(((float(lmax_band[i])/10)-(float(lmin_band[i])/10))*(Raster(bnd) / 255.0))
            gdal_calc = (
                "python "
                + py_script_dir
                + "/gdal_calc.py -A "
                + basedir
                + "/"
                + bnd
                + " --outfile="
                + basedir
                + "/RF_"
                + str(i + 1)
                + ".img"
                + " --calc="
                + '"( '
                + str_lmin_band_i
                + " /10.0) + ((( "
                + str_lmax_band_i
                + "/10.0) - ("
                + str_lmin_band_i
                + '/10.0)) * (A / 255.0))" '
                + "--type=Float64"
                + " --overwrite --quiet --NoDataValue=-9999"
            )
            if DEBUGFLAG == 1:
                write_log(gdal_calc)
            os.system(gdal_calc)

if ls == 8:
    for i in range(0, 7):
        if i < 5 or i == 6:
            bnd = "R_" + str(i + 1) + ".img"
            str_lmin_band_i = str(lmin_band[i + 1])
            str_lmax_band_i = str(lmax_band[i + 1])
            str_esun_i = str(esun[i])
            str_dr = str(dr)
            REFt = "RFt_" + str(i + 1) + ".img"
            REF = "RF_" + str(i + 1) + ".img"
            if DEBUGFLAG == 1:
                write_log(
                    " band ",
                    REF,
                    " lmin_band ",
                    str_lmin_band_i,
                    " lmax_band ",
                    str_lmax_band_i,
                )
            delete_file_w_path(basedir + "/" + REFt)
            #            REF = (float(lmin_band[i]) + (float(lmax_band[i]) * Raster(bnd))) / costheta0815_img
            gdal_calc = (
                "python "
                + py_script_dir
                + "/gdal_calc.py -A "
                + basedir
                + "/"
                + bnd
                + " --A_band=1 -B "
                + basedir
                + "/"
                + costheta0815_img
                + " --outfile="
                + basedir
                + "/RFt_"
                + str(i + 1)
                + ".img"
                + " --calc="
                + '"('
                + str_lmin_band_i
                + " + ("
                + str_lmax_band_i
                + ' * A)) / B"'
                + " --type=Float64"
                + " --overwrite --quiet --NoDataValue=-9999"
            )
            if DEBUGFLAG == 1:
                write_log(gdal_calc)
            ret = os.system(gdal_calc)
            clip_img(basedir + "/" + REFt, REF, aoi)
        else:
            bnd = "R_" + str(i + 1) + ".img"
            str_lmin_band_i = str(lmin_band[6])
            str_lmax_band_i = str(lmax_band[6])
            try:
                os.remove(basedir + "/RF_" + str(i + 1) + ".img")
            except OSError:
                pass
            gdal_calc = (
                "python "
                + py_script_dir
                + "/gdal_calc.py -A "
                + basedir
                + "/"
                + bnd
                + " --A_band=1 --outfile="
                + basedir
                + "/RFt_"
                + str(i + 1)
                + ".img"
                + " --calc="
                + '"(A) "'
                + " --type=Float64"
                + " --overwrite --quiet --NoDataValue=-9999"
            )
            if DEBUGFLAG == 1:
                write_log(gdal_calc)
            os.system(gdal_calc)
            # write_log("lmin_band {0} lmax_band {1} esun {2} i = {3}".format(str_lmin_band_i, str_lmax_band_i,str_esun_i,i))
            REFt = "RFt_" + str(i + 1) + ".img"
            REF = "RF_" + str(i + 1) + ".img"
            clip_img(basedir + "/" + REFt, REF, aoi)
            delete_file_w_path(basedir + "/" + REFt)

if ls == 9:
    for i in range(0, 7):
        if i < 5 or i == 6:
            bnd = "R_" + str(i + 1) + ".img"
            str_lmin_band_i = str(lmin_band[i + 1])
            str_lmax_band_i = str(lmax_band[i + 1])
            str_esun_i = str(esun[i])
            str_dr = str(dr)
            REFt = "RFt_" + str(i + 1) + ".img"
            REF = "RF_" + str(i + 1) + ".img"
            if DEBUGFLAG == 1:
                write_log(
                    " band ",
                    REF,
                    " lmin_band ",
                    str_lmin_band_i,
                    " lmax_band ",
                    str_lmax_band_i,
                )
            delete_file_w_path(basedir + "/" + REFt)
            #            REF = (float(lmin_band[i]) + (float(lmax_band[i]) * Raster(bnd))) / costheta0815_img
            gdal_calc = (
                    "python "
                    + py_script_dir
                    + "/gdal_calc.py -A "
                    + basedir
                    + "/"
                    + bnd
                    + " --A_band=1 -B "
                    + basedir
                    + "/"
                    + costheta0815_img
                    + " --outfile="
                    + basedir
                    + "/RFt_"
                    + str(i + 1)
                    + ".img"
                    + " --calc="
                    + '"('
                    + str_lmin_band_i
                    + " + ("
                    + str_lmax_band_i
                    + ' * A)) / B"'
                    + " --type=Float64"
                    + " --overwrite --quiet --NoDataValue=-9999"
            )
            if DEBUGFLAG == 1:
                write_log(gdal_calc)
            ret = os.system(gdal_calc)
            clip_img(basedir + "/" + REFt, REF, aoi)
        else:
            bnd = "R_" + str(i + 1) + ".img"
            str_lmin_band_i = str(lmin_band[6])
            str_lmax_band_i = str(lmax_band[6])
            try:
                os.remove(basedir + "/RF_" + str(i + 1) + ".img")
            except OSError:
                pass
            gdal_calc = (
                    "python "
                    + py_script_dir
                    + "/gdal_calc.py -A "
                    + basedir
                    + "/"
                    + bnd
                    + " --A_band=1 --outfile="
                    + basedir
                    + "/RFt_"
                    + str(i + 1)
                    + ".img"
                    + " --calc="
                    + '"(A) "'
                    + " --type=Float64"
                    + " --overwrite --quiet --NoDataValue=-9999"
            )
            if DEBUGFLAG == 1:
                write_log(gdal_calc)
            os.system(gdal_calc)
            # write_log("lmin_band {0} lmax_band {1} esun {2} i = {3}".format(str_lmin_band_i, str_lmax_band_i,str_esun_i,i))
            REFt = "RFt_" + str(i + 1) + ".img"
            REF = "RF_" + str(i + 1) + ".img"
            clip_img(basedir + "/" + REFt, REF, aoi)
            delete_file_w_path(basedir + "/" + REFt)

    # Process: Raster Calculator (18)
for i in range(0, 7):
    if i != 5:
        band1 = "RF_" + str(i + 1) + ".img"
        stralb = str(alb[i])
        if DEBUGFLAG > 1:
            write_log("bandl = ", band1, "alb[", i + 1, "] ", stralb)
        stralb = str(alb[i])
        try:
            os.remove(basedir + "/r" + str(i + 1) + "_alb.img")
        except OSError:
            pass

        r_i_alb_img = "r" + str(i + 1) + "_alb.img"
        delete_file_w_path(basedir + "/" + r_i_alb_img)
        gdal_calc = (
            "python "
            + py_script_dir
            + "/gdal_calc.py -A "
            + basedir
            + "/"
            + band1
            + " --outfile="
            + basedir
            + "/r_"
            + str(i + 1)
            + "_alb.img"
            + " --calc="
            + '"(A*'
            + stralb
            + ')" '
            + "--type=Float64"
            + " --overwrite --quiet --NoDataValue=-9999"
        )
        if DEBUGFLAG == 1:
            write_log(gdal_calc)
        os.system(gdal_calc)

# albedo_toa = Raster("r_1_alb.img") + Raster("r_2_alb.img")+Raster("r_3_alb.img") +Raster("r_4_alb.img")+Raster("r_5_alb.img")+Raster("r_7_alb.img")
albedo_toa_img = "albedo_toa.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + "r_1_alb.img"
    + " -B "
    + basedir
    + "/"
    + "r_2_alb.img"
    + " -C "
    + basedir
    + "/"
    + "r_3_alb.img"
    + " -D "
    + basedir
    + "/"
    + "r_4_alb.img"
    + " -E "
    + basedir
    + "/"
    + "r_5_alb.img"
    + " -F "
    + basedir
    + "/"
    + "r_7_alb.img"
    + " --outfile="
    + basedir
    + "/"
    + albedo_toa_img
    + " --calc="
    + '"(A+B+C+D+E+F)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(albedo_toa_img, gdal_calc)

ndvi_img = "ndvi.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + "RF_4.img"
    + " -B "
    + basedir
    + "/"
    + "RF_3.img"
    + " --outfile="
    + basedir
    + "/"
    + ndvi_img
    + " --calc="
    + '"((A-B)/(A+B))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(ndvi_img, gdal_calc)
check_min_max(ndvi_img, 0.0, 1.0)

min_ndvi, NDVI_MAX, avg_ndvi, std_ndvi = get_band_stats_original(ndvi_img)

# NDVI_MIN = avg_ndvi + 1.5*std_ndvi
# write_log('NDVI_MAX',NDVI_MAX,'NDVI_MIN',NDVI_MIN,'avg_ndvi',avg_ndvi,'std_ndvi',std_ndvi,'\n')
str_NDVI_MIN = str(NDVI_MIN)
str_NDVI_MAX = str(NDVI_MAX)
if NDVI_MIN > NDVI_MAX:
    write_log(
        "The Maximum NDVI value for this image is % 5.2f which is less than the Minimum NDVI required of % 5.2f and therefore too low. \nThis is a SEVERE error and the run is stopped."
        % (NDVI_MAX, NDVI_MIN)
    )
    exit(1)

# LAGgdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + ndvi_r + " --outfile=" + basedir + "/ndvi_alb_mask.img" + " --calc=" + '"where(A>'+str_NDVI_MIN+',(where(A<,1,0)),0)" ' + "--type=Float64" + " --overwrite --NoDataValue=-9999 --quiet --NoDataValue=-9999"
ndvi_alb_mask_img = "ndvi_alb_mask.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + ndvi_img
    + " --outfile="
    + basedir
    + "/"
    + ndvi_alb_mask_img
    + " --calc="
    + '"(1*(A>'
    + str_NDVI_MIN
    + "))*(1*(A<"
    + str_NDVI_MAX
    + '))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(ndvi_alb_mask_img, gdal_calc)

trans_img = "trans.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + dem_img
    + " --outfile="
    + basedir
    + "/"
    + trans_img
    + " --calc="
    + '"(0.75+(2.0*(pow(10.0,- 5))*A))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(trans_img, gdal_calc)

strac = str(ac)
surface_albedo_img = "surface_albedo.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + albedo_toa_img
    + " -B "
    + basedir
    + "/"
    + trans_img
    + " --outfile="
    + basedir
    + "/"
    + surface_albedo_img
    + " --calc="
    + '"((A-'
    + strac
    + ')/(pow(B,2.0)))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(surface_albedo_img, gdal_calc)
check_min_max(surface_albedo_img, 0.0, 1.0)
if DEBUGCODE > 1:
    copyFile(surface_albedo_img, "surface_albedo_0.img")

surf_alb_ndvi_img = "surf_alb_ndvi.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + ndvi_alb_mask_img
    + " -B "
    + basedir
    + "/"
    + surface_albedo_img
    + " --outfile="
    + basedir
    + "/"
    + surf_alb_ndvi_img
    + " --calc="
    + '"where(A==-9999,-9999,B)"'
    + " --type=Float64 --overwrite --NoDataValue=-9999 --quiet"
)
gdal_calc_func(surf_alb_ndvi_img, gdal_calc)

# gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A c:/stats/ndvi_alb_mask2.img --outfile c:/stats/ndvi_alb_mask_avg.img --calc=mean(A) --type=Float64 --overwrite --NoDataValue=-999 --type=Float64'
# os.system(gdal_calc)

# register all of the drivers
gdal.AllRegister()

min_v, max_v, avg_v, std_d = get_band_stats_original(surf_alb_ndvi_img)
# remove the statistics file
try:
    os.remove(surf_alb_ndvi_img + ".aux.xml")
except OSError:
    pass

if avg_v > 0:
    Albmean = avg_v
    if DEBUGFLAG > 1:
        write_log("Albmean {0}".format(Albmean))

alb_cnt = 1
while Albmean < albedo_min_range or Albmean > albedo_max_range:

    albcof = Alb_calc(ac, Albmean, albedo_min_range, albedo_max_range)
    if DEBUGFLAG > 1:
        write_log("albcof ", albcof, "alb_cnt ", alb_cnt)

    str_albcof = str(albcof)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + albedo_toa_img
        + " -B "
        + basedir
        + "/"
        + "trans.img"
        + " --outfile="
        + basedir
        + "/"
        + surface_albedo_img
        + " --calc="
        + '"((A-'
        + str_albcof
        + ')/(pow(B,2.0)))" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(surface_albedo_img, gdal_calc)
    check_min_max(surface_albedo_img, 0.0, 1.0)
    if DEBUGCODE > 1:
        copyFile(surface_albedo_img, "surface_albedo_" + str(alb_cnt) + ".img")
    alb_cnt += 1

    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + ndvi_alb_mask_img
        + " -B "
        + basedir
        + "/"
        + surface_albedo_img
        + " --outfile="
        + basedir
        + "/"
        + surf_alb_ndvi_img
        + " --calc="
        + '"where(A==-9999,-9999,B)"'
        + " --type=Float64 --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(surf_alb_ndvi_img, gdal_calc)

    # register all of the drivers
    gdal.AllRegister()

    min_v, max_v, avg_v, std_d = get_band_stats_original(surf_alb_ndvi_img)
    # remove the statistics file
    try:
        os.remove(surf_alb_ndvi_img + ".aux.xml")
    except OSError:
        pass

    if avg_v > 0:
        Albmean = avg_v
        if DEBUGFLAG > 1:
            write_log("Albmean ", Albmean)

        ac = albcof
        if ac < 0.0 or ac > 0.1:
            write_log(ac, " Albedo Coef is out of range")
            exit(6)

#############################
## Creating Hot and cold rasters

# LAG savi_img = ((1.0 +  float(L_SAVI)) * ( Raster("RF_4.img") - Raster("RF_3.img"))) / ( float(L_SAVI)+Raster("RF_4.img")+Raster("RF_3.img") )
# LAG savi_img.save("savi.img")
strL_SAVI = str(L_SAVI)
savi_img = "savi.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + "RF_4.img"
    + " -B "
    + basedir
    + "/"
    + "RF_3.img"
    + " --outfile="
    + basedir
    + "/"
    + savi_img
    + " --calc="
    + '"(((1.0+'
    + strL_SAVI
    + ")*(A-B))/("
    + strL_SAVI
    + '+A+B))" '
    + "--type=Float64"
    + " --overwrite --NoDataValue=-9999 --quiet"
)
gdal_calc_func(savi_img, gdal_calc)

# LAG lai_r_img =Con (savi_img > 0.687, 6, ( - (Ln( (.69- savi_img)/.59 ) /.91) ))
# LAG lai_r_img.save("lai_r.img")
lai_r_img = "lai_r.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + savi_img
    + " --outfile="
    + basedir
    + "/"
    + lai_r_img
    + " --calc="
    + '"where(A>0.687,6,(-(log((0.69-A)/0.59)/0.91)))" '
    + "--type=Float64"
    + " --overwrite --NoDataValue=-9999 --quiet"
)
gdal_calc_func(lai_r_img, gdal_calc)

lai_img = "lai.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + lai_r_img
    + " --outfile="
    + basedir
    + "/"
    + lai_img
    + " --calc="
    + '"where(A>3,3,A)" '
    + "--type=Float64"
    + " --overwrite --NoDataValue=-9999 --quiet"
)
gdal_calc_func(lai_img, gdal_calc)

nx2mem_img = "nx2mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + lai_img
    + " --outfile="
    + basedir
    + "/"
    + nx2mem_img
    + " --calc="
    + '"where(A>3.0,0.985,(0.95+.01*A))" '
    + "--type=Float64"
    + " --overwrite --NoDataValue=-9999 --quiet"
)
gdal_calc_func(nx2mem_img, gdal_calc)

nx8mem_img = "nx8mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + ndvi_img
    + " -B "
    + basedir
    + "/"
    + surface_albedo_img
    + " -C "
    + basedir
    + "/"
    + nx2mem_img
    + " --outfile="
    + basedir
    + "/"
    + nx8mem_img
    + " --calc="
    + '"where(logical_and(A<0.0,B<0.15),0.985,C)" '
    + "--type=Float64"
    + " --overwrite --NoDataValue=-9999 --quiet"
)
# gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + surface_albedo_img + ' -B ' + basedir + '/' + nx2mem_img + " --outfile=" + basedir + "/nx8mem.img" + " --calc=" + '"where(A<0.15,0.985,B)" ' + "--type=Float64" + " --overwrite --NoDataValue=-9999 --quiet"
gdal_calc_func(nx8mem_img, gdal_calc)

nx10mem_img = "nx10mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + ndvi_img
    + " -B "
    + basedir
    + "/"
    + surface_albedo_img
    + " -C "
    + basedir
    + "/"
    + nx8mem_img
    + " --outfile="
    + basedir
    + "/"
    + nx10mem_img
    + " --calc="
    + '"where(logical_and(A<0.0,B>0.47),0.985,C)" '
    + "--type=Float64"
    + " --overwrite --NoDataValue=-9999 --quiet"
)
gdal_calc_func(nx10mem_img, gdal_calc)

# Process: Raster Calculator (15)
# LAG emissivity_img=Con(nx10_mem_img > .9,nx10_mem_img,.9)
# LAG emissivity_img.save("emissivity.img")
emissivity_img = "emissivity.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + nx10mem_img
    + " --outfile="
    + basedir
    + "/"
    + emissivity_img
    + " --calc="
    + '"where(A>0.9,A,0.9)" '
    + "--type=Float64"
    + " --overwrite --NoDataValue=-9999 --quiet"
)
gdal_calc_func(emissivity_img, gdal_calc)

# Process: Raster Calculator (18)
if ls == 7 or ls == 5:
    str_c_t1 = str(c_t1)
    str_c_t2 = str(c_t2)
    n5_mem_img = "n5_mem.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + "RF_6.img"
        + " --outfile="
        + basedir
        + "/"
        + n5_mem_img
        + " --calc="
        + '"('
        + str_c_t1
        + "/log("
        + str_c_t2
        + '/A+1.0))" '
        + "--type=Float32"
        + " --overwrite --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(n5_mem_img, gdal_calc)

if ls == 8 or ls == 9:
    n3_mem_img = "n3_mem.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + "RF_6.img"
        + " --outfile="
        + basedir
        + "/"
        + n3_mem_img
        + " --calc="
        + '"((0.00033420*A)+0.1)" '
        + "--type=Float64"
        + " --overwrite --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(n3_mem_img, gdal_calc)

    # LAG    n5_mem_img=float(c_t1)/(Ln(float(c_t2)/n3_memeory_img+1))
    str_c_t1 = str(c_t1)
    str_c_t2 = str(c_t2)
    n5_mem_img = "n5_mem.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + n3_mem_img
        + " --outfile="
        + basedir
        + "/"
        + n5_mem_img
        + " --calc="
        + '"('
        + str_c_t1
        + "/log("
        + str_c_t2
        + '/A+1.0))" '
        + "--type=Float32"
        + " --overwrite --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(n5_mem_img, gdal_calc)
# Process: Raster Calculator (10)

n2_mem_img = "n2_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + lai_img
    + " --outfile="
    + basedir
    + "/"
    + n2_mem_img
    + " --calc="
    + '"where(A>3.0,0.985,(0.97+0.0033*A))" '
    + "--type=Float64"
    + " --overwrite --NoDataValue=-9999 --quiet"
)
gdal_calc_func(n2_mem_img, gdal_calc)

# Process: Raster Calculator (9)
n8_mem_img = "n8_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + ndvi_img
    + " -B "
    + basedir
    + "/"
    + surface_albedo_img
    + " -C "
    + basedir
    + "/"
    + n2_mem_img
    + " --outfile="
    + basedir
    + "/"
    + n8_mem_img
    + " --calc="
    + '"where((logical_and(A<0.0,B<0.15)),0.99,C)" '
    + "--type=Float64"
    + " --overwrite --NoDataValue=-9999 --quiet"
)
gdal_calc_func(n8_mem_img, gdal_calc)

# Process: Raster Calculator (11)
n10_mem_img = "n10_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + ndvi_img
    + " -B "
    + basedir
    + "/"
    + surface_albedo_img
    + " -C "
    + basedir
    + "/"
    + n8_mem_img
    + " --outfile="
    + basedir
    + "/"
    + n10_mem_img
    + " --calc="
    + '"where((logical_and(A<0.0,B>0.47)),0.99,C)" '
    + "--type=Float64"
    + " --overwrite --NoDataValue=-9999 --quiet"
)
gdal_calc_func(n10_mem_img, gdal_calc)

# Process: Raster Calculator (12)
enb_img = "enb.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n10_mem_img
    + " --outfile="
    + basedir
    + "/"
    + enb_img
    + " --calc="
    + '"where(A>0.9,A,0.90)" '
    + "--type=Float64"
    + " --overwrite --NoDataValue=-9999 --quiet"
)
gdal_calc_func(enb_img, gdal_calc)

# Process: Raster Calculator (19)
st_img = "st.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n5_mem_img
    + " -B "
    + basedir
    + "/"
    + enb_img
    + " --outfile="
    + basedir
    + "/"
    + st_img
    + " --calc="
    + '"(A/pow(B,0.25))" '
    + "--type=Float64"
    + " --overwrite --NoDataValue=-9999 --quiet"
)
gdal_calc_func(st_img, gdal_calc)
check_min_max(st_img, 250.0, 400.0)

st_dem_img = "st_dem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + dem_img
    + " -B "
    + basedir
    + "/"
    + mean_elev
    + " -C "
    + basedir
    + "/"
    + st_img
    + " --outfile="
    + basedir
    + "/"
    + st_dem_img
    + " --calc="
    + '"((A-B)*0.0065+C)" '
    + " --type=Float64"
    + " --overwrite --NoDataValue=-9999 --quiet"
)
gdal_calc_func(st_dem_img, gdal_calc)

st_dem_c_img = "st_dem_c.img"
# This statement was changed from aoi_cold to area_shp since otherwise it would clip to the aoi_cold
clip_img(basedir + "/" + st_dem_img, st_dem_c_img, area_shp)

# Check if the NDVI values are within the range otherwise no ET will be calculated
min_v, max_v, avg_v, std_d = get_band_stats_original(ndvi_img)

if (max_v < ncm_min) or (min_v > ncm_max):
    write_log(
        "This image does NOT have NDVI values within the range of ",
        ncm_min,
        " and ",
        ncm_max,
        "therefore no ET will be calculated.",
    )
    exit(4029)

### Process: Raster Calculator (2)
str_ncm_min = str(ncm_min)
str_ncm_max = str(ncm_max)

ncm_img = "ncm.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + ndvi_img
    + " --outfile="
    + basedir
    + "/ncm.img"
    + " --calc="
    + '"where(logical_or(A > '
    + str_ncm_max
    + ", A < "
    + str_ncm_min
    + '),0,1)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999 --quiet"
)
gdal_calc_func(ncm_img, gdal_calc)

write_log("Extracting landuse data ~40% of run completed.")
nass_img = "nass_clip.img"
delete_file_w_path(basedir + "/" + nass_img)

#Generating the GFSAD mask with the cold aoi mask so we exclude the LS7 scan error areas
gfsad_mask()

nass_cold_mask, nass_cold_mask_0, nass_hot_mask = extract_nass(ls_year)

# calculate the surface temperature mask the first time for alfalfa and corn fields only.
# Layer nass_cold_mask_0 contains 1 where there is data and 0 were there is not vs nass_cold_mask which contains 1 and -9999 (no data)
#no_nass_cat_found = calc_s_t_cold_mask(nass_cold_mask_0[len_basedir + 1 :])
no_nass_cat_found = calc_s_t_cold_mask()
if no_nass_cat_found == 0:
    nass_cold_pt_mask()

write_log("Calculating the cold and hot points ~50% of run completed.")

cold_num_points = 0
if USER_COLD_PTS_FILE == 0:
    cold_pts = "cold_pts.shp"
    if no_nass_cat_found == 0:
        cold_num_points = finding_cold_pts_set()
    if DEBUGFLAG == 1:
        write_log(
            "Cold number of points with first NASS mask (Alfalfa and Corn) ",
            cold_num_points,
        )
    if cold_num_points == 0:
        if DEBUGFLAG == 1:
            write_log(
                "NO cold points were identified in the first NASS filter a more NASS categories 1-38 are being considered"
            )
        nass_cold_mask, nass_cold_mask_0 = nass_second_cold_mask()
        # calculate the surface temperature mask the first time for alfalfa and corn fields only.
        # Layer nass_cold_mask_0 contains 1 where there is data and 0 were there is not vs nass_cold_mask which contains 1 and -9999 (no data)
#        no_nass_cat_found = calc_s_t_cold_mask(nass_cold_mask_0[len_basedir + 1 :])
        no_nass_cat_found = calc_s_t_cold_mask()
        if no_nass_cat_found == 0:
            nass_cold_pt_mask()
            cold_num_points = finding_cold_pts_set()
    if cold_num_points == 0:
        if DEBUGFLAG == 1:
            write_log(
                "NO cold points were identified in the second filter which is only GFSAD"
            )
#        nass_cold_mask, nass_cold_mask_0 = nass_third_cold_mask()
        # calculate the surface temperature mask the first time for alfalfa and corn fields only.
        # Layer nass_cold_mask_0 contains 1 where there is data and 0 were there is not vs nass_cold_mask which contains 1 and -9999 (no data)
#        no_nass_cat_found = calc_s_t_cold_mask(nass_cold_mask_0[len_basedir + 1 :])
#        if no_nass_cat_found == 0:
#            nass_cold_pt_mask()
#            cold_num_points = finding_cold_pts_set()
#    if cold_num_points == 0:
#        if DEBUGFLAG == 1:
#            write_log(
#                "NO cold points were identified with NASS or GFSAD therefore we will use the AOI excluding clouds will be used instead"
#            )
#        copyFile("aoi_cloud_mask.img", "nass_cold_mask1.img")
#        nass_cold_mask = "nass_cold_mask1.img"
#        no_nass_cat_found = calc_s_t_cold_mask(nass_cold_mask)
#        if no_nass_cat_found == 0:
#            nass_cold_pt_mask()
#            cold_num_points = finding_cold_pts_set()
#            nass_cold_mask, nass_cold_mask_0 = nass_third_cold_mask()

    if cold_num_points == 0:
        write_log(
            "No cold points were identified by the model.  \nUser needs to identify the cold point(s) using the USER_COLD_PTS_FILE token and re-run the model"
        )
        exit(8)

    os.system(
        'ogr2ogr -f "ESRI Shapefile" -a_srs EPSG:326'
        + utm_zone
        + " -oo X_POSSIBLE_NAMES=X* -oo Y_POSSIBLE_NAMES=Y* -oo KEEP_GEOM_COLUMNS=NO {0}.shp {0}_sorted.csv".format(
            basedir + "/cold_pts_set"
        )
    )
    delete_file_w_path(basedir + "/" + "cold_pts_set.prj")
    try:
        copyFile(basedir + "/" + aoi_file[:-4] + ".prj", basedir + "/cold_pts_set.prj")
    except OSError:
        pass

if USER_HOT_PTS_FILE == 0:
    s_t_hot1_img = "s_t_hot1.img"
    hot_num_points = 0
    #    write_log('\n\n\n BEFORE FIRST while hot_num_points and nhm_max ',hot_num_points, nhm_max)
    while hot_num_points == 0 and nhm_max <= 0.3:
        s_t_hot1_img = calc_s_t_hot1(nhm_min, nhm_max)
        nass_hot_pt_mask()
        hot_pts = "hot_pts.shp"
        hot_num_points = finding_hot_pts_set()
        if DEBUGFLAG > 1:
            write_log("hot number of points ", hot_num_points)
        nhm_max += 0.05
    #        write_log('\n\n\n AT END OF FIRST while hot_num_points and nhm_max ',hot_num_points, nhm_max)

    # if after increasing the nhm_max from 0.2 to 0.3 no hot points are identified then the NASS mask is removed and then the model tries to identify hot points starting
    # at an nhm_max of 0.2 and increasing in 0.05 increments until 0.3.  If after these steps no hot points are found the model will stop and ask the user to idenfity the
    # hot points manually.
#    if hot_num_points == 0:
#        nhm_max = 0.2
#        nass_hot_mask = nass_second_hot_mask()
        #        write_log('\n\n\n BEFORE SECOND while hot_num_points and nhm_max ', hot_num_points, nhm_max)
#        while hot_num_points == 0 and nhm_max <= 0.3:
#            s_t_hot1_img = calc_s_t_hot1(nhm_min, nhm_max)
#            nass_hot_pt_mask()
#            hot_num_points = finding_hot_pts_set()
#            if DEBUGFLAG > 1:
#                write_log(
#                    "hot number of points after nass_second_hot_mask ", hot_num_points
#                )
#            nhm_max += 0.05
    #            write_log('\n\n\n AT END OF SECOND while hot_num_points and nhm_max ',hot_num_points, nhm_max)

    if hot_num_points == 0:
        write_log(
            "No hot points were identified by the model.  \nUser needs to identify the hot point using the USER_HOT_PTS_FILE token and re-run the model"
        )
        exit(1)

    os.system(
        'ogr2ogr -f "ESRI Shapefile" -a_srs EPSG:326'
        + utm_zone
        + ' -oo X_POSSIBLE_NAMES=X* -oo Y_POSSIBLE_NAMES=Y* -oo KEEP_GEOM_COLUMNS=NO -clipdstwhere "Z>0" {0}.shp {0}_sorted.csv'.format(
            basedir + "/hot_pts_set"
        )
    )
    delete_file_w_path(basedir + "/" + "hot_pts_set.prj")
    copyFile(basedir + "/" + aoi_file[:-4] + ".prj", basedir + "/hot_pts_set.prj")
    delete_shp_file("outPoint_H_S.shp")
    delete_shp_file("outPoint_H_S1.shp")
    Hrows = int(getTopXPercentCount(hot_num_points, 2))
    if Hrows < 10:
        if hot_num_points < 10:
            Hrows = hot_num_points
        else:
            Hrows = 10
    if DEBUGFLAG == 1:
        write_log(
            "Top 2 percent of Hot Points and if less than 10 then set to 10 or less if 10 are not available",
            Hrows,
        )
    where = '"FID"' + " <= " + str(Hrows) + " "
    if DEBUGFLAG > 1:
        write_log("where statement ", where)

    WriteTopXPercent("hot_pts_set_sorted.csv", basedir, "hot_pts.csv", Hrows)
    os.system(
        'ogr2ogr -f "ESRI Shapefile" -a_srs EPSG:326'
        + utm_zone
        + ' -oo X_POSSIBLE_NAMES=X* -oo Y_POSSIBLE_NAMES=Y* -oo KEEP_GEOM_COLUMNS=NO -clipdstwhere "Z>0" {0}.shp {0}.csv'.format(
            basedir + "/hot_pts"
        )
    )
    delete_file_w_path(basedir + "/" + "hot_pts.prj")
    copyFile(basedir + "/" + aoi_file[:-4] + ".prj", basedir + "/hot_pts.prj")
else:
    Hrows = find_n_rows_shp(basedir + "/" + hot_pts)

# Creating T hot raster
zField = "GRID_CODE"
inPointFeatures1 = hot_pts
idw_power = 2.0
count_H = Hrows
if DEBUGFLAG > 1:
    write_log("Count_H ", count_H)

st_hot2_img = "st_hot2.img"
st_hot1_img = "st_hot1.img"
if count_H >= 2:
    create_csv_vrt(hot_pts, "Z")
    delete_file_w_path(basedir + "/" + "st_hot1.img")
    delete_file_w_path(basedir + "/" + "st_hot2.img")
    gdal_grid_text = str(
        'gdal_grid -a invdist:power={0} -txe {1} {2} -tye {3} {4} -outsize {5} {6} -a_srs EPSG:326{7} -zfield "Z" -of GTiff -l {8} -ot Float64 {9} {10}'.format(
            idw_power,
            clip_x1,
            clip_x2,
            clip_y2,
            clip_y1,
            x_cells,
            y_cells,
            utm_zone,
            hot_pts[:-4],
            basedir + "/" + hot_pts[:-4] + ".vrt",
            basedir + "/st_hot2.img",
        )
    )
    if DEBUGFLAG == 1:
        write_log("gdal_grid_text ", gdal_grid_text)
    os.system(gdal_grid_text)
    clip_img(basedir + "/" + st_hot2_img, st_hot1_img, aoi)
else:
    field_val = fnd_val_single_pt(inPointFeatures1, "Z")
    if DEBUGFLAG > 1:
        write_log("field val ", field_val)
    str_field_val = str(field_val)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + dem_img
        + " --outfile="
        + basedir
        + "/"
        + st_hot2_img
        + " --calc="
        + '"('
        + str_field_val
        + ')"'
        + " --type=Float64"
        + " --overwrite --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func(st_hot2_img, gdal_calc)
    clip_img(basedir + "/" + st_hot2_img, st_hot1_img, aoi)
    delete_file_w_path(basedir + "/" + st_hot2_img)

## Convert input search radius to meters
srm = kmToM(sr)
srm_max = kmToM(sr_m)
if DEBUGFLAG > 1:
    write_log("Cold Point Initial Search Radius in mts = ", srm)
    write_log("Cold Point Search Radius maximum in mts = ", srm_max)

######### if USER_UNCALIB == 0: Means that the run is CALIBRATED

## Selecting each weather station and points near to it
num_ws = find_n_rows_shp(basedir + "/" + ws)
if DEBUGFLAG == 1:
    write_log("Number of weather stations ", num_ws)

if USER_COLD_PTS_FILE == 0:
    if DEBUGCODE == 1:
        delete_shp_file("cold_pts_set_out0.shp")
        copy_shp_file("cold_pts_set.shp", "cold_pts_set_out0.shp")
    delete_shp_file("cold_pts.shp")
    n_ws_cold_pts = fnd_pt_by_loc(
        ws, "cold_pts_set.shp", "cold_pts.shp", srm, "Z", "ASCENDING"
    )
    if DEBUGCODE == 1:
        delete_shp_file("cold_pts_out0.shp")
        copy_shp_file("cold_pts.shp", "cold_pts_out0.shp")
    searchRadius2 = 150000
else:
    n_ws_cold_pts = find_n_rows_shp(basedir + "/" + cold_pts)

st_cold2_img = "st_cold2.img"
st_cold1_img = "st_cold1.img"

if n_ws_cold_pts >= 2:
    create_csv_vrt(cold_pts, "Z")
    delete_file_w_path(basedir + "/" + "st_cold2.img")
    gdal_grid_text = str(
        'gdal_grid -a invdist:power={0} -txe {1} {2} -tye {3} {4} -outsize {5} {6} -a_srs EPSG:326{7} -zfield "Z" -of GTiff -l {8} -ot Float64 {9} {10}'.format(
            idw_power,
            clip_x1,
            clip_x2,
            clip_y2,
            clip_y1,
            x_cells,
            y_cells,
            utm_zone,
            cold_pts[:-4],
            basedir + "/" + cold_pts[:-4] + ".vrt",
            basedir + "/st_cold2.img",
        )
    )
    if DEBUGFLAG == 1:
        write_log("gdal_grid_text ", gdal_grid_text)
    os.system(gdal_grid_text)
    clip_img(basedir + "/" + st_cold2_img, st_cold1_img, aoi)
    delete_file_w_path(basedir + "/" + st_cold2_img)

else:
    field_val = fnd_val_single_pt(cold_pts, "Z")
    str_field_val = str(field_val)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + dem_img
        + " --outfile="
        + basedir
        + "/"
        + "st_cold2.img"
        + " --calc="
        + '"('
        + str_field_val
        + ')"'
        + " --type=Float64"
        + " --overwrite --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func("st_cold2.img", gdal_calc)
    clip_img(basedir + "/" + st_cold2_img, st_cold1_img, aoi)
    delete_file_w_path(basedir + "/" + st_cold2_img)

t_one_img = "t_one.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + trans_img
    + " --outfile="
    + basedir
    + "/t_one.img"
    + " --calc="
    + '"log(A)" '
    + "--type=Float64"
    + " --overwrite --NoDataValue=-9999 --quiet"
)
gdal_calc_func(t_one_img, gdal_calc)

n14_mem_img = "n14_mem_0.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + "st_cold1.img"
    + " -B "
    + basedir
    + "/"
    + t_one_img
    + " --outfile="
    + basedir
    + "/n14_mem_0.img"
    + " --calc="
    + '"(1.08*(pow(-B, 0.265))*5.67*0.00000001*(pow(A,4.0)))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n14_mem_img, gdal_calc)

# Rn is the net surface irradiance (commonly referred to as the net radiation)
rn_img = "rn.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + surface_albedo_img
    + " -B "
    + basedir
    + "/"
    + costheta0815_img
    + " -C "
    + basedir
    + "/"
    + trans_img
    + " -D "
    + basedir
    + "/"
    + n14_mem_img
    + " -E "
    + basedir
    + "/"
    + emissivity_img
    + " -F "
    + basedir
    + "/"
    + st_img
    + " --outfile="
    + basedir
    + "/rn.img"
    + " --calc="
    + '"((1.0-A)*1367.0*B*'
    + str_dr
    + '*C)+D-(E*(5.67*pow(10,-8))*pow(F,4.0))-((1.0-E)*D)" '
    + "--type=Float32"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(rn_img, gdal_calc)

if DEBUGCODE == 1:
    copyFile(basedir + "/" + rn_img, basedir + "/rn_0.img")

if USER_COLD_PTS_FILE == 0:
    delete_shp_file("cold_pts_set_rn.shp")
    copy_shp_file("cold_pts_set.shp", "cold_pts_set_rn.shp")
    # Process: Extract Multi Values to Points
    pnt_val_from_rast(rn_img, "cold_pts_set_rn.shp", "NONE")
    #   getting the NDVI value for the cold points
    pnt_val_from_rast(ndvi_img, "cold_pts_set_rn.shp", "NONE")
    #   getting the combined NDVI and RN normalized values for figuring which is the best point to use
    rn_ndvi_pnt_val("cold_pts_set_rn.shp")
    delete_shp_file("cold_pts_set_rn_out1.shp")

    n_ws_cold_pts = fnd_pt_by_loc(
        ws, "cold_pts_set_rn.shp", "cold_pts_rn.shp", srm, "rn", "DESCENDING"
    )

    if DEBUGFLAG == 1:
        write_log("Number of cold points line 4348 ", n_ws_cold_pts)
else:
    # LAG - IMPORTANT NEEDED WHEN TRYING TO MATCH WITH ARCPY CODE - SECOND SET OF COLD POINTS!!!!!    cold_pts = 'frio_pts_second.shp'
    cold_pts_rn = cold_pts
    copy_shp_file(cold_pts, "cold_pts_rn.shp")
    n_ws_cold_pts = find_n_rows_shp(basedir + "/" + cold_pts_rn)

st_cold2_img = "st_cold2.img"
st_cold1_img = "st_cold1.img"
cold_pts_rn = "cold_pts_rn.shp"

if n_ws_cold_pts >= 2:
    create_csv_vrt(cold_pts_rn, "rn")
    delete_file_w_path(basedir + "/" + "st_cold2.img")
    gdal_grid_text = str(
        'gdal_grid -a invdist:power={0} -txe {1} {2} -tye {3} {4} -outsize {5} {6} -a_srs EPSG:326{7} -zfield "Z" -of GTiff -l {8} -ot Float64 {9} {10}'.format(
            idw_power,
            clip_x1,
            clip_x2,
            clip_y2,
            clip_y1,
            x_cells,
            y_cells,
            utm_zone,
            cold_pts_rn[:-4],
            basedir + "/" + cold_pts_rn[:-4] + ".vrt",
            basedir + "/st_cold2.img",
        )
    )
    if DEBUGFLAG == 1:
        write_log("gdal_grid_text ", gdal_grid_text)
    os.system(gdal_grid_text)
    clip_img(basedir + "/" + st_cold2_img, st_cold1_img, aoi)
    delete_file_w_path(basedir + "/" + st_cold2_img)

else:
    field_val = fnd_val_single_pt(cold_pts, "Z")
    if DEBUGFLAG == 1:
        write_log("field val ", field_val)
    str_field_val = str(field_val)
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + dem_img
        + " --outfile="
        + basedir
        + "/"
        + "st_cold2.img"
        + " --calc="
        + '"('
        + str_field_val
        + ')"'
        + " --type=Float64"
        + " --overwrite --NoDataValue=-9999 --quiet"
    )
    gdal_calc_func("st_cold2.img", gdal_calc)
    clip_img(basedir + "/" + st_cold2_img, st_cold1_img, aoi)
    delete_file_w_path(basedir + "/" + st_cold2_img)

n14_mem_img = "n14_mem_1.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + "st_cold1.img"
    + " -B "
    + basedir
    + "/"
    + t_one_img
    + " --outfile="
    + basedir
    + "/n14_mem_1.img"
    + " --calc="
    + '"(1.08*(pow(-B, 0.265))*5.67*0.00000001*(pow(A,4.0)))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n14_mem_img, gdal_calc)
if DEBUGCODE == 1:
    copyFile(
        basedir + "/" + "st_cold1.img",
        basedir + "/" + "st_cold1_1.img",
    )

# Rn is the net surface irradiance (commonly referred to as the net radiation)
rn_img = "rn.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + surface_albedo_img
    + " -B "
    + basedir
    + "/"
    + costheta0815_img
    + " -C "
    + basedir
    + "/"
    + trans_img
    + " -D "
    + basedir
    + "/"
    + n14_mem_img
    + " -E "
    + basedir
    + "/"
    + emissivity_img
    + " -F "
    + basedir
    + "/"
    + st_img
    + " --outfile="
    + basedir
    + "/rn.img"
    + " --calc="
    + '"((1.0-A)*1367.0*B*'
    + str_dr
    + '*C)+D-(E*(0.0000000567*pow(F,4.0)))-((1.0-E)*D)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(rn_img, gdal_calc)
if DEBUGCODE == 1:
    copy_shp_file(rn_img, "rn_1.img")

n18_mem_img = "n18_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + st_img
    + " -B "
    + basedir
    + "/"
    + surface_albedo_img
    + " -C "
    + basedir
    + "/"
    + ndvi_img
    + " -D "
    + basedir
    + "/"
    + rn_img
    + " --outfile="
    + basedir
    + "/n18_mem.img"
    + " --calc="
    + '"((A - 273.1) / B) * (0.0038*B+ 0.0074 *pow(B,2.0)) * (1 - 0.98 * (pow(C,4.0))) * D" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n18_mem_img, gdal_calc)

# Process: Raster Calculator (3)
str_s1 = str(s1)
str_s2 = str(s2)
SecT_img = "SecT.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + rn_img
    + " --outfile="
    + basedir
    + "/SecT.img"
    + " --calc="
    + '"(A*'
    + str_s1
    + " - "
    + str_s2
    + ')" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(SecT_img, gdal_calc)

n13_mem_img = "n13_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + ndvi_img
    + " -B "
    + basedir
    + "/"
    + SecT_img
    + " -C "
    + basedir
    + "/"
    + n18_mem_img
    + " --outfile="
    + basedir
    + "/n13_mem.img"
    + " --calc="
    + '"where(A<0,B,C)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n13_mem_img, gdal_calc)

# Process: Raster Calculator (5)

go_img = "go.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + st_img
    + " -B "
    + basedir
    + "/"
    + surface_albedo_img
    + " -C "
    + basedir
    + "/"
    + rn_img
    + " -D "
    + basedir
    + "/"
    + n13_mem_img
    + " --outfile="
    + basedir
    + "/go.img"
    + " --calc="
    + '"where(logical_and(A<277.0,B>0.47),(0.5*C),D)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(go_img, gdal_calc)

# Process: Raster Calculator (7)
zo_img = "zo.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + lai_r_img
    + " --outfile="
    + basedir
    + "/zo.img"
    + " --calc="
    + '"where((0.018*A)<0.005,0.005, 0.018*A)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(zo_img, gdal_calc)

# Process: Raster Calculator (8)
u_star_ost_img = "u_star_ost.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + u200_img
    + " -B "
    + basedir
    + "/"
    + zo_img
    + " --outfile="
    + basedir
    + "/u_star_ost.img"
    + " --calc="
    + '"((A*0.41)/(log(200.0/B)))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(u_star_ost_img, gdal_calc)
copyFile("u_star_ost.img", "u_star_ost_rahone.img")

# Process: Raster Calculator (10)
rahone_img = "rahone.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + u_star_ost_img
    + " --outfile="
    + basedir
    + "/rahone.img"
    + " --calc="
    + '"'
    + str_aerod_res
    + '/(A*0.41)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(rahone_img, gdal_calc)

# NOTE in the original GIS code the fields for the layers created in the F12_4 were modified
# by which the rah_1st.img (rahone.img) was called "rah" and the surftemp_dem.img (st_dem.img)
# was called surftemp.  In the GDAL the field name is based on the name of the layer so the
# layers rahone.img was copied into rah.img and st_dem.img into st.img temporary so the
# field generated in the hot and cold layers coincided with the GIS fields.

try:
    os.rename(basedir + "/rah.img", basedir + "/rah.img1")
except OSError:
    pass
copyFile(rahone_img, "rah.img")
rah_img = "rah.img"
try:
    os.rename(basedir + "/st.img", basedir + "/st.img1")
except OSError:
    pass
st_img = "st.img"
delete_file_w_path(basedir + "/" + st_img)
copyFile(st_dem_img, st_img)

# NOTE rahone is being copied into rah so that the variable field created is rah in F12_4.  Then rah.img is restored back (not sure it even exists at this point)

write_log(
    "Extracting the hot and cold pixels from the go, rn, rah and st raster layers ~55% of run completed."
)
F12_4(go_img, rn_img, rah_img, st_img, "NONE")
interp(go_img, rn_img, rah_img, st_img)

cnt = 1

# Reverting the files back to their original names if they existed (see NOTE about call the F12_4 (17 lines above)
delete_file_w_path(basedir + "/" + rah_img)
delete_file_w_path(basedir + "/" + st_img)
try:
    os.rename(basedir + "/rah.img1", basedir + "/rah.img")
except OSError:
    pass
try:
    os.rename(basedir + "/st.img1", basedir + "/st.img")
except OSError:
    pass

# Process: Raster Calculator (8)
rn_img_hot = "rn_hot.img"
go_img_hot = "go_hot.img"
# CAUTION - h_hot_img is saved as h_img_hot so it replaces the surface generated from hot points from the h surface.
h_hot_img = "h_hot_I.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + rn_img_hot
    + " -B "
    + basedir
    + "/"
    + go_img_hot
    + " --outfile="
    + basedir
    + "/h_hot_I.img"
    + " --calc="
    + '"(A - B)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(h_hot_img, gdal_calc)

c_one_img = "c_one.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + dem_img
    + " --outfile="
    + basedir
    + "/c_one.img"
    + " --calc="
    + '"(0.0065 * A)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(c_one_img, gdal_calc)

i3_img = "i3.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + c_one_img
    + " --outfile="
    + basedir
    + "/i3.img"
    + " --calc="
    + '"((((101.3*pow(((293.0-A)/293.0),5.26))*1000.0)/84931.91)*'
    + str_Cp
    + ')" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(i3_img, gdal_calc)

# Process: Raster Calculator (9)
rah_img_hot = "rah_hot.img"
dt_hot_img = "dt_hot.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + h_hot_img
    + " -B "
    + basedir
    + "/"
    + rah_img_hot
    + " -C "
    + basedir
    + "/"
    + i3_img
    + " --outfile="
    + basedir
    + "/dt_hot.img"
    + " --calc="
    + '"((A*B)/C)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(dt_hot_img, gdal_calc)

st_img_cold = "st_cold.img"
# If running in uncalibrated mode then skip these lines
if USER_UNCALIB == 0:
    # Process: Raster Calculator (6)
    lambda_img = "lambda.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + st_cold1_img
        + " --outfile="
        + basedir
        + "/lambda.img"
        + " --calc="
        + '"((2.501-(0.00236 * ( A - 273.0)))*1000000.0)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(lambda_img, gdal_calc)

    # Process: Raster Calculator (5)
    et_ins_mod_img = "et_ins_mod.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + etinst_ws_img
        + " --outfile="
        + basedir
        + "/et_ins_mod.img"
        + " --calc="
        + '"(A * 1.05)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(et_ins_mod_img, gdal_calc)

    # Process: Raster Calculator (7)
    le_img = "le.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + lambda_img
        + " -B "
        + basedir
        + "/"
        + et_ins_mod_img
        + " --outfile="
        + basedir
        + "/le.img"
        + " --calc="
        + '"(A * B / 3600.0)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(le_img, gdal_calc)

    rn_img_cold = "rn_cold.img"
    go_img_cold = "go_cold.img"
    # Process: Raster Calculator (4)
    h_cold_img = "h_cold_I.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + rn_img_cold
        + " -B "
        + basedir
        + "/"
        + go_img_cold
        + " -C "
        + basedir
        + "/"
        + le_img
        + " --outfile="
        + basedir
        + "/h_cold_I.img"
        + " --calc="
        + '"(A - B - C)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(h_cold_img, gdal_calc)

    # Process: Raster Calculator (3)
    rah_img_cold = "rah_cold.img"
    dt_cold_img = "dt_cold.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + h_cold_img
        + " -B "
        + basedir
        + "/"
        + rah_img_cold
        + " -C "
        + basedir
        + "/"
        + i3_img
        + " --outfile="
        + basedir
        + "/dt_cold.img"
        + " --calc="
        + '"(A*B/C)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(dt_cold_img, gdal_calc)

    # Process: Raster Calculator (10)
    slop_img = "slop.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + basedir
        + "/"
        + dt_hot_img
        + " -B "
        + basedir
        + "/"
        + dt_cold_img
        + " -C "
        + basedir
        + "/"
        + st_hot1_img
        + " -D "
        + basedir
        + "/"
        + st_cold1_img
        + " --outfile="
        + basedir
        + "/slop.img"
        + " --calc="
        + '"(A-B)/(C-D)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(slop_img, gdal_calc)
else:
    # Process: Raster Calculator (10)
    slop_img = "slop.img"
    gdal_calc = (
        "python "
        + py_script_dir
        + "/gdal_calc.py -A "
        + dt_hot_img
        + " -B "
        + st_hot1_img
        + " -C "
        + st_cold1_img
        + " --outfile="
        + basedir
        + "/slop.img"
        + " --calc="
        + '"where((B-C)!=0,A/(B-C),0)" '
        + "--type=Float64"
        + " --overwrite --quiet --NoDataValue=-9999"
    )
    gdal_calc_func(slop_img, gdal_calc)

# Process: Raster Calculator (11)
intercept_img = "intercept.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + dt_hot_img
    + " -B "
    + basedir
    + "/"
    + st_hot1_img
    + " -C "
    + basedir
    + "/"
    + slop_img
    + " --outfile="
    + basedir
    + "/intercept.img"
    + " --calc="
    + '"(A-B*C)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(intercept_img, gdal_calc)

# Process: Raster Calculator (5)
n10_temp_img = "n10_temp.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + slop_img
    + " -B "
    + basedir
    + "/"
    + st_dem_img
    + " -C "
    + basedir
    + "/"
    + intercept_img
    + " --outfile="
    + basedir
    + "/n10_temp.img"
    + " --calc="
    + '"(A*B+C)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n10_temp_img, gdal_calc)

# Process: Raster Calculator (4)
n9_mem_img = "n9_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + st_img
    + " -B "
    + basedir
    + "/"
    + n10_temp_img
    + " --outfile="
    + basedir
    + "/n9_mem.img"
    + " --calc="
    + '"(A-B)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n9_mem_img, gdal_calc)

# Process: Raster Calculator (3)
air_d_img = "air_d.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n9_mem_img
    + " -B "
    + basedir
    + "/"
    + dem_img
    + " -C "
    + basedir
    + "/"
    + st_img
    + " --outfile="
    + basedir
    + "/air_d.img"
    + " --calc="
    + '"(349.467 * (pow(((A-0.0065*B)/A),5.26)/C))" '
    + "--type=Float64"
    + " --overwrite --quiet  --NoDataValue=-9999"
)
gdal_calc_func(air_d_img, gdal_calc)

# Process: Raster Calculator
h_img = "h.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + air_d_img
    + " -B "
    + basedir
    + "/"
    + n10_temp_img
    + " -C "
    + basedir
    + "/"
    + rahone_img
    + " --outfile="
    + basedir
    + "/h.img"
    + " --calc="
    + '"(A*'
    + str_Cp
    + '*B/C)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(h_img, gdal_calc)

# Process: Raster Calculator (2)
u_star_img = "u_star.img"
delete_file_w_path(basedir + "/" + u_star_img)
copyFile(u_star_ost_img, u_star_img)

## 11-rah

# Process: Raster Calculator
n5_temp_img = "n5_temp.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + h_img
    + " -B "
    + basedir
    + "/"
    + air_d_img
    + " -C "
    + basedir
    + "/"
    + u_star_img
    + " -D "
    + basedir
    + "/"
    + st_img
    + " --outfile="
    + basedir
    + "/n5_temp.img"
    + " --calc="
    + '"(where(A!=0,((-'
    + str_Cp
    + '*B*pow(C,3.0)*D)/(0.41*9.81*A)),1000))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n5_temp_img, gdal_calc)

# Process: Raster Calculator (4)
n12_z1_mem_img = "n12_z1_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n5_temp_img
    + " --outfile="
    + basedir
    + "/n12_z1_mem.img"
    + " --calc="
    + '"(where(A<0,(pow((1.0-16.0*('
    + str_z1
    + '/A)),0.5)),1))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n12_z1_mem_img, gdal_calc)

# Process: Raster Calculator (4)
n12_z2_mem_img = "n12_z2_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n5_temp_img
    + " --outfile="
    + basedir
    + "/n12_z2_mem.img"
    + " --calc="
    + '"(where(A<0,(pow((1.0-16.0*('
    + str_z2
    + '/A)),0.5)),1))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n12_z2_mem_img, gdal_calc)

# Process: Raster Calculator (3)
n11_z1_mem_img = "n11_z1_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n5_temp_img
    + " -B "
    + basedir
    + "/"
    + n12_z1_mem_img
    + " --outfile="
    + basedir
    + "/n11_z1_mem.img"
    + " --calc="
    + '"(where(A<0,(2.0*log((1.0+B)/2.0)),(-5.0*'
    + str_z1
    + '/A)))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n11_z1_mem_img, gdal_calc)

# Process: Raster Calculator (3)
n11_z2_mem_img = "n11_z2_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n5_temp_img
    + " -B "
    + basedir
    + "/"
    + n12_z2_mem_img
    + " --outfile="
    + basedir
    + "/n11_z2_mem.img"
    + " --calc="
    + '"(where(A<0,(2.0*log((1.0+B)/2.0)),(-5.0*'
    + str_z2
    + '/A)))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n11_z2_mem_img, gdal_calc)

# Process: Raster Calculator (2)
n25_mem_img = "n25_mem.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n5_temp_img
    + " --outfile="
    + basedir
    + "/n25_mem.img"
    + " --calc="
    + '"(where(A<0,pow(1.0-16.0*(200.0/A),0.25),1.0))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n25_mem_img, gdal_calc)

# Process: Raster Calculator (6)
n10_memory_img = "n10_memory.img"
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n5_temp_img
    + " -B "
    + basedir
    + "/"
    + n25_mem_img
    + " --outfile="
    + basedir
    + "/n10_memory.img"
    + " --calc="
    + '"where(A<0.0,(2.0*log((1.0+B)/2.0) + log((1.0 + pow(B,2.0))/2.0)-2.0*arctan(B)+ 0.5 * math.pi),(-5.0*2.0/A))" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(n10_memory_img, gdal_calc)

# Process: Raster Calculator (5)
u_star_o1_img = "u_star_o1.img"
delete_file_w_path(basedir + "/" + u_star_o1_img)
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + u200_img
    + " -B "
    + basedir
    + "/"
    + zo_img
    + " -C "
    + basedir
    + "/"
    + n10_memory_img
    + " --outfile="
    + basedir
    + "/"
    + u_star_o1_img
    + " --calc="
    + '"(A*0.41)/(log(200.0 /B)-C)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(u_star_o1_img, gdal_calc)
copy_shp_file("u_star_o1.img", "u_star.img")

# Process: Raster Calculator (8)
gdal_calc = (
    "python "
    + py_script_dir
    + "/gdal_calc.py -A "
    + basedir
    + "/"
    + n11_z1_mem_img
    + " -B "
    + basedir
    + "/"
    + n11_z2_mem_img
    + " -C "
    + basedir
    + "/"
    + u_star_o1_img
    + " --outfile="
    + basedir
    + "/rah.img"
    + " --calc="
    + '"('
    + str_aerod_res
    + '-B+A)/(0.41*C)" '
    + "--type=Float64"
    + " --overwrite --quiet --NoDataValue=-9999"
)
gdal_calc_func(rah_img, gdal_calc)

write_log(
    "Extracting the hot and cold pixels from the air_d, h, rah raster layers ~65% of run completed."
)
F12_4(air_d_img, h_img, rah_img, "X", "NONE")
interp(air_d_img, h_img, rah_img, "X")

cnt += 1  # 2

F13si(cnt)
F10it(cnt)
rah(cnt)
dt_hot_u_img = "dt_hot_u.img"
F12_4(air_d_img, h_img, rah_img, dt_hot_u_img, "NONE")

try:
    os.remove(basedir + "/" + "inPointFeature_h_one.shp")
except OSError:
    pass
copy_shp_file("inPointFeature_H.shp", "inPointFeature_h_one.shp")
try:
    os.remove(basedir + "/" + "inPointFeature_c_one.shp")
except OSError:
    pass
copy_shp_file("inPointFeature_C.shp", "inPointFeature_c_one.shp")
cnt += 1  # 4
interp(air_d_img, rah_img, "X", "X")
write_log(
    "Extracting the hot and cold pixels from the air_d, and rah raster layers ~ 75% of run completed."
)

## First iteration
cnt += 1  # 5
F13si(cnt)
F10it(cnt)
rah(cnt)
F12_4(air_d_img, h_img, rah_img, dt_hot_u_img, "NONE")

max_rah_old = 100
max_dt_hot_u_old = 100

max_rah, max_dt_hot_u = calc_convergence(max_rah_old, max_dt_hot_u_old, cnt)
write_log("Calculating the 24 hour ET ~97% of run completed.")
final_ET_calcs()
max_hot_pnt = check_hot_ET()

copy_shp_file("cold_pts_rn.shp", ls_image_name + "_cold_pts_rn.shp")
delete_shp_file("cold_pts_rn.shp")
copy_shp_file("hot_pts.shp", ls_image_name + "_hot_pts.shp")
delete_shp_file("hot_pts.shp")
delete_file_w_path(basedir + "/" + ls_image_name + "_NDVI.img")
os.rename(ndvi_img, ls_image_name + "_NDVI.img")
delete_file_w_path(basedir + "/" + ls_image_name + "_evapo_fr.img")
os.rename("evapo_fr.img", ls_image_name + "_evapo_fr.img")
delete_file_w_path(basedir + "/" + ls_image_name + "_ET_inst.img")
os.rename("et_inst.img", ls_image_name + "_ET_inst.img")
if seasonal:
    delete_file_w_path(basedir + "/" + ls_image_name + "_ET_24_raster.img")
    delete_file_w_path(basedir + "/" + ls_image_name + "_ETa_to_ETr.img")
    delete_file_w_path(basedir + "/" + ls_image_name + "_ET_24_raster_uncal.img")
    delete_file_w_path(basedir + "/" + ls_image_name + "_ETa_to_ETr_uncal.img")
if USER_UNCALIB == 0:
    os.rename("et_24_raster.img", ls_image_name + "_ET_24_raster.img")
    os.rename("ETa_to_ETr.img", ls_image_name + "_ETa_to_ETr.img")
else:
    os.rename("et_24_raster_uncal.img", ls_image_name + "_ET_24_raster_uncal.img")
    os.rename("ETa_to_ETr.img", ls_image_name + "_ETa_to_ETr_uncal.img")
copyFile(wind_img1, ls_image_name + "_" + wind_img1)
copyFile(etinst_ws_img1, ls_image_name + "_" + etinst_ws_img1)
copyFile(et24_ws_img1, ls_image_name + "_" + et24_ws_img1)
copyFile(dem_img, ls_image_name + "_" + dem_img)
copy_shp_file(aoi_file, ls_image_name + "_" + aoi_file_orig)
copy_shp_file(ws, ls_image_name + "_" + ws)
delete_shp_file(aoi_file2)
delete_shp_file(aoi_file3)

write_log("Model run is completed and 24 hour ET raster grid was generated.")

# A temporary AOI file is created (aoi_file_name)1.shp.  At the end of the run this file can be removed.
DEBUGFLAG = 0
#import pdb;pdb.set_trace()
if seasonal:
    copyFile(ls_basedir + "/" + ls_image_name + "_MTL.txt", basedir + "/" + ls_image_name + "_MTL.txt")
    delete_files_w_pattern(ls_basedir1, "*.*")
    delete_files_w_pattern(ls_basedir1 + "/gap_mask", "*.*")
if DEBUGFLAG == 0:
    del_tmp_files(basedir, ls_basedir)
#    if seasonal:
#        delete_files_w_pattern(basedir, ls_image_name +"*.*")
    copyFile(ls_basedir+"/"+ls_image_name+"_MTL.txt",basedir+"/"+ls_image_name+"_MTL.txt")
    copyFile(ls_basedir+"/"+ls_image_name+"_BQA.TIF",basedir+"/"+ls_image_name+"_BQA.TIF")

# collected = gc.collect()
# write_log("Garbage collector: collected %d objects." % (collected))
pf.close()
