# import warnings
# warnings.simplefilter(action='ignore', category=FutureWarning)
import argparse
import csip
import datetime
import geopandas
import glob
import logging
import os
import pytz
import runpy
from shapely.geometry import shape
import shutil
import math
import sys

sys.path.append("..")
# LAG 5-19-22 import send_email
# from AgroET_seasonal_luis import run_seasonal
import send_email
import gdal_utils
from imagery.main import Imagery
from AgroET_automatic import run_automatic
from Setup_AgroET_for_seasonal_VM_API import run_generate_seasonal_ET
from era5.era5 import ERA5
from pathlib import Path
from osgeo import ogr
from climate.main import Climate
from climate.cimis import Cimis
import logging
import psycopg2
import pdb
from AgroET_library import write_log

DEBUGFLAG = 1

logging.basicConfig(
    filename="reset.log", level=os.environ.get("LOGLEVEL", "DEBUG").upper()
)
imagery_cache_dir = "/reset"


def get_climate(station_ids, start_date, end_date):
    """Retrieve the data for the station with the given station_id"""
    conn = None
    db_host = "cache_db"
    db_port = 5432

    #    pdb.set_trace()
    try:
        conn = psycopg2.connect(
            "dbname=agroet user=agroet password='resetnumber1!' host={host} port={port}".format(
                host=db_host, port=db_port
            )
        )
    except psycopg2.OperationalError:
        logging.getLogger().error(
            f"Unable to connect to cache database on {db_host}:{db_port}"
        )

    if conn:
        c = conn.cursor()
        c.execute(
            """SELECT station_id, dt, eto, windrun FROM daily WHERE station_id IN %s AND dt >= %s AND dt <= %s ORDER BY station_id, dt """,
            (tuple(station_ids), start_date, end_date),
        )
        data = c.fetchall()
        #        c = conn.cursor()
        #        c.execute("SELECT station_id, dt, eto from daily where station_id=%s order by dt", (station_id,) )
        #        data = c.fetchall()
        write_log(data)
    return data


def date_to_mjd(yr_m_d, date_flag):
    """
    Modified function to convert a date to Julian Day.  It sets Dec 31, 1979 as date 0 by subtracting 2444238.5 from absolute julian date
    Algorithm from 'Practical Astronomy with your Calculator or Spreadsheet',
        4th ed., Duffet-Smith and Zwart, 2011.
    Parameters
    ----------
    month : int
        Month as integer, Jan = 1, Feb. = 2, etc.
    day : int
        Day
    year : int
        Year as integer. Years preceding 1 A.D. should be 0 or negative.
        The year before 1 A.D. is 0, 10 B.C. is year -9.
    date_flag == 0 (means it is year and DOY) or date_flag == 1 (means it is Year-Month-Day)
    Returns
    -------
    jd : int
        Julian Day
    Examples
    --------
    February 17, 1985 to Julian Day
    >>> date_to_jd(1985,2,17)
    1875
    """
    if date_flag == 0:
        year = int(yr_m_d[0:4])
        month = int(yr_m_d[4:6])
        day = int(yr_m_d[6:8])
    # LAG 1-3-22        month, day, year = gregorian(int(yr_m_d[0:4]),int(yr_m_d[4:7]))
    elif date_flag == 2:
        line_list = yr_m_d.split("/")
        month = int(line_list[0])
        day = int(line_list[1])
        year = int(line_list[2])
    elif date_flag == 3:
        line_list = yr_m_d.split("-")
        year = int(line_list[0])
        month = int(line_list[1])
        day = int(line_list[2])
    else:
        line_list = yr_m_d.split("-")
        month = int(line_list[0])
        day = int(line_list[1])
        year = int(line_list[2])

    if month == 1 or month == 2:
        yearp = year - 1
        monthp = month + 12
    else:
        yearp = year
        monthp = month
    # this checks where we are in relation to October 15, 1582, the beginning
    # of the Gregorian calendar.
    if (
        (year < 1582)
        or (year == 1582 and month < 10)
        or (year == 1582 and month == 10 and day < 15)
    ):
        # before start of Gregorian calendar
        B = 0
    else:
        # after start of Gregorian calendar
        A = math.trunc(yearp / 100.0)
        B = 2 - A + math.trunc(A / 4.0)
    if yearp < 0:
        C = math.trunc((365.25 * yearp) - 0.75)
    else:
        C = math.trunc(365.25 * yearp)
    D = math.trunc(30.6001 * (monthp + 1))
    jd = B + C + D + day + 1720994.5
    # Set julian date 1 as January 1st 1980 by subtracting 2444238.5
    i_jd = int(jd - 2444238.5)
    return i_jd


def mjd_to_date(jd, flag):
    """
    Modified convert Julian Day to date. It sets Dec 31, 1979 as date 0 by substracting 2444238.5 from absolute julian date
    Algorithm from 'Practical Astronomy with your Calculator or Spreadsheet',
        4th ed., Duffet-Smith and Zwart, 2011.
    Parameters
    ----------
    jd : float
        Julian Day
    Returns
    -------
    year : int
        Year as integer. Years preceding 1 A.D. should be 0 or negative.
        The year before 1 A.D. is 0, 10 B.C. is year -9.
    month : int
        Month as integer, Jan = 1, Feb. = 2, etc.
    day : int
        Day
    Examples
    --------
    Convert Julian Day 2446113.75 to year, month, and day.
    >>> jd_to_date(185)
    (1985, 2, 17)

    """
    jd = jd + 0.5 + 2444238.5
    F, I = math.modf(jd)
    I = int(I)
    A = math.trunc((I - 1867216.25) / 36524.25)
    if I > 2299160:
        B = I + 1 + A - math.trunc(A / 4.0)
    else:
        B = I
    C = B + 1524
    D = math.trunc((C - 122.1) / 365.25)
    E = math.trunc(365.25 * D)
    G = math.trunc((C - E) / 30.6001)
    day = int(C - E + F - math.trunc(30.6001 * G))
    if G < 13.5:
        month = G - 1
    else:
        month = G - 13
    if month > 2.5:
        year = D - 4716
    else:
        year = D - 4715
    if flag == 0:
        y_m_d = str(month).zfill(2) + "-" + str(day).zfill(2) + "-" + str(year)
    elif flag == 1:
        y_m_d = str(year) + str(month).zfill(2) + str(day).zfill(2)
    elif flag == 2:
        doy = cal_to_doy(year, month, day)
        y_m_d = str(year) + str(doy).zfill(3)
    elif flag == 3:
        y_m_d = str(year) + "-" + str(month).zfill(2) + "-" + str(day).zfill(2)
    return y_m_d


def delete_shp_file(shp_name):
    if os.path.exists(shp_name):
        driver = ogr.GetDriverByName("ESRI Shapefile")
        driver.DeleteDataSource(shp_name)


def copyFile(src, dest):
    try:
        shutil.copyfile(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        write_log("Error source and destination are the same : %s" % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        write_log(
            "Error source file does not exist: ", e.strerror, " source file is ", src
        )


def copy_shp_file(basedir, basedir1, base_name):
    delete_shp_file(basedir1 + "/" + base_name)
    itemList = os.listdir(basedir)
    for item in itemList:
        if base_name[:-4] == item[:-4]:
            #            isFile = os.path.isfile(basedir1 + '/' + base_name[:-4] + item[-4:])
            #            if(isFile):
            #            write_log("copying shapefile ",item)
            copyFile(basedir + "/" + item, basedir1 + "/" + base_name[:-4] + item[-4:])


#            else:
#                write_log("shapefile already exists so will not copy ",basedir1 + '/' + base_name[:-4] + item[-4:])
# write_log('file copied is ', basedir + '/' + new_base_name[:-4] + item[-4:])
#            try:
#                os.remove(basedir1 + '/' + base_name[:-4] + item[-4:])
#            except OSError:
# LAG 8-12                if DEBUGFLAG == 1:
# LAG 8-12                write_log('!!!!!!!! could not remove file ', basedir + '/' + new_base_name[:-4] + item[-4:])
#                pass
#            copyFile(basedir + '/' + item, basedir1 + '/' + base_name[:-4] + item[-4:])


# def extract_daily_climate(aoi, start_date, end_date, metadata, scene_proj):
def extract_daily_climate(aoi, start_date, end_date, metadata):
    climate = Climate(aoi, logger=logging.getLogger())
    station_list = climate.extract_climate_range(start_date, end_date)
    return station_list


def extract_stations(aoi, scene_proj):
    #    write_log("calling Climate with the aoi")
    climate = Climate(aoi, logger=logging.getLogger())
    #    write_log("calling climnate.extract_stations")
    station_features = climate.extract_stations()
    #    write_log("After extract station features ")
    #    write_log(" station features ", station_features)
    gdf = geopandas.GeoDataFrame.from_features(station_features)
    #    write_log(" After gdf ")
    gdf.geometry = gdf.geometry.apply(shape)
    #    write_log(" After gdf.geometry ")
    gdf.crs = "epsg:4326"
    gdf.to_file("daily_climate.shp")
    #    write_log("After create climate.shp")
    scene_climate_df = gdf.to_crs(scene_proj)
    scene_climate_df.to_file("ws.shp")
    #    write_log("After create ws.shp")
    return


def extract_climate(aoi, metadata, scene_proj):
    climate = Climate(aoi, logger=logging.getLogger())
    extraction_date = datetime.datetime.strptime(
        "{0} {1}".format(metadata["DATE_ACQUIRED"], metadata["SCENE_CENTER_TIME"][1:6]),
        "%Y-%m-%d %H:%M",
    )
    utc = pytz.utc
    extraction_date = utc.localize(extraction_date)

    station_features = climate.extract_climate(extraction_date)
    #    write_log("before station features ")
    #    station_features = climate.extract_climate1(extraction_date)
    #    write_log(" station features ", station_features)
    gdf = geopandas.GeoDataFrame.from_features(station_features)
    gdf.geometry = gdf.geometry.apply(shape)
    gdf.crs = "epsg:4326"
    #    gdf.to_file("climate.shp")
    scene_climate_df = gdf.to_crs(scene_proj)
    scene_climate_df.to_file("ws.shp")
    return station_features


def run_seasonal_automatic(
    start_date_str,
    end_date_str,
    aoi,
    row,
    path,
    project,
    flat_dem,
    generate_daily_weather,
    weather_data,
    ndvi,
):
    write_log("Inside Seasonal Automatic")
    write_log("Inside the AgroET_seasonal_luis trying to run seasonal")
    write_log("Season Start Date ", start_date_str)
    write_log("end date is ", end_date_str)
    write_log("row ", row)
    write_log("path ", path)
    write_log("project ", project)
    write_log("generate_daily_weather ", generate_daily_weather)
    write_log("NDVI ", ndvi)

    #    import pdb; pdb.set_trace()

    MJD_IMGD_SD = date_to_mjd(start_date_str, 3) - 32
    #    write_log("Converting start date to MDJ ", MJD_IMGD_SD)
    IMGD_SD = str(mjd_to_date(MJD_IMGD_SD, 3))
    #    write_log("Imagery Download Start Date ", IMGD_SD)
    MJD_IMGD_ED = date_to_mjd(end_date_str, 3) + 32
    #    write_log("Converting end date to MDJ ", MJD_IMGD_ED)
    IMGD_ED = str(mjd_to_date(MJD_IMGD_ED, 3))
    #    write_log("Imagery Download End Date ", IMGD_ED)
    #    write_log("project name is !!!! ", project)
    if project == None:
        #        write_log("No project defined - STOPPING RUN")
        exit(1)
    # LAG 6-20-22    basedir0 = '/tmp/csip/python/seasonal'
    basedir0 = "/reset/seasonal_projects"
    basedir = os.path.join(basedir0, str(project))
    if DEBUGFLAG == 1:
        write_log("Basedir is ", basedir)
    if not os.path.isdir(basedir):
        write_log("Basedir does NOT exist it will be created")
        os.makedirs(basedir)
    else:
        if DEBUGFLAG == 1:
            write_log("Basedir exist")

    imagery = Imagery(force_usgs=False)
    #    write_log("returned from Imagery!!")
    #    write_log("right BEFORE converting start_date to a date")
    start_date = datetime.datetime.strptime(IMGD_SD, "%Y-%m-%d").date()
    #    start_date = datetime.datetime.strptime(start_date_str, "%Y-%m-%d").date()

    #    write_log("right BEFORE converting end_date to a date")
    end_date = datetime.datetime.strptime(IMGD_ED, "%Y-%m-%d").date()
    #    end_date = datetime.datetime.strptime(end_date_str, "%Y-%m-%d").date()

    #    write_log("right BEFORE geopandas.readfile(aoi)")
    df = geopandas.read_file(aoi)
    #    write_log("right AFTER geopandas.readfile(aoi)")
    # Should be single feat
    df = df.to_crs({"init": "epsg:4326"})
    aoi_geom = df.loc[0].geometry
    centroid = [aoi_geom.centroid.x, aoi_geom.centroid.y]
    #    write_log("right before import pdb")
    #    import pdb; pdb.set_trace()
    #    write_log("right after import pdb")
    #   write_log('right after pdb.set_trace')
    script = "AgroET_automatic.py"
    #    write_log("right before setting the script_path")
    script_path = os.path.join("/tmp/csip/python", script)
    #    write_log("right before checking if the script path exists ", script)
    if not os.path.exists(script_path):
        # local development
        rundir = "/home/dave/work/reset/csip-reset/src/java/python"
        script_path = os.path.join(rundir, script)

    write_log("Seasonal run started!!!")

    # Get images
    row = None  # NOTE THIS DISABLES USING ROW and PATH
    #    import pdb; pdb.set_trace();
    if row:
        #        write_log(
        #            "finding the images based on row and path - calling imagery.get_products_row_path"
        #        )
        prods = imagery.get_products_row_path(row, path, start_date, end_date)
    else:
        #        write_log("finding the images based on centroid")
        #        import pdb; pdb.set_trace()
        #        prods = imagery.get_products(aoi_geom.centroid.x, aoi_geom.centroid.y, centroid, start_date, end_date)
        prods = imagery.get_products(centroid, start_date, end_date)
    #        write_log("imagery returned the following products ",prods)
    cwd = os.getcwd()
    if not prods:
        write_log("No LS images found")
        raise Exception("No LS images found - exception")
    for image_data in prods:
        #        write_log("Inside image_data loop !!!!")
        #        write_log("image date ",image_data)
        #        current_wd = os.getcwd()
        #        write_log("The present working directory is ", cwd)
        #        write_log("image_data datetime ", image_data["temporalCoverage"]["endDate"])
        #        img_date1 = str(image_data["datetime"])
        img_date1 = str(image_data["temporalCoverage"]["endDate"])
        img_date = img_date1[0:10]
        #        write_log("Converted acquisition date ", img_date)
        #        write_log("entityId", image_data["entityId"])
        LS_name0 = image_data["entityId"]
        LS_name = LS_name0[:-5]
        #        write_log("Name of Landsat is ", LS_name)
        AgroET_result0 = LS_name + "_ET_24_raster.img"
        AgroET_result_cloudy = LS_name + "_cloudy_no_run"
        AgroET_result_albedo = LS_name + "_albedo_no_run"
        AgroET_result_cold_points = LS_name + "_cold_points_no_run"
        AgroET_result_no_ET = LS_name + "_no_ET_run"

        #        write_log("current working directory is ", cwd)
        AgroET_result = cwd + "/" + AgroET_result0
        AgroET_MTL = cwd + "/" + LS_name + "_MTL.txt"
        #        AgroET_BQA = cwd + '/' + LS_name + '_BQA.TIF'
        #        AgroET_B1 = cwd + '/image/' + LS_name + '_B1.TIF'

        #        write_log("Source of result file ", AgroET_result)

        result_file = basedir + "/" + AgroET_result0
        result_file_cloudy = basedir + "/" + AgroET_result_cloudy
        result_file_albedo = basedir + "/" + AgroET_result_albedo
        result_file_cold_points = basedir + "/" + AgroET_result_cold_points
        result_file_no_ET = basedir + "/" + AgroET_result_no_ET
        result_MTL = basedir + "/" + LS_name + "_MTL.txt"
        #        result_BQA = basedir + '/' + LS_name + "_BQA.TIF"
        #        result_B1 = basedir + '/' + LS_name + "_B1.TIF"

        #       os.chdir(basedir)

        copy_shp_file(cwd, basedir, aoi)
        copy_shp_file(cwd, basedir, "ws.shp")
        #        write_log("Destination of result file", result_file)
        #        import pdb;pdb.set_trace()
        isFile = os.path.exists(result_file)
        #        write_log("Checking if outfile ",result_file," already exists and if so skip running AgroET ", isFile)
        #        path = Path(result_file)
        #        if path.is_file():
        # import pdb; pdb.set_trace();
        if isFile:
            write_log(
                "The AgroET result file ",
                result_file,
                " EXISTS so NO NEED TO RUN THE AgroET MODEL",
            )
        elif os.path.exists(result_file_cloudy):
            write_log(
                "Cloudy date for ", LS_name, " therefore the AgroET model will NOT RUN"
            )
        elif os.path.exists(result_file_albedo):
            write_log(
                "Albedo out of range date for ",
                LS_name,
                " therefore the AgroET model will NOT RUN",
            )
        elif os.path.exists(result_file_no_ET):
            write_log(
                "No ET or ET was zero in all weather stations date for ",
                LS_name,
                " therefore the AgroET model will NOT RUN",
            )
        else:
            write_log(
                "The AgroET result file ",
                result_file,
                " DOES NOT !!! EXIST so NEED TO RUN THE AgroET MODEL",
            )

            #        write_log("image_data ",image_data)
            # Make sure AOI is completely contained.
            #        spatial_footprint_geom = image_data["spatialFootprint"]

            spatial_footprint_geom = image_data["spatialBounds"]
            geom = shape(spatial_footprint_geom)
            #           import pdb;pdb.set_trace()
            if geom.contains(aoi_geom) and (isFile == False):
                img_date1 = str(image_data["temporalCoverage"]["endDate"])
                img_date = img_date1[0:10]
                write_log("Running AgroET for ", img_date)
                # Run automatic for this image
                try:
                    write_log(
                        "Right before calling run_automatic - line 513 AgroET_seasonal_automatic"
                    )
                    value = run_automatic(
                        img_date,
                        aoi,
                        row,
                        path,
                        ls7_disable_fill_gap=0,
                        user_uncalib=0,
                        max_cloud_perc=25,
                        flat_dem=flat_dem,
                        #                        flat_dem=1,
                        weather_data=weather_data,
                        seasonal=1,
                    )
                except Exception as e:
                    import pdb

                    pdb.set_trace()
                    # LAG Commented the raise statement below which made the code stop when a cloudy image was used
                    #                    raise
                    value = -1
                    write_log(
                        "run failed !!!! - captured in AgroET_seasonal_automatic line 530 ",
                        str(e),
                    )
                    if str(e) == "Exit code 5":
                        write_log(
                            "writing file ",
                            result_file_cloudy,
                            " since run failed due to too many clouds",
                        )
                        cf = open(result_file_cloudy, "a+")
                        cf.write("Scene is cloudy and AgroET will not run")
                        cf.close()
                    elif str(e) == "Exit code 6":
                        write_log(
                            "writing file ",
                            result_file_albedo,
                            " since run failed due to Albedo Coef out of range",
                        )
                        cf = open(result_file_albedo, "a+")
                        cf.write(
                            "Scene has Albedo that is out of range and AgroET will not run"
                        )
                        cf.close()
                    elif str(e) == "Exit code 7":
                        write_log(
                            "No prepared or available downloads at the image download URL"
                        )
                    elif str(e) == "Exit code 8":
                        write_log(
                            "writing file ",
                            result_file_cold_points,
                            " since run failed due to no cold points identified",
                        )
                        cf = open(result_file_cold_points, "a+")
                        cf.write(
                            "Scene has no cold points identified and AgroET will not run"
                        )
                        cf.close()
                    else:
                        write_log(
                            "AgroET failed for unknown error (not trapped by the known exceptions) ",
                            e,
                        )
                        #                        cf = open(result_file_no_ET, "a+")
                        #                        cf.write(
                        #                            "AgroET run failed"
                        #                        )
                        #                        cf.close()
                        exit(1)
                write_log("value return from run_automatic is ", value)
                if value == 10:
                    write_log(
                        "writing file ",
                        result_file_no_ET,
                        " since run failed due to no ET weather data",
                    )
                    cf = open(result_file_no_ET, "a+")
                    cf.write(
                        "No ET weather data is available or ET is zero for this date.  Therefore, AgroET will not run"
                    )
                    cf.close()

        #        import pdb;pdb.set_trace()
        write_log("AgroET runs for individual image date finished successfully!!!")
        isDirectory = os.path.isdir(basedir)
        write_log("basedir ", basedir, " isDirectory ", isDirectory, " isFile ", isFile)
        if isDirectory and (isFile == False):
            write_log("AgroET_result ", AgroET_result, " result_file ", result_file)
            write_log("os.path.isfile(AgroET_result)", os.path.isfile(AgroET_result))
            if os.path.isfile(AgroET_result):
                write_log(
                    "copying the result file to the project directory ",
                    AgroET_result,
                    result_file,
                )
                shutil.copyfile(AgroET_result, result_file)
                write_log("after copyfile agroet_results")
                #                import pdb;pdb.set_trace()
                shutil.copyfile(AgroET_MTL, result_MTL)
                write_log("after copyfile agroet_MTL")
                files = glob.iglob(os.path.join(basedir, "ws.*"))
                for file in files:
                    if os.path.isfile(file):
                        shutil.copy2(file, project)
                write_log("after copyfile ws shapefile ")
        #                shutil.copyfile(AgroET_BQA, result_BQA)
        #                shutil.copyfile(AgroET_B1, result_B1)
        elif isFile == True:
            write_log(
                "result file already exists in the project directory so skip copying it"
            )
        else:
            write_log("directory not found so it will be created")
            os.mkdir(basedir)
            #            write_log(
            #                "copying the result file to the project directory ",
            #                AgroET_result,
            #                result_file,
            #            )
            if os.path.isfile(result_file):
                shutil.copyfile(AgroET_result, result_file)
    #        write_log("done with run for ", LS_name, " for date ", img_date)
    #        for x in range(3): write_log()

    write_log("generating the list of ws inside the AOI")
    # import pdb; pdb.set_trace()

    image_data1 = prods[0]
    image_name1 = image_data1["id"]
    # LAG 7-18 - if get imagery does not work    image_name1 = 'LE70420352021346LGN00'
    #    b2_path = basedir + '/' + image_name1[:-5] + '_B2.TIF'
    #    write_log("b2 path is ", b2_path)
    #    write_log("dem file is ", cwd + "/dem.tif")
    proj_file = cwd + "/dem.tif"
    #    import pdb; pdb.set_trace();
    isFile = os.path.exists(proj_file)
    if isFile == False:
        proj_file = result_file
    metadata = imagery.get_metadata(image_data1["id"])
    # LAG 7-18 - if get imagery does not work    metadata = imagery.get_metadata(image_name1)
    #    write_log("metadata ", metadata)
    if os.path.isfile(proj_file):
        result2 = gdal_utils.gdalinfo(proj_file)
    else:
        write_log("File being use to determine the projection does not exit", proj_file)
        exit(11)
    write_log("result2[proj4]", result2["proj4"], " AgroET_seasonal_automatic.py 554")
    write_log("extract daily climate - AgroET_seasonal_automatic 493", weather_data)
    #    import pdb; pdb.set_trace();
    if weather_data == 4:
        era5 = ERA5()
        # import pdb; pdb.set_trace();
        write_log("Before calling extract era seasonal")
        era5_extract = era5.extract_era5_seasonal(
            LS_name, aoi, start_date, end_date, "/reset/seasonal_projects/" + project
        )
        write_log("After calling extract era seasonal")
        climate = 0  # initializing climate so it is defined as zero
    else:
        write_log("before extract daily climate")
        climate = extract_daily_climate(aoi, start_date, end_date, metadata)
        #    write_log("climate is ", climate)

        #    write_log("Extracting weather stations ")
        extract_stations(aoi, result2["proj4"])
        #    extract_stations(aoi, b2["proj4"])
    write_log("After Extracting weather stations cwd, basedir ", cwd, basedir)
    copy_shp_file(cwd, basedir, "ws.shp")
    #    import pdb; pdb.set_trace();
    MJD_Season_SD = date_to_mjd(str(start_date), 3) + 32
    write_log("Converting Season start date to MDJ + 32", start_date, MJD_Season_SD)
    Mod_Season_SD = str(mjd_to_date(MJD_Season_SD, 3))
    write_log("Season Start Date ", Mod_Season_SD)
    MJD_Season_ED = date_to_mjd(str(end_date), 3) - 32
    write_log("Converting Season end date to MDJ - 32", end_date, MJD_Season_ED)
    Mod_Season_ED = str(mjd_to_date(MJD_Season_ED, 3))
    write_log("Season End Date ", Mod_Season_ED)
    ET_cum_season = (
        basedir
        + "/ET_seasonal_exact_dates_"
        + Mod_Season_SD
        + "_"
        + Mod_Season_ED
        + ".img"
    )

    #    import pdb; pdb.set_trace();
    ET_NDVI_cum_season = (
        "ET_seasonal_NDVI_exact_dates_" + Mod_Season_SD + "_" + Mod_Season_ED + ".img"
    )
    NDVI_call = (
        "PYTHONPATH=/tmp/csip/python python3 /tmp/csip/python/seasonal/NDVI/NDVI_VM.py --project "
        + basedir
        + " "
        + str(start_date)
        + " "
        + str(end_date)
    )
    write_log(NDVI_call)
    os.system(NDVI_call)
    NDVI_Ratio_call = (
        "PYTHONPATH=/tmp/csip/python python3 /tmp/csip/python/seasonal/NDVI/NDVI_RATIO_VM.py --project "
        + basedir
    )
    write_log(NDVI_Ratio_call)
    os.system(NDVI_Ratio_call)

    write_log("ET_cum_season ", ET_cum_season)
    isFile = os.path.exists(ET_cum_season)
    write_log(
        "The return of checking if the seasonal file exists with isFile is ", isFile
    )
    if isFile:
        write_log(
            "The AgroET seasonal file ",
            ET_cum_season,
            " EXISTS so NO NEED TO RUN THE AgroET SEASONAL MODEL",
        )
        #        import pdb; pdb.set_trace();
        isFile1 = os.path.exists(ET_NDVI_cum_season)
        if isFile1:
            write_log(
                "The AgroET seasonal RATIO file ",
                ET_NDVI_cum_season,
                " EXISTS so NO NEED TO RUN THE AgroET SEASONAL RATIO MODEL",
            )
            exit(0)
    if isFile is False or isFile1 is False:
        write_log("Calling run_generate_seasonal_ET with basedir ", basedir)
        os.chdir(basedir)
        # import pdb; pdb.set_trace()
        run_generate_seasonal_ET(
            start_date,
            end_date,
            basedir,
            aoi,
            generate_daily_weather,
            weather_data,
            ndvi,
            climate,
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Run the AgroET model for a date and an AOI."
    )
    parser.add_argument(
        "start_date",
        metavar="start_date",
        type=str,
        help="Start date in YYYY-MM-DD format",
    )

    parser.add_argument(
        "end_date",
        metavar="end_date",
        type=str,
        help="End date in YYYY-MM-DD format",
    )
    parser.add_argument(
        "--aoi",
        type=str,
        #        default="aoi.shp",
        help="No AOI shafile name",
    )
    parser.add_argument(
        "--no-flat-dem", action="store_true", help="Do not use a flat DEM."
    )
    parser.add_argument("--row", type=str, default=None, help="row in DDD format")
    parser.add_argument("--path", type=str, default=None, help="path in DDD format")
    parser.add_argument(
        "--max-cloud-perc",
        type=int,
        default=25,
        help="Do not process the image if the cloudcover is greater than this",
    )
    parser.add_argument(
        "--project", type=str, default=None, help="project name for seasonal run"
    )
    parser.add_argument(
        "--generate_daily_weather",
        action="store_true",
        help="Flag to determine if daily weather data will be generated or not",
    )
    parser.add_argument(
        "--weather-data",
        type=int,
        default=1,
        help="1. Use either weather station or gridded data (default); 2. Use weather station data ONLY; 3. Use gridded data ONLY",
    )
    parser.add_argument(
        "--use-gridded-data",
        action="store_true",
        help="Flag to determine if daily weather data will be based on gridded data",
    )
    parser.add_argument(
        "--debug",
        type=int,
        default=0,
        help="0. Default do not print messages; 1. Print debug messages",
    )
    parser.add_argument(
        "--ndvi",
        action="store_true",
        help="Flag to determine if the seasonal NDVI corrected ET will be generated.  It takes a significant amount of time.",
    )

    args = parser.parse_args()
    admin_site = os.getenv(
        "admin_url",
        "http://www.irrigator.co/?next=/reset_admin/&workspace={0}".format(os.getcwd()),
    )
    try:
        run_seasonal_automatic(
            args.start_date,
            args.end_date,
            args.aoi,
            args.row,
            args.path,
            args.project,
            args.no_flat_dem,
            args.generate_daily_weather,
            args.weather_data,
            args.ndvi,
        )
        # all finished, send email
        try:
            send_email.send_results(
                "finished run",
                "Automatic run finished. Name is {0}. Workspace is {1}".format(
                    args.prefix or "", os.getcwd()
                ),
            )
        except Exception as e:
            write_log(str(e))
    except Exception as e:
        try:
            send_email.send(
                "Error in run",
                "Automatic run failed. Workspace is {0}. Error is {1}".format(
                    os.getcwd(), e
                ),
            )
            raise
        except Exception as e:
            write_log(str(e))
# LAG Commented this statement which made the code stop when a cloudy image was used
#            raise
