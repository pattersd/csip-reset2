from __future__ import print_function

# This must be the first statement before other statements.
# You may only put a quoted or triple quoted string,
# Python comments, other future statements, or blank lines before the __future__ line.

try:
    import __builtin__
except ImportError:
    # Python 3
    import builtins as __builtin__

from osgeo import gdal
from osgeo import osr
from osgeo import ogr
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
# import csip
import numpy as np
import math
import subprocess
import rasterio
import rasterio.features
import json, logging, re, os, sys, glob, requests, shutil, subprocess, urllib
import xml.etree.ElementTree as ET
import psycopg2
import psycopg2.extras
from rasterstats.utils import stats_to_csv
from rasterstats import zonal_stats
import osgeo.ogr
import pdb
from os import path
import datetime
from datetime import date, timedelta
from AgroET_Seasonal import Generate_AgroET_Seasonal_ET
from era5.era5 import ERA5
from climate.main import Climate
from climate.cimis import Cimis
from AgroET_library import write_log

DEBUGFLAG = 1
#AgroET_Setup_Seasonal_file = '/tmp/csip/python/seasonal/AgroET_setup_image_and_AgroET_for_seasonal_input.txt'
#if not os.path.exists(AgroET_Setup_Seasonal_file):
#    raise Exception('AgroET input file',AgroET_Setup_Seasonal_file,'does not exist.  You need to create it !!!!')
#Conversion from ETo to ETr
ETo_to_ETr = 1.2
#Conversion from km to miles as applied to Wind Run
km_to_miles = 0.621371
#Invert Distance Weighting weight
idw_power = 2.0
# Raster layers cell size
cellSize = 30

def jul_to_cal(julian_day):
#    str_jul_day = YEAR + str(julian_day)
    str_jul_day = str(julian_day)
    str_cal_day = str(datetime.datetime.strptime(str_jul_day, '%%Yj').date())
    return (str_cal_day)


def mjd_to_date(jd):
    """
    Modified convert Julian Day to date. It sets Dec 31, 1979 as date 0 by substracting 2444238.5 from absolute julian date
    Algorithm from 'Practical Astronomy with your Calculator or Spreadsheet',
        4th ed., Duffet-Smith and Zwart, 2011.
    Parameters
    ----------
    jd : float
        Julian Day
    Returns
    -------
    year : int
        Year as integer. Years preceding 1 A.D. should be 0 or negative.
        The year before 1 A.D. is 0, 10 B.C. is year -9.
    month : int
        Month as integer, Jan = 1, Feb. = 2, etc.
    day : int
        Day
    Examples
    --------
    Convert Julian Day 2446113.75 to year, month, and day.
    >>> jd_to_date(185)
    (1985, 2, 17)

    """
    jd = jd + 0.5 + 2444238.5
    F, I = math.modf(jd)
    I = int(I)
    A = math.trunc((I - 1867216.25) / 36524.25)
    if I > 2299160:
        B = I + 1 + A - math.trunc(A / 4.)
    else:
        B = I
    C = B + 1524
    D = math.trunc((C - 122.1) / 365.25)
    E = math.trunc(365.25 * D)
    G = math.trunc((C - E) / 30.6001)
    day = int(C - E + F - math.trunc(30.6001 * G))
    if G < 13.5:
        month = G - 1
    else:
        month = G - 13
    if month > 2.5:
        year = D - 4716
    else:
        year = D - 4715
    m_d_y=str(month).zfill(2) + "-" + str(day).zfill(2) + "-" + str(year)
    return m_d_y


def pad_date(date,divider):
    if (divider == "-"):
        #        month, day, year = gregorian(int(yr_m_d[0:4]),int(yr_m_d[4:7]))
        m, d, y = date.split("-")
        month = int(m)
        day = int(d)
        year = int(y)
    elif (divider == "/"):
        #        month, day, year = gregorian(int(yr_m_d[0:4]),int(yr_m_d[4:7]))
        m, d, y = date.split("/")
        month = int(m)
        day = int(d)
        year = int(y)
    else:
        write_log('incorrect divider provided ')
        exit(1)
    return(str(month).zfill(2),str(day).zfill(2),str(year))


def date_to_mjd(yr_m_d):
    """
    Modified function to convert a date to Julian Day.  It sets Dec 31, 1979 as date 0 by subtracting 2444238.5 from absolute julian date
    Algorithm from 'Practical Astronomy with your Calculator or Spreadsheet',
        4th ed., Duffet-Smith and Zwart, 2011.
    Parameters
    ----------
    month : int
        Month as integer, Jan = 1, Feb. = 2, etc.
    day : int
        Day
    year : int
        Year as integer. Years preceding 1 A.D. should be 0 or negative.
        The year before 1 A.D. is 0, 10 B.C. is year -9.
    date_flag == 0 (means it is year and DOY) or date_flag == 1 (means it is Year-Month-Day)
    Returns
    -------
    jd : int
        Julian Day
    Examples
    --------
    February 17, 1985 to Julian Day
    >>> date_to_jd(1985,2,17)
    1875
    """
    month = int(yr_m_d.strftime("%m"))
    day = int(yr_m_d.strftime("%d"))
    year = int(yr_m_d.strftime("%Y"))

    if month == 1 or month == 2:
        yearp = year - 1
        monthp = month + 12
    else:
        yearp = year
        monthp = month
    # this checks where we are in relation to October 15, 1582, the beginning
    # of the Gregorian calendar.
    if ((year < 1582) or
            (year == 1582 and month < 10) or
            (year == 1582 and month == 10 and day < 15)):
        # before start of Gregorian calendar
        B = 0
    else:
        # after start of Gregorian calendar
        A = math.trunc(yearp / 100.)
        B = 2 - A + math.trunc(A / 4.)
    if yearp < 0:
        C = math.trunc((365.25 * yearp) - 0.75)
    else:
        C = math.trunc(365.25 * yearp)
    D = math.trunc(30.6001 * (monthp + 1))
    jd = B + C + D + day + 1720994.5
    # Set julian date 1 as January 1st 1980 by subtracting 2444238.5
    i_jd = int(jd - 2444238.5)
    return i_jd


def doy_to_cal(jd1,flag):
    # if flag is 0 then do m-d-yr otherwise do ymd with no -
    jd = str(jd1)
    # initializing day number
    day_num = jd[4:7]
    # adjusting day num
    day_num.rjust(3 + len(day_num), '0')
    # Initialize year
    year = int(jd[0:4])
    # Initializing start date
    strt_date = date(int(year), 1, 1)
    # converting to date
    res_date = strt_date + timedelta(days=int(day_num) - 1)
    if (flag == 0):
        res = res_date.strftime("%m-%d-%Y")
    elif (flag == 1):
        res = res_date.strftime("%Y%m%d")
    elif (flag == 2):
        res = res_date.strftime("%Y-%m-%d")
    else:
        write_log("Wrong Flag was used flag should be 0 or 1", flag)
        exit(1)
# printing result
    return res


def check_directory_exists(directory_path, error_text):
    dir_exists = path.exists(directory_path)
    if (dir_exists == 0):
        write_log(error_text, directory_path,' does not exist !!!!')
        exit(0)


def copyFile(src, dest):
    try:
        shutil.copyfile(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        write_log('Error source and destination are the same : %s' % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        write_log('Error source file does not exist: ', e.strerror, ' source file is ', src)


def copy_shp_file(base_name, new_base_name, basedir):
    delete_shp_file(new_base_name)
    itemList = os.listdir(basedir)
    for item in itemList:
        if base_name[:-4] == item[:-4]:
            try:
                os.remove(basedir + '/' + new_base_name[:-4] + item[-4:])
            except OSError:
                pass
            copyFile(basedir + '/' + item, basedir + '/' + new_base_name[:-4] + item[-4:])


def reproj_shp(ls_utm_z, shp_name, basedir):
    if DEBUGFLAG == 1:
        write_log("reprojecting shapefile ", shp_name," to UTM zone ", ls_utm_z, " in directory ", basedir)
    orig_shp = shp_name[:-4] + "_orig.shp"
    copy_shp_file(shp_name, orig_shp, basedir)
    delete_shp_file(shp_name)
    os.system(
        "ogr2ogr -t_srs EPSG:326"
        + str(ls_utm_z)
        + " "
        + basedir
        + "/"
        + shp_name
        + " "
        + basedir
        + "/"
        + orig_shp
    )


def reproj_rast(ls_utm_z, rast_name, basedir):
    orig_rast = rast_name[:-4] + "_orig." + rast_name[-3:]
    copyFile(basedir + "/" + rast_name, basedir + "/" + orig_rast)
    delete_file_w_path(basedir + "/" + rast_name)
    str_cellSize = str(cellSize)
    os.system(
        "gdalwarp -t_srs EPSG:326"
        + ls_utm_z
        + " -tr "
        + str_cellSize
        + " "
        + str_cellSize
        + " "
        + basedir
        + "/"
        + orig_rast
        + " "
        + basedir
        + "/"
        + rast_name
    )


def check_utm_prj(basedir, ls_utm_z, proj_file, file_name, f_type):
    if(f_type == 'shapefile'):
        check_file_exists(basedir, file_name[:-4] + '.prj', ' ',0,'Projection file (.prj) for ' +file_name+ ' is missing.')
        fproj = open(proj_file, "r")
        ln = fproj.readline()
        fproj.close()
        s2 = "Zone"
        start = ln.find(s2)
        if(start == -1):
            write_log(file_name,'Shapefile was not in UTM Coordinates.  \nIt was projected into the same UTM coordinates as the Landsat image!!!!')
            reproj_shp(ls_utm_z, file_name, basedir)
        utm_z = ln[start + 5:start + 7]
        if DEBUGFLAG == 1:
            write_log('Filename is ',file_name)
            write_log('Projection file line ', ln)
            write_log('start of UTM Zone ID ',start)
            write_log('Shapefile UTM ZONE is ', utm_z,' Landsat UTM ZONE is ',ls_utm_z)
        fproj.close()
        if( utm_z != ls_utm_z):
            reproj_shp(ls_utm_z, file_name, basedir)
            write_log(file_name,'Shapefile is in a different UTM zone than the Landsat image.\nIt was projected into the same UTM zone as the Landsat image!!!!')
    if(f_type == 'raster'):
        rast_utm_z = raster_utm_prj(basedir + '/' + file_name)
        if(rast_utm_z != ls_utm_z):
            reproj_rast(ls_utm_z, file_name, basedir)


def check_file_exists(basedir, file_name, f_type, ls_utm_zone, error_text):
    if DEBUGFLAG == 1:
        write_log(basedir, file_name, f_type, ls_utm_zone, error_text)
    file_exists = os.path.exists(basedir + '/' + file_name)
    if (file_exists == 0):
        write_log(error_text + basedir + '/' + file_name + ' does not exist !!!!')
        exit(0)
    if(f_type == 'raster'):
        check_utm_prj(basedir, ls_utm_zone, ' ', file_name, 'raster')
    if(f_type == 'shapefile'):
        check_utm_prj(basedir, ls_utm_zone, basedir + '/' + file_name[:-4] + ".prj", file_name, 'shapefile')


def shapefile_utm_prj(file_name):
    prjfile_name = file_name[:-4] + ".prj"
    write_log("prjfile_name is ",prjfile_name)
    if (os.path.isfile(prjfile_name)):
        fproj = open(prjfile_name, "r")
        ln = fproj.readline()
        fproj.close()
        s2 = "Zone"
        start = ln.find(s2)
        utm_z = ln[start + 5 : start + 7]
    else:
        write_log(prjfile_name, "DOES NOT EXIST")
        exit(10)
    return(utm_z)


def raster_utm_prj(prj_tif):
    check_file = os.path.isfile(prj_tif)
    if check_file == 1:
        write_log(prj_tif ,"exists ")
    else:
        write_log(prj_tif ,"DOES NOT exists setup_AgroET 368")
        exit(368)
    tif = gdal.Open(prj_tif, GA_ReadOnly)
    # set spatial reference and transformation
    targetprj1 = osr.SpatialReference(wkt=tif.GetProjection())
    targetprj = r"{}".format(targetprj1)
    start_prj = targetprj.find("zone")
    tif = None
    if(start_prj == -1):
        write_log('The UTM zone for ',prj_tif,'was not found. Therefore, it will try to be reprojected to the Landsat UTM zone!!!!')
        utm_prj = -1
    else:
        utm_prj = targetprj[start_prj + 5:start_prj + 7]
    if DEBUGFLAG == 1:
        write_log('utm_prj string is ',utm_prj)
    return(utm_prj)


def get_climate(aoi_lyr, station_ids, start_date, end_date):
    """Retrieve the data for the station with the given station_id"""
    conn = None
    db_host  = "cache_db"
    db_port = 5432
    try:
        conn = psycopg2.connect(
            "dbname=agroet user=agroet password='resetnumber1!' host={host} port={port}".format(
                host=db_host, port=db_port
            )
        )
    except psycopg2.OperationalError:
        logging.getLogger().error(
            f"Unable to connect to cache database on {db_host}:{db_port}"
        )

    if conn:
        c = conn.cursor()
        c.execute(
            """SELECT station_id, dt, eto, windrun FROM daily WHERE station_id IN %s AND dt >= %s AND dt <= %s ORDER BY station_id, dt """,
            (tuple(station_ids), start_date, end_date))
        data = c.fetchall()
    else:
        data = extract_daily_climate(aoi_lyr, start_date, end_date, metadata)
    return data


def create_weather_daily_csv(weather_data, basedir, ws_path, jul_start, jul_end,aoi_lyr):
    DEBUGFLAG = 1
    if DEBUGFLAG == 1:
        write_log("inside create weather daily csv")
    wsf = open(ws_path)
    ws_info=[]
    ws_ids=[]
    lines = wsf.readlines()
    wsf.close()
    for iline in range(1,len(lines)):
        line1 = lines[iline].rstrip()
        line_list = line1.split(",")
        for s in line_list:
            line_list[0] = int(line_list[0])
        if DEBUGFLAG == 1:
            write_log('Line list',line_list)
        ws_info.append(line_list)
    sorted_ws_info = sorted(ws_info, key=lambda t: t[0])
    ws_info_2d=[]
    num_ws = 0
    for row in sorted_ws_info:
        ws_info_2d.append([])
        ws_info_2d[num_ws].append(row[0])
        ws_info_2d[num_ws].append(row[1])
        ws_info_2d[num_ws].append(row[2])
        ws_ids.append(row[0])
        num_ws += 1
    start_date = mjd_to_date(jul_start)
    end_date = mjd_to_date(jul_end)

    if (weather_data == 4):
        era5 = None
        write_log("weather-data flag is 4 - USING GRIDDED WEATHER DATA - ERA5!!!!")
        ls_basedir = os.getcwd() + "/image"
        FileList = glob.glob(os.path.join(ls_basedir, "*B1.TIF"))
        len_ls_basedir = len(ls_basedir)
        LS_name = FileList[0][len_ls_basedir + 1: -7]
        #            import pdb; pdb.set_trace()
        era5 = ERA5()
        era5_extract = era5.extract_era5(LS_name, aoi)
    else:
#        import pdb; pdb.set_trace();
        climate_dat = get_climate(aoi_lyr, ws_ids, start_date, end_date)


# sort of julian date (t[6]) and then station ID t[0]
#NOTE column [9] is added and based on having a couple of extra columns.  Tbis number will change if DAVE creates the file
    sorted_climate_dat = sorted(climate_dat, key=lambda t: (t[1],t[0]))
    jul_day = 0
#    import pdb; pdb.set_trace();
    num_lines = 0
    jul_day = 0
    for row in sorted_climate_dat:
#       Checking the the line is not blank
        if len(row) < 4:
           write_log('row less than 4')
        if (jul_start <= date_to_mjd(row[1]) and jul_end >= date_to_mjd(row[1])):
            if DEBUGFLAG == 1:
                write_log("start_date ",start_date,"<= row[1]",row[1],"end_date",end_date,">=row[1]",row[1])
        #       Checking the the line is not blank
            if not (row[0] == "" or row[1] == ""):
                # Generation a csv file with the ETr data for each day
                for w in range(0, num_ws):
                    if(ws_info_2d[w][0] == int(row[0])):
                        X = ws_info_2d[w][1]
                        Y = ws_info_2d[w][2]
                        if not row[2]:
                #           The ET value is blank
                            y = list(row)
                            y[2]=""
                            row = tuple(y)
                        elif(jul_day != date_to_mjd(row[1])):
                            my_date = row[1].strftime("%m-%d-%Y")
                            if(num_lines > 1):
                                out_et_csv.close()
                            pad_juld = str(row[2]).zfill(3)
                            out_et_csv = open(basedir + '/' + my_date + '_ETr_WS_daily.csv', 'w')
                            out_et_csv.write('X,Y,Z,ID,Jul_Day\n')
                            out_et_csv.write("{0},{1},{2},{3},{4}\n".format(X,Y,float(row[2])*ETo_to_ETr,row[0],my_date))
                            num_lines = 0
                            jul_day = date_to_mjd(row[1])
                        else:
                            if DEBUGFLAG == 1:
                                write_log("writing second weather station to date file",my_date)
                            out_et_csv.write("{0},{1},{2},{3},{4}\n".format(X,Y,float(row[2])*ETo_to_ETr,row[0],my_date))
                            num_lines += 1
                            jul_day = date_to_mjd(row[1])
    return


def aoi_extent(aoi):
    inDriver = ogr.GetDriverByName("ESRI Shapefile")
    inDataSource = inDriver.Open(aoi, 0)
    inLayer = inDataSource.GetLayer()
    extent = inLayer.GetExtent()
    del inDataSource
    clip_x1 = extent[0]
    clip_y1 = extent[3]
    clip_x2 = extent[1]
    clip_y2 = extent[2]
    x_cells = round((clip_x2 - clip_x1) / cellSize)
    y_cells = round((clip_y1 - clip_y2) / cellSize)
    return (clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells)


# Create a vrt file based on the shapefile name and the zfield that contains the values to be used for and csv file
def create_csv_vrt1(basedir, file_name, utm_zone):
    try:
        os.remove(basedir + '/' + file_name + '.vrt')
    except OSError:
        pass
    out_vrt = open(basedir + '/' + file_name + '.vrt', 'w')
    out_vrt.write('<OGRVRTDataSource>\n')
    out_vrt.write('    <OGRVRTLayer name="' + file_name + '" >\n')
    out_vrt.write('        <SrcDataSource>' + basedir + '/' + file_name + '.csv</SrcDataSource>\n')
    out_vrt.write('        <GeometryType>wkbPoint</GeometryType>\n')
    out_vrt.write('        <LayerSRS>EPSG:326' + str(utm_zone) + '</LayerSRS>\n')
    out_vrt.write('        <GeometryField encoding="PointFromColumns" x="X" y="Y" z="Z"/>\n')
#    out_vrt.write('        <GeometryField encoding="PointFromColumns" x="X" y="Y" z="' + zfield + '"/>\n')
    out_vrt.write('    </OGRVRTLayer>\n')
    out_vrt.write('</OGRVRTDataSource>')
    out_vrt.close()


# Create the input files for AgroET
def create_AgroET_Seasonal_input_file(basedir,SEASON_SD,SEASON_ED,sorted_arr,AgroET_count):
    if DEBUGFLAG == 1:
        write_log("inside create_agroet_seasonal_input",basedir,SEASON_SD,SEASON_ED,sorted_arr,AgroET_count)
    src_root = "/tmp/csip/python/seasonal"
    try:
        os.remove(src_root + '/AgroET_Seasonal_input.txt')
    except OSError:
        pass
    out_AgroET = open(src_root + '/AgroET_Seasonal_input.txt', 'w')
    out_AgroET.write('DEBUG_FLAG = ' + str(DEBUGFLAG) + '\n')
    out_AgroET.write('DATA_DIRECTORY = ' + basedir + '\n')
    out_AgroET.write('AOI_FILE = ' + aoi_file + '\n')
    out_AgroET.write('GENERATE_CLOUD_MASK = 1\n')
    out_AgroET.write('WS_FILE = ' + ws +'\n')
    out_AgroET.write('FILL_START_END_ET\n')
#    out_AgroET.write('KEEP_INTERMEDIARY_FILES = ' + str(KEEP_INTERMEDIARY_FILES) + '\n')
#    out_AgroET.write('REGENERATE_AgroET_SEASONAL = ' + str(REGENERATE_AgroET_SEASONAL) + '\n')
    out_AgroET.write('SEASON_START_DATE = ' + SEASON_SD.strftime("%Y-%m-%d") + '\n')
    out_AgroET.write('SEASON_END_DATE = ' + SEASON_ED.strftime("%Y-%m-%d") + '\n')
    str_AgroET_dates = ' '.join([doy_to_cal(str(item[0]),0) for item in sorted_arr])
    out_AgroET.write('NUMBER_ET_IMAGES = '+ str(AgroET_count) + ' ' + str_AgroET_dates + '\n')
    #Generating a list of the names of the LS images that correspond to each of the julian dates for AgroET images to generate the seasonal ET
    str_AgroET_names = ' '.join([str(item[1]) for item in sorted_arr])
    out_AgroET.write('NAMES_ET_IMAGES = '+ str_AgroET_names + '\n')
    out_AgroET.write('#\n')
    out_AgroET.close()


def delete_shp_file(shp_name):
    if os.path.exists(shp_name):
        driver = ogr.GetDriverByName("ESRI Shapefile")
        driver.DeleteDataSource(shp_name)


def delete_file(basedir, filename):
    full_name = basedir + '/' + filename
    if (os.path.isfile(full_name)):
        try:
            os.remove(full_name)
        except OSError:
            pass


def delete_files_w_pattern(dirpath, pattern):
    # Get a list of all the file paths that ends with .txt from in specified directory
    fileList = glob.glob(os.path.join(dirpath, pattern))
    # Iterate over the list of filepaths & remove each file.
    for filePath in fileList:
        try:
            os.remove(filePath)
        except:
            if DEBUGFLAG == 1:
                write_log("!!!!!!!! could not remove file ", filePath)
            pass


def PixelResolution(fp_path):
    fp_path1 = fp_path + '1'
    ascii_sub = "tr -cd '\11\12\15\40-\176'" + '< ' + fp_path + '> ' + fp_path1
    os.system(ascii_sub)
    fp = open(fp_path1,'r',encoding="utf-8")
    for line in fp:
        if "Pixel Size " in line:
            line2 = line.split('=' or ",")[-1].strip()
            line3 = line2[1:-1]
            if DEBUGFLAG == 1:
                write_log("Before map call in Pixel Resolution")
            xres, yres = map(real, line3.split(","))
            if DEBUGFLAG == 1:
                write_log("After map call in Pixel Resolution xres, yres ",xres,yres)
            if DEBUGFLAG >= 3:
                write_log('Pixel Size String ', line3, 'X resolution ', xres, 'Y resolution ', yres)
            break
    fp.close()
    return (abs(float(xres)), abs(float(yres)))


# This function adds a buffer to a shapefile.
def shp_buffer(in_shp, buff, out_shp):
    delete_shp_file(out_shp)
    shp = ogr.Open(in_shp)
    drv = shp.GetDriver()
    drv.CopyDataSource(shp, out_shp)
    shp.Destroy()
    buf1 = ogr.Open(out_shp, 1)
    lyr1 = buf1.GetLayer(0)
    for i in range(0, lyr1.GetFeatureCount()):
        feat = lyr1.GetFeature(i)
        lyr1.DeleteFeature(i)
        geom1 = feat.GetGeometryRef()
        feat.SetGeometry(geom1.Buffer(buff))
        lyr1.CreateFeature(feat)
    feature = lyr1.GetNextFeature()
    while feature is not None:
        geom = feature.GetGeometryRef()
        try:
            area = geom.GetArea()
#            feature.SetField('Area', area)
#            lyr1.SetFeature(feature)
            if area < cellSize*cellSize*1.1:
                lyr1.DeleteFeature(feature.GetFID())
        except:
            lyr1.DeleteFeature(feature.GetFID())
        feature = lyr1.GetNextFeature()
    buf1.Destroy()


def clip_img1(in_image, out_image, aoi_lyr, basedir1):
    if DEBUGFLAG == 1:
        write_log("inside clip_img1")
    out_image1 = out_image[:-4] + '_ZZ' + out_image[-4:]
    delete_file(basedir1, out_image)
    delete_file(basedir1, out_image1)
    delete_file(basedir1, "gdalinfo_output.txt")
    gdalinfo = "gdalinfo -nomd -norat -noct " + basedir1 + '/' + in_image + " > " + basedir1 + "/gdalinfo_output.txt"
    if DEBUGFLAG == 1:
        write_log(gdalinfo)
    os.system(gdalinfo)
    if DEBUGFLAG == 1:
        write_log("Before pixelresolution")
    xres, yres = PixelResolution(basedir1 + "/gdalinfo_output.txt")
    if DEBUGFLAG == 1:
        write_log("After pixelresolution xres, yres ",xres, yres)
    if DEBUGFLAG == 3:
        write_log('X_res is ',xres, 'Yres is ',yres)
    shp_buffer(aoi_lyr, 0.0, aoi_lyr[:-4]+'_buff.shp')
    gdal_warp = ["gdalwarp", "-dstnodata", "-9999", "-overwrite",
                              "-tr", str(xres), str(yres), "-cutline", aoi_lyr[:-4]+'_buff.shp',
                              basedir1 + '/' + in_image, basedir1 + '/' + out_image1]
    if DEBUGFLAG == 1:
       write_log(' '.join(gdal_warp))
    out = subprocess.check_output(gdal_warp)
    ds = gdal.Open(basedir1 + '/' + out_image1)
    clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells = aoi_extent(aoi_lyr)
    ds = gdal.Translate(basedir1 + '/' + out_image, ds, projWin=[clip_x1, clip_y1, clip_x2, clip_y2])
    ds = None
    delete_file(basedir1, out_image1)


# This function creates a csv file of the weather stations
def ws_to_csv(ws,basedir):
    ws_shp = basedir + '/' + ws
    if DEBUGFLAG == 1:
        write_log("Weather Station Name ", ws_shp)
    ws_source = osgeo.ogr.Open(ws_shp)
    if ws_source is None:
        raise Exception("Open of Weather Station failed ", ws_shp, " file not found ")
    ws_layer = ws_source.GetLayer()
    wsfield_names = [field.name for field in ws_layer.schema]
    if DEBUGFLAG == 1:
        write_log('Weather Station field_names ', wsfield_names)
    ws_fields = []
    st_id = -1
    for i in range(0, ws_layer.GetFeature(0).GetFieldCount()):
        field = ws_layer.GetFeature(0).GetDefnRef().GetFieldDefn(i).GetName()
        ws_fields.append(field)
        if(field == 'Std_Id' or field == 'Station_Nu' or field == 'StationNbr'):
            st_id = i
    if DEBUGFLAG == 1:
        write_log(' WS field ', ws_fields)
    if st_id == -1:
        write_log('WS file did not contain a field called \"Std_Id\".  Please add this field and run the program again.')
        exit(1)
    ws = []
    out_ws_csv = open(ws_shp[:-4] + '.csv', 'w')
    out_ws_csv.write('Std Id, X, Y\n')
    for index in range(ws_layer.GetFeatureCount()):
        feature = ws_layer.GetFeature(index)
        geometry = feature.GetGeometryRef()
        ws.append((geometry.GetX(), geometry.GetY()))
        if DEBUGFLAG == 1:
            write_log("Weather Station ", feature[st_id], geometry.GetX(), geometry.GetY())
        out_ws_csv.write("{0}, {1}, {2}\n".format(int(feature[st_id]), geometry.GetX(), geometry.GetY()))
    out_ws_csv.close()
    return


def generate_daily_weather_csv_grids(weather_data, basedir, jul_start, jul_end, utm_zone, aoi_lyr, climate):
    DEBUGFLAG = 1
    write_log("DEBUGFLAG is hardwired to 1 in generate_daily_weather_csv inside of Setup+AgroET_for_seasonal_VM_API.py")
    delete_files_w_pattern(basedir, "*.csv")
    if DEBUGFLAG == 1:
        write_log("inside generate_daily_weather_csv")
    ws_to_csv(ws,basedir)
    if DEBUGFLAG == 1:
        write_log("after ws to csv")
    clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells = aoi_extent(aoi_lyr)
    if DEBUGFLAG == 1:
        write_log("after aoi extent and before create weather daily csv")
        write_log(weather_data, basedir, basedir + '/' + ws[:-4] + '.csv', jul_start, jul_end)
    create_weather_daily_csv(weather_data, basedir, basedir + '/' + ws[:-4] + '.csv', jul_start, jul_end, aoi_lyr)
    if DEBUGFLAG == 1:
        write_log("after create weather daily csv jul_start ", jul_start, "jul_end ", jul_end)
#    import pdb; pdb.set_trace()
    for t in range(jul_start, jul_end + 1):
#Generating grid of ETr daily
        if DEBUGFLAG == 1:
            write_log(" t is ", t)
        str_t = mjd_to_date(t) + "_ETr_WS_daily"
        if DEBUGFLAG == 1:
            write_log(" str_t is ", str_t)
        create_csv_vrt1(basedir, str_t, utm_zone)
        gdal_grid_text = str(
            'gdal_grid -a invdist:power={0} -txe {1} {2} -tye {3} {4} -outsize {5} {6} -a_srs EPSG:326{7} -zfield "Z" -of GTiff -l {8} -ot Float64 {9} {10}'.format(
                idw_power, clip_x1, clip_x2, clip_y2, clip_y1, x_cells, y_cells, utm_zone, str_t,
                basedir + '/' + str_t + '.vrt', basedir + '/' + str_t + '1.img'))
        if DEBUGFLAG == 1:
            write_log(gdal_grid_text)
        delete_file(basedir, str_t + '1.img')
        os.system(gdal_grid_text)
        if DEBUGFLAG == 1:
            write_log("Before clip_img1")
        clip_img1(str_t + '1.img', str_t + '.img', aoi_lyr, basedir)
        if DEBUGFLAG == 1:
            write_log("After clip_img1")
        delete_file(basedir, str_t + '1.img')


def find_landsat_images(basedir):
    write_log("Inside find_landsat_images with basedir", basedir)
    DEBUGFLAG = 1
    ls_count1 = -1 #because the first value in the array is 0 so when you add 1 you end up with 0
    if DEBUGFLAG == 1:
        write_log(basedir)
    for f in os.listdir(basedir):
        if f.find("_ET_24_raster.img") >= 0:
            ls_count1 += 1
            t1 = f[:-17]
            if DEBUGFLAG == 1:
                write_log('found the ET_24_results file and the root name is ',t1)               
#            import pdb; pdb.set_trace()
            ls_image_names.append(f[:-8])
            JulDate = f[-24:-17]
            write_log("JulDate for image is ", JulDate)
            ls_dates.append(JulDate)
            if DEBUGFLAG == 1:
                write_log('LandSat Directory is ', ls_image_names[ls_count1], "ls_count1 is ", ls_count1)
                write_log('LandSat Image Date is ', JulDate)
    return(ls_count1)


def find_AgroET_results(basedir, MJD_IMGD_SD, MJD_IMGD_ED, ratio_flag):
    AgroET_image_dates = []
    AgroET_image_names = []    
    AgroET_image_dates0 = []
    AgroET_image_names0 = []

    AgroET_count1 = 0
    if DEBUGFLAG == 1:
        write_log('basedir ')
        write_log(basedir)
    for f in os.listdir(basedir):
        if f.endswith('ET_24_raster.img'):
            if (ratio_flag == 0 and "NDVI_Ratio_ET_" not in f) or ratio_flag == 1:
                    split_tup = os.path.splitext(f)
                    if(split_tup[1] == '.img'):
                        doy_ls_img1 = datetime.datetime.strptime(f[-24:-17],'%Y%j').strftime("%Y-%m-%d")
                        doy_ls_img = datetime.datetime.strptime(doy_ls_img1, "%Y-%m-%d")
                        AgroET_result_date = date_to_mjd(doy_ls_img)
        #               check if the AgroET results are between the image start date and image end date
                        if(AgroET_result_date >= MJD_IMGD_SD and AgroET_result_date <= MJD_IMGD_ED):
                            AgroET_count1 += 1;
                            AgroET_image_names0.append(f[:-4])
                            AgroET_image_dates0.append(f[-24:-17])
                            if DEBUGFLAG == 1:
                                write_log("agroET count ", AgroET_count1, "name of LS ",f[:-4]," date is ",f[-24:-17])
    if DEBUGFLAG == 1:
        write_log('AgroET_image_dates BEFORE sorting', AgroET_image_dates0)
    AgroET_image_dates = sorted(AgroET_image_dates0)
    if DEBUGFLAG == 1:
        write_log('AgroET_image_dates AFTER sorting', AgroET_image_dates)
#    import pdb; pdb.set_trace()
    for f1 in AgroET_image_dates:
        count = -1
        for f2 in AgroET_image_names0:
            write_log("f1", f1, "f2", f2[-24:-17])
            count += 1
            if f2.find(f1) >= 0:
#            if f1 == f2[9:16]:
#                import pdb; pdb.set_trace()
                AgroET_image_names.append(f2)
                break
    if DEBUGFLAG == 1:
        write_log('AgroET_image_dates sorted', AgroET_image_dates)
        write_log('AgroET_image_names sorted', AgroET_image_names)

    return(AgroET_count1, AgroET_image_names, AgroET_image_dates)


ls_dates=[]
ls_image_names=[]
daily_weather_file = 'cimis.csv'
#daily_weather_file = 'daily_weather.csv'
ws = 'ws.shp'

def run_generate_seasonal_ET(
        SEASON_SD,
        SEASON_ED,
        basedir,
        aoi_file,
        generate_daily_weather,
        weather_data,
        ndvi,
        climate
):
#    import pdb; pdb.set_trace()
    if DEBUGFLAG == 1:
        write_log("Inside run_generate_seasonal_ET - file Setup_AgroET_for_seasonal_VM_API.py ")
        write_log("Season start date is ",SEASON_SD)
        write_log("Season end date is ",SEASON_ED)
        write_log("Basedir is ",basedir)

    MJD_IMGD_SD = date_to_mjd(SEASON_SD)
    IMGD_SD = mjd_to_date(MJD_IMGD_SD)
    if DEBUGFLAG == 1:
        write_log('Season Start Date ', SEASON_SD)
        write_log('Imagery Download Start Date ', IMGD_SD)
    MJD_IMGD_ED = date_to_mjd(SEASON_ED)
    IMGD_ED = mjd_to_date(MJD_IMGD_ED)
    if DEBUGFLAG == 1:
        write_log("Start date, MJD, end date MJD ", MJD_IMGD_SD, IMGD_SD, MJD_IMGD_ED, IMGD_ED )
    if DEBUGFLAG == 1:
        write_log('Season End Date ', SEASON_ED)
        write_log('Imagery Download End Date ', IMGD_ED)
    basedir = os.path.abspath(basedir)
    if DEBUGFLAG == 1:
        write_log("base directory for seasonal ET is ",basedir)
    check_directory_exists(basedir, 'Base directory ')
    if DEBUGFLAG == 1:
        write_log('BEFORE checking utm zone')
    aoi = basedir + '/' + aoi_file
    #Find the number of landsat images that are in the directory and will be used for the seasonal calculation
    utm_zone = shapefile_utm_prj(aoi)
    if DEBUGFLAG == 1:
        write_log('past checking utm zone')
    ls_count = find_landsat_images(basedir)
    if DEBUGFLAG == 1:
        write_log('before checking if aoi_file exists', aoi_file)
    check_file_exists(basedir, aoi_file, 'shapefile', utm_zone, 'Area of Interest (AOI) shapefile')
    # Checking if the Weather Station file exists and is in the correct projection
    if DEBUGFLAG == 1:
        write_log('before checking if ws file exists')
    check_file_exists(basedir, ws, 'shapefile', utm_zone, 'Weather Station shapefile ')
    # Set the working directory to basedir
    os.chdir(basedir)
    sorted_ls_dates = sorted(ls_dates)
    if DEBUGFLAG == 1:
        write_log('sorted the LS dates',sorted_ls_dates)
    doy_start = int(sorted_ls_dates[0])
    cal_start = doy_to_cal(doy_start,2)
    if DEBUGFLAG == 1:
        write_log("doy_start ",doy_start,"cal_start ",cal_start)
    d_cal_start = datetime.datetime.strptime(cal_start, "%Y-%m-%d")
    jul_start = date_to_mjd(d_cal_start)
    doy_end = int(sorted_ls_dates[ls_count])
    cal_end = doy_to_cal(doy_end,2)
    d_cal_end = datetime.datetime.strptime(cal_end, "%Y-%m-%d")
    if DEBUGFLAG == 1:
        write_log("calendar start ",cal_start,'calendar end ',cal_end)
    jul_end = date_to_mjd(d_cal_end)
#    import pdb; pdb.set_trace()
    if DEBUGFLAG == 1:
        write_log("Setup_AgroET_for_Seasonal_VM_API 938 !!!!!!!!!!!!!!!!!!generate_daily_weather flag is HARDWIRED TO ", generate_daily_weather, "weather data flag ", weather_data)
#   check if the ETr Weather Station grid for the first day of the season exists
#   if it does not then generate the daily ETr grids by setting generate_daily_weather to 1    
    str_t = mjd_to_date(jul_start) + "_ETr_WS_daily.img"
    #if not os.path.exists(str_t):
    #    generate_daily_weather = 1
    if generate_daily_weather == 0 and weather_data != 4:
        generate_daily_weather_csv_grids(weather_data,basedir,jul_start,jul_end,utm_zone, aoi, climate)
        if DEBUGFLAG == 1:
            write_log('Returned from calling generate_daily_weather_csv_grids')
    if DEBUGFLAG == 1:
        write_log('Before calling generate AgroET_seasonal')
    ratio_flag = 0 # First Time DO NOT Use the NDVI ratio correction - value is 0
    sorted_arr = sort_ET_images(basedir, MJD_IMGD_SD, MJD_IMGD_ED,ratio_flag)  
#    import pdb; pdb.set_trace()
    Generate_AgroET_Seasonal_ET(basedir, SEASON_SD, SEASON_ED, sorted_arr, aoi_file, ratio_flag)
    NDVI_ET_call = "PYTHONPATH=/tmp/csip/python python3 /tmp/csip/python/seasonal/NDVI/NDVI_ET_VM.py --project " + basedir + " --aoi " + aoi_file
#    import pdb; pdb.set_trace()
    write_log(NDVI_ET_call)
    os.system(NDVI_ET_call)
#    import pdb; pdb.set_trace()
#   ndvi is the flag that tells if you want to run the NDVI ET correction. 1 is YES and 0 is NO
    if ndvi == 1:
        ratio_flag = 1 # Second time USE the NDVI ratio correction - values is 1   
    #    import pdb; pdb.set_trace();
        sorted_arr = sort_ET_images(basedir, MJD_IMGD_SD, MJD_IMGD_ED, ratio_flag)
        Generate_AgroET_Seasonal_ET(basedir, SEASON_SD, SEASON_ED, sorted_arr, aoi_file, ratio_flag)
        if DEBUGFLAG == 1:
            write_log('After calling generate AgroET_seasonal')
    exit()

def sort_ET_images(basedir, MJD_IMGD_SD, MJD_IMGD_ED, ratio_flag):
    AgroET_image_dates = []
    AgroET_image_names = []
    AgroET_count,AgroET_image_names, AgroET_image_dates = find_AgroET_results(basedir, MJD_IMGD_SD, MJD_IMGD_ED, ratio_flag)
    arr = [[0 for i in range(2)] for j in range(AgroET_count)]
 #   AgroET_image_dates = sorted(AgroET_image_dates0)
    i = 0
#    import pdb; pdb.set_trace()
    if DEBUGFLAG == 1:
        write_log("AgroET count is ", AgroET_count)
        write_log(AgroET_image_dates)
        write_log(AgroET_image_names)
#    import pdb; pdb.set_trace();

    while i < AgroET_count:
        arr[i][0] = AgroET_image_dates[i]
        arr[i][1] = AgroET_image_names[i]
        if DEBUGFLAG == 1:
            write_log('i ',i,'arr[i][0] and arr[i][1] ',arr[i][0],arr[i][1])
        i += 1

    sorted_arr = sorted(arr, key=lambda t: t[0])
    i = 0
    while i < AgroET_count:
        write_log('Sorted AgroET_name and date', sorted_arr[i][0], sorted_arr[i][1],AgroET_image_dates[i],AgroET_image_names[i])
        i += 1
    return sorted_arr


if __name__ == "__main__":
    write_log('Setup AgroET Seasonal for Seasonal VM API ET Version 1.1')
    write_log('Starting calculations - calling run_generate_seasonal_ET ')
    run_generate_seasonal_ET(
        SEASON_SD,
        SEASON_ED,
        basedir,
        aoi_file,
        generate_daily_weather,
        weather_data,
        ndvi,
        climate
    )