from __future__ import print_function

# This must be the first statement before other statements.
# You may only put a quoted or triple quoted string,
# Python comments, other future statements, or blank lines before the __future__ line.

try:
    import __builtin__
except ImportError:
    # Python 3
    import builtins as __builtin__

from osgeo import gdal
from osgeo import osr
from osgeo import ogr
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
#import csi
import numpy as np
import subprocess
import rasterio
import rasterio.features
import json, logging, re, os, sys, glob, requests, shutil, subprocess, urllib
import xml.etree.ElementTree as ET
from rasterstats.utils import stats_to_csv
from rasterstats import zonal_stats
import osgeo.ogr
import shapely.geometry
from shapely.wkt import dumps, loads
from os import path
import math
import struct
import gzip
import datetime
from datetime import date, timedelta
import sys, getopt
import send_email
from AgroET_library import write_log 

DEBUGFLAG = 1

# Redefine print so that it sends results to logger.
_print = print


def print(*args):
    msg = ' '.join([str(a) for a in args])
    logging.debug(msg)
    _print(msg)


def gdal_calc_func(outfile_name, gdal_calc, basedir):
    delete_file(outfile_name, basedir)
    if DEBUGFLAG == 1:
        write_log(gdal_calc)
    else:
        gdal_calc += "> /dev/null" 
    os.system(gdal_calc)


# register all of the drivers
gdal.AllRegister()
# this allows GDAL to throw Python Exceptions
gdal.UseExceptions()

# Raster layers cell size
cellSize = 30
# Maximum number of cells to fill in gaps
Max_search_fill_gap = 15
# Initial Search Radius for cold points around Weather Stations (km)
sr = 20
#Conversion from ETo to ETr
ETo_to_ETr = 1.2
#Conversion from km to miles as applied to Wind Run
km_to_miles = 0.621371

# DEBUGFLAG sets a flag that prints the different commands that are being executed (mainly gdal_calc).
# 0 produces only output that GDAL generates and send to the console.
# 1 prints the most output to the console (all gdal_calc commands and more).
# 2 prints only some variable values.
DEBUGFLAG = 1

KEEP_INTERMEDIARY_FILES = 0

#LAG AgroET_file = 'C:/AgroET/AgroET_input.txt'
#AgroET_file = 'AgroET_seasonal_input.txt'
#if not os.path.exists(AgroET_file):
#    raise Exception('AgroET input file',AgroET_file,'does not exist.  You need to create it !!!!')

def copyFile(src, dest):
    try:
        shutil.copyfile(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        write_log('Error source and destination are the same : %s' % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        write_log('Error source file does not exist: ', e.strerror, ' source file is ', src)


def delete_files_w_pattern(dirpath,pattern):
    # Get a list of all the file paths that ends with .txt from in specified directory
    fileList = glob.glob(os.path.join(dirpath, pattern))

    # Iterate over the list of filepaths & remove each file.
    for filePath in fileList:
        try:
            os.remove(filePath)
        except:
            pass


def copy_shp_file(base_name, new_base_name, basedir):
    delete_shp_file(new_base_name)
    itemList = os.listdir(basedir)
    for item in itemList:
        if base_name[:-4] == item[:-4]:
            try:
                os.remove(basedir + '/' + new_base_name[:-4] + item[-4:])
            except OSError:
                pass
            copyFile(basedir + '/' + item, basedir + '/' + new_base_name[:-4] + item[-4:])


def delete_shp_file(shp_name):
    if os.path.exists(shp_name):
        driver = ogr.GetDriverByName("ESRI Shapefile")
        driver.DeleteDataSource(shp_name)


def delete_file(filename, basedir):
    full_name = basedir + '/' + filename
    if (os.path.isfile(full_name)):
        try:
            os.remove(full_name)
        except OSError:
            pass


def delete_file_w_path(filename):
    full_name = filename
    if (os.path.isfile(full_name)):
        try:
            os.remove(full_name)
        except OSError:
            pass


def reproj_shp(ls_utm_z, shp_name):
    orig_shp = shp_name[:-4] + "_orig.shp"
    copy_shp_file(shp_name, orig_shp)
    delete_shp_file(shp_name)
    os.system(
        "ogr2ogr -t_srs EPSG:326"
        + str(ls_utm_z)
        + " "
        + basedir
        + "/"
        + shp_name
        + " "
        + basedir
        + "/"
        + orig_shp
    )


def check_utm_prj(ls_utm_z, file_name, f_type, basedir):
    if(f_type == 'shapefile'):
        proj_file = basedir + '/' + file_name[:-4] + '.prj'
        check_file_exists(file_name[:-4] + '.prj','Projection file (.prj) for ' +file_name+ ' is missing.', basedir)
        fproj = open(proj_file, "r")

        ln = fproj.readline()
        fproj.close
        s2 = "Zone"
        start = ln.find(s2)
        if(start == -1):
            reproj_shp(ls_utm_z, file_name)
            write_log(file_name,'Shapefile was not in UTM Coordinates.  \nIt was projected into the same UTM coordinates as the Landsat image!!!!')
        utm_z = ln[start + 5:start + 7]
        if DEBUGFLAG == 1:
            write_log('Projection file line ', ln)
            write_log('start of UTM Zone ID ',start)
            write_log('Shapefile UTM ZONE is ', utm_z,' Landsat UTM ZONE is ',ls_utm_z)
        fproj.close()


def check_file_exists(file_name, error_text, basedir):
    file_exists = os.path.isfile(basedir + '/' + file_name)
    if (file_exists == 0):
        write_log(error_text, basedir + '/' + file_name, ' does not exist !!!!')
        exit(1)


def check_directory_exists(directory_path, error_text):
    dir_exists = path.exists(directory_path)
    if (dir_exists == 0):
        write_log(error_text, directory_path,' does not exist !!!!')
        exit(1)


def shape_utm_prj(prj_shape):
    driver = ogr.GetDriverByName('ESRI Shapefile')
    dataset = driver.Open(prj_shape)
    layer = dataset.GetLayer()
    spatialRef = layer.GetSpatialRef()
    # set spatial reference and transformation
    targetprj = r"{}".format(spatialRef)
    start_prj = targetprj.find("Zone")
    if(start_prj == -1):
        start_prj = targetprj.find("zone")
    if(start_prj == -1):
        write_log('The UTM zone for ',prj_shape,'was not found. Therefore, it will try to be reprojected to the Landsat UTM zone!!!!')
        utm_prj = -1
    else:
        utm_prj = targetprj[start_prj + 5:start_prj + 7]
    if(DEBUGFLAG == 1):
        write_log('utm_prj string is ',utm_prj)
    return(utm_prj)


def PixelResolution(fp_path):
    fp = open(fp_path)
    for line in fp:
        if "Pixel Size " in line:
            line2 = line.split('=' or ",")[-1].strip()
            line3 = line2[1:-1]
            xres, yres = map(real, line3.split(","))
            if DEBUGFLAG >= 3:
                write_log('Pixel Size String ', line3, 'X resolution ', xres, 'Y resolution ', yres)
            break
    fp.close()
    return (abs(float(xres)), abs(float(yres)))


def del_tmp_files(basedir1):
    outf = ["*ZZ*.*","*buff.*","Cloud_mask*.*","ET_cum1.*","*.aux.xml"]
    for x in outf:
        delete_files_w_pattern(basedir1, x)


#12-14-21 def find_ls_name(j_date):
def find_ls_name(mjd, name_reset_et):
    str_m_d_yr = mjd_to_date(int(mjd),2)
    name = ''
    found_name = 0

    for t in name_reset_et:
        if str_m_d_yr in t:
            name = t + ".img"
            found_name = 1
            break
    if (found_name == 0):
#        import pdb;pdb.set_trace()
        write_log('Date was not found in images', str_m_d_yr)
        exit(1)
    if DEBUGFLAG == 1:
        write_log('\n\nInside find_ls_name yr_j_date',str_m_d_yr,'LS name ',name)
    return(name)


# def clip_img1(in_image, out_image, aoi_lyr):
def clip_img1(in_image, out_image, aoi_lyr, basedir1):
    out_image1 = out_image[:-4] + '_ZZ' + out_image[-4:]
    delete_file(out_image, basedir1)
    delete_file(out_image1, basedir1)
    delete_file("gdalinfo_output.txt", basedir1)
    if not os.path.exists(basedir1 + '/' + in_image):
        if DEBUGFLAG == 1:
            write_log("inside clip_img1 input file does not exit ", basedir1 + '/' + in_image)
        exit(272)
    else:
        gdalinfo = "gdalinfo -nomd -norat -noct " + basedir1 + '/' + in_image + " > " + basedir1 + "/gdalinfo_output.txt"
    if DEBUGFLAG == 1:
        write_log(gdalinfo)
    os.system(gdalinfo)
    xres, yres = PixelResolution(basedir1 + "/gdalinfo_output.txt")
    if DEBUGFLAG == 3:
       write_log('X_res is ',xres, 'Yres is ',yres)
    shp_buffer(aoi_lyr, 0.0, aoi_lyr[:-4]+'_buff.shp')
    gdal_warp = ["gdalwarp", "-dstnodata", "-9999", "-overwrite",
                              "-tr", str(xres), str(yres), "-cutline", aoi_lyr[:-4]+'_buff.shp',
                              basedir1 + '/' + in_image, basedir1 + '/' + out_image1]
    clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells = aoi_extent(aoi_lyr)
    if DEBUGFLAG == 1:
        write_log(' '.join(gdal_warp))
    out = subprocess.check_output(gdal_warp)
    ds = gdal.Open(basedir1 + '/' + out_image1)
    ds = gdal.Translate(basedir1 + '/' + out_image, ds, projWin=[clip_x1, clip_y1, clip_x2, clip_y2])
    ds = None
#5-20-20    delete_file_w_path(basedir1 + '/' + out_image1)


# This function adds a buffer to a shapefile.
def shp_buffer(in_shp, buff, out_shp):
    delete_shp_file(out_shp)
    shp = ogr.Open(in_shp)
    drv = shp.GetDriver()
    drv.CopyDataSource(shp, out_shp)
    shp.Destroy()
    buf1 = ogr.Open(out_shp, 1)
    lyr1 = buf1.GetLayer(0)
    for i in range(0, lyr1.GetFeatureCount()):
        feat = lyr1.GetFeature(i)
        lyr1.DeleteFeature(i)
        geom1 = feat.GetGeometryRef()
        feat.SetGeometry(geom1.Buffer(buff))
        lyr1.CreateFeature(feat)
    feature = lyr1.GetNextFeature()
    while feature is not None:
        geom = feature.GetGeometryRef()
        try:
            area = geom.GetArea()
            if area < cellSize*cellSize*1.1:
                lyr1.DeleteFeature(feature.GetFID())
        except:
            lyr1.DeleteFeature(feature.GetFID())
        feature = lyr1.GetNextFeature()
    buf1.Destroy()


def aoi_extent(aoi):
    inDriver = ogr.GetDriverByName("ESRI Shapefile")
    inDataSource = inDriver.Open(aoi, 0)
    inLayer = inDataSource.GetLayer()
    extent = inLayer.GetExtent()
    del inDataSource
    clip_x1 = extent[0]
    clip_y1 = extent[3]
    clip_x2 = extent[1]
    clip_y2 = extent[2]
    x_cells = round((clip_x2 - clip_x1) / cellSize)
    y_cells = round((clip_y1 - clip_y2) / cellSize)
    return (clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells)


def get_projection_shapefile(file_path):
    source = osgeo.ogr.Open(file_path)
    layer = source.GetLayerByIndex(0)
    proj = layer.GetSpatialRef()
    return proj


def get_geojson(aoi_file, basedir):
    # reproject aoi to epsg:4326 and convert to geojson
    basename, ext = os.path.splitext(aoi_file)
    aoi_geojson = basename + '.geojson'
    delete_file(aoi_geojson, basedir)
    check = subprocess.check_output('ogr2ogr -f GeoJSON -t_srs epsg:4326 {0} {1}'.format(aoi_geojson, aoi_file).split())
    if b'Error' in check:
        raise Exception('Failed to reproject aoi to 4326: {0}'.format(check))
    with open(aoi_geojson) as fp:
        boundary = json.load(fp)
    delete_file(aoi_geojson, basedir)
    # I'm getting an error that the crs string is unparsable
    del boundary['crs']
    return boundary


def reproject(raster_path, projection, basedir, cell_size=None):
    basename, ext = os.path.splitext(raster_path)
    reproj_path = basename + '_reproj' + ext
    args = ['gdalwarp', '-t_srs', projection]
    if cell_size:
        args += ['-tr', str(cell_size), str(cell_size)]
    args += [raster_path, reproj_path]
    delete_file(reproj_path, basedir)
    check = subprocess.check_output(args)
    if b'Error' in check:
        raise Exception('Failed to reproject nlcd to aoi projection: {0}'.format(check))
    return reproj_path


def Daily_ET_calc1(py_script_dir, t_inter, j_start, j_end, mask, level, name_reset_et, basedir, aoi_file):
    # Calculating the ratio of the beginning of the seasonal ET - AgroET Remote Sensing ET ("jul_start"_ET_24.img) divided by the weather station IDW interpolated ETr ("jul_start_1.img)
    str_cal_day = mjd_to_date(j_start,0)
    str_cal_day1 = mjd_to_date(j_end,0)
    ETr1 = 'ETr1_'+ str_cal_day +"_"+ str_cal_day1 +".img"
    ETr1a = 'ETr1a_'+ str_cal_day +"_"+ str_cal_day1 +".img"
    delete_file_w_path(basedir + '/' + ETr1)
    delete_file_w_path(basedir + '/' + ETr1a)
    if (t_inter > 1):
        str_start_ls_name = find_ls_name(str(j_start-1), name_reset_et)
        ET_rs1 = str_start_ls_name
        ET_rs1t = str_start_ls_name[:-4] + "_t.img"
        clip_img1(ET_rs1, ET_rs1t, aoi_file, basedir)
        delete_file_w_path(basedir + '/' + ET_rs1)
        os.rename(basedir + '/' + ET_rs1t, basedir + '/' + ET_rs1)
        str_t = mjd_to_date(j_start-1,0)
        ET_ws1 = str_t + "_ETr_WS_daily.img"
        num_days = str(j_end - j_start + 1)
    else:
        str_start_ls_name = find_ls_name(str(j_start), name_reset_et)
        ET_rs1 = str_start_ls_name
        ET_rs1t = str_start_ls_name[:-4] + "_t.img"
        clip_img1(ET_rs1, ET_rs1t, aoi_file, basedir)
        delete_file_w_path(basedir + '/' + ET_rs1)
        os.rename(basedir + '/' + ET_rs1t, basedir + '/' + ET_rs1)
        str_t = mjd_to_date(j_start,0)
        ET_ws1 = str_t + "_ETr_WS_daily.img"
        num_days = str(j_end - j_start)
    if DEBUGFLAG == 1:
        write_log('\n ET_r1 calculation j_start and j_end ',j_start, j_end,'start calendar date',str_t,'ET_ws1',ET_rs1,'ETr1a ',ET_ws1,'ETr1',ETr1,'ETr1a',ETr1a,'\n')
    gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + ET_rs1 + ' -B ' + basedir + '/' + ET_ws1 +  ' -C ' + basedir + '/' + mask +" --outfile=" + basedir + "/" + ETr1a + " --calc=" + '"where(C=='+level+',(A / B),0)"' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
    gdal_calc_func(ETr1a, gdal_calc, basedir)
    conv_nodata_to_zero(py_script_dir, ETr1a, ETr1, basedir)
    delete_file(ETr1a, basedir)

# Calculating the ratio of the end of the seasonal ET - AgroET Remote Sensing ET ("jul_end"_ET_24.img) divided by the weather station IDW interpolated ETr ("jul_end_1.img)
    str_cal_day = mjd_to_date(j_start,0)
    str_cal_day1 = mjd_to_date(j_end,0)
    ETr2 = 'ETr2_'+ str_cal_day + "_" + str_cal_day1 +".img"
    ETr2a = 'ETr2a_'+ str_cal_day +"_"+ str_cal_day1 +".img"
    delete_file_w_path(basedir + '/' + ETr2)
    delete_file_w_path(basedir + '/' + ETr2a)
    str_end_ls_dates = find_ls_name(str(j_end), name_reset_et)
    ET_rs2 = str_end_ls_dates
    ET_rs2t = str_end_ls_dates[:-4] + "_t.img"
    clip_img1(ET_rs2, ET_rs2t, aoi_file, basedir)
    delete_file_w_path(basedir + '/' + ET_rs2)
    os.rename(basedir + '/' + ET_rs2t, basedir + '/' + ET_rs2)
    str_t = mjd_to_date(j_end,0)
    ET_ws2 = str_t + "_ETr_WS_daily.img"
    if DEBUGFLAG == 1:
        write_log('\n ET_r2 calculation j_start and j_end ',j_start, j_end,'end calendar date',str_t,'ET_rs2', ET_rs2, 'ET_ws2',ET_ws2,'ETr2',ETr2,'ETr2a ',ETr2a,'\n')
    gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + ET_rs2 + ' -B ' + basedir + '/' + ET_ws2 + ' -C ' + basedir + '/' + mask +" --outfile=" + basedir + "/" + ETr2a + " --calc=" + '"where(C=='+level+',(A / B),0)"' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
    gdal_calc_func(ETr2a,gdal_calc, basedir)
    conv_nodata_to_zero(py_script_dir, ETr2a, ETr2, basedir)
    delete_file(ETr2a, basedir)

# Calculating the daily correction factor between the beginning of the season and the end of the season
    str_cal_day = mjd_to_date(j_start,0)
    str_cal_day1 = mjd_to_date(j_end,0)
    ET_corr_fact = 'ET_corr_fact_' + str_cal_day + "_" + str_cal_day1 + ".img"
    ET_corr_facta = 'ET_corr_facta_' + str_cal_day + "_" + str_cal_day1 + ".img"
    delete_file_w_path(basedir + '/' + ET_corr_fact)
    gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + ETr1 + ' -B ' + basedir + '/' + ETr2 + ' -C ' + basedir + '/' + mask +" --outfile=" + basedir + "/" + ET_corr_facta + " --calc=" + '"where(C=='+level+',((A - B)/'+num_days+'),0)"' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
    gdal_calc_func(ET_corr_facta,gdal_calc, basedir)
    conv_nodata_to_zero(py_script_dir, ET_corr_facta,ET_corr_fact, basedir)
    delete_file(ET_corr_facta, basedir)

    for t in range(j_start, j_end+1):
        #If an internal daily_ET (between landsat images) then the first day corresponds to the day after the landsat image - therefore the need to add 1 to the day_of_season
        if (t_inter > 1):
            day_of_seas = str((int(t - j_start)+1))
        else:
            day_of_seas = str(t - j_start)
        str_cal_day = mjd_to_date(j_start,0)
        str_cal_day1 = mjd_to_date(j_end,0)
        str_cal_day2 = mjd_to_date(t,0)

        ET_dm = "ETr_WS_daily_" + str_cal_day + "_" + str_cal_day2 + '_' + str_cal_day1 + ".img"
        season_day = t - j_start
        str_t = mjd_to_date(t,0)
        ET_d = str_t +'_ETr_WS_daily.img' # IDW interpolated ET from weather station data
        gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + ETr1 + ' -B ' + basedir + '/' + ET_corr_fact + ' -C ' + basedir + '/' + ET_d + ' -D ' + basedir + '/' + ETr2 + " --outfile=" + basedir + "/" + ET_dm + " --calc=" + '"where(logical_and(D>0,A>0),((A - ('+day_of_seas+'*B))*C),0)"' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
        gdal_calc_func(ET_dm, gdal_calc, basedir)
    KEEP_INTERMEDIARY_FILES = 0
    if(KEEP_INTERMEDIARY_FILES == 0):
        delete_file_w_path(basedir + '/' + ETr1)
        delete_file_w_path(basedir + '/' + ETr2)
        delete_file_w_path(basedir + '/' + ET_corr_fact) # 11-05-20


def Seasonal_ET_calc1(py_script_dir, j_start, j_end, basedir, ET_cum2, ET_cum3):
    for tt in range(j_start, j_end+1):
        str_cal_day = mjd_to_date(j_start,0)
        str_cal_day1 = mjd_to_date(j_end,0)
        str_cal_day2 = mjd_to_date(tt,0)
        ET_dm = "ETr_WS_daily_" + str_cal_day + "_" + str_cal_day2 + '_' + str_cal_day1 + ".img"
        gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + ET_dm + ' -B ' + basedir + '/' + ET_cum3 + " --outfile=" + basedir + "/" + ET_cum2 + " --calc=" + '"(A+B)" ' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
        gdal_calc_func(ET_cum2, gdal_calc, basedir)
#        write_log("seasonal_et_calc1 gdal_calc ET_cum2",gdal_calc)
        delete_file_w_path(basedir + '/' + ET_cum3)
        os.rename(basedir + '/' + ET_cum2, basedir + '/' + ET_cum3)
        str_tt = mjd_to_date(tt,0)
        ET_day = "ET_day_" + str_tt + ".img"
        ET_day_cum = "ET_day_cum_" + str_tt + ".img"
        delete_file_w_path(basedir + '/' + ET_day_cum)
        gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + ET_dm + ' -B ' + basedir + '/' + ET_day + " --outfile=" + basedir + "/" + ET_day_cum + " --calc=" + '"(A+B)" ' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
        gdal_calc_func(ET_day_cum, gdal_calc, basedir)
#        write_log("seasonal_et_calc1 gdal_calc ET_day_cum",gdal_calc)
        delete_file_w_path(basedir + '/' + ET_day)
        os.rename(basedir + '/' + ET_day_cum, basedir + '/' + ET_day)


def Seasonal_ET_calc2(py_script_dir, start, end, basedir, ET_cum4, ET_cum5):
    # generate a zero value grid as the initial value for the seasonal ET sum
    delete_file_w_path(basedir + '/' + ET_cum5)
    delete_file_w_path(basedir + '/' + ET_cum4)
    j_start = date_to_mjd(str(start),3)
    j_end = date_to_mjd(str(end),3)
    str_t = mjd_to_date(j_start, 0)
    gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + 'ET_day_' + str_t + '.img' + " --outfile=" + basedir + "/" + ET_cum4 + " --calc=" + '"(0.0*A)"' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
    gdal_calc_func(ET_cum4, gdal_calc, basedir)
#    write_log("seasonal_et_calc2 gdal_calc ET_cum4", gdal_calc)
    for tt in range(j_start, j_end+1):
#LAG Seasonal begin
        if DEBUGFLAG == 1:
            write_log('tt is ',tt)
        str_tt = mjd_to_date(tt,0)
        ET_day = "ET_day_" + str_tt + ".img"
        gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + ET_cum4 + ' -B ' + basedir + '/' + ET_day + " --outfile=" + basedir + "/" + ET_cum5 + " --calc=" + '"(A+B)" ' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
        gdal_calc_func(ET_cum5, gdal_calc, basedir)
#        write_log("seasonal_et_calc2 gdal_calc ET_cum5",gdal_calc)
        delete_file_w_path(basedir + '/' + ET_cum4)
        os.rename(basedir + '/' + ET_cum5, basedir + '/' + ET_cum4)
    delete_file_w_path(basedir + '/' + ET_cum5)
    os.rename(basedir + '/' + ET_cum4, basedir + '/' + ET_cum5)


def gen_seasonal_cloud_mask(py_script_dir, start, end, aoi_file, basedir, Seasonal_cloud_mask, name_reset_et):
    delete_file_w_path(basedir + '/' + Seasonal_cloud_mask)
    Cloud_mask1 = 'Cloud_mask1.img'
    delete_file_w_path(basedir + '/' + Cloud_mask1)
    str_start_ls_name = name_reset_et[start]+'.img'
    ET0 = str_start_ls_name
    ET0t = str_start_ls_name[:-4] + "_t.img"
    str_end_ls_name = name_reset_et[end]+'.img'
    ET1 = str_end_ls_name
    ET1t = str_end_ls_name[:-4] + "_t.img"
    if DEBUGFLAG == 1:
        write_log("\n gen_seasonal_cloud_mask start, end ", ET0, ET1, "\n")
    clip_img1(ET0, ET0t, aoi_file, basedir)
    delete_file_w_path(basedir + '/' + ET0)
    os.rename(basedir + '/' + ET0t, basedir + '/' + ET0)
    clip_img1(ET1, ET1t, aoi_file, basedir)
    delete_file_w_path(basedir + '/' + ET1)
    os.rename(basedir + '/' + ET1t, basedir + '/' + ET1)
    gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + ET0 + ' -B ' + basedir + '/' + ET1 + " --outfile=" + basedir + "/" + Cloud_mask1 + " --calc=" + '"(A*B)"' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
    gdal_calc_func(Cloud_mask1,gdal_calc, basedir)
    gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + Cloud_mask1 + " --outfile=" + basedir + "/" + Seasonal_cloud_mask + " --calc=" + '"where(A>=0,1,-9999)" ' + "--type=Float64" + " --NoDataValue=-9999 --quiet"
    gdal_calc_func(Seasonal_cloud_mask,gdal_calc, basedir)


def shp_to_img(shape_name, clip_x1, clip_y1, clip_x2, clip_y2, utm_zone, basedir):
    gdal_grid_text = str(
        'gdal_rasterize -a OBJECTID -te {0} {1} {2} {3} -tr {4} {4} -a_srs EPSG:326{5} -l {6} {7} {8}'.format(
        clip_x1,
        clip_y2,
        clip_x2,
        clip_y1,
        cellSize,
        utm_zone,
        shape_name[:-4],
        basedir + '/' + shape_name,
        basedir + '/'+shape_name[:-4]+'.img'))
    if DEBUGFLAG == 1:
        write_log(" shp_to_img ", gdal_grid_text)
    os.system(gdal_grid_text)


def bookend_cloud_mask(py_script_dir, FILL_START_END_ET, start, end, num_reset_et, ls_counter, name_reset_et, basedir, aoi_file):
    if DEBUGFLAG == 1:
        write_log("bookend_cloud_mask ",py_script_dir, FILL_START_END_ET, start, end, num_reset_et, ls_counter, name_reset_et, basedir, aoi_file)
    aoi_img = aoi_file[:-4]+'.img'
    aoi_img1 = aoi_file[:-4]+'1.img'
    Cloud_mask = 'masks_Interp_mask_' + str(start) + '_' + str(end) + '.img'
    Cloud_mask1 = 'Cloud_mask1_' + str(end) + '.img'
    delete_file_w_path(basedir + '/' + Cloud_mask)
    delete_file_w_path(basedir + '/' + Cloud_mask1)
    str_start_ls_name = find_ls_name(str(start), name_reset_et)
    #fill any missing ET from the initial AgroET processed image
#    write_log("NOTE FILLING START AND ENDING LS IN CLOUDY AREAS TO CHECK IF IT AFFECTS RESULTS")
#    FILL_START_END_ET = 1
#    if FILL_START_END_ET == 1:
#        write_log("fill_start_gap - 574")
#        write_log("fill_start_gap - basedir ", basedir)
#        write_log("fill_start_gap - str_start_ls_name ", str_start_ls_name)
#        write_log("fill_start_gap - start ", start)
#        write_log("fill_start_gap - aoi_img ", aoi_img)
#        write_log("fill_start_gap - aoi_file ", aoi_file)
#        write_log("fill_start_gap - mjd_to_date ", mjd_to_date(start,0)+'_ETr_WS_daily.img')
#        fill_start_end_gap(py_script_dir, str_start_ls_name, mjd_to_date(start,0)+'_ETr_WS_daily.img', basedir, aoi_img)
    if DEBUGFLAG == 1:
        write_log("clip_img1 str_start_ls_name, Cloud_mask1, aoi_file, basedir - 563 ",str_start_ls_name, Cloud_mask1, aoi_file, basedir)
    clip_img1(str_start_ls_name, Cloud_mask1, aoi_file, basedir)
#    import pdb;pdb.set_trace()
    str_end_ls_name = find_ls_name(str(end), name_reset_et)
    ET1 = str_end_ls_name
    ET0 = str_end_ls_name[:-4] + "1.img"
    #fill any missing ET from the final AgroET processed image
    if FILL_START_END_ET == 1:
        if DEBUGFLAG == 1:
            write_log("fill_end_gap - AgroET_seasonal 567", ET1, mjd_to_date(end,0)+'_ETr_WS_daily.img', basedir, aoi_img)
        fill_start_end_gap(py_script_dir, ET1, mjd_to_date(end,0)+'_ETr_WS_daily.img', basedir, aoi_img)
    if DEBUGFLAG == 1:
        write_log("clip_img1, ET1, ET0, aoi_file, basedir - 572 ", ET1, ET0, aoi_file, basedir)
    clip_img1(ET1, ET0, aoi_file, basedir)
    if DEBUGFLAG == 1:
        write_log("after clip_img1, ET1, ET0, aoi_file, basedir - 583 ", ET0, ET1)
    #This fills any clouds at the beginning of the season
    delete_file_w_path(basedir + '/' + ET1)
    os.rename(basedir + '/' + ET0, basedir + '/' + ET1)
    gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + ET1 + ' -B ' + basedir + '/' + Cloud_mask1 + " --outfile=" + basedir + "/" + Cloud_mask + " --calc=" + '"(A+B)"' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
    gdal_calc_func(Cloud_mask,gdal_calc, basedir)
    if DEBUGFLAG == 1:
        write_log("gdal_calc - 595 ", gdal_calc)
    rast_names = []
    interp_names = []
    mask_names = []
    mask_names2 = []
    mask_names1 = []
    str_cal_day = mjd_to_date(start,0)
    str_cal_day1 = mjd_to_date(end,0)
    rast_names.append('masks_Interp_mask_' + str_cal_day + '_' + str_cal_day1 + '.img')
    interp_names.append('masks_Interp_mask_' + str_cal_day + '_' + str_cal_day1 + '.img')
    mask_names.append('masks_mask_' + str_cal_day + '.img')
    mask_names2.append('masks_mask2_' + str_cal_day + '.img')
    str_start_ls_name = find_ls_name(str(start), name_reset_et)
    mask_names1.append(str_start_ls_name)
    for t in range(1, ls_counter):
        str_ls_name = name_reset_et[t-1]+'.img'
        str_ls_date = num_reset_et[t]
        rast_names.append(str_ls_name)
        interp_names.append('masks_Interp_mask_' + str_cal_day + '.img')
        mask_names1.append(str_ls_name)
        mask_names.append('masks_mask_' + str_ls_date + '.img')
        mask_names2.append('masks_mask2_' + str_ls_date + '.img')

    for t in range(0, ls_counter):
        Cloud_mask = mask_names[t]
        Cloud_mask1 = mask_names1[t]
        Cloud_mask2 = mask_names2[t]
        Cloud_mask_bookend = interp_names[t]
        Cloud_mask_bookend1 = 'Cloud_mask1.img'
        Cloud_mask_bookend2 = 'Cloud_mask2.img'
        Cloud_mask_bookend2t = 'Cloud_mask2t.img'
        Cloud_mask_bookend3 = 'Cloud_mask3.img'
        delete_file_w_path(basedir + '/' + Cloud_mask_bookend)
        delete_file_w_path(basedir + '/' + Cloud_mask_bookend2)
        delete_file_w_path(basedir + '/' + Cloud_mask_bookend3)
        # add 10 to the 
        gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + Cloud_mask1 + " --outfile=" + basedir + "/" + Cloud_mask_bookend1 + " --calc=" + '"A+10" ' + " --NoDataValue=0 --type=Float64" + " --quiet"
        gdal_calc_func(Cloud_mask_bookend1, gdal_calc, basedir)
        gdal_translate_txt = str('gdal_translate {0} {1} -a_nodata -1'.format(Cloud_mask_bookend1,Cloud_mask_bookend2t))
        if DEBUGFLAG == 1:
            write_log(gdal_translate_txt)
        os.system(gdal_translate_txt)
        clip_img1(Cloud_mask_bookend2t, Cloud_mask_bookend2, aoi_file, basedir)
        delete_file(Cloud_mask_bookend2t, basedir)
        gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + Cloud_mask_bookend2 + " --outfile=" + basedir + "/" + Cloud_mask2 + " --calc=" + '"where(A<=0,1,0)" ' + "--type=Float64" + " --overwrite --NoDataValue=-9999 --quiet"
        gdal_calc_func(Cloud_mask2, gdal_calc, basedir)
        clip_img1(Cloud_mask2, Cloud_mask, aoi_file, basedir)
        gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + Cloud_mask + " --outfile=" + basedir + "/" + Cloud_mask_bookend3 + " --calc=" + '"where(A==0,1,-9999)" ' + " --NoDataValue=-9999 --type=Float64" + " --quiet"
#7-15-20        gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + Cloud_mask_bookend2 + " --outfile=" + basedir + "/" + Cloud_mask_bookend3 + " --calc=" + '"where(A==0,1,-9999)" ' + " --NoDataValue=-9999 --type=Float64" + " --quiet"
        gdal_calc_func(Cloud_mask_bookend3, gdal_calc, basedir)
        gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + Cloud_mask_bookend3 + ' -B ' + basedir + '/' + aoi_img1 + " --outfile=" + basedir + "/" + Cloud_mask_bookend + " --calc=" + '"where((A*B)==1,1,-9999)" ' + " --NoDataValue=-9999 --type=Float64" + " --quiet"
        gdal_calc_func(Cloud_mask_bookend, gdal_calc, basedir)
    delete_files_w_pattern(basedir, "masks_Interp*.*")


def conv_nodata_to_zero(py_script_dir, file_name, file_name1, basedir):
    temp_file = 'temp_file.img'
    temp_file1 = 'temp_file1.img'
    delete_file(temp_file1, basedir)
#    copyFile(file_name,temp_file)
    gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + file_name + " --outfile=" + basedir + "/" + temp_file + " --calc=" + '"A+10" ' + " --NoDataValue=0 --type=Float64" + " --quiet"
    gdal_calc_func(temp_file,gdal_calc, basedir)
    gdal_translate_txt = str('gdal_translate {0} {1} -a_nodata -1'.format(temp_file, temp_file1))
    if DEBUGFLAG == 1:
        write_log(gdal_translate_txt)
    os.system(gdal_translate_txt)
    gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + temp_file1 + " --outfile=" + basedir + "/" + file_name1 + " --calc=" + '"where((A-10)==-10,0,A-10)" ' + " --NoDataValue=-9999 --type=Float64" + " --quiet"
    gdal_calc_func(file_name1,gdal_calc, basedir)
    delete_file(temp_file, basedir)
    delete_file(temp_file1, basedir)


def intermediary_cloud_mask(py_script_dir, start, end, num_reset_et, ls_counter, basedir):
    cum_filet = 'masks_temp_file.img'
    for t in range(0, ls_counter-1):
        if (t > 1):
            str_ls_name = num_reset_et[t]
            day = str(int(str_ls_name[3:5])+1)
            str_ls_date = str_ls_name[0:2].zfill(2) + '-' + (day).zfill(2) + '-' + str_ls_name[6:10]
            str_ls_date1 = num_reset_et[t+1]
            cum_file1 = 'masks_cum_mask1_' + str_ls_date + '_' + str_ls_date1 + '.img'
        else:
            str_ls_date = num_reset_et[t]
            str_ls_date1 = num_reset_et[t+1]
            cum_file1 = 'masks_cum_mask1_' + str_ls_date + '_' + str_ls_date1 + '.img'
        cum_file = 'masks_cum_mask_' + str_ls_date + '.img'
        delete_file_w_path(basedir + '/' + cum_file)
        delete_file_w_path(basedir + '/' + cum_file1)
        str_ls_date = num_reset_et[t]
        copyFile(basedir + '/masks_mask_' + str_ls_date + '.img', basedir + '/' + cum_file)
        count = 0
        for t1 in range(t+1, ls_counter):
            count += 1
            str_cnt = str(count)
            str_ls_date = num_reset_et[t1]
            cloud_mask = 'masks_mask_' + str_ls_date + '.img'
            gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + cum_file + ' -B ' + basedir + '/' + cloud_mask + " --outfile=" + basedir + "/" + cum_file1 + " --calc=" + '"(A+B)"' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
            gdal_calc_func(cum_file1, gdal_calc, basedir)
            str_ls_date = num_reset_et[t]
            str_ls_date1 = num_reset_et[t1]
            cum_file = 'masks_cum_mask_' + str_ls_date + '_' + str_ls_date1 + '.img'
            gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + cum_file1 + " --outfile=" + basedir + "/" + cum_filet + " --calc=" + '"where(A=='+ str_cnt + ','+ str_cnt +',0)"' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
            gdal_calc_func(cum_filet, gdal_calc, basedir)
            copyFile(basedir + '/' + cum_filet, basedir + '/' + cum_file)
            delete_file_w_path(basedir + '/' + cum_filet)
            j_date = date_to_mjd(num_reset_et[t1], 1)
            if (int(j_date) < end):
                if (t > 1):
                    str_ls_name = num_reset_et[t]
                    day = str(int(str_ls_name[3:5]) + 1)
                    str_ls_date = str_ls_name[0:2].zfill(2) + '-' + (day).zfill(2) + '-' + str_ls_name[6:10]
                    str_ls_date1 = num_reset_et[t1 + 1]
                    cum_file1 = 'masks_cum_mask1_' + str_ls_date + '_' + str_ls_date1 + '.img'
                else:
                    str_ls_date = num_reset_et[t]
                    str_ls_date1 = num_reset_et[t1+1]
                    cum_file1 = 'masks_cum_mask1_' + str_ls_date + '_' + str_ls_date1 + '.img'


def fill_start_end_gap(py_script_dir, input, et_day, basedir, aoi_img ):
    if DEBUGFLAG == 1:
        write_log("inside fill_start_end_gap ", py_script_dir, input, et_day, basedir)
    LS_mask2 = "LS_masked2.img"
    LS_trans1 = "LS_trans1.img"
    ls_name_orig = input[:-4] + '_orig.img'
    if not (os.path.isfile(ls_name_orig)):
        copyFile(input, ls_name_orig)
        gdal_translate_txt = str(
            'gdal_translate -a_nodata -6 {0} {1}'.format(input, LS_trans1))
        os.system(gdal_translate_txt)
        delete_file_w_path(basedir + '/' + LS_mask2)
        gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + LS_trans1 + ' -B ' + basedir + '/' + aoi_img + " --outfile=" + basedir + '/' + LS_mask2 + " --calc=" + '"(A*B)"' + " --type=Float64 --overwrite --NoDataValue=-9998 --quiet"
        os.system(gdal_calc)
        delete_file_w_path(basedir + '/' + LS_trans1)
        delete_file_w_path(basedir + '/' + input)
        gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + LS_mask2 + ' -B ' + basedir + '/' + et_day + " --outfile=" + basedir + '/' + input + " --calc=" + '"where(A==-9999,B*0.3,A)"' + " --type=Float64 --overwrite --NoDataValue=-9998 --quiet"
        os.system(gdal_calc)
        if DEBUGFLAG == 1:
            write_log(gdal_calc)
        delete_file_w_path(basedir + '/' + LS_mask2)


def cal_to_doy(year,month,day):
    # 6-25-20    str_jul_day = YEAR + str(julian_day)
    cal_date1 = datetime.datetime(year,month,day)
    day_of_year = (cal_date1 - datetime.datetime(cal_date1.year, 1, 1)).days + 1
    return (day_of_year)


def pad_date(string,token):
    line_list = string.split(token)
    m = line_list[0]
    d = line_list[1]
    y = line_list[2]
    return m,d,y


def date_to_mjd(yr_m_d,date_flag):
    """
    Modified function to convert a date to Julian Day.  It sets Dec 31, 1979 as date 0 by subtracting 2444238.5 from absolute julian date
    Algorithm from 'Practical Astronomy with your Calculator or Spreadsheet',
        4th ed., Duffet-Smith and Zwart, 2011.
    Parameters
    ----------
    month : int
        Month as integer, Jan = 1, Feb. = 2, etc.
    day : int
        Day
    year : int
        Year as integer. Years preceding 1 A.D. should be 0 or negative.
        The year before 1 A.D. is 0, 10 B.C. is year -9.
    date_flag == 0 (means it is year and DOY) or date_flag == 1 (means it is Year-Month-Day)
    Returns
    -------
    jd : int
        Julian Day
    Examples
    --------
    February 17, 1985 to Julian Day
    >>> date_to_jd(1985,2,17)
    1875
    """
    if (date_flag == 0):
        year = int(yr_m_d[0:4])
        month = int(yr_m_d[4:6])
        day = int(yr_m_d[6:8])
    elif(date_flag == 2):
        line_list = yr_m_d.split("/")
        month = int(line_list[0])
        day = int(line_list[1])
        year = int(line_list[2])
    elif(date_flag == 3):
        line_list = yr_m_d.split("-")
        year = int(line_list[0])
        month = int(line_list[1])
        day = int(line_list[2])
    else:
        line_list = yr_m_d.split("-")
        month = int(line_list[0])
        day = int(line_list[1])
        year = int(line_list[2])

    if month == 1 or month == 2:
        yearp = year - 1
        monthp = month + 12
    else:
        yearp = year
        monthp = month
    # this checks where we are in relation to October 15, 1582, the beginning
    # of the Gregorian calendar.
    if ((year < 1582) or
            (year == 1582 and month < 10) or
            (year == 1582 and month == 10 and day < 15)):
        # before start of Gregorian calendar
        B = 0
    else:
        # after start of Gregorian calendar
        A = math.trunc(yearp / 100.)
        B = 2 - A + math.trunc(A / 4.)
    if yearp < 0:
        C = math.trunc((365.25 * yearp) - 0.75)
    else:
        C = math.trunc(365.25 * yearp)
    D = math.trunc(30.6001 * (monthp + 1))
    jd = B + C + D + day + 1720994.5
    # Set julian date 1 as January 1st 1980 by subtracting 2444238.5
    i_jd = int(jd - 2444238.5)
    return i_jd


def mjd_to_date(jd,flag):
    """
    Modified convert Julian Day to date. It sets Dec 31, 1979 as date 0 by substracting 2444238.5 from absolute julian date
    Algorithm from 'Practical Astronomy with your Calculator or Spreadsheet',
        4th ed., Duffet-Smith and Zwart, 2011.
    Parameters
    ----------
    jd : float
        Julian Day
    Returns
    -------
    year : int
        Year as integer. Years preceding 1 A.D. should be 0 or negative.
        The year before 1 A.D. is 0, 10 B.C. is year -9.
    month : int
        Month as integer, Jan = 1, Feb. = 2, etc.
    day : int
        Day
    Examples
    --------
    Convert Julian Day 2446113.75 to year, month, and day.
    >>> jd_to_date(185)
    (1985, 2, 17)

    """
    jd = jd + 0.5 + 2444238.5
    F, I = math.modf(jd)
    I = int(I)
    A = math.trunc((I - 1867216.25) / 36524.25)
    if I > 2299160:
        B = I + 1 + A - math.trunc(A / 4.)
    else:
        B = I
    C = B + 1524
    D = math.trunc((C - 122.1) / 365.25)
    E = math.trunc(365.25 * D)
    G = math.trunc((C - E) / 30.6001)
    day = int(C - E + F - math.trunc(30.6001 * G))
    if G < 13.5:
        month = G - 1
    else:
        month = G - 13
    if month > 2.5:
        year = D - 4716
    else:
        year = D - 4715
    if(flag == 0):
        y_m_d=str(month).zfill(2) + "-" + str(day).zfill(2) + "-" + str(year)
    elif(flag == 1):
        y_m_d= str(year) + str(month).zfill(2) + str(day).zfill(2)
    elif(flag == 2):
        doy = cal_to_doy(year, month, day)
        y_m_d = str(year) + str(doy).zfill(3)
    elif (flag == 3):
        y_m_d = str(year) + "-" + str(month).zfill(2) + "-" + str(day).zfill(2)
    return y_m_d


def doy_to_cal(jd1):
    jd = str(jd1)
    # initializing day number
    day_num = jd[4:7]
    # adjusting day num
    day_num.rjust(3 + len(day_num), '0')
    # Initialize year
    year = int(jd[0:4])
    # Initializing start date
    strt_date = date(int(year), 1, 1)
    # converting to date
    res_date = strt_date + timedelta(days=int(day_num) - 1)
    res = res_date.strftime("%m-%d-%Y")
    return res


def jul_to_cal(jd):
    """
    Modified convert Julian Day to date. It sets Dec 31, 1979 as date 0 by substracting 2444238.5 from absolute julian date
    Algorithm from 'Practical Astronomy with your Calculator or Spreadsheet',
        4th ed., Duffet-Smith and Zwart, 2011.
    Parameters
    ----------
    jd : float
        Julian Day
    Returns
    -------
    year : int
        Year as integer. Years preceding 1 A.D. should be 0 or negative.
        The year before 1 A.D. is 0, 10 B.C. is year -9.

    month : int
        Month as integer, Jan = 1, Feb. = 2, etc.
    day : int
        Day
    Examples
    --------
    Convert Julian Day 2446113.75 to year, month, and day.
    >>> jd_to_date(185)
    (1985, 2, 17)
    """
    jd = jd + 0.5 + 2444238.5
    F, I = math.modf(jd)
    I = int(I)
    A = math.trunc((I - 1867216.25) / 36524.25)
    if I > 2299160:
        B = I + 1 + A - math.trunc(A / 4.)
    else:
        B = I
    C = B + 1524
    D = math.trunc((C - 122.1) / 365.25)
    E = math.trunc(365.25 * D)
    G = math.trunc((C - E) / 30.6001)
    day = int(C - E + F - math.trunc(30.6001 * G))
    if G < 13.5:
        month = G - 1
    else:
        month = G - 13
    if month > 2.5:
        year = D - 4716
    else:
        year = D - 4715
    mdy =  str('{:0>2}'.format(str(month))) + '-' + str('{:0>2}'.format(str(day))) + '-' + str(year)
    return mdy


def Generate_AgroET_Seasonal_ET(
    basedir,
    Season_SD,
    Season_ED,
    sorted_arr,
    aoi_file,
    ratio_flag
):
#    DEBUGFLAG = 1
#    AgroET_file = 'AgroET_seasonal_input.txt'
#    aoi_file = 'aoi.shp'
    ws = 'ws_orig.shp'
    write_log('\nAgroET Seasonal ET Version 1.56\n')
    write_log('Starting calculation')
    write_log('\nAgroET Seasonal ET Version 1.56\n')
    write_log('Starting calculation')
    
#    import pdb; pdb.set_trace()
    
    GENERATE_CLOUD_MASK = 1
    FILL_START_END_ET = 1
    name_reset_et = []
    num_reset_et =[]
    ls_counter = 0
    for item in sorted_arr:
        # Incrementing counter variable to get each item in the list
        name_reset_et.append(str(item[1]))
        num_reset_et.append(doy_to_cal(str(item[0])))
        ls_counter = ls_counter + 1
    if DEBUGFLAG == 1:
        write_log("LS_counter and sorted_arr ", ls_counter, num_reset_et)
    # Checking if the base directory exists
    basedir = os.path.abspath(basedir)
    check_directory_exists(basedir, 'Base directory ')
    aoi = basedir + '/' + aoi_file
    check_file_exists(aoi_file, 'Area of Interest (AOI) shapefile ', basedir)
    utm_zone = shape_utm_prj(aoi)
    check_utm_prj(utm_zone, aoi_file,'shapefile', basedir)
    check_file_exists(ws, 'Weather Station shapefile ', basedir)
    check_utm_prj(utm_zone, ws,'shapefile', basedir)
    if DEBUGFLAG == 1:
        write_log("after checking the utm projection of the weather stations ")
    # Checking if the AOI file exists and is in the correct projection
    py_script_dir = (r"C:\Users\lag\AppData\Local\Continuum\anaconda3\envs\AgroET\Scripts")
    py1_script_dir = (r"C:\OSGeo4W64\bin")
    py_script_dir = "/mnt/work/work_home/work/new/reset/csip-reset2/src/java/python"
    if not os.path.exists(py_script_dir):
        py_script_dir = '/usr/bin'
    if not os.path.exists(os.path.join(py_script_dir, 'gdal_calc.py')):
        py_script_dir = '/usr/local/bin/'
    os.chdir(basedir)
    delete_shp_file(aoi_file[:-4] + "1.shp")
    if DEBUGFLAG == 1:
        write_log("before copying the aoi file to aoi1")
    aoi_file1 = aoi_file[:-4] + "1.shp"
    copy_shp_file(aoi_file, aoi_file1, basedir)
#    aoi_file = aoi_file1
    # Checking if the AOI file exists
    aoi_exists = os.path.isfile(aoi)
    if DEBUGFLAG == 1:
        write_log("aoi and aoi_file are ", aoi, aoi_file, aoi_file1)
    if (aoi_exists == 0):
        raise Exception('Area of Interest shapefile ', aoi, ' does not exist !!!!')
    workspace = basedir
    # Change directory to the workspace
    os.chdir(workspace)
    cwd = os.getcwd()

    if DEBUGFLAG == 1:
        write_log("checking the extents of the aoi file", aoi_file)
    clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells = aoi_extent(aoi_file)

    if DEBUGFLAG == 1:
        write_log("before the jul_start and jul_end",num_reset_et[0],ls_counter,num_reset_et[ls_counter-1], num_reset_et)
    
    jul_start = date_to_mjd(num_reset_et[0],1)
    jul_end = date_to_mjd(num_reset_et[ls_counter-1],1)
    
    if DEBUGFLAG == 1:
        write_log("AFTER the jul_start and jul_end", jul_start, jul_end)
        write_log('Jul_start, Jul_end',jul_start,jul_end)
    delete_files_w_pattern(basedir, "ET_day*.*")
    if(ratio_flag == 0):
        delete_files_w_pattern(basedir, "ET_seasonal_*.*")
    else:
        delete_files_w_pattern(basedir, "ET_seasonal_NDVI_*.*")
    if GENERATE_CLOUD_MASK == 1:
        delete_files_w_pattern(basedir, "mask/*.*")
        Cloud_mask = 'Cloud_mask.img'
        if DEBUGFLAG == 1:
            write_log('Before shp_to_img ', aoi_file)
        shp_to_img(aoi_file, clip_x1, clip_y1, clip_x2, clip_y2, utm_zone, basedir)
        if DEBUGFLAG == 1:
            write_log('Before bookend_cloud_mask Jul_start, jul_end', jul_start, jul_end)
            write_log('Before bookend_cloud_mask basedir ', basedir)
            write_log('Before bookend_cloud_mask aoi_file ', aoi_file)
            write_log('Before bookend_cloud_mask num_reset_et ', num_reset_et)
            write_log('Before bookend_cloud_mask ls_counter', ls_counter)
            write_log('Before bookend_cloud_mask py_script_dir, FILL_START_END_ET, jul_start, jul_end ', py_script_dir, FILL_START_END_ET, jul_start, jul_end)
            write_log('Before bookend_cloud_mask name_reset_et', name_reset_et)
        bookend_cloud_mask(py_script_dir, FILL_START_END_ET, jul_start, jul_end, num_reset_et, ls_counter, name_reset_et, basedir, aoi_file)
        if DEBUGFLAG == 1:
            write_log('Before intermediary_cloud_mask Jul_start, jul_end', jul_start, jul_end)
        intermediary_cloud_mask(py_script_dir, jul_start, jul_end, num_reset_et, ls_counter, basedir)
        delete_files_w_pattern(basedir, "masks_mask*.*") # 11-05-20
        if DEBUGFLAG == 1:
            write_log('After intermediary_cloud_mask Jul_start, jul_end', jul_start, jul_end)

    Seasonal_cloud_mask = 'Seasonal_Cloud_mask.img'
    gen_seasonal_cloud_mask(py_script_dir, 0, ls_counter-1, aoi_file, basedir, Seasonal_cloud_mask, name_reset_et)
    if DEBUGFLAG == 1:
        write_log('After gen_seasonal_cloud_mask Jul_start, jul_end', jul_start, jul_end)

#LAG Seasonal beginning
    str_cal_day = mjd_to_date(jul_start,0)
    str_cal_day1 = mjd_to_date(jul_end,0)

    if(ratio_flag == 0):
        ET_cum = "ET_seasonal_" + str_cal_day + "_" + str_cal_day1 + ".img"
    else:
        ET_cum = "ET_seasonal_NDVI_" + str_cal_day + "_" + str_cal_day1 + ".img"
    delete_file_w_path(basedir + '/' + ET_cum)
    ET_cum1 = 'ET_cum1.img'
    ET_cum3 = 'ET_cum3.img'
    ET_cum4 = 'ET_cum4.img'
    ET_cum5 = 'ET_cum5.img'
    delete_file_w_path(basedir + '/' + ET_cum1)
    delete_file_w_path(basedir + '/' + ET_cum3)
    delete_file_w_path(basedir + '/' + ET_cum4)
    delete_file_w_path(basedir + '/' + ET_cum5)
    # generate a zero value grid as the initial value for the seasonal ET sum
    str_t = mjd_to_date(jul_start,0)
    gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + str_t +'_ETr_WS_daily.img' + " --outfile=" + basedir + "/" + ET_cum1 + " --calc=" + '"(0.0*A)"' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
#    write_log(gdal_calc)
    gdal_calc_func(ET_cum1, gdal_calc, basedir)
    copyFile(basedir + '/' + ET_cum1, basedir + '/' + ET_cum3)
    copyFile(basedir + '/' + ET_cum1, basedir + '/' + ET_cum4)
    copyFile(basedir + '/' + ET_cum1, basedir + '/' + ET_cum5)
#    if DEBUGFLAG == 1:
    write_log("NOTICE THAT KEEP_INTERMEDIARY_FILES IS SET TO FALSE (0) - line 1094")
    KEEP_INTERMEDIARY_FILES = 0
    if(KEEP_INTERMEDIARY_FILES == 0):
        delete_files_w_pattern(basedir, "ET_day_*.*")
#    write_log('jul_start ',jul_start,'jul_end ',jul_end)
    for t in range(jul_start, jul_end + 1):
        str_t = mjd_to_date(t,0)
#        write_log(' t is ',t,'str_t is ',str_t)
        ET_day = "ET_day_"+ str_t +".img"
#        write_log('ET_day is ', ET_day)
        # generate a zero value grid as the initial value for the daily ET
        str_t = mjd_to_date(jul_start,0)
#        write_log(' jul_start is ', jul_start, 'str_t is ', str_t)
        gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + str_t +'_ETr_WS_daily.img' + " --outfile=" + basedir + "/" + ET_day + " --calc=" + '"(0.0*A)"' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
#        write_log('gdal_calc before KEEP INTERMEDIARY ', gdal_calc)
        gdal_calc_func(ET_day,gdal_calc, basedir)
    # LAG Seasonal end
    if(KEEP_INTERMEDIARY_FILES == 0):
#        delete_files_w_pattern(basedir, "*_ETr_WS_daily.img")
#        delete_files_w_pattern(basedir, "ET_daily_*.*")
        delete_files_w_pattern(basedir, "ETr*.*")
        delete_files_w_pattern(basedir, "ET_corr_fact*.*")

    if DEBUGFLAG == 0 or DEBUGFLAG == 1:
        write_log("finished with first loop to calculate ET for each day")
#    import pdb;pdb.set_trace()
    for t in range(1, ls_counter):
        count = 0
        if DEBUGFLAG == 1:
            write_log("inside outside loop for ET_cum t counter is ", t)
        for t1 in range(t+1, ls_counter):
            if (t > 1):
                str_ls_name = num_reset_et[t]
                day = str(int(str_ls_name[3:5])+1)
                str_j_start = str_ls_name[0:2].zfill(2) + '-' + (day).zfill(2) + '-' + str_ls_name[6:10]
                j_start = date_to_mjd(str_j_start,1)
                str_ls_date = str_j_start
                str_ls_date1 = num_reset_et[t1]
                cum_mask = 'masks_cum_mask1_' + str_ls_date + '_' + str_ls_date1 + '.img'
    #LAG Seasonal beginning
                if(ratio_flag == 0):
                    ET_cum2 = 'ET_seasonal_' + str_ls_date + '_' + str_ls_date1 + '.img'
                else:
                    ET_cum2 = 'ET_seasonal_NDVI_' + str_ls_date + '_' + str_ls_date1 + '.img'
    # LAG Seasonal end
            else:
    #            j_start = date_to_mjd(Season_SD, 1)
                j_start = date_to_mjd(num_reset_et[t], 1)
                str_ls_date = num_reset_et[t]
                str_ls_date1 = num_reset_et[t1]
                cum_mask = 'masks_cum_mask1_' + str_ls_date + '_' + str_ls_date1 + '.img'
    # LAG Seasonal
                if(ratio_flag == 0):        
                    ET_cum2 = 'ET_seasonal_' + str_ls_date + '_' + str_ls_date1 + '.img'
                else:
                    ET_cum2 = 'ET_seasonal_NDVI_' + str_ls_date + '_' + str_ls_date1 + '.img'
    # LAG Seasonal end
            num_agroet_scenes = str(count)
            str_j_end = num_reset_et[t1]
            j_end = date_to_mjd(str_j_end, 1)
            if DEBUGFLAG == 1:
                write_log("before Daily_ET_calc1", t, ls_counter)
            Daily_ET_calc1(py_script_dir,t,j_start,j_end,cum_mask,num_agroet_scenes, name_reset_et, basedir, aoi_file)
            count +=1
    # LAG Seasonal
            if DEBUGFLAG == 1 or DEBUGFLAG == 0:
                write_log("before Seasonal_ET_calc1 t, t1, counter ", t, t1, count)
                write_log("checking if ET_cum3 exists Line 1181", os.path.isfile(basedir + '/' + ET_cum3))
            Seasonal_ET_calc1(py_script_dir, j_start, j_end, basedir, ET_cum2, ET_cum3)
        str_cal_day = mjd_to_date(j_start,0)
        delete_files_w_pattern(basedir, "ETr_WS_daily_" + str_cal_day + "_" + "*.*")

    # LAG Seasonal end
#    delete_files_w_pattern(basedir, "*_ETr_WS_daily.img")
    delete_files_w_pattern(basedir, "masks_cum_mask*.*")
#    delete_files_w_pattern(basedir, "ET_daily_*.*")
    delete_files_w_pattern(basedir, "Cloud_mask*.*")

    # generate a zero value grid as the initial value for the seasonal ET sum
    gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + ET_cum3 + ' -B ' + basedir + '/' + Seasonal_cloud_mask + " --outfile=" + basedir + "/" + ET_cum + " --calc=" + '"(A*B)"' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
    gdal_calc_func(ET_cum, gdal_calc, basedir)
#    delete_file_w_path(basedir + '/' + ET_cum3)
    write_log('Calculate Seasonal ET start of season ',Season_SD,'Calculate end of season ',Season_ED)
    MJD_Season_SD = date_to_mjd(str(Season_SD), 3) + 32
    if DEBUGFLAG == 1:
        write_log("Converting Season start date to MDJ + 32", Season_SD, MJD_Season_SD)
    Mod_Season_SD = str(mjd_to_date(MJD_Season_SD, 3))
    if DEBUGFLAG == 1:
        write_log('Season Start Date ', Mod_Season_SD)
    MJD_Season_ED = date_to_mjd(str(Season_ED), 3) - 32
    if DEBUGFLAG == 1:
        write_log("Converting Season end date to MDJ - 32", Season_ED, MJD_Season_ED)
    Mod_Season_ED = str(mjd_to_date(MJD_Season_ED, 3))
    if DEBUGFLAG == 1:
        write_log('Season Start Date ', Mod_Season_ED)

    Seasonal_ET_calc2(py_script_dir, Mod_Season_SD, Mod_Season_ED, basedir, ET_cum4, ET_cum5)
    if(ratio_flag == 0):
        ET_cum_season = "ET_seasonal_exact_dates_" + Mod_Season_SD + "_" + Mod_Season_ED + ".img"
    else:
        ET_cum_season = "ET_seasonal_NDVI_exact_dates_" + Mod_Season_SD + "_" + Mod_Season_ED + ".img"
    delete_file_w_path(basedir + '/' + ET_cum_season)
    gdal_calc = 'python ' + py_script_dir + '/gdal_calc.py -A ' + basedir + '/' + ET_cum5 + ' -B ' + basedir + '/' + Seasonal_cloud_mask + " --outfile=" + basedir + "/" + ET_cum_season + " --calc=" + '"(A*B)"' + " --type=Float64" + " --overwrite --quiet --NoDataValue=-9999"
    gdal_calc_func(ET_cum_season, gdal_calc, basedir)
    delete_file_w_path(basedir + '/' + ET_cum5)
    delete_file_w_path(basedir + '/' + Seasonal_cloud_mask)

    write_log('Generated ET Seasonal File')
    write_log('Generated ET Seasonal File = ',ET_cum_season)

    send_email.send(
    "AgroET Generated ET Seasonal File",
    "Please rerun the API to download the results - they are ready!!")

    #A temporary AOI file is created (aoi_file_name)1.shp.  At the end of the run this file can be removed.
    del_tmp_files(basedir)
    return()

if __name__ == "__main__":
    write_log('Running AgroET Seasonal 1.0')
    send_email.send(
    "AgroET Seasonal Model Done with Pre-prosessing all data ready to generate Seasonal ET",
    "AgroET Seasonal Model Done with Pre-prosessing all data ready to generate Seasonal ET")

    Generate_AgroET_Seasonal_ET(
        basedir,
        Season_SD,
        Season_ED,
        sorted_arr,
        aoi_file,
        ratio_flag
    )