import os
from requests import Session
import time
from AgroET_library import write_log

def Harmonized_Dual_Download(
    min_lon, max_lon, min_lat, max_lat, start_date, end_date
):
#    write_log("Inside Harmonized Download ")
#    write_log("arguments x ", min_lon)
#    write_log("arguments x,y ", min_lon, max_lon, min_lat, max_lat)
#    write_log("arguments date ", start_date, end_date)
    # Set the download directory where data will be saved
    download_dir = "hls_data_F"

    # Define the date range (start and end dates)
#    start_date = "2020-06-01"  # Replace with your start date
#    end_date = "2020-06-30"  # Replace with your end date

    # Earthdata Login credentials (replace with your own Earthdata token)
    earthdata_token = "eyJ0eXAiOiJKV1QiLCJvcmlnaW4iOiJFYXJ0aGRhdGEgTG9naW4iLCJzaWciOiJlZGxqd3RwdWJrZXlfb3BzIiwiYWxnIjoiUlMyNTYifQ.eyJ0eXBlIjoiVXNlciIsInVpZCI6Imlkc2dyb3VwMSIsImV4cCI6MTcxMTA3NDc1MCwiaWF0IjoxNzA1ODkwNzUwLCJpc3MiOiJFYXJ0aGRhdGEgTG9naW4ifQ.WgKEnbPD8miffOdEoQ4Xsj2oMkTRwJPEzubJRk5kdXts7v6fEdSjqsIxdgh5gbZ9mbFYnxdb2IwXNIRjOrR4d4JDM0w10Gmlgpk_uib4fvvkRQjPop067rHZq3fLNmFT4VBD6gFi8gw-B_vaMghSGUOW6NgUqw-R2igSOO6jDLHAM32ML3qC52_vH2Ys5BmM8Qj8g92_IrYl-FYQgCC0MlCpc_9wnlPt9i4GdLW3Dnrb60-KNa6PftDQR7mD1hoThRnZQZ3ElVm81RvnGe40c4zy78cRSI03gZd5wYlpYqIOmUm1fO3k_PCnak32zpfqZIj-_mGDF2HtunxZLtttEg"

    # Specify the collection short_names and versions in a list
    HLS_col = [
        {"short_name": "HLSS30", "version": "2.0"},
        {"short_name": "HLSL30", "version": "2.0"},
    ]

    # Define the bounding box coordinates (10 km by 10 km around a specific location in California)
    # Replace with the actual coordinates you want to use
 #   min_lon = -115.7  # Minimum longitude
 #   max_lon = -114.4  # Maximum longitude
 #   min_lat = 32.6  # Minimum latitude
 #   max_lat = 33.3  # Maximum latitude

    # Specify the desired layers (bands) for Sentinel-2 and Landsat 8
    # Sentinel 2 layers
    sentinel_layers = ["B8A", "B04", "Fmask"]
    # Landsat 8 layers
    landsat_layers = ["B05", "B04", "Fmask"]

    # Create the download directory if it doesn't exist
    os.makedirs(download_dir, exist_ok=True)

    # Define the Earthdata Search API endpoint
    search_url = "https://cmr.earthdata.nasa.gov/search/granules.json"

    # Specify the maximum allowed cloud cover percentage (10%)
    max_cloud_cover = 99
    with Session() as session:
        session.headers = {"Authorization": f"Bearer {earthdata_token}"}

        # Loop through the HLS_col list
        for collection_info in HLS_col:
            collection_short_name = collection_info["short_name"]
            collection_version = collection_info["version"]

            # Define the data request parameters for the current collection
            request_params = {
                "short_name": collection_short_name,
                "version": collection_version,
                "temporal": f"{start_date}T00:00:00Z,{end_date}T23:59:59Z",
                "bounding_box": f"{min_lon},{min_lat},{max_lon},{max_lat}",
                "page_size": 200,  # Adjust as needed
                "page_num": 1,
                # "token": earthdata_token,
            }

            # Submit the data request to Earthdata Search API for the current collection
            response = session.get(search_url, params=request_params)

            if response.status_code == 200:
                granules = response.json().get("feed", {}).get("entry", [])

                if not granules:
                    write_log(
                        f"No data granules found for {collection_short_name} {collection_version}."
                    )
                else:
                    # Create a list of download URLs for the granules with cloud cover condition
                    download_urls = []
                    if collection_short_name == "HLSL30":
                        layers = landsat_layers
                    if collection_short_name == "HLSS30":
                        layers = sentinel_layers

                    for g in granules:
                        cloud_cover = int(g.get("cloud_cover", "0"))
#                        write_log(f"cloud cover is {cloud_cover} for granule {g}")
                        if cloud_cover >= max_cloud_cover:
#                            write_log(
#                                f"not adding because cloud_cover {cloud_cover} > {max_cloud_cover}"
#                            )
                            continue
                        links = g.get("links", [])
                        for link in links:
                            download_url = link.get("href", "")
                            if any(
                                layer in download_url
                                for layer in layers
                            ):
                                download_urls.append(download_url)

                    # Download the granules with a retry mechanism
                    max_retries = 1  # Maximum number of retries
                    retry_delay = 1  # Delay between retries in seconds

                    for url in download_urls:
                        retry_count = 0
                        filename = os.path.join(
                            download_dir, os.path.basename(url)
                            )
                        if os.path.exists(filename): continue
                         
                        while retry_count < max_retries:
                            # the s3 urls don't work
                            if "s3" in url:
                                write_log("skipping", url)
                                break

                            try:
                                response = session.get(url)
                                if response.status_code == 200:
                                    # Save the file directly to the download directory
                                    with open(filename, "wb") as f:
                                        f.write(response.content)
#                                    write_log(f"Downloaded: {filename}")
                                    write_log(f"Downloaded: {filename}")
                                    break
                                else:
#                                    write_log("response is", response)
#                                    write_log(f"Failed to download {url}. Retrying...")
                                    write_log(
                                        f"Failed to download {url}. Retrying..."
                                    )
                                    retry_count += 1
                                    time.sleep(retry_delay)
                            except Exception as e:
#                                write_log(f"Failed to download {url}. Retrying...")
                                write_log(f"Failed to download {url}. Retrying...")
                                retry_count += 1
                                time.sleep(retry_delay)
                        else:
                            # Max retries reached, give up on this granule
#                            write_log(f"Failed to download {url} after {max_retries} retries.")
                            write_log(
                                f"Failed to download {url} after {max_retries} retries."
                            )
            else:
#                write_log(
#                    f"Data request for {collection_short_name} {collection_version} failed. Status code: {response.status_code}"
#                )
#                write_log(f"Response content: {response.content.decode('utf-8')}")
                write_log(
                    f"Data request for {collection_short_name} {collection_version} failed. Status code: {response.status_code}"
                )

#        write_log("All downloads completed.")

