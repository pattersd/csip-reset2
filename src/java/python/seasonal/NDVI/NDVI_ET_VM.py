from __future__ import print_function

# This must be the first statement before other statements.
# You may only put a quoted or triple quoted string,
# Python comments, other future statements, or blank lines before the __future__ line.

try:
    import __builtin__
except ImportError:
    import builtins as __builtin__

             
import os
import math
import pdb
from osgeo import ogr
import argparse  
import geopandas
from operator import itemgetter
import numpy as np
import shutil
import subprocess
from datetime import date, datetime, timedelta
from AgroET_library import write_log

from osgeo.gdalnumeric import *
#from osgeo.gdalconst import *

# Raster layers cell size
cellSize = 30

DEBUGFLAG = 1

def delete_shp_file(shp_name):
    if os.path.exists(shp_name):
        driver = ogr.GetDriverByName("ESRI Shapefile")
        driver.DeleteDataSource(shp_name)


# This function adds a buffer to a shapefile.
def shp_buffer(in_shp, buff, out_shp):
    delete_shp_file(out_shp)
    shp = ogr.Open(in_shp)
    drv = shp.GetDriver()
    drv.CopyDataSource(shp, out_shp)
    shp.Destroy()
    buf1 = ogr.Open(out_shp, 1)
    lyr1 = buf1.GetLayer(0)
    for i in range(0, lyr1.GetFeatureCount()):
        feat = lyr1.GetFeature(i)
        lyr1.DeleteFeature(i)
        geom1 = feat.GetGeometryRef()
        feat.SetGeometry(geom1.Buffer(buff))
        lyr1.CreateFeature(feat)
    feature = lyr1.GetNextFeature()
    while feature is not None:
        geom = feature.GetGeometryRef()
        try:
            area = geom.GetArea()
            if area < cellSize*cellSize*1.1:
                lyr1.DeleteFeature(feature.GetFID())
        except:
            lyr1.DeleteFeature(feature.GetFID())
        feature = lyr1.GetNextFeature()
    buf1.Destroy()


def PixelResolution(fp_path):
    fp = open(fp_path)
    for line in fp:
        if "Pixel Size " in line:
            line2 = line.split('=' or ",")[-1].strip()
            line3 = line2[1:-1]
            xres, yres = map(real, line3.split(","))
            if DEBUGFLAG >= 3:
                write_log('Pixel Size String ', line3, 'X resolution ', xres, 'Y resolution ', yres)
            break
    fp.close()
    return (abs(float(xres)), abs(float(yres)))


def delete_file(filename, basedir):
    full_name = basedir + '/' + filename
    if (os.path.isfile(full_name)):
        try:
            os.remove(full_name)
        except OSError:
            pass


def aoi_extent(aoi):
    inDriver = ogr.GetDriverByName("ESRI Shapefile")
    inDataSource = inDriver.Open(aoi, 0)
    inLayer = inDataSource.GetLayer()
    extent = inLayer.GetExtent()
    del inDataSource
    clip_x1 = extent[0]
    clip_y1 = extent[3]
    clip_x2 = extent[1]
    clip_y2 = extent[2]
    x_cells = round((clip_x2 - clip_x1) / cellSize)
    y_cells = round((clip_y1 - clip_y2) / cellSize)
    return (clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells)


# def clip_img1(in_image, out_image, aoi_lyr):
def clip_img1(in_image, out_image, aoi_lyr, basedir1):
    out_image1 = out_image[:-4] + '_ZZ' + out_image[-4:]
    delete_file(out_image, basedir1)
    delete_file(out_image1, basedir1)
    delete_file("gdalinfo_output.txt", basedir1)
    if not os.path.exists(basedir1 + '/' + in_image):
        if DEBUGFLAG == 1:
            write_log("inside clip_img1 input file does not exit ", basedir1 + '/' + in_image)
        exit(272)
    else:
        gdalinfo = "gdalinfo -nomd -norat -noct " + basedir1 + '/' + in_image + " > " + basedir1 + "/gdalinfo_output.txt"
    if DEBUGFLAG == 1:
        write_log(gdalinfo)
    os.system(gdalinfo)
    xres, yres = PixelResolution(basedir1 + "/gdalinfo_output.txt")
    shp_buffer(aoi_lyr, 0.0, aoi_lyr[:-4]+'_buff.shp')
    gdal_warp = ["gdalwarp", "-dstnodata", "-9999", "-overwrite",
                              "-tr", str(xres), str(yres), "-cutline", aoi_lyr[:-4]+'_buff.shp',
                              basedir1 + '/' + in_image, basedir1 + '/' + out_image1]
    clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells = aoi_extent(aoi_lyr)
    if DEBUGFLAG == 1:
        write_log(' '.join(gdal_warp))
    out = subprocess.check_output(gdal_warp)
    ds = gdal.Open(basedir1 + '/' + out_image1)
    ds = gdal.Translate(basedir1 + '/' + out_image, ds, projWin=[clip_x1, clip_y1, clip_x2, clip_y2])
    ds = None
#5-20-20    delete_file_w_path(basedir1 + '/' + out_image1)


def doy_to_cal(jd1):
    jd = str(jd1)
    # initializing day number
    day_num = jd[4:7]
    # adjusting day num
    day_num.rjust(3 + len(day_num), '0')
    # Initialize year
    year = int(jd[0:4])
    # Initializing start date
    strt_date = date(int(year), 1, 1)
    # converting to date
    res_date = strt_date + timedelta(days=int(day_num) - 1)
    res = res_date.strftime("%m-%d-%Y")
    return res


def delete_file_w_path(filename):
    full_name = filename
    if os.path.isfile(full_name):
        try:
            os.remove(full_name)
        except OSError:
            # LAG 8-12            if DEBUGFLAG == 1:
            # LAG 8-12                write_log('!!!!!!!! could not remove file ', full_name)
            pass


def gdal_calc_func(outfile_name, gdal_calc, basedir):
    delete_file_w_path(basedir + "/" + outfile_name)
#    write_log("gdal_calc_func ", gdal_calc)
    if DEBUGFLAG == 2:
        write_log(gdal_calc)
    os.system(gdal_calc)


def NDVI_ET_VM(project, aoi_lyr):
    # Raster layers cell size
    cellSize = 30

    NDVI_res_dir = "NDVI"

    basedir0 = "/reset/seasonal_projects"
    basedir = os.path.join(basedir0, str(project))
    path_NDVI = os.path.join(basedir,NDVI_res_dir)

    ratio_image_names=[]
    ratio_dates1=[]
    ratio_dates2=[]
    ratio_count = -1


    for f in os.listdir(path_NDVI):
        if f.endswith("_NDVI_ratio.img"):
#            import pdb; pdb.set_trace()
            ratio_count += 1
            ratio_date0 = f[15:-15]
            write_log('ratio_date0 ',ratio_date0, 'f ', f)
            ratio_date = doy_to_cal(ratio_date0)
            ratio_dates1.append([ratio_date0,ratio_date])
            write_log('found the NDVI_ratio file DATE IS ',ratio_dates1, ratio_date)
            ratio_image_names.append(f)
#            Specify the date and format within strptime in this case format is %Y-%m-%d (if your data was 2020/03/30' then use %Y/%m/%d) and specify the format you want to convert into in strftime, in this case it is %Y%j. This gives out Julian date in the form CCYYDDD, if you need only in YYDDD give the format as %y%j
            write_log('Sentinel ratio File is ', ratio_image_names[ratio_count], "ratio_count is ", ratio_count)
            write_log('Sentinel ratio Image Date is ', ratio_date, ratio_date0)
    
    write_log("ratio_dates1 is ",ratio_dates1)

#    import pdb;pdb.set_trace() 

    cnt_ratios = 0
    for [ratio_date0,ratio_date] in ratio_dates1:
        write_log("ratio date is ",ratio_date)
        for f in os.listdir(basedir):
#            import pdb;pdb.set_trace() 
            if f.startswith("ET_day_") and ratio_date in f:
                if not os.path.exists(basedir + "/NDVI_Ratio_ET_"+ str(ratio_date0) + "_ET_24_raster.img"):
                    clip_img1(ratio_image_names[cnt_ratios], "temp.img", aoi_lyr, path_NDVI)
                    write_log("ratio file is ",ratio_image_names[cnt_ratios],"Day ET file is ",f)
                    gdal_calc = (
                        "gdal_calc.py -A " 
                        + basedir
                        + "/"
                        + f
                        + " -B "
                        + path_NDVI
                        + "/temp.img"
                        + " --outfile="
                        + basedir + "/" 
                        + "NDVI_Ratio_ET_"+ str(ratio_date0) + "_ET_24_raster.img"
                        + " --calc="
                        + '"(A*B)" '
                        + "--type=Float64"
                        + " --overwrite --quiet --NoDataValue=-9999"
                    )
                    write_log("gdal_calc = ",gdal_calc,"\n")
                    gdal_calc_func(basedir + "/" 
                        + "NDVI_Ratio_ET_"+ ratio_date + "_ET_24_raster.img", gdal_calc, basedir)
                    cnt_ratios += 1
                break      
    return

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Run the AgroET model for a date and an AOI."
    )
    
    parser.add_argument(
        "--project", type=str, default=None, help="project name for seasonal run"
    )

    parser.add_argument(
        "--aoi",
        type=str,
#        default="aoi.shp",
        help="No AOI shafile name",
    )

    args = parser.parse_args()

    NDVI_ET_VM(
        args.project,
        args.aoi
    )
    # all finished
