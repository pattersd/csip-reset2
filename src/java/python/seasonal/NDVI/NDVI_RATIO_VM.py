from __future__ import print_function

# This must be the first statement before other statements.
# You may only put a quoted or triple quoted string,
# Python comments, other future statements, or blank lines before the __future__ line.

try:
    import __builtin__
except ImportError:
    import builtins as __builtin__

#from osgeo import gdal
#from osgeo import osr
#from osgeo import ogr
import os
import argparse
import pdb
#import gdal
from harmo_w_filt_dual import Harmonized_Dual_Download
import geopandas
from operator import itemgetter
import numpy as np
#import subprocess, shutil
import shutil
#import rasterio
from AgroET_library import *

def find_NDVI_images(NDVI_image_names, NDVI_dates, path):
    ls_count1 = -1 #because the first value in the array is 0 so when you add 1 you end up with 0
    
    for f in os.listdir(path):
#        write_log('basedir file is ',f)
#        if os.path.isdir(os.path.join(basedir, t)):
#            for f in os.listdir(basedir + '/' + t):
        if f.endswith("NDVI.tif") and f.startswith("HLS.S"):
            ls_count1 += 1
            JulDate = f[15:-21]
#            print('found the NDVI file JUL DATE IS ',JulDate)
#            utm_zone1 = raster_utm_prj(basedir + '/' + t + '/' + t1 + '_B1.TIF')
#            utm_zone1 = raster_utm_prj(basedir + '/' + t1 + '_B1.TIF')
            NDVI_image_names.append(f)
#            Specify the date and format within strptime in this case format is %Y-%m-%d (if your data was 2020/03/30' then use %Y/%m/%d) and specify the format you want to convert into in strftime, in this case it is %Y%j. This gives out Julian date in the form CCYYDDD, if you need only in YYDDD give the format as %y%j
            NDVI_dates.append(JulDate)
#            print('Sentinel File is ', NDVI_image_names[ls_count1], "ls_count1 is ", ls_count1)
#            print('Sentinel Image Date is ', JulDate)
            
    return(ls_count1)


def find_LS_NDVI_images(LS_image_names, LS_dates, path):
    ls_count1 = -1 #because the first value in the array is 0 so when you add 1 you end up with 0
#    print(path)
    
    for f in os.listdir(path):
#        write_log('basedir file is ',f)
#        if os.path.isdir(os.path.join(basedir, t)):
#            for f in os.listdir(basedir + '/' + t):
#        print("name for LS_NDVI is ",f)
        if f.endswith("NDVI.tif") and f.startswith("HLS.L"):
            ls_count1 += 1
            JulDate = f[15:-21]
#            print('found the NDVI LS file JUL DATE IS ',JulDate)
#            utm_zone1 = raster_utm_prj(basedir + '/' + t + '/' + t1 + '_B1.TIF')
#            utm_zone1 = raster_utm_prj(basedir + '/' + t1 + '_B1.TIF')
            LS_image_names.append(f)
#            Specify the date and format within strptime in this case format is %Y-%m-%d (if your data was 2020/03/30' then use %Y/%m/%d) and specify the format you want to convert into in strftime, in this case it is %Y%j. This gives out Julian date in the form CCYYDDD, if you need only in YYDDD give the format as %y%j
            LS_dates.append(JulDate)
#            print('LandSat File is ', LS_image_names[ls_count1], "ls_count1 is ", ls_count1)
#            print('LandSat Image Date is ', JulDate)
            
    return(ls_count1)


def find_AgroET_images(LS_image_names, LS_dates, path):
    ls_count1 = -1 #because the first value in the array is 0 so when you add 1 you end up with 0
#    print(path)
    
    for f in os.listdir(path):
#        write_log('basedir file is ',f)
#        if os.path.isdir(os.path.join(basedir, t)):
#            for f in os.listdir(basedir + '/' + t):
        if f.endswith("ET_24_raster.img"):
            ls_count1 += 1
            JulDate = f[9:-17]
#            print('found the AgroET run file JUL DATE IS ',JulDate)
#            utm_zone1 = raster_utm_prj(basedir + '/' + t + '/' + t1 + '_B1.TIF')
#            utm_zone1 = raster_utm_prj(basedir + '/' + t1 + '_B1.TIF')
            LS_image_names.append(f)
#            Specify the date and format within strptime in this case format is %Y-%m-%d (if your data was 2020/03/30' then use %Y/%m/%d) and specify the format you want to convert into in strftime, in this case it is %Y%j. This gives out Julian date in the form CCYYDDD, if you need only in YYDDD give the format as %y%j
            LS_dates.append(JulDate)
#            print('Sentinel File is ', LS_image_names[ls_count1], "ls_count1 is ", ls_count1)
#            print('Sentinel Image Date is ', JulDate)
            
    return(ls_count1)


def NDVI_RATIO_VM(project):
    # Raster layers cell size
    cellSize = 30

    NDVI_res_dir = "NDVI"

    basedir0 = "/reset/seasonal_projects"
    basedir = os.path.join(basedir0, str(project))
    path_NDVI = os.path.join(basedir,NDVI_res_dir)

    NDVI_image_names=[]
    NDVI_dates = []
    sorted_NDVI_dates = []
    sorted_NDVI_dates1 = []
    num_NDVI_images = find_NDVI_images(NDVI_image_names, NDVI_dates, path_NDVI)
#    print("There are ",num_NDVI_images," NDVI Sentinel dates")
    sorted_NDVI_dates1 = sorted(NDVI_dates)
#    print("Sorted NDVI dates ", sorted_NDVI_dates)
#    print("Finished sorting NDVI images")

    LS_image_names=[]
    LS_dates = []
    sorted_LS_dates = []
    num_LS_images = find_LS_NDVI_images(LS_image_names, LS_dates, path_NDVI)
#    print("There are ",num_LS_images," AgroET run dates")
    sorted_LS_dates = sorted(LS_dates)
#    print("Sorted AgroET run dates ", sorted_LS_dates)
#    print("Finished sorting LS images")
    
    i=0
#    print("sorted LS dates",sorted_LS_dates)
#    print("sorted NDVI dates",sorted_NDVI_dates)

#   Removing elements present in other list
#   using list comprehension
    sorted_NDVI_dates = [i for i in sorted_NDVI_dates1 if i not in sorted_LS_dates]
     
    while(i < num_LS_images-1):
        for t1 in sorted_LS_dates:
            t = int(t1)
            if(t1 != sorted_LS_dates[num_LS_images]): 
#                print("LS date",t,"i",i)
                half_ls_day_gap = (int(sorted_LS_dates[i+1]) - int(sorted_LS_dates[i]))/2.0
                j=0
                for tt in sorted_NDVI_dates:
#                    print("NDVI date",tt,"j",j,"i",i)
    #                if(int(sorted_LS_dates[i+1]) <= int(tt)):
    #                    print("sorted_LS_dates[i+1]",sorted_LS_dates[i+1],"<=",int(tt))
    #                    i = i+1  
    #                    break
                    if(int(tt) > t):
#                        print("tt",tt,"greater than t",t)
                        tt_to_ls_begin = int(tt) - t
#                        print("tt_to_ls_begin ", tt_to_ls_begin)
#                        import pdb;pdb.set_trace()        

                        # if it does NOT exist create it otherwise skip it
                        if(tt_to_ls_begin < half_ls_day_gap):
                            ndvi_date = sorted_NDVI_dates[j]
                            for n in NDVI_image_names:
                                if (ndvi_date in n):
                                    ndvi_name = n
#                            print("ndvi_name ",ndvi_name)
#                            print("tt is LESS than half of ls_day_gap", tt,t,tt_to_ls_begin,half_ls_day_gap)
                            if not os.path.exists(path_NDVI + "/" + ndvi_name[:-21]+'_NDVI_ratio.img'):
                                gdal_calc = (
                                    "gdal_calc.py -A " 
                                    + path_NDVI
                                    + "/"
                                    + NDVI_image_names[j]
                                    + " -B "
                                    + path_NDVI
                                    + "/"
                                    + LS_image_names[i]
                                    + " --outfile="
                                    + path_NDVI + "/" 
                                    + ndvi_name[:-21]+'_NDVI_ratio.img'
                                    + " --calc="
                                    + '"(A/B)" '
                                    + "--type=Float64"
                                    + " --overwrite --quiet --NoDataValue=-9999"
                                )
#                                print("gdal_calc = ",gdal_calc,"\n")
                                gdal_calc_func(ndvi_name[:-21]+'_NDVI_ratio.img', gdal_calc)
                        elif(tt_to_ls_begin == half_ls_day_gap):
                            ndvi_date = sorted_NDVI_dates[j]
                            for n in NDVI_image_names:
                                if (ndvi_date in n):
                                    ndvi_name = n
#                            print("ndvi_name ",ndvi_name)
#                            print("tt is EXACTLY at half of ls_day_gap", tt,t,tt_to_ls_begin,half_ls_day_gap)
                            if not os.path.exists(path_NDVI + "/" + ndvi_name[:-21]+'_NDVI_ratio.img'):
                                gdal_calc = (
                                    "gdal_calc.py -A " 
                                    + path_NDVI
                                    + "/"
                                    + NDVI_image_names[j]
                                    + " -B "
                                    + path_NDVI
                                    + "/"
                                    + LS_image_names[i]
                                    + " -C "
                                    + path_NDVI
                                    + "/"
                                    + LS_image_names[i+1]
                                    + " --outfile="
                                    + path_NDVI + "/" 
                                    + ndvi_name[:-21]+'_NDVI_ratio.img'
                                    + " --calc="
                                    + '"((A/B) + (A/C))/2.0" '
                                    + "--type=Float64"
                                    + " --overwrite --quiet --NoDataValue=-9999"
                                )
#                                print("gdal_calc = ",gdal_calc,"\n")
                                gdal_calc_func(ndvi_name[:-21]+'_NDVI_ratio.img', gdal_calc)
                        elif(tt_to_ls_begin > half_ls_day_gap):
                            ndvi_date = sorted_NDVI_dates[j]
                            for n in NDVI_image_names:
                                if (ndvi_date in n):
                                    ndvi_name = n
#                            print("ndvi_name ",ndvi_name)
#                            print("tt is GREATER than half of ls_day_gap", tt,t,tt_to_ls_begin,half_ls_day_gap)
                            if not os.path.exists(path_NDVI + "/" + ndvi_name[:-21]+'_NDVI_ratio.img'):
                                gdal_calc = (
                                    "gdal_calc.py -A " 
                                    + path_NDVI
                                    + "/"
                                    + NDVI_image_names[j]
                                    + " -B "
                                    + path_NDVI
                                    + "/"
                                    + LS_image_names[i+1]
                                    + " --outfile="
                                    + path_NDVI + "/" 
                                    + ndvi_name[:-21]+'_NDVI_ratio.img'
                                    + " --calc="
                                    + '"(A/B)" '
                                    + "--type=Float64"
                                    + " --overwrite --quiet --NoDataValue=-9999"
                                )
#                                print("gdal_calc = ",gdal_calc,"\n")
                                gdal_calc_func(ndvi_name[:-21]+'_NDVI_ratio.img', gdal_calc)
                        else:
                            print("tt is not less equal or greater - SOMETHING IS WRONG ", tt,t,tt_to_ls_begin,half_ls_day_gap)
                            exit(3)
                    if(int(sorted_LS_dates[i+1]) <= t):
                        break
                    j = j+1
                i = i+1  
    return

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Run the AgroET model for a date and an AOI."
    )
    
    parser.add_argument(
        "--project", type=str, default=None, help="project name for seasonal run"
    )

    args = parser.parse_args()

    NDVI_RATIO_VM(
        args.project
    )
    # all finished
