from __future__ import print_function

# This must be the first statement before other statements.
# You may only put a quoted or triple quoted string,
# Python comments, other future statements, or blank lines before the __future__ line.

try:
    import __builtin__
except ImportError:
    import builtins as __builtin__

import os
import argparse
from harmo_w_filt_dual import Harmonized_Dual_Download
import geopandas
from operator import itemgetter
import numpy as np
import shutil
from AgroET_library import *

def find_NDVI_images(NDVI_image_names, NDVI_dates, path):
    ls_count1 = -1 #because the first value in the array is 0 so when you add 1 you end up with 0
    
    for f in os.listdir(path):
        if f.endswith("NDVI.tif"):
            ls_count1 += 1
            JulDate = f[15:-21]
            NDVI_image_names.append(f)
#            Specify the date and format within strptime in this case format is %Y-%m-%d (if your data was 2020/03/30' then use %Y/%m/%d) and specify the format you want to convert into in strftime, in this case it is %Y%j. This gives out Julian date in the form CCYYDDD, if you need only in YYDDD give the format as %y%j
            NDVI_dates.append(JulDate)
            
    return(ls_count1)


def NDVI_VM(start_date, end_date, aoi,project):
    # Raster layers cell size
    cellSize = 30

    # Define the shapefile for the area of interest.  This is the bounding box that will be used to download the Sentinel images.
    # This shapefile needs to be in the same directory as the python code.
    #aoi_file = "aoi.shp"
    aoi_file = "AOI_ET_FLUX_UTM_10.shp"

#    shp_buffer(aoi_file, +1000.0, "aoi_plus.shp")

#    df = geopandas.read_file("aoi_plus.shp")
    df = geopandas.read_file(aoi_file)

    # Should be single feat
    df = df.to_crs({"init": "epsg:4326"})
    bounds = df.geometry.apply(lambda x: x.bounds).tolist()
    x_min, y_min, x_max, y_max = min(bounds, key=itemgetter(0))[0], min(bounds, key=itemgetter(1))[1], max(bounds, key=itemgetter(2))[2], max(bounds, key=itemgetter(3))[3] #https://stackoverflow.com/questions/13145368/find-the-maximum-value-in-a-list-of-tuples-in-python

    delete_shp_file("aoi_bb.shp")
    shp_bbox(".", "aoi_plus.shp","aoi_bb")

    Harmonized_Dual_Download(
            x_min,
            x_max,
            y_min,
            y_max,
            start_date,
            end_date
        )

#    import pdb;pdb.set_trace() 
    NDVI_res_dir = "NDVI"
    NDVI_directory = "hls_data_F"
    path_NDVI = os.path.join(os.getcwd(),NDVI_res_dir)
    path_hls_data_F = os.path.join(os.getcwd(),NDVI_directory)
    basedir0 = "/reset/seasonal_projects"
    basedir = os.path.join(basedir0, str(project))
#    print("Checking if there is an NDVI directory in the project otherwise create it!!!")
    isDirectory = os.path.isdir(basedir + "/NDVI")
    if isDirectory == False:
        os.mkdir(basedir + "/NDVI")
    for t in os.listdir(basedir):
        if t.endswith("ET_24_raster.img"):
            clip_file = basedir + "/" + t
            break
    # check whether directory already exists
    if not os.path.exists(path_NDVI):
#        import pdb;pdb.set_trace() 
        os.mkdir(path_NDVI)

    for t in os.listdir(NDVI_directory):
        t_fp = os.path.join(os.getcwd(),"hls_data_F",t)
        t_NDVI = os.path.join(os.getcwd(),"NDVI",t)
        ndvi_img = False
        if t_fp.endswith("B05.tif"):
            tt = t_fp.replace("B05.tif","B04.tif")
            ndvi_img = t_NDVI.replace("B05.tif","NDVI1.tif")
        if t_fp.endswith("B8A.tif"):
            tt = t_fp.replace("B8A.tif","B04.tif")
            ndvi_img = t_NDVI.replace("B8A.tif","NDVI1.tif")
                    
        if(ndvi_img):
            file1 = basedir + "/" + NDVI_res_dir + "/" + t[:-7] + "NDVI.tif"
            too_much_nodata = basedir +"/NDVI/" + t[:-7] + "NDVI_too_much_no_data"

            if os.path.exists(file1):
                continue
            elif os.path.exists(too_much_nodata):
                continue
            else:
                delete_file_w_path(ndvi_img)
                gdal_calc = (
                    "gdal_calc.py -A "
                    + t_fp
                    + " -B "
                    + tt
                    + " --outfile="
                    + ndvi_img
                    + " --calc="
                    + '"((A-B)/(A+B))" '
                    + "--type=Float64"
                    + " --overwrite --quiet --NoDataValue=-9999"
                )
                gdal_calc_func(ndvi_img, gdal_calc)
                df = geopandas.read_file("aoi_bb.shp")
                bounds = df.geometry.apply(lambda x: x.bounds).tolist()
                x_utm_min, y_utm_min, x_utm_max, y_utm_max = min(bounds, key=itemgetter(0))[0], min(bounds, key=itemgetter(1))[1], max(bounds, key=itemgetter(2))[2], max(bounds, key=itemgetter(3))[3] #https://stackoverflow.com/questions/13145368/find-the-maximum-value-in-a-list-of-tuples-in-python
                clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells = aoi_extent(
                "aoi_bb.shp"
                )
                for t1 in os.listdir(path_NDVI):
                    ndvi_img = False
                    if t1.endswith("NDVI1.tif"):
                        t = t1[:-5] + ".tif"
                        delete_file_w_path(path_NDVI + "/" + t)
                        clip_img(clip_file, path_NDVI, t1, t, aoi_file)
                        nodata_value = -9999 # enter the NoData value here
                        perc_nodata = check_image(path_NDVI + "/" + t, nodata_value)   
                        orig_t = t[:-4] + "_orig." + t[-3:]
                        delete_file_w_path(path_NDVI +"/" + orig_t)
                        delete_file_w_path(path_NDVI +"/" + t1)
                        if(perc_nodata < 80):
                            delete_file_w_path(path_NDVI + "/" + t)
                            cf = open(too_much_nodata, "a+")
                            cf.write("Scene has too much no data")
                            cf.close()
    NDVI_image_names=[]
    NDVI_dates = []
    sorted_NDVI_dates = []
    num_NDVI_images = find_NDVI_images(NDVI_image_names, NDVI_dates, path_NDVI)
    sorted_NDVI_dates = sorted(NDVI_dates)
            
    for t1 in os.listdir(path_NDVI):
        ndvi_img = False
        if t1.endswith("NDVI.tif"):
            isFile = os.path.exists(basedir + "/NDVI/" + t1)
            if isFile != True:
                shutil.copyfile(path_NDVI + "/" + t1, basedir + "/NDVI/" + t1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Run the AgroET model for a date and an AOI."
    )
    parser.add_argument(
        "start_date",
        metavar="start_date",
        type=str,
        help="Start date in YYYY-MM-DD format",
    )

    parser.add_argument(
        "end_date",
        metavar="end_date",
        type=str,
        help="End date in YYYY-MM-DD format",
    )
    parser.add_argument(
        "--aoi",
        type=str,
#        default="aoi.shp",
        help="No AOI shafile name",
    )
    parser.add_argument(
        "--max-cloud-perc",
        type=int,
        default=25,
        help="Do not process the image if the cloudcover is greater than this",
    )
    parser.add_argument(
        "--project", type=str, default=None, help="project name for seasonal run"
    )

    args = parser.parse_args()

    NDVI_VM(
        args.start_date,
        args.end_date,
        args.aoi,
        args.project
    )
    # all finished
