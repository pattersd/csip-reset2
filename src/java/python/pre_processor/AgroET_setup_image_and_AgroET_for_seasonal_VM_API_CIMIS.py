from __future__ import print_function

# This must be the first statement before other statements.
# You may only put a quoted or triple quoted string,
# Python comments, other future statements, or blank lines before the __future__ line.

try:
    import __builtin__
except ImportError:
    # Python 3
    import builtins as __builtin__

from osgeo import gdal
from osgeo import osr
from osgeo import ogr
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
# import csip
import numpy as np
import subprocess
import rasterio
import rasterio.features
import json, logging, re, os, sys, glob, requests, shutil, subprocess, urllib
import xml.etree.ElementTree as ET
from rasterstats.utils import stats_to_csv
from rasterstats import zonal_stats
import osgeo.ogr
from os import path
import datetime
from datetime import date, timedelta
#from AgroET_seasonal_luis import run_seasonal


DEBUGFLAG = 1
AgroET_Setup_Seasonal_file = '/tmp/csip/python/pre_processor/AgroET_setup_image_and_AgroET_for_seasonal_input.txt'
if not os.path.exists(AgroET_Setup_Seasonal_file):
    raise Exception('AgroET input file',AgroET_Setup_Seasonal_file,'does not exist.  You need to create it !!!!')
#Conversion from ETo to ETr
ETo_to_ETr = 1.2
#Conversion from km to miles as applied to Wind Run
km_to_miles = 0.621371
#Invert Distance Weighting weight
idw_power = 2.0
# Raster layers cell size
cellSize = 30


def write_log(*text_args):
    text = ' '.join([str(a) for a in text_args])
    pf.write(text + '\n')
    print(text + '\n')
    pf.flush()
    os.fsync(pf)
pf = open("progress.log", "a+")


def jul_to_cal(julian_day):
#    str_jul_day = YEAR + str(julian_day)
    str_jul_day = str(julian_day)
    str_cal_day = str(datetime.datetime.strptime(str_jul_day, '%Y%j').date())
    return (str_cal_day)


def mjd_to_date(jd):
    """
    Modified convert Julian Day to date. It sets Dec 31, 1979 as date 0 by substracting 2444238.5 from absolute julian date
    Algorithm from 'Practical Astronomy with your Calculator or Spreadsheet',
        4th ed., Duffet-Smith and Zwart, 2011.
    Parameters
    ----------
    jd : float
        Julian Day
    Returns
    -------
    year : int
        Year as integer. Years preceding 1 A.D. should be 0 or negative.
        The year before 1 A.D. is 0, 10 B.C. is year -9.
    month : int
        Month as integer, Jan = 1, Feb. = 2, etc.
    day : int
        Day
    Examples
    --------
    Convert Julian Day 2446113.75 to year, month, and day.
    >>> jd_to_date(185)
    (1985, 2, 17)

    """
    jd = jd + 0.5 + 2444238.5
    F, I = math.modf(jd)
    I = int(I)
    A = math.trunc((I - 1867216.25) / 36524.25)
    if I > 2299160:
        B = I + 1 + A - math.trunc(A / 4.)
    else:
        B = I
    C = B + 1524
    D = math.trunc((C - 122.1) / 365.25)
    E = math.trunc(365.25 * D)
    G = math.trunc((C - E) / 30.6001)
    day = int(C - E + F - math.trunc(30.6001 * G))
    if G < 13.5:
        month = G - 1
    else:
        month = G - 13
    if month > 2.5:
        year = D - 4716
    else:
        year = D - 4715
    m_d_y=str(month).zfill(2) + "-" + str(day).zfill(2) + "-" + str(year)
    return m_d_y


def pad_date(date,divider):
    if (divider == "-"):
        #        month, day, year = gregorian(int(yr_m_d[0:4]),int(yr_m_d[4:7]))
        m, d, y = date.split("-")
        month = int(m)
        day = int(d)
        year = int(y)
    elif (divider == "/"):
        #        month, day, year = gregorian(int(yr_m_d[0:4]),int(yr_m_d[4:7]))
        m, d, y = date.split("/")
        month = int(m)
        day = int(d)
        year = int(y)
    else:
        print('incorrect divider provided ')
        exit(1)
    return(str(month).zfill(2),str(day).zfill(2),str(year))


def date_to_mjd(yr_m_d,date_flag):
    """
    Modified function to convert a date to Julian Day.  It sets Dec 31, 1979 as date 0 by subtracting 2444238.5 from absolute julian date
    Algorithm from 'Practical Astronomy with your Calculator or Spreadsheet',
        4th ed., Duffet-Smith and Zwart, 2011.
    Parameters
    ----------
    month : int
        Month as integer, Jan = 1, Feb. = 2, etc.
    day : int
        Day
    year : int
        Year as integer. Years preceding 1 A.D. should be 0 or negative.
        The year before 1 A.D. is 0, 10 B.C. is year -9.
    date_flag == 0 (means it is year and DOY) or date_flag == 1 (means it is Year-Month-Day)
    Returns
    -------
    jd : int
        Julian Day
    Examples
    --------
    February 17, 1985 to Julian Day
    >>> date_to_jd(1985,2,17)
    1875
    """
    if (date_flag == 0):
        #        month, day, year = gregorian(int(yr_m_d[0:4]),int(yr_m_d[4:7]))
        m, d, y = yr_m_d.split("-")
        month = int(m)
        day = int(d)
        year = int(y)
    elif (date_flag == 2):
#        month, day, year = gregorian(int(yr_m_d[0:4]),int(yr_m_d[4:7]))
        m, d, y = yr_m_d.split("/")
        month = int(m)
        day = int(d)
        year = int(y)

    elif (date_flag == 3):
#        month, day, year = gregorian(int(yr_m_d[0:4]),int(yr_m_d[4:7]))
        year = int(yr_m_d[0:4])
        month = int(yr_m_d[4:6])
        day = int(yr_m_d[6:9])
    else:
        line_list = yr_m_d.split("-")
        month = int(line_list[0])
        day = int(line_list[1])
        year = int(line_list[2])

    if month == 1 or month == 2:
        yearp = year - 1
        monthp = month + 12
    else:
        yearp = year
        monthp = month
    # this checks where we are in relation to October 15, 1582, the beginning
    # of the Gregorian calendar.
    if ((year < 1582) or
            (year == 1582 and month < 10) or
            (year == 1582 and month == 10 and day < 15)):
        # before start of Gregorian calendar
        B = 0
    else:
        # after start of Gregorian calendar
        A = math.trunc(yearp / 100.)
        B = 2 - A + math.trunc(A / 4.)
    if yearp < 0:
        C = math.trunc((365.25 * yearp) - 0.75)
    else:
        C = math.trunc(365.25 * yearp)
    D = math.trunc(30.6001 * (monthp + 1))
    jd = B + C + D + day + 1720994.5
    # Set julian date 1 as January 1st 1980 by subtracting 2444238.5
    i_jd = int(jd - 2444238.5)
    return i_jd


def doy_to_cal(jd1,flag):
    # if flag is 0 then do m-d-yr otherwise do ymd with no -
    print("input date ",jd1)
    jd = str(jd1)
    # initializing day number
    day_num = jd[4:7]

    # print day number
#    print("The day number : " + str(day_num))

    # adjusting day num
    day_num.rjust(3 + len(day_num), '0')

    # Initialize year
    year = int(jd[0:4])

    # print year
#    print("The year number : " + str(year))

    # Initializing start date
    strt_date = date(int(year), 1, 1)

    # converting to date
#    print('day_num ',day_num)
    res_date = strt_date + timedelta(days=int(day_num) - 1)
    if (flag == 0):
        res = res_date.strftime("%m-%d-%Y")
    elif (flag == 1):
        res = res_date.strftime("%Y%m%d")
    else:
        print("Wrong Flag was used flag should be 0 or 1", flag)
        exit(1)
# printing result
#    print("Resolved date : " + str(res))
    return res
#    return year, month, day


def check_directory_exists(directory_path, error_text):
    dir_exists = path.exists(directory_path)
    if (dir_exists == 0):
        write_log(error_text, directory_path,' does not exist !!!!')
        exit(0)


def check_utm_prj(ls_utm_z, proj_file, file_name, f_type):
    if(f_type == 'shapefile'):
        check_file_exists(file_name[:-4] + '.prj', ' ',0,'Projection file (.prj) for ' +file_name+ ' is missing.')
        fproj = open(proj_file, "r")

        ln = fproj.readline()
        fproj.close()
        s2 = "Zone"
        start = ln.find(s2)
        if(start == -1):
            reproj_shp(ls_utm_z, file_name)
            print(file_name,'Shapefile was not in UTM Coordinates.  \nIt was projected into the same UTM coordinates as the Landsat image!!!!')
        utm_z = ln[start + 5:start + 7]
        if DEBUGFLAG == 1:
            print('Filename is ',file_name)
            print('Projection file line ', ln)
            print('start of UTM Zone ID ',start)
            print('Shapefile UTM ZONE is ', utm_z,' Landsat UTM ZONE is ',ls_utm_z)
        fproj.close()
        if( utm_z != ls_utm_z):
            reproj_shp(ls_utm_z, file_name)
            print(file_name,'Shapefile is in a different UTM zone than the Landsat image.\nIt was projected into the same UTM zone as the Landsat image!!!!')

    if(f_type == 'raster'):
        rast_utm_z = raster_utm_prj(basedir + '/' + file_name)
        if(rast_utm_z != ls_utm_z):
            reproj_rast(ls_utm_z, file_name)


def reproj_shp(ls_utm_z, shp_name):
    orig_shp = shp_name[:-4] + "_orig.shp"
    copy_shp_file(shp_name, orig_shp)
    delete_shp_file(shp_name)
    os.system(
        "ogr2ogr -t_srs EPSG:326"
        + ls_utm_z
        + " "
        + basedir
        + "/"
        + shp_name
        + " "
        + basedir
        + "/"
        + orig_shp
    )


def copyFile(src, dest):
    try:
        shutil.copyfile(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        print("Error source and destination are the same : %s" % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        print("Error source file does not exist: ", e.strerror, " source file is ", src)



def copy_shp_file(base_name, new_base_name):
    delete_shp_file(new_base_name)
    itemList = os.listdir(basedir)
    for item in itemList:
        if base_name[:-4] == item[:-4]:
            # print('file copied is ', basedir + '/' + new_base_name[:-4] + item[-4:])
            try:
                os.remove(basedir + "/" + new_base_name[:-4] + item[-4:])
            except OSError:
                # LAG 8-12                if DEBUGFLAG == 1:
                # LAG 8-12                print('!!!!!!!! could not remove file ', basedir + '/' + new_base_name[:-4] + item[-4:])
                pass
            copyFile(
                basedir + "/" + item, basedir + "/" + new_base_name[:-4] + item[-4:]
            )



def check_file_exists(file_name, f_type, ls_utm_zone, error_text):
    file_exists = os.path.isfile(basedir + '/' + file_name)
    if (file_exists == 0):
        write_log(error_text + basedir + '/' + file_name + ' does not exist !!!!')
        exit(0)
    if(f_type == 'raster'):
        check_utm_prj(ls_utm_zone, ' ', file_name, 'raster')
    if(f_type == 'shapefile'):
        check_utm_prj(ls_utm_zone, basedir + "/" + file_name[:-4] + ".prj", file_name, 'shapefile')


def raster_utm_prj(prj_tif):
    # tif with projections I want
    write_log(prj_tif)
    tif = gdal.Open(prj_tif, GA_ReadOnly)

    # set spatial reference and transformation
    targetprj1 = osr.SpatialReference(wkt=tif.GetProjection())
    targetprj = r"{}".format(targetprj1)
    start_prj = targetprj.find("zone")
    tif = None

    if(start_prj == -1):
        print('The UTM zone for ',prj_tif,'was not found. Therefore, it will try to be reprojected to the Landsat UTM zone!!!!')
        utm_prj = -1
    else:
        utm_prj = targetprj[start_prj + 5:start_prj + 7]
    if(DEBUGFLAG == 1):
        print('utm_prj string is ',utm_prj)
    return(utm_prj)


def create_weather_daily_csv(w_d_path, ws_path):
    wsf = open(ws_path)
    ws_info=[]
    lines = wsf.readlines()
    wsf.close()
    for iline in range(1,len(lines)):
        line1 = lines[iline].rstrip()
        line_list = line1.split(",")
        for s in line_list:
            line_list[0] = int(line_list[0])
        print('Line list',line_list)
        ws_info.append(line_list)
    sorted_ws_info = sorted(ws_info, key=lambda t: t[0])
    ws_info_2d=[]
    num_ws = 0
    for row in sorted_ws_info:
        ws_info_2d.append([])
        ws_info_2d[num_ws].append(row[0])
        ws_info_2d[num_ws].append(row[1])
        ws_info_2d[num_ws].append(row[2])
        num_ws += 1

    fp = open(w_d_path)
    climate_dat=[]
    lines = fp.readlines()
    fp.close()
    for iline in range(1,len(lines)):
       line=lines[iline]
#       write_log(line)
       line_list = line.split(",")
#       write_log('Daily line_list')
       line_list[0] = int(line_list[0])
       line_list[3] = int(line_list[3])
       line_list[4] = float(line_list[4])
       if(len(line_list[2])):
          mjd = date_to_mjd(line_list[2],2)
          line_list.append(mjd)
          print('mjd ',mjd,'line_list',line_list)
          climate_dat.append(line_list)
# sort of julian date (t[6]) and then station ID t[0]
#LAG 1-2-22    sorted_climate_dat = sorted(climate_dat, key=lambda t: (t[4],t[0]))
#NOTE column [9] is added and based on having a couple of extra columns.  Tbis number will change if DAVE creates the file
    sorted_climate_dat = sorted(climate_dat, key=lambda t: (t[4],t[3],t[0]))
    num_lines = 0
    jul_day = 0
    sum_daily_wr = 0.0
    sum_daily_et = 0.0
    for row in sorted_climate_dat:
#       Checking the the line is not blank
#LAG 1-2-22        if( MJD_IMGD_SD <= row[33] and MJD_IMGD_ED >= row[33]):
        if len(row) < 5:
           write_log('row less than 5')
        if (jul_start <= int(row[25]) and jul_end >= int(row[25])):
        #       Checking the the line is not blank
            if not (row[0] == "" or row[1] == ""):
                # Generation a csv file with the ETr data for each day
                for w in range(0, num_ws):
                    if(ws_info_2d[w][0] == int(row[0])):
                        X = ws_info_2d[w][1]
                        Y = ws_info_2d[w][2]
                        if not row[5]:
                #           The ET value is blank
                            row[5]=""
                        elif(jul_day != int(row[25])):
                            m, d, y = pad_date(row[2], "/")
                            my_date = m + '-' + d + '-' + y
                            # if a new julian day then close the .csv file from the previous before opening a new .csv for the current julian day
                            if(num_lines > 0):
                                out_et_csv.close()
                                out_wr_csv.close()
                            if(int(row[3]) != 2400):
                                sum_daily_et = sum_daily_et + float(row[5])
                                sum_daily_wr = sum_daily_wr + float(row[19])
                            elif(int(row[3]) == 2400):
                                pad_juld = str(row[3]).zfill(3)
                                out_et_csv = open(basedir + '/' + my_date + '_ETr_WS_daily.csv', 'w')
        #LAG 12-28-21                        out_et_csv = open(basedir + '/' + str(row[3][-4:]) + str('{:0>3}'.format(row[4])) + '_ETr_WS.csv', 'w')
                                out_et_csv.write('X,Y,Z,ID,Stat_Name,Jul_Day\n')
                                out_et_csv.write("{0},{1},{2},{3},{4},{5}\n".format(X,Y,sum_daily_et*ETo_to_ETr,row[0],row[1],my_date))
                                out_wr_csv = open(basedir + '/' + my_date + '_WR_daily.csv', 'w')
                                out_wr_csv.write('X,Y,Z,ID,Stat_Name,Jul_Day\n')
                                out_wr_csv.write("{0},{1},{2},{3},{4},{5}\n".format(X,Y,sum_daily_wr*km_to_miles,row[0],row[1],row[3]))
                                num_lines = 0
                                sum_daily_wr = 0.0
                                sum_daily_et = 0.0
                        else:
                            if(int(row[3]) != 2400):
                                sum_daily_et = sum_daily_et + float(row[5])
                                sum_daily_wr = sum_daily_wr + float(row[19])
                            elif(int(row[3]) == 2400):
                                out_et_csv.write("{0},{1},{2},{3},{4},{5}\n".format(X,Y,sum_daily_et*ETo_to_ETr,row[0],row[1],my_date))
                                out_wr_csv.write("{0},{1},{2},{3},{4},{5}\n".format(X,Y,sum_daily_wr*km_to_miles,row[0],row[1],row[3]))
                                num_lines += 1
                                sum_daily_wr = 0.0
                                sum_daily_et = 0.0
                # Generation a csv file with the windrun data for each day
#                num_lines = 0
#                jul_day = 0
#                for w in range(0, num_ws):
#                    if (ws_info_2d[w][0] == int(row[0])):
#                        X = ws_info_2d[w][1]
#                        Y = ws_info_2d[w][2]
#                        if not row[5]:
#                #           The windrun value is blank
#                            row[5]=""
#                        elif(jul_day != row[3]):
#LAG 1-2-22                            my_date = datetime.datetime.strptime(str(row[3]), "%m/%d/%Y")
#                            # if a new julian day then close the .csv file from the previous before opening a new .csv for the current julian day
#                            if(num_lines > 0):
#                                out_wr_csv.close()
#LAG 1-2-22                            pad_juld = str(row[4]).zfill(3)
#LAG 1-2-22                            out_wr_csv = open(basedir + '/' + doy_to_cal(str(my_date.year) + pad_juld) + '_WR_daily.csv', 'w')
#                            m,d,y = pad_date(row[2],"/")
#                            my_date = m + '-' + d + '-' + y
#                            out_wr_csv = open(basedir + '/' + my_date + '_WR_daily.csv', 'w')
#    #LAG 12-28-21                        out_wr_csv = open(basedir + '/' + str(row[3][-4:]) + str('{:0>3}'.format(row[4])) + '_WR_daily.csv', 'w')
#                            out_wr_csv.write('X,Y,Z,ID,Stat_Name,Jul_Day\n')
#                            out_wr_csv.write("{0},{1},{2},{3},{4},{5}\n".format(X,Y,float(row[5])*km_to_miles,row[0],row[1],row[3]))
#                            num_lines = 0
#                            jul_day = row[3]
#                        else:
#                            out_wr_csv.write("{0},{1},{2},{3},{4},{5}\n".format(X,Y,float(row[5])*km_to_miles,row[0],row[1],row[3]))
#                            num_lines += 1
#                            jul_day = row[3]
#    out_wr_csv.close
    return


def create_weather_hourly_csv(w_d_path, ws_path):
#    import pdb; pdb.set_trace()
    wsf = open(ws_path)
    ws_info=[]
    lines = wsf.readlines()
    wsf.close
    for iline in range(1,len(lines)):
        line1 = lines[iline].rstrip()
        line_list = line1.split(",")
        for s in line_list:
            line_list[0] = int(line_list[0])
        ws_info.append(line_list)
    sorted_ws_info = sorted(ws_info, key=lambda t: t[0])
    ws_info_2d=[]
    num_ws = 0
    for row in sorted_ws_info:
        ws_info_2d.append([])
        ws_info_2d[num_ws].append(row[0])
        ws_info_2d[num_ws].append(row[1])
        ws_info_2d[num_ws].append(row[2])
        num_ws += 1

    fp = open(w_d_path)
    climate_dat=[]
    lines = fp.readlines()
    fp.close
    for iline in range(1,len(lines)):
        line = lines[iline]
        print('Hourly file ',line)
        line_list = line.split(",")
        line_list[0] = int(line_list[0])
        line_list[3] = int(line_list[3])
        line_list[4] = float(line_list[4])
#        del line_list[6]
        #            my_date = datetime.datetime.strptime(str(line_list[3]), "%m/%d/%Y")
#            yr1 = line_list[3]
        if(len(line_list[2])):
            mjd = date_to_mjd(line_list[2],2)
            print('mjd ',mjd,'line_list[2]',line_list[2])
            line_list.append(mjd)
            climate_dat.append(line_list)

# sort of julian date (t[6]) and then station ID t[0]
#LAG 1-2-22    sorted_climate_dat = sorted(climate_dat, key=lambda t: (t[5],t[4],t[0]))
    sorted_climate_dat = sorted(climate_dat, key=lambda t: (t[4],t[3],t[0]))
    num_lines = -1
    jul_day = 0
    for row in sorted_climate_dat:
#LAG 1-2-22        if (MJD_IMGD_SD <= row[26] and MJD_IMGD_ED >= row[26]):
#        import pdb;pdb.set_trace()
        print(jul_start, jul_end, row)
        if (jul_start <= row[25] and jul_end >= row[25]):
        #       Checking that the line is not blank
            if not (row[0] == "" or row[1] == ""):
                for w in range(0, num_ws):
                    if(ws_info_2d[w][0] == int(row[0])):
                        X = ws_info_2d[w][1]
                        Y = ws_info_2d[w][2]
                        if not row[5]:
                #           The ET value is blank
                            row[5]=""
                        elif(jul_day != row[25]):
                            m, d, y = pad_date(row[2], "/")
                            my_date = m + '-' + d + '-' + y
                            # if a new julian day then close the .csv file from the previous before opening a new .csv for the current julian day
                            if(num_lines >= 0):
                                out_csv_h.close()
                            if(int(row[3]) == 1000):
                                partial_et1 = float(row[5]) * 0.14
                            elif(int(row[3]) == 1100):
                                partial_et2 = float(row[5]) * 0.86
                                et_h = (partial_et1 + partial_et2) * ETo_to_ETr
                                pad_juld = str(row[4]).zfill(3)
#LAG 1-2-22                                my_date = datetime.datetime.strptime(str(row[3]), "%m/%d/%Y")
                                print('creating hourly file ',basedir + '/' + my_date + '_ETr_hourly.csv')
#LAG 1-2-22                                out_csv_h = open(basedir + '/' + doy_to_cal(str(my_date.year) + pad_juld)  + '_ETr_hourly.csv', 'w')
                                out_csv_h = open(basedir + '/' + my_date + '_ETr_hourly.csv', 'w')
                                out_csv_h.write('X,Y,Z,ID,Stat_Name,Jul_Day\n')
                                out_csv_h.write("{0},{1},{2},{3},{4},{5}\n".format(X,Y,et_h,row[0],row[1],my_date))
                                num_lines = 0
                        else:
                            if(int(row[3]) == 1000):
                                partial_et1 = float(row[5]) * 0.14
                            elif(int(row[3]) == 1100):
                                partial_et2 = float(row[5]) * 0.86
                                et_h = (partial_et1 + partial_et2) * ETo_to_ETr
                                out_csv_h.write("{0},{1},{2},{3},{4},{5}\n".format(X,Y,float(row[4])*ETo_to_ETr,row[0],row[1],my_date))
                                num_lines += 1
    return


def aoi_extent(aoi):
    inDriver = ogr.GetDriverByName("ESRI Shapefile")
    inDataSource = inDriver.Open(aoi, 0)
    inLayer = inDataSource.GetLayer()
    extent = inLayer.GetExtent()
    del inDataSource
    clip_x1 = extent[0]
    clip_y1 = extent[3]
    clip_x2 = extent[1]
    clip_y2 = extent[2]
    x_cells = round((clip_x2 - clip_x1) / cellSize)
    y_cells = round((clip_y1 - clip_y2) / cellSize)
    return (clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells)


# Create a vrt file based on the shapefile name and the zfield that contains the values to be used for and csv file
def create_csv_vrt1(file_name):
    try:
        os.remove(basedir + '/' + file_name + '.vrt')
    except OSError:
        pass
    out_vrt = open(basedir + '/' + file_name + '.vrt', 'w')
    out_vrt.write('<OGRVRTDataSource>\n')
    out_vrt.write('    <OGRVRTLayer name="' + file_name + '" >\n')
    out_vrt.write('        <SrcDataSource>' + basedir + '/' + file_name + '.csv</SrcDataSource>\n')
    out_vrt.write('        <GeometryType>wkbPoint</GeometryType>\n')
    out_vrt.write('        <LayerSRS>EPSG:326' + utm_zone + '</LayerSRS>\n')
    out_vrt.write('        <GeometryField encoding="PointFromColumns" x="X" y="Y" z="Z"/>\n')
#    out_vrt.write('        <GeometryField encoding="PointFromColumns" x="X" y="Y" z="' + zfield + '"/>\n')
    out_vrt.write('    </OGRVRTLayer>\n')
    out_vrt.write('</OGRVRTDataSource>')
    out_vrt.close()


# Create the input files for AgroET
def create_AgroET_Seasonal_input_file():
    try:
        os.remove(src_root + '/AgroET_Seasonal/AgroET_Seasonal_input.txt')
    except OSError:
        pass
    out_AgroET = open(src_root + '/AgroET_Seasonal/AgroET_Seasonal_input.txt', 'w')
    out_AgroET.write('DEBUG_FLAG = ' + str(DEBUGFLAG) + '\n')
    out_AgroET.write('DATA_DIRECTORY = ' + basedir + '\n')
    out_AgroET.write('AOI_FILE = ' + aoi_file + '\n')
    out_AgroET.write('GENERATE_CLOUD_MASK = 1\n')
    out_AgroET.write('WS_FILE = ' + ws +'\n')
    out_AgroET.write('FILL_START_END_ET\n')
    out_AgroET.write('KEEP_INTERMEDIARY_FILES = ' + str(KEEP_INTERMEDIARY_FILES) + '\n')
    out_AgroET.write('REGENERATE_AgroET_SEASONAL = ' + str(REGENERATE_AgroET_SEASONAL) + '\n')
    out_AgroET.write('SEASON_START_DATE = ' + SEASON_SD + '\n')
    out_AgroET.write('SEASON_END_DATE = ' + SEASON_ED + '\n')
    str_AgroET_dates = ' '.join([doy_to_cal(str(item[0]),0) for item in sorted_arr])
    out_AgroET.write('NUMBER_ET_IMAGES = '+ str(AgroET_count) + ' ' + str_AgroET_dates + '\n')
    #Generating a list of the names of the LS images that correspond to each of the julian dates for AgroET images to generate the seasonal ET
    str_AgroET_names = ' '.join([str(item[1]) for item in sorted_arr])
    out_AgroET.write('NAMES_ET_IMAGES = '+ str_AgroET_names + '\n')
    out_AgroET.write('#\n')
    out_AgroET.close()


# Create the input file for the Seasonal ET model
def create_AgroET_input_file(dir_name,j_date):
    j_date1 =  doy_to_cal(j_date,0)
    try:
        os.remove(src_root + '/AgroET/AgroET_input_' + dir_name + '.txt')
    except OSError:
        pass
    out_AgroET = open(src_root + '/AgroET/AgroET_input_' + dir_name + '.txt', 'w')
    if DEBUGFLAG == 1:
        print('create AgroET input file name is = ',out_AgroET)
    out_AgroET.write('DEBUG_FLAG=' + str(DEBUGFLAG) +'\n')
    out_AgroET.write('DATA_DIRECTORY=' + basedir + '\n')
    out_AgroET.write('LS_IMAGE_DIR=' + dir_name + '\n')
    out_AgroET.write('AOI_FILE=' + aoi_file + '\n')
    out_AgroET.write('LOCAL_NASS_DIR=' + local_nass_dir + '\n')
    out_AgroET.write('climate_windrun_mile_day=' + j_date1 +'_WR_daily.img\n')
    out_AgroET.write('climate_etr_hourly=' + j_date1 +'_ETr_hourly.img\n')
    out_AgroET.write('climate_etr=' + j_date1 +'_ETr_WS_daily.img\n')
    out_AgroET.write('WS_FILE=' + ws +'\n')

    out_AgroET.write('#\n')
    out_AgroET.close()


# Create the input file for the Image Download model
def create_imagery_download_input():
    write_log('scr_root')
    try:
        os.remove(src_root + '/AgroET_image_download_input.txt')
    except OSError:
        pass
    image_AgroET = open(src_root + '/AgroET_Imagery/AgroET_image_download_input.txt', 'w')
    print(src_root + '/AgroET_Imagery/AgroET_image_download_input.txt')
    if DEBUGFLAG == 1:
        print('create AgroET image download file name is = ',image_AgroET)
    image_AgroET.write('DATA_DIRECTORY=' + basedir + '\n')
    image_AgroET.write('AOI_FILE=' + aoi_file + '\n')
    image_AgroET.write('IMAGERY_DOWNLOAD_START_DATE = ' + IMGD_SD + '\n')
    image_AgroET.write('IMAGERY_DOWNLOAD_END_DATE = ' + IMGD_ED + '\n')
    image_AgroET.write('IMAGERY_DOWNLOAD_PATH = ' + IMGD_PATH + '\n')
    image_AgroET.write('IMAGERY_DOWNLOAD_ROW = ' + IMGD_ROW + '\n')
    image_AgroET.write('#\n')
    image_AgroET.close()

def generate_AgroET_imagery():
    write_log('calling AgroET_seasonal_luis as a function')
#    run_seasonal(pf,IMGD_SD,IMGD_ED,aoi_file,IMGD_ROW,IMGD_PATH,"PVID")
#    gdalinfo = "python3 " + src_root + "/AgroET_API/AgroET_seasonal_luis.py"

#    gdalinfo = "python3 " + src_root + "/AgroET_API/AgroET_seasonal_luis.py -start_date " + IMGD_SD + " -end_date " + IMGD_ED + " -path " + IMGD_PATH + " -row " + IMGD_ROW + " -aoi " + aoi_file + " -project imperial"
#    write_log(gdalinfo)
#    if (DEBUGFLAG == 1):
#        print(gdalinfo)
#    os.system(gdalinfo)
    write_log('returning from AgroET_seasonal_luis.py')

#    write_log('calling driver.py')
#    gdalinfo = "python3 " + src_root + "/AgroET_Imagery/driver.py -i " + src_root + '/AgroET_Imagery/AgroET_image_download_input.txt'
#    write_log(gdalinfo)
#    if (DEBUGFLAG == 1):
#        print(gdalinfo)
#    os.system(gdalinfo)
#    write_log('returning from driver.py')


def delete_shp_file(shp_name):
    if os.path.exists(shp_name):
        driver = ogr.GetDriverByName("ESRI Shapefile")
        driver.DeleteDataSource(shp_name)


def delete_file(filename):
    full_name = basedir + '/' + filename
    if (os.path.isfile(full_name)):
        try:
            os.remove(full_name)
        except OSError:
            pass

def PixelResolution(fp_path):
    fp_path1 = fp_path + '1'
    ascii_sub = "tr -cd '\11\12\15\40-\176'" + '< ' + fp_path + '> ' + fp_path1
    os.system(ascii_sub)
    fp = open(fp_path1,'r',encoding="utf-8")
    for line in fp:
        if "Pixel Size " in line:
            line2 = line.split('=' or ",")[-1].strip()
            line3 = line2[1:-1]
            xres, yres = map(real, line3.split(","))
            if DEBUGFLAG >= 3:
                print('Pixel Size String ', line3, 'X resolution ', xres, 'Y resolution ', yres)
            break
    fp.close()
    return (abs(float(xres)), abs(float(yres)))


# This function adds a buffer to a shapefile.
def shp_buffer(in_shp, buff, out_shp):
    delete_shp_file(out_shp)
    shp = ogr.Open(in_shp)
    drv = shp.GetDriver()
    drv.CopyDataSource(shp, out_shp)
    shp.Destroy()
    buf1 = ogr.Open(out_shp, 1)
    lyr1 = buf1.GetLayer(0)
    for i in range(0, lyr1.GetFeatureCount()):
        feat = lyr1.GetFeature(i)
        lyr1.DeleteFeature(i)
        geom1 = feat.GetGeometryRef()
        feat.SetGeometry(geom1.Buffer(buff))
        lyr1.CreateFeature(feat)
    feature = lyr1.GetNextFeature()
    while feature is not None:
        geom = feature.GetGeometryRef()
        try:
            area = geom.GetArea()
#            feature.SetField('Area', area)
#            lyr1.SetFeature(feature)
            if area < cellSize*cellSize*1.1:
                lyr1.DeleteFeature(feature.GetFID())
        except:
            lyr1.DeleteFeature(feature.GetFID())
        feature = lyr1.GetNextFeature()
    buf1.Destroy()


def clip_img1(in_image, out_image, aoi_lyr, basedir1):
    out_image1 = out_image[:-4] + '_ZZ' + out_image[-4:]
    delete_file(out_image)
    delete_file(out_image1)
    delete_file("gdalinfo_output.txt")
    gdalinfo = "gdalinfo -nomd -norat -noct " + basedir1 + '/' + in_image + " > " + basedir1 + "/gdalinfo_output.txt"
    if DEBUGFLAG == 1:
        print(gdalinfo)
    os.system(gdalinfo)
    xres, yres = PixelResolution(basedir1 + "/gdalinfo_output.txt")
    if DEBUGFLAG == 3:
        print('X_res is ',xres, 'Yres is ',yres)
    shp_buffer(aoi_lyr, 0.0, aoi_lyr[:-4]+'_buff.shp')
    gdal_warp = ["gdalwarp", "-dstnodata", "-9999", "-overwrite",
                              "-tr", str(xres), str(yres), "-cutline", aoi_lyr[:-4]+'_buff.shp',
                              basedir1 + '/' + in_image, basedir1 + '/' + out_image1]
    if DEBUGFLAG == 1:
        print(' '.join(gdal_warp))
    out = subprocess.check_output(gdal_warp)
    ds = gdal.Open(basedir1 + '/' + out_image1)
    ds = gdal.Translate(basedir1 + '/' + out_image, ds, projWin=[clip_x1, clip_y1, clip_x2, clip_y2])
    ds = None
    delete_file(out_image1)


#Generate the input files for running AgroET for each of the LS images used in the seasonal model
def generate_AgroET_input():
    for t in os.listdir(basedir):
        if os.path.isdir(os.path.join(basedir, t)):
            #This finds the index in the julian dates for the LS images (ls_dates) and returns the julian date for that image
            if(t[0:2] == 'LC' or t[0:2] == 'LE'):
                t_date = doy_to_cal(t[9:16],1)
    #LAG 1-7-22            if (t_date in ls_image_names):
                ls_name1 = str([string for string in ls_image_names if t_date in string])
                ls_name = ls_name1[2:42]
                print('ls_name ',ls_name1,ls_name)
                if(len(ls_name)):
    #            if (any(t_date in string for string in ls_image_names)):
                    ls_jul_date=ls_image_names.index(ls_name)
                    print('ls_name is ',ls_name,'ls_jul_date is ',ls_jul_date)
                    create_AgroET_input_file(t, ls_dates[ls_jul_date])
                    Agro_ET_Results_Exist = os.path.isfile(ls_name + '_ET_24_raster.img')
                    write_log('src_root')
                    write_log(src_root)
                    if (Agro_ET_Results_Exist == 0 or Regenerate_AgroET == 1):
                        gdalinfo = "python3 " + src_root + "/AgroET/AgroET_WEB_LS8.py -i " + src_root + '/AgroET/AgroET_input_' + t + '.txt'
                        write_log('gdalinfo for running AgroET')
                        write_log(gdalinfo)
                        if (DEBUGFLAG == 1):
                            print(gdalinfo)
                        os.system(gdalinfo)
            else:
                print('This directory is NOT in the image list!!!!! ', t)
#               if a directory is empty in where the image directory should be delete it since it is likely due to an update image being available (same name with new update date)
                d_empty =  os.listdir(os.path.join(basedir, t))
                if len(d_empty) == 0:
                    try:
                        os.rmdir(os.path.join(basedir, t))
                    except OSError as e:
                        print("Error: %s : %s" % (os.path.join(basedir, t), e.strerror))


def generate_AgroET_Seasonal_ET():
    gdalinfo = "python3 " + src_root + "/AgroET_Seasonal/AgroET_Seasonal.py -i " + src_root + '/AgroET_Seasonal/AgroET_Seasonal_input.txt'
    if (DEBUGFLAG == 1):
        print(gdalinfo)
    os.system(gdalinfo)


# This function creates a csv file of the weather stations
def ws_to_csv(ws):
    ws_shp = basedir + '/' + ws
    if DEBUGFLAG == 1:
        print("Weather Station Name ", ws_shp)
    ws_source = osgeo.ogr.Open(ws_shp)
    if ws_source is None:
        raise Exception("Open of Weather Station failed ", ws_shp, " file not found ")
    ws_layer = ws_source.GetLayer()
    wsfield_names = [field.name for field in ws_layer.schema]
    if DEBUGFLAG == 1:
        print('Weather Station field_names ', wsfield_names)
    ws_fields = []
    st_id = -1
    for i in range(0, ws_layer.GetFeature(0).GetFieldCount()):
        field = ws_layer.GetFeature(0).GetDefnRef().GetFieldDefn(i).GetName()
        ws_fields.append(field)
        if(field == 'Std_Id' or field == 'Station_Nu' or field == 'StationNbr' or field=='Id'):
            st_id = i
    if DEBUGFLAG == 1:
        print(' WS field ', ws_fields)
    if st_id == -1:
        print('WS file did not contain a field called \"Std_Id\".  Please add this field and run the program again.')
        exit(1)
    ws = []
    out_ws_csv = open(ws_shp[:-4] + '.csv', 'w')
    out_ws_csv.write('Std Id, X, Y\n')
    for index in range(ws_layer.GetFeatureCount()):
        feature = ws_layer.GetFeature(index)
        geometry = feature.GetGeometryRef()
        ws.append((geometry.GetX(), geometry.GetY()))
        if DEBUGFLAG == 1:
            print("Weather Station ", feature[st_id], geometry.GetX(), geometry.GetY())
        out_ws_csv.write("{0}, {1}, {2}\n".format(int(feature[st_id]), geometry.GetX(), geometry.GetY()))
    out_ws_csv.close()
    return


def generate_daily_weather_csv_grids():
#    delete_files_w_pattern(basedir, "*.csv")
    ws_to_csv(ws)
    create_weather_daily_csv(basedir + '/' + daily_weather_file, basedir + '/' + ws[:-4] + '.csv')
    for t in range(jul_start, jul_end + 1):
#Generating grid of ETr daily
#LAG 12-28-21        str_t = str(int(t))  + "_ETr_WS"
#LAG 1-2-22        str_t = doy_to_cal(t) + "_ETr_WS_daily"
        str_t = mjd_to_date(t) + "_ETr_WS_daily"
        create_csv_vrt1(str_t)
        gdal_grid_text = str(
            'gdal_grid -a invdist:power={0} -txe {1} {2} -tye {3} {4} -outsize {5} {6} -a_srs EPSG:326{7} -zfield "Z" -of GTiff -l {8} -ot Float64 {9} {10}'.format(
                idw_power, clip_x1, clip_x2, clip_y2, clip_y1, x_cells, y_cells, utm_zone, str_t,
                basedir + '/' + str_t + '.vrt', basedir + '/' + str_t + '1.img'))
        write_log(gdal_grid_text)
        if DEBUGFLAG == 1:
            print('gdal_grid_text ', gdal_grid_text)
        delete_file(str_t + '1.img')
        os.system(gdal_grid_text)
        clip_img1(str_t + '1.img', str_t + '.img', aoi, basedir)
        delete_file(str_t + '1.img')
#        str_cal_day = jul_to_cal(str_t[:-10])
#        clip_img1(str_t + '.img', str_cal_day + '_ETr_WS.img', aoi, basedir)
# Generating grid of Wind Run daily
#LAG 12-28-21        str_t = str(int(t))  + "_WR_daily"
#LAG 1-2-22        str_t =  doy_to_cal(t)  + "_WR_daily"
        str_t = mjd_to_date(t) + "_WR_daily"
        create_csv_vrt1(str_t)
        gdal_grid_text = str(
            'gdal_grid -a invdist:power={0} -txe {1} {2} -tye {3} {4} -outsize {5} {6} -a_srs EPSG:326{7} -zfield "Z" -of GTiff -l {8} -ot Float64 {9} {10}'.format(
                idw_power, clip_x1, clip_x2, clip_y2, clip_y1, x_cells, y_cells, utm_zone, str_t,
                basedir + '/' + str_t + '.vrt', basedir + '/' + str_t + '1.img'))
        if DEBUGFLAG == 1:
            print('gdal_grid_text ', gdal_grid_text)
        delete_file(str_t + '1.img')
        os.system(gdal_grid_text)
        clip_img1(str_t + '1.img', str_t + '.img', aoi, basedir)
        delete_file(str_t + '1.img')
#LAG 12-19-19        clip_img1(str_t + '.img', str_t + '_ET_ws.img', aoi, basedir)


def generate_hourly_weather_csv_grids():
    create_weather_hourly_csv(basedir + '/' + hourly_weather_file, basedir + '/' + ws[:-4] + '.csv')
    #    for t in range(1, int(num_reset_et[0])+1):
    import pdb; pdb.set_trace()
    for jul_day in range(jul_start, jul_end):
#        jul_day = int(sorted_ls_dates[t])
        str_t_hourly =  mjd_to_date(jul_day) + '_ETr_hourly'
        create_csv_vrt1(str_t_hourly)
        gdal_grid_text = str(
            'gdal_grid -a invdist:power={0} -txe {1} {2} -tye {3} {4} -outsize {5} {6} -a_srs EPSG:326{7} -zfield "Z" -of GTiff -l {8} -ot Float64 {9} {10}'.format(
                idw_power, clip_x1, clip_x2, clip_y2, clip_y1, x_cells, y_cells, utm_zone, str_t_hourly,
                basedir + '/' + str_t_hourly + '.vrt', basedir + '/' + str_t_hourly + '1.img'))
        write_log(gdal_grid_text)
        if DEBUGFLAG == 1:
            print('gdal_grid_text ', gdal_grid_text)
        delete_file(str_t_hourly + '1.img')
        os.system(gdal_grid_text)
        #        str_cal_day = jul_to_cal(str_t_hourly[:-11])
        #        clip_img1(str_t_hourly + '.img', str_cal_day + '_climate_etr_hourly.img', aoi, basedir)
        clip_img1(str_t_hourly + '1.img', str_t_hourly + '.img', aoi, basedir)
        delete_file(str_t_hourly + '1.img')
    return


def find_landsat_images():
#    import pdb; pdb.set_trace()
    ls_count1 = -1 #because the first value in the array is 0 so when you add 1 you end up with 0
    write_log(basedir)
    for t in os.listdir(basedir):
   #     if os.path.isdir(os.path.join(basedir, t)):
            for f in os.listdir(basedir):
                if f.find("MTL.txt") >= 0:
                    write_log('found the MTL file')
                    ls_count1 += 1;
                    t1 = f[:-8]
                    utm_zone1 = raster_utm_prj(basedir + '/' + t1 + '_B1.TIF')
                    write_log('utm_zone1 is')
                    write_log(utm_zone1)
                    ls_image_names.append(f[:-8])
                    dest = basedir + '/' + t1 + "_MTL.txt"  # TEMPORARY NEED TO DELETE
                    shakes = open(dest, "r")
                    for line in shakes:
                        if "DATE_ACQUIRED" in line:
                            GregDate = line.split('=')[-1].strip()
                            # Specify the date and format within strptime in this case format is %Y-%m-%d (if your data was 2020/03/30' then use %Y/%m/%d) and specify the format you want to convert into in strftime, in this case it is %Y%j. This gives out Julian date in the form CCYYDDD, if you need only in YYDDD give the format as %y%j
                            JulDate = datetime.datetime.strptime(GregDate, "%Y-%m-%d").strftime('%Y%j')
                            # Print your variable to display the result
                            ls_dates.append(JulDate)
                            if DEBUGFLAG == 1:
                                print('LandSat Directory is ', ls_image_names[ls_count1])
                                print('LandSat Image Date is ', GregDate)
                            break
                    shakes.close()
    return(utm_zone1,ls_count1)


def find_AgroET_results():
    AgroET_count1 = -1 #because the first value in the array is 0 so when you add 1 you end up with 0
    write_log('basedir ')
    write_log(basedir)
    for f in os.listdir(basedir):
        write_log('ET 24 files')
        write_log(f)
        if f.find("ET_24_raster.img") >= 0:
            AgroET_result_date = date_to_mjd(f[17:25],3)
# check if the AgroET results are between the image start date and image end date
            if(AgroET_result_date >= MJD_IMGD_SD and AgroET_result_date <= MJD_IMGD_ED):
                AgroET_count1 += 1;
                AgroET_image_names0.append(f[:-4])
    for f1 in AgroET_image_names0:
        count = -1
        f1_start = f1[:-13]
        for f2 in ls_image_names:
            count += 1
            if f1_start == f2:
                AgroET_image_dates0.append(ls_dates[count])
                break
    print('AgroET_image_dates before sorting', AgroET_image_dates0)
    AgroET_image_dates = sorted(AgroET_image_dates0)
    print('AgroET_image_dates AFTER sorting', AgroET_image_dates)
    for f1 in AgroET_image_dates:
        count = -1
        for f2 in AgroET_image_names0:
            count += 1
            if doy_to_cal(f1,1) == f2[17:25]:
                AgroET_image_names.append(f2)
                break
    print('AgroET_image_dates sorted', AgroET_image_dates)
    print('AgroET_image_names sorted', AgroET_image_names)

#    i = 0
#    while i < count:
#        print('AgroET_name and date',AgroET_image_names[i],AgroET_image_dates[i])
#        i += 1
    return(AgroET_count1)


write_log('AgroET Setup Image Runs for Seasonal ET Version 1.0')
write_log('Starting calculation')

input = open(AgroET_Setup_Seasonal_file, "r")
basedir = os.getcwd()
GENERATE_DAILY_WEATHER_CSV = 1
GENERATE_CLOUD_MASK = 1
REGENERATE_AgroET_SEASONAL = 0
FILL_START_END_ET = 0
for line in input:
    line1 = line.split('=')[0].strip()
    if DEBUGFLAG >= 1:
        print('Line in AgroET_setup_imagery_runs_for_seasonal_input.txt file is ', line1)
    if "DATA_DIRECTORY" == line1:
        basedir = line.split('=')[-1].strip()
        if DEBUGFLAG >= 1:
            print('basedir ', basedir)
    elif "SRC_ROOT_DIRECTORY" == line1:
        src_root = line.split('=')[-1].strip()
        if DEBUGFLAG >= 1:
            print('Source Code Root Directory ', src_root)
    elif "WS_FILE" == line1:
        ws = line.split('=')[-1].strip()
        if DEBUGFLAG >= 1:
            print('Weather Station File ', ws)
    elif "LOCAL_NASS_DIR" == line1:
        local_nass_dir = line.split('=')[-1].strip()
        if DEBUGFLAG >= 1:
            print('Local NASS directory is ', local_nass_dir)
    elif "DAILY_WEATHER_FILE" == line1:
            daily_weather_file = line.split('=')[-1].strip()
            if DEBUGFLAG >= 1:
                print('Daily weather file ', daily_weather_file)
    elif "HOURLY_WEATHER_FILE" == line1:
            hourly_weather_file = line.split('=')[-1].strip()
            if DEBUGFLAG >= 1:
                print('Hourly weather file ', hourly_weather_file)
    elif "GENERATE_DAILY_WEATHER_CSV" == line1:
        GENERATE_DAILY_WEATHER_CSV = int(line.split('=')[-1].strip())
        if DEBUGFLAG >= 1:
            print('The comma separated values (csv) option for daily files of the weather data will be generated (0 - not run or 1 - run) the option selected is ',GENERATE_DAILY_WEATHER_CSV)
    elif "AOI_FILE" == line1:
        aoi_file = line.split('=')[-1].strip()
        if DEBUGFLAG >= 1:
            print('Area of Interest (AOI) file ', aoi_file)
    elif "IMAGERY_DOWNLOAD" == line1:
        IMGD = int(line.split('=')[-1].strip())
        if DEBUGFLAG >= 1:
            print('Imagery Download - 0=NO, 1=YES ', IMGD)
    elif "SEASON_START_DATE" == line1:
        SEASON_SD = line.split('=')[-1].strip()
        MJD_IMGD_SD = date_to_mjd(SEASON_SD,0)
        IMGD_SD = mjd_to_date(MJD_IMGD_SD)
        if DEBUGFLAG >= 1:
            print('Season Start Date ', SEASON_SD)
            print('Imagery Download Start Date ', IMGD_SD)
    elif "SEASON_END_DATE" == line1:
        SEASON_ED = line.split('=')[-1].strip()
        MJD_IMGD_ED = date_to_mjd(SEASON_ED,0)
        IMGD_ED = mjd_to_date(MJD_IMGD_ED)
        if DEBUGFLAG >= 1:
            print('Season End Date ', SEASON_ED)
            print('Imagery Download End Date ', IMGD_ED)
    elif "IMAGERY_DOWNLOAD_PATH" == line1:
        IMGD_PATH = line.split('=')[-1].strip()
        if DEBUGFLAG >= 1:
            print('Imagery Download PATH ', IMGD_PATH)
    elif "IMAGERY_DOWNLOAD_ROW" == line1:
        IMGD_ROW = line.split('=')[-1].strip()
        if DEBUGFLAG >= 1:
            print('Imagery Download ROW ', IMGD_ROW)
    elif "RUN_AgroET" == line1:
        RUN_AgroET = int(line.split('=')[-1].strip())
        if DEBUGFLAG >= 1:
            print('The Run AgroET option is (0 - not run or 1 - run) the option selected is ',RUN_AgroET)
    elif "Regenerate_AgroET" == line1:
        Regenerate_AgroET = int(line.split('=')[-1].strip())
        if DEBUGFLAG >= 1:
            print('The Regenerate AgroET option is (0 - not run or 1 - run) the option selected is ',Regenerate_AgroET)
    elif "GENERATE_HOURLY_WEATHER_CSV" == line1:
        GENERATE_HOURLY_WEATHER_CSV = int(line.split('=')[-1].strip())
        if DEBUGFLAG >= 1:
            print('User selected to generate hourly weather data')
    elif "RUN_AgroET_Seasonal" == line1:
        RUN_AgroET_Seasonal = int(line.split('=')[-1].strip())
        if DEBUGFLAG >= 1:
            print('The Run AgroET Seasonal option is (0 - not run or 1 - run) the option selected is ',RUN_AgroET_Seasonal)
    elif "REGENERATE_AgroET_SEASONAL" == line1:
        REGENERATE_AgroET_SEASONAL = int(line.split('=')[-1].strip())
        if DEBUGFLAG >= 1:
            print('The Regenerate AgroET Seasonal option is (0 - not run or 1 - run) the option selected is ',REGENERATE_AgroET_SEASONAL)
    elif "KEEP_INTERMEDIARY_FILES" == line1:
        KEEP_INTERMEDIARY_FILES = int(line.split('=')[-1].strip())
        if DEBUGFLAG >= 1:
            print('The KEEP_INTERMEDIARY_FILES option is (0 - not run or 1 - run) the option selected is ', KEEP_INTERMEDIARY_FILES)
    elif "DEBUG_FLAG" == line1:
        DEBUGFLAG = int(line.split('=')[-1].strip())
        if DEBUGFLAG >= 1:
            print('DEBUG flag set to ', DEBUGFLAG)
    elif "#" == line1:
        break
    else:
        print('The following line: ',line,'in the file AgroET_setup_image_runs_for_seasonal_input.txt does not contain a valid token!!!')
        exit(1)
input.close()

# Checking if the base directory exists

ls_dates=[]
ls_image_names=[]
basedir = os.path.abspath(basedir)
check_directory_exists(basedir, 'Base directory ')
write_log('BEFORE checking utm zone')
#Find the number of landsat images that are in the directory and will be used for the seasonal calculation
utm_zone,ls_count = find_landsat_images()
write_log('past checking utm zone')
#basedir = os.getcwd()
aoi = basedir + '/' + aoi_file
check_file_exists(aoi_file, 'shapefile', utm_zone, 'Area of Interest (AOI) shapefile ')
# Checking if the Weather Station file exists and is in the correct projection
check_file_exists(ws, 'shapefile', utm_zone, 'Weather Station shapefile ')
# Set the working directory to basedir
os.chdir(basedir)

clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells = aoi_extent(aoi_file)

if IMGD == 1:
    print('Creating the imagery download input')
    create_imagery_download_input()
    write_log('BEFORE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! going to Generate AgroET imagery')
    generate_AgroET_imagery()
    write_log('returned from generate_AgroET_imagery')

#sorted_ls_dates = sorted(ls_dates)
#write_log('sorted the LS dates')

#doy_start = int(sorted_ls_dates[0])
#jul_start = date_to_mjd(doy_to_cal(doy_start,0),0)
#doy_end = int(sorted_ls_dates[ls_count])
#jul_end = date_to_mjd(doy_to_cal(doy_end,0),0)

jul_start = MJD_IMGD_SD
jul_end = MJD_IMGD_ED

if GENERATE_DAILY_WEATHER_CSV == 1:
    write_log('calling generate_daily_weather_csv_grids')
    generate_daily_weather_csv_grids()
    write_log('Returned from calling generate_daily_weather_csv_grids')

if GENERATE_HOURLY_WEATHER_CSV == 1:
    write_log('calling hourly_weather_csv_grids')
    generate_hourly_weather_csv_grids()
    write_log('Returned from calling hourly_weather_csv_grids')

if RUN_AgroET == 1:
    write_log('calling generate_AgroET_input')
    generate_AgroET_input()

AgroET_image_dates0 = []
AgroET_image_names0 = []
AgroET_image_dates = []
AgroET_image_names = []
AgroET_count = find_AgroET_results()
arr = [[0 for i in range(2)] for j in range(AgroET_count)]
AgroET_image_dates = sorted(AgroET_image_dates0)
i = 0
while i < AgroET_count:
    arr[i][0] = AgroET_image_dates[i]
    arr[i][1] = AgroET_image_names[i]
    print('i ',i,'arr[i][0] and arr[i][1] ',arr[i][0],arr[i][1])
    i += 1

sorted_arr = sorted(arr, key=lambda t: t[0])
i = 0
while i < AgroET_count:
    print('Sorted AgroET_name and date', sorted_arr[i][0], sorted_arr[i][1],AgroET_image_dates[i],AgroET_image_names[i])
    i += 1

write_log('before calling AgroET_seasonal input')
# create_AgroET_Seasonal_input_file()
write_log('After calling AgroET_seasonal input')

if RUN_AgroET_Seasonal == 1:
    write_log('Before calling generate AgroET_seasonal')
    generate_AgroET_Seasonal_ET()
    write_log('After calling generate AgroET_seasonal')


exit()

