# pip install cdsapi

import cdsapi
import os
import time
import netCDF4
import rasterio
import datetime

c = cdsapi.Client(url='https://cds.climate.copernicus.eu/api/v2', key='159:be7b2361-c564-4d19-8aef-48c86b1a7e72')

c.retrieve(
    'reanalysis-era5-single-levels',
    {
        'product_type': 'reanalysis',
        'format': 'netcdf',
        'variable': [
            '10m_u_component_of_wind', '10m_v_component_of_wind', 'evaporation',
            'potential_evaporation',
        ],
        'month': '07',
        'year': '2023',
        'day': [
            '12', '12',
        ],
        'time': [
            '00:00', '01:00', '02:00',
            '03:00', '04:00', '05:00',
            '06:00', '07:00', '08:00',
            '09:00', '10:00', '11:00',
            '12:00', '13:00', '14:00',
            '15:00', '16:00', '17:00',
            '18:00', '19:00', '20:00',
            '21:00', '22:00', '23:00',
        ],
        'area': [
            90, -180, -90,
            180,
        ],
    },
    'output_folder/download.nc'  # Specify the desired output file path
)

output_file='output_folder/download.nc'
# Wait until the file is downloaded
while not os.path.exists(output_file):
    time.sleep(1)  # Pause for 1 second

print("Download completed!")

import rasterio
import netCDF4
import datetime

# Open the downloaded NetCDF file using netCDF4
nc_file = 'output_folder/download.nc'
dataset = netCDF4.Dataset(nc_file)

# Print the variables in the NetCDF file
print("Variables in the NetCDF file:")
for variable in dataset.variables.keys():
    print(variable)

# Variables to extract from the NetCDF file
variables_to_extract = ['u10', 'v10', 'e', 'pev']

# Get the latitude and longitude variables
latitude = dataset.variables['latitude'][:]
longitude = dataset.variables['longitude'][:]

# Get the time variable and convert to datetime objects
time_variable = dataset.variables['time']
time_units = time_variable.units
time_values = time_variable[:]

# Iterate over each time step
for i, time_value in enumerate(time_values):
    # Convert the time value to datetime object
    time_datetime = netCDF4.num2date(time_value, units=time_units, calendar='standard')

    # Format the time value as YYYYMMDD_HH
    date_str = time_datetime.strftime("%Y%m%d_%H")

    # Iterate over each variable to extract
    for variable in variables_to_extract:
        # Skip variables that are not present in the NetCDF file
        if variable not in dataset.variables:
            print(f"Variable {variable} not found in the NetCDF file.")
            continue

        # Read the variable data for the current time step
        data = dataset.variables[variable][i]

        # Check if the data array is empty
        if data.size == 0:
            print(f"No data found for variable {variable} at time {time_datetime}.")
            continue

        # Specify the output file path for the TIFF file using variable name and date
        output_file = f'output_folder/{variable}_{date_str}.tif'

        # Reproject and save the data as a TIFF file
        with rasterio.open(output_file, 'w', driver='GTiff', height=data.shape[0], width=data.shape[1], count=1, dtype=data.dtype, crs='+proj=latlong', transform=rasterio.transform.from_bounds(longitude.min(), latitude.min(), longitude.max(), latitude.max(), data.shape[1], data.shape[0])) as dst:
            dst.write(data, 1)

# Close the netCDF dataset
dataset.close()


