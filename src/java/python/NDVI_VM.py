from __future__ import print_function

# This must be the first statement before other statements.
# You may only put a quoted or triple quoted string,
# Python comments, other future statements, or blank lines before the __future__ line.

try:
    import __builtin__
except ImportError:
    import builtins as __builtin__

#from osgeo import gdal
#from osgeo import osr
#from osgeo import ogr
import os
import argparse
#import gdal
from harmo_w_filt_dual import Harmonized_Dual_Download
import geopandas
from operator import itemgetter
import numpy as np
#import subprocess, shutil
import shutil
#import rasterio
from AgroET_Library.seasonal_library import *
DEBUGFLAG = 1

def find_NDVI_images(NDVI_image_names, NDVI_dates, path):
    ls_count1 = -1 #because the first value in the array is 0 so when you add 1 you end up with 0
    print(path)
    
    for f in os.listdir(path):
#        write_log('basedir file is ',f)
#        if os.path.isdir(os.path.join(basedir, t)):
#            for f in os.listdir(basedir + '/' + t):
        if f.endswith("NDVI.tif"):
            ls_count1 += 1
            JulDate = f[15:-21]
            print('found the NDVI file JUL DATE IS ',JulDate)
#            utm_zone1 = raster_utm_prj(basedir + '/' + t + '/' + t1 + '_B1.TIF')
#            utm_zone1 = raster_utm_prj(basedir + '/' + t1 + '_B1.TIF')
            NDVI_image_names.append(f)
#            Specify the date and format within strptime in this case format is %Y-%m-%d (if your data was 2020/03/30' then use %Y/%m/%d) and specify the format you want to convert into in strftime, in this case it is %Y%j. This gives out Julian date in the form CCYYDDD, if you need only in YYDDD give the format as %y%j
            NDVI_dates.append(JulDate)
            print('Sentinel File is ', NDVI_image_names[ls_count1], "ls_count1 is ", ls_count1)
            print('Sentinel Image Date is ', JulDate)
            
    return(ls_count1)


def NDVI_VM(start_date, end_date, aoi,project):
    # Raster layers cell size
    cellSize = 30

    #py_script_dir = r"C:\Users\lag\AppData\Local\Continuum\anaconda3\envs\AgroET\Scripts"
    #py_script_dir = "/usr/bin"
    #if not os.path.exists(os.path.join(py_script_dir, "gdal_calc.py")):
    #    py_script_dir = "/usr/local/bin/"

    #basedir = os.getcwd()
    #print("basedir is",basedir)

    # Define the shapefile for the area of interest.  This is the bounding box that will be used to download the Sentinel images.
    # This shapefile needs to be in the same directory as the python code.
    #aoi_file = "aoi.shp"
    aoi_file = "AOI_ET_FLUX_UTM_10.shp"

    shp_buffer(aoi_file, +1000.0, "aoi_plus.shp")

    df = geopandas.read_file("aoi_plus.shp")
    #df = geopandas.read_file(aoi_file)

    # Should be single feat
    #df = df.to_crs({"init": "epsg:32610"})
    df = df.to_crs({"init": "epsg:4326"})
    bounds = df.geometry.apply(lambda x: x.bounds).tolist()
    print("bounds",bounds)

    x_min, y_min, x_max, y_max = min(bounds, key=itemgetter(0))[0], min(bounds, key=itemgetter(1))[1], max(bounds, key=itemgetter(2))[2], max(bounds, key=itemgetter(3))[3] #https://stackoverflow.com/questions/13145368/find-the-maximum-value-in-a-list-of-tuples-in-python
    print(x_min, y_min, x_max, y_max)

    delete_shp_file("aoi_bb.shp")
    shp_bbox(".", "aoi_plus.shp","aoi_bb")

    # Define the date range (start and end dates)
    #start_date = "2020-08-01"
    #end_date = "2020-09-30"

    Harmonized_Dual_Download(
            x_min,
            x_max,
            y_min,
            y_max,
            start_date,
            end_date
        )

    NDVI_res_dir = "NDVI"
    NDVI_directory = "hls_data_F"
    path = os.path.join(os.getcwd(),NDVI_res_dir)

    basedir0 = "/reset/seasonal_projects"
    basedir = os.path.join(basedir0, str(project))

    # check whether directory already exists
    if not os.path.exists(path):
        os.mkdir(path)
        print("Folder %s created!" % path)
    else:
        print("Folder %s already exists" % path)

    for t in os.listdir(NDVI_directory):
        t_fp = os.path.join(os.getcwd(),"hls_data_F",t)
        
        print("t is ",t_fp)
        ndvi_img = False
        if t_fp.endswith("B05.tif"):
            tt = t_fp.replace("B05.tif","B04.tif")
            print("t = ",t,"tt = ",tt)
            ndvi_img = t_fp.replace("B05.tif","NDVI1.tif")
        if t.endswith("B8A.tif"):
            tt = t_fp.replace("B8A.tif","B04.tif")
            print("t = ",t_fp,"tt = ",tt)
            ndvi_img = t.replace("B8A.tif","NDVI1.tif")
                    
        if(ndvi_img):
            file1 = os.path.join(os.getcwd(),NDVI_res_dir,ndvi_img[:-5] + ".tif")
            print("NDVI file check to see if it exists ", file1)
            too_much_nodata = basedir +"/NDVI/" + ndvi_img[:-5] + "_too_much_no_data"

            if os.path.exists(file1):
                print("file exists")
                continue
            elif os.path.exists(too_much_nodata):
                print("this scene has too much no data")
                continue
            else:
                delete_file_w_path(ndvi_img)
                gdal_calc = (
                    "gdal_calc.py -A "
                    + t_fp
                    + " -B "
                    + tt
                    + " --outfile="
                    + path
                    +"/"
                    + ndvi_img
                    + " --calc="
                    + '"((A-B)/(A+B))" '
                    + "--type=Float64"
                    + " --overwrite --quiet --NoDataValue=-9999"
                )
                print("gdal_calc = ",gdal_calc,"\n")
                gdal_calc_func(ndvi_img, gdal_calc)

                df = geopandas.read_file("aoi_bb.shp")

                bounds = df.geometry.apply(lambda x: x.bounds).tolist()

                x_utm_min, y_utm_min, x_utm_max, y_utm_max = min(bounds, key=itemgetter(0))[0], min(bounds, key=itemgetter(1))[1], max(bounds, key=itemgetter(2))[2], max(bounds, key=itemgetter(3))[3] #https://stackoverflow.com/questions/13145368/find-the-maximum-value-in-a-list-of-tuples-in-python
                print("x_utm_min, y_utm_min, x_utm_max, y_utm_max",x_utm_min, y_utm_min, x_utm_max, y_utm_max)

                clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells = aoi_extent(
                "aoi_bb.shp"
                )
                print("clip_x1, clip_y1, clip_x2, clip_y2",clip_x1, clip_y1, clip_x2, clip_y2)

                for t1 in os.listdir(path):
                    ndvi_img = False
                    if t1.endswith("NDVI1.tif"):
                        t = t1[:-5] + ".tif"
                        delete_file_w_path(path + "/" + t)
                        print('current directory ',os.getcwd())
                        clip_img_wo_align(path, t1, t,"aoi_bb.shp")
                        nodata_value = -9999 # enter the NoData value here
                        perc_nodata = check_image(path + "/" + t,nodata_value)   
                        print("File name is ",t,"percent no data is ",perc_nodata,"\\n") 
                        orig_t = t[:-4] + "_orig." + t[-3:]
                        delete_file_w_path(path +"/" + orig_t)
                        delete_file_w_path(path +"/" + t1)
                        if(perc_nodata < 80):
                            delete_file_w_path(path + "/" + t)
                            print(
                            "writing that this file will not work ",
                            t1,
                            " since it has too much no data"
                            )
                            cf = open(too_much_nodata, "a+")
                            cf.write("Scene has too much no data")
                            cf.close()

    NDVI_image_names=[]
    NDVI_dates = []
    sorted_NDVI_dates = []
    num_NDVI_images = find_NDVI_images(NDVI_image_names, NDVI_dates, path)
    print("There are ",num_NDVI_images," NDVI Sentinel dates")
    sorted_NDVI_dates = sorted(NDVI_dates)
    print("Sorted NDVI dates ", sorted_NDVI_dates)
    print("Finished generating NDVI images")
    
    print("Checking if there is an NDVI directory in the project otherwise create it!!!")
    isDirectory = os.path.isdir(basedir + "/NDVI")
    if isDirectory == False:
        os.mkdir(basedir + "/NDVI")
        
    for t1 in os.listdir(path):
        print("t1 is ",t1)
        ndvi_img = False
        if t1.endswith("NDVI.tif"):
            print(
                "copying the NDVI file to the project directory ",
                t1)
            isFile = os.path.exists(basedir + "/NDVI/" + t1)
            if isFile == True:
                print(
                    "NDVI file already exists in the project directory so skip copying it"
                )
            else:
                print(
                    "Copying NDVI file ",t1," to the project directory"
                )
                shutil.copyfile(path + "/" + t1, basedir + "/NDVI/" + t1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Run the AgroET model for a date and an AOI."
    )
    parser.add_argument(
        "start_date",
        metavar="start_date",
        type=str,
        help="Start date in YYYY-MM-DD format",
    )

    parser.add_argument(
        "end_date",
        metavar="end_date",
        type=str,
        help="End date in YYYY-MM-DD format",
    )
    parser.add_argument(
        "--aoi",
        type=str,
#        default="aoi.shp",
        help="No AOI shafile name",
    )
    parser.add_argument(
        "--max-cloud-perc",
        type=int,
        default=25,
        help="Do not process the image if the cloudcover is greater than this",
    )
    parser.add_argument(
        "--project", type=str, default=None, help="project name for seasonal run"
    )

    args = parser.parse_args()

    NDVI_VM(
        args.start_date,
        args.end_date,
        args.aoi,
        args.project
    )
    # all finished
