import os
from osgeo import ogr
from osgeo import gdal
import numpy as np
import shutil
import rasterio

cellSize = 30.0
DEBUGFLAG = 1

def write_log(*text_args):
    pf = open("progress.log", "a+")
    text = " ".join([str(a) for a in text_args])
    pf.write(text + "\n")
    pf.flush()
    os.fsync(pf)


def align_rast(clip_file, basedir, rast_name):
    UL_x, UL_y, LL_x, LL_y, UR_x, UR_y, LR_x, LR_y = Read_Raster_Extent(clip_file)
    orig_rast = rast_name[:-4] + "_orig." + rast_name[-3:]
    copyFile(basedir + "/" + rast_name, basedir + "/" + orig_rast)
    delete_file_w_path(basedir + "/" + rast_name)
    str_cellSize = str(cellSize)
    str_LL_x = str(LL_x)
    str_UR_x = str(UR_x)
    str_LL_y = str(LL_y)
    str_UR_y = str(UR_y)
    os.system(
        "gdalwarp -tr "
        + str_cellSize
        + " "
        + str_cellSize
        + " -te "
        + str_LL_x
        + " "
        + str_LL_y
        + " "
        + str_UR_x
        + " "
        + str_UR_y
        + " "
        + basedir
        + "/"
        + orig_rast
        + " "
        + basedir
        + "/"
        + rast_name
    )


# This functions clips a raster image (in_image) based on a shapefile (aoi_lyr)
def clip_img(clip_file, basedir, in_image, out_image, aoi_lyr):
    clip_img_wo_align(clip_file, basedir, in_image, out_image, aoi_lyr)
    align_rast(clip_file,basedir, out_image)


# This functions clips a raster image (in_image) based on a shapefile (aoi_lyr)
def clip_img_wo_align(clip_file, basedir, in_image, out_image, aoi_lyr):
    UL_x, UL_y, LL_x, LL_y, UR_x, UR_y, LR_x, LR_y = Read_Raster_Extent(clip_file)
    out_image1 = out_image[:-4] + "_ZZ" + out_image[-4:]
    delete_file_w_path(basedir + "/" + out_image)
    delete_file_w_path(basedir + "/" + out_image1)
    # GENERATION A 0.0 BUFFER IS NEEDED TO FIX A GEOMETRY FATAL ERROR IN GDALWARP
    shp_buffer(aoi_lyr, 0.0, aoi_lyr[:-4] + "_buff.shp")
    str_LL_x = str(LL_x)
    str_UR_x = str(UR_x)
    str_LL_y = str(LL_y)
    str_UR_y = str(UR_y)
#    write_log("before gdalwarp ",in_image,out_image1)
    os.system(
        "gdalwarp"
        + " -dstnodata -9999"
        + " -overwrite"
        + " -tr "
        + str(cellSize)
        + " "
        + str(cellSize)
        + " -te "
        + str_LL_x
        + " "
        + str_LL_y
        + " "
        + str_UR_x
        + " "
        + str_UR_y
        + " "
#        "-cutline",
#        aoi_lyr[:-4] + "_buff.shp",
# 01-03-23        basedir + "/" + in_image,
        + basedir 
        + "/" 
        + in_image
        + " "
        + basedir 
        + "/" 
        + out_image1
    )
    clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells = aoi_extent(
    "aoi_bb.shp")
    ds = gdal.Open(basedir + "/" + out_image1)
    ds = gdal.Translate(
        basedir + "/" + out_image, ds, projWin=[clip_x1, clip_y1, clip_x2, clip_y2])
    ds = None
    delete_file_w_path(basedir + "/" + out_image1)


def check_image(img, nodata_value):
    # Read in image as a numpy array
    array = rasterio.open(img).read(1)
    # Count the occurance of NoData values in np array
    nodata_count = np.count_nonzero(array == nodata_value)
    # Get a total pixel count
    total_count = array.shape[0] * array.shape[1]
    # Get a % of NoData pixels
    val = 100 - (nodata_count/total_count * 100)
    # Check if image meets threshold of 80% valid pixels
#    if val < 95:
#    write_log("Image {0} has {1} of pixels that are valid".format(os.path.basename(img), val))
    #    return True
    #else:
    #    write_log("Image {0} has <80% of pixels that are valid".format(os.path.basename(img)))
    #    return False
    return(val)
    
    
def Read_Raster_Extent(in_image):
    delete_file_w_path("gdalinfo_output.txt")
    gdalinfo = (
        "gdalinfo -nomd -norat -noct "
        + in_image
        + " > "
        + "gdalinfo_output.txt"
    )
#    write_log("in_image, gdalinfo ", in_image, gdalinfo)
    os.system(gdalinfo)
    fp = open("gdalinfo_output.txt")
    for line in fp:
        if "Upper Left " in line:
            line2 = line.split(" " or ")")
            UL_x = line2[5][:-1]
            UL_y = line2[6][:-1]
#            write_log("Upper Left ", line2, "X ", UL_x, "Y ", UL_y)
        if "Lower Left " in line:
            line2 = line.split(" " or ")")
            LL_x = line2[5][:-1]
            LL_y = line2[6][:-1]
#            write_log("Lower Left ", line2, "X ", LL_x, "Y ", LL_y)
        if "Upper Right " in line:
            line2 = line.split(" " or ")")
            UR_x = line2[4][:-1]
            UR_y = line2[5][:-1]
#            write_log("Upper Right ", line2, "X ", UR_x, "Y ", UR_y)
#        if "Lower Right " in line:
            line2 = line.split(" " or ")")
            LR_x = line2[4][:-1]
            LR_y = line2[5][:-1]
#            write_log("Lower Right ", line2, "X ", LR_x, "Y ", LR_y)
            break
    fp.close()
#    delete_file_w_path(basedir + "/" + "gdalinfo_output.txt")
    return (
        abs(float(UL_x)),
        abs(float(UL_y)),
        abs(float(LL_x)),
        abs(float(LL_y)),
        abs(float(UR_x)),
        abs(float(UR_y)),
        abs(float(LR_x)),
        abs(float(LR_y)),
    )


def delete_shp_file(shp_name):
    if os.path.exists(shp_name):
        driver = ogr.GetDriverByName("ESRI Shapefile")
        driver.DeleteDataSource(shp_name)


def delete_file_w_path(filename):
    full_name = filename
    if os.path.isfile(full_name):
#        try:
        os.remove(full_name)
#        except OSError:
#            write_log("FILE DOES NOT EXIST")
#            pass


def copyFile(src, dest):
    try:
        shutil.copyfile(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        write_log("Error source and destination are the same : %s" % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        write_log("Error source file does not exist: ", e.strerror, " source file is ", src)


# This function determines the four corners of the AOI extent (North, South, East, West).  From these points
# a rectangle can be created that will encompass the AOI
def aoi_extent(aoi):
    inDriver = ogr.GetDriverByName("ESRI Shapefile")
    inDataSource = inDriver.Open(aoi, 0)
    inLayer = inDataSource.GetLayer()
    extent = inLayer.GetExtent()
    del inDataSource
    x_min = extent[0]
    y_max = extent[3]
    x_max = extent[1]
    y_min = extent[2]
    x_cells = round((x_max - x_min) / cellSize)
    y_cells = round((y_max - y_min) / cellSize)
#    write_log("x_min ", extent[0])
#    write_log("y_max ", extent[3])
#    write_log("x_max ", extent[1])
#    write_log("y_min ", extent[2])
#    write_log("x_cells = ", x_cells, "y_cells = ", y_cells)
    return (x_min, y_max, x_max, y_min, x_cells, y_cells)


def shp_bbox(basedir,in_shape,shape_bb):
#    write_log(in_shape,shape_bb)
    driver = ogr.GetDriverByName("ESRI Shapefile")
    vector = driver.Open(in_shape)
    in_lyr = vector.GetLayer()

    driver = ogr.GetDriverByName('ESRI Shapefile') # will select the driver for our shp-file creation.
    shapeData = driver.CreateDataSource(basedir) #so there we will store our data
    out_lyr = shapeData.CreateLayer(shape_bb, in_lyr.GetSpatialRef(), ogr.wkbPolygon) #this will create a corresponding layer for our data with given spatial information.
    out_lyr.CreateFields(in_lyr.schema)
    out_defn = out_lyr.GetLayerDefn()
    out_feat = ogr.Feature(out_defn)

    feature = in_lyr.GetFeature(0)
    geom = feature.GetGeometryRef()
    geom.GetEnvelope()
    (minX, maxX, minY, maxY) = geom.GetEnvelope()
#    write_log("minX, maxX, minY, maxY",minX, maxX, minY, maxY,"\\n")
  
# Then you create the ring for the polygon:
    ring = ogr.Geometry(ogr.wkbLinearRing)
    ring.AddPoint(minX, minY)
    ring.AddPoint(maxX, minY)
    ring.AddPoint(maxX, maxY)
    ring.AddPoint(minX, maxY)
    ring.AddPoint(minX, minY)

#   Now you create your polygon for the bounding box:
    polygon_env = ogr.Geometry(ogr.wkbPolygon)
    polygon_env.AddGeometry(ring)

# Add the polygon to the new feature.
    out_feat.SetGeometry(polygon_env)
    out_lyr.CreateFeature(out_feat)


# This function adds a buffer (buff) to a shapefile (in_shp) and returns a new shapefile (out_shp).
def shp_buffer(in_shp, buff, out_shp):
    # LAG1    aoi_cold = 'aoi_cold6.shp'
#    write_log("inside shp_buffer in_shp", in_shp, "out_shp", out_shp)
    delete_shp_file(out_shp)
#    import pdb; pdb.set_trace()
    shp = ogr.Open(in_shp)
    drv = shp.GetDriver()
    drv.CopyDataSource(shp, out_shp)
    shp.Destroy()
    buf1 = ogr.Open(out_shp, 1)
    lyr1 = buf1.GetLayer(0)
    for i in range(0, lyr1.GetFeatureCount()):
        feat = lyr1.GetFeature(i)
        lyr1.DeleteFeature(i)
        geom1 = feat.GetGeometryRef()
        feat.SetGeometry(geom1.Buffer(buff))
        lyr1.CreateFeature(feat)
    # This code adds an area attribute to the aoi buffered file.  Once you buffer
    # some of the polygons are empty and therefore have no area.  This code
    # determines if a polygon has no area and deletes it.  If we do not delete
    # empty polygons the code crashes later in gdalwarp.
    feature = lyr1.GetNextFeature()
    while feature is not None:
        geom = feature.GetGeometryRef()
        try:
            area = geom.GetArea()
            if area < cellSize * cellSize * 1.1:
                lyr1.DeleteFeature(feature.GetFID())
        except Exception as e:
            write_log("ERROR IN DELETE FEATURES",e)
            lyr1.DeleteFeature(feature.GetFID())
        feature = lyr1.GetNextFeature()
    buf1.Destroy()
#    write_log("finished with shp_buffer")

def gdal_calc_func(outfile_name, gdal_calc):
    delete_file_w_path(outfile_name)
#    write_log("gdal_calc_func ", gdal_calc)
#    write_log(gdal_calc)
    os.system(gdal_calc)

