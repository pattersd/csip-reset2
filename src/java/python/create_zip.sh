#!/bin/sh

# cp /home/dave/work/reset/er2_web/app/er2/apps/er2_map/gis/gdal_utils.py .
# These seem to be the climate stations for colorado climate and cimis.
#   disabled because this path doesn't exist
# cp /home/dave/work/reset/er2_web/app/er2/apps/er2_reset/data/*.json .
rm -f automatic.zip
rm -rf __pycache__ climate/__pycache__ imagery/__pycache__
zip -r automatic.zip *.py climate* imagery* seasonal* era5/* nlads/* AgroET_input.txt *.json
