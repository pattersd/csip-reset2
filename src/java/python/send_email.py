import smtplib
import ssl, os, shutil
from email.mime.multipart import MIMEMultipart
from time import sleep

def send(subject, body):
    port = 587  # For starttls
    smtp_server = "smtp.gmail.com"
    sender_email = "resetagro@gmail.com"
    receiver_email = [
        "elhaddads@gmail.com",
        "luisgarcia7007@gmail.com",
        "pattersd@colostate.edu",
    ]
    password = "pdydymziunhvxwuc"
    context = ssl._create_unverified_context()

    msg = f"Subject: {subject}\n\n{body}"
    with smtplib.SMTP(smtp_server, port) as server:
        server.starttls(context=context)
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, msg)


def send_results(subject, body):
    """Send an email that contains links to result files."""
    cwd = os.getcwd()
    suid = os.path.basename(cwd)
    results_dir = cwd.replace('work', 'results')
    results = os.listdir(results_dir)
    print(f"checking results: {[r for r in results]}")
    if False:
        # zip up everything
        zip_file = os.path.join(results_dir, 'results')
        print(f'zipfile is {zip_file}, suid is {suid}, results_dir is {results_dir}')
        results_zip = shutil.make_archive(zip_file, 'zip', results_dir)
        shutil.copyfile(zip_file + ".zip", "/tmp/results.zip")
        print("done zipping")
        body += f'\nhttp://agroet.westus.cloudapp.azure.com:8080/csip-reset/q/{suid}/results.zip'
    else:
        for f in results:
            body += f'\nhttp://agroet.westus.cloudapp.azure.com:8080/csip-reset/q/{suid}/{f}'
    send(subject, body) 
    sleep(60)

if __name__ == "__main__":
    send("A test subject line", "Body text")

