import argparse
import datetime
import geopandas
import logging
import os
from AgroET_automatic import run_automatic
from imagery.main import Imagery
import send_email
from AgroET_library import write_log

logging.basicConfig(filename='reset.log', level=os.environ.get('LOGLEVEL', 'DEBUG').upper())
imagery_cache_dir = '/reset'


def run_batch(batch_start_date_str, batch_end_date_str, aoi, row, path, raster_prefix=None,
              progress_file='batch_progress.log',
              user_cloud_buffer=None, user_cold_buffer=None,
              cold_aoi=None, cold_pts=None, hot_aoi=None, hot_pts=None):
    with open(progress_file, 'w') as pf:
        df = geopandas.read_file(aoi)
        df = df.to_crs({'init': 'epsg:4326'})
        aoi_geom = df.loc[0].geometry
        centroid = [aoi_geom.centroid.x, aoi_geom.centroid.y]

        imagery = Imagery(force_usgs=False)
        batch_start_date = datetime.datetime.strptime(batch_start_date_str, '%Y-%m-%d').date()
        batch_end_date = datetime.datetime.strptime(batch_end_date_str, '%Y-%m-%d').date()
        batch_date = batch_start_date
        while batch_date <= batch_end_date:
            date_str = batch_date.strftime('%Y-%m-%d')
            # Get image
            if row:
                prods = imagery.get_products_row_path(row, path, batch_date, batch_date)
            else:
                prods = imagery.get_products(centroid, batch_date, batch_date)
            if len(prods):
                write_log('Getting imagery for {0}'.format(batch_date))
                prefix = date_str
                if raster_prefix:
                    prefix = raster_prefix + '_' + date_str,
                run_automatic(date_str, aoi, row, path, prefix, 'progress_{0}.log'.format(date_str), False,
                              user_cloud_buffer, user_cold_buffer, cold_aoi, cold_pts, hot_aoi, hot_pts)
                write_log('Generated results for {0}'.format(prefix))
            else:
                write_log('No imagery for {0}'.format(batch_date))
            batch_date += datetime.timedelta(days=1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run the AgroET model for a series of dates and an AOI.')
    parser.add_argument('batch_start_date', metavar='batch_start_date', type=str, help='Batch start date in YYYY-MM-DD format')
    parser.add_argument('batch_end_date', metavar='batch_end_date', type=str, help='Batch end date in YYYY-MM-DD format')
    parser.add_argument('--aoi', type=str, default='aoi.shp', help='name of AOI shapefile if not aoi.shp')
    parser.add_argument('--row', type=str, default=None, help='row in DDD format')
    parser.add_argument('--path', type=str, default=None, help='path in DDD format')
    parser.add_argument('--prefix', type=str, default=None, help='string to prepend to output raster names')
    parser.add_argument('--user', type=str, default='<undefined user>', help='user who made the request')
    parser.add_argument('--cold-buffer', type=str, default=None, help='cold buffer value')
    parser.add_argument('--cloud-buffer', type=str, default=None, help='cloud buffer value')
    parser.add_argument('--cold-aoi', type=str, default=None, help='cold aoi layer')
    parser.add_argument('--cold-pts', type=str, default=None, help='cold points layer')
    parser.add_argument('--hot-aoi', type=str, default=None, help='hot aoi layer')
    parser.add_argument('--hot-pts', type=str, default=None, help='hot points layer')
    args = parser.parse_args()
    admin_site = os.getenv('admin_url', 'http://www.irrigator.co/?next=/reset_admin/&workspace={0}'.format(os.getcwd()))
    try:
        run_batch(args.batch_start_date, args.batch_end_date, args.aoi, args.row, args.path, args.prefix,
                  user_cold_buffer=args.cold_buffer, user_cloud_buffer=args.cloud_buffer,
                  cold_aoi=args.cold_aoi, cold_pts=args.cold_pts, hot_aoi=args.hot_aoi, hot_pts=args.hot_pts)
        # all finished, send email
        send_email.send('finished run', 'Automatic run finished for user {0}. Name is {1}. Workspace is {2}. Go to {3}'.format(args.user, args.prefix or '', os.getcwd(), admin_site))
    except Exception as e:
        send_email.send('Error in run', 'Automatic run failed for user {0}. Workspace is {1}. Error is {2}. Go to {3}'.format(args.user, os.getcwd(), e, admin_site))
        raise
