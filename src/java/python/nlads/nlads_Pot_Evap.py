# STEP 1
import sys
import re, math
import http
import json, shutil
import urllib3
import certifi
import requests
import subprocess
from time import sleep
from http.cookiejar import CookieJar
import urllib.request
from urllib.parse import urlencode
import getpass
import netCDF4
import xarray as xr
from netCDF4 import Dataset
import sys
import rasterio
import os
import osgeo.ogr
from osgeo import gdal, osr
from rasterio.warp import reproject, Resampling
import gdal_utils
import numpy as np

cellSize = 30.0

# STEP 2
# Create a urllib PoolManager instance to make requests.
http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())
# Set the URL for the GES DISC subset service endpoint
url = 'https://disc.gsfc.nasa.gov/service/subset/jsonwsp'


# STEP 3
# This method POSTs formatted JSON WSP requests to the GES DISC endpoint URL
# It is created for convenience since this task will be repeated more than once

def get_http_data(request):
    hdrs = {'Content-Type': 'application/json',
            'Accept'      : 'application/json'}
    data = json.dumps(request)
    r = http.request('POST', url, body=data, headers=hdrs)
    response = json.loads(r.data)
    # Check for errors
    if response['type'] == 'jsonwsp/fault' :
        raise Exception('API Error: faulty %s request' % response['methodname'])
        sys.exit(1)
    return response


# This function returns the extent of a shapefile
def get_extent_shapefile(file_path, projection=None):
    reproj_file_path = file_path
    if projection:
        reproj_file_path = project_shapefile(file_path, projection)
    source = osgeo.ogr.Open(reproj_file_path)
    layer = source.GetLayerByIndex(0)
    return layer.GetExtent()


def delete_file_w_path(filename):
    full_name = filename
    if os.path.isfile(full_name):
        try:
            os.remove(full_name)
        except OSError:
            print('filename does not exist ', filename)
            # LAG 8-12            if DEBUGFLAG == 1:
            # LAG 8-12                print('!!!!!!!! could not remove file ', full_name)
            pass


def project_shapefile(shp_path, projection):
    basename, ext = os.path.splitext(shp_path)
    reproj_path = basename + "_reproj" + ext
    args = ["ogr2ogr", "-t_srs", projection, reproj_path, shp_path]
    check = subprocess.check_output(args)
    if b"Error" in check:
        raise Exception("Failed to reproject NLCD to aoi projection: {0}".format(check))
        exit(1)
    return reproj_path


def copyFile(src, dest):
    try:
        shutil.copyfile(src, dest)
    # eg. src and dest are the same file
    except shutil.Error as e:
        print("Error source and destination are the same : %s" % e)
    # eg. source or destination doesn't exist
    except IOError as e:
        raise Exception("Error source file does not exist: ", e.strerror, " source file is ", src)


def delete_files_w_pattern(dirpath, pattern):
    # Get a list of all the file paths that ends with .txt from in specified directory
    fileList = glob.glob(os.path.join(dirpath, pattern))

    # Iterate over the list of filepaths & remove each file.
    for filePath in fileList:
        try:
            os.remove(filePath)
        except:
            if DEBUGFLAG == 1:
                print("!!!!!!!! could not remove file ", filePath)
            pass


def delete_file_w_path(filename):
    full_name = filename
    if os.path.isfile(full_name):
        try:
            os.remove(full_name)
        except OSError:
            print('file does not exist', filename)
            # LAG 8-12            if DEBUGFLAG == 1:
            # LAG 8-12                print('!!!!!!!! could not remove file ', full_name)
            pass


def wind_speed_2m(ws, z):
    """
    Convert wind speed measured at different heights above the soil
    surface to wind speed at 2 m above the surface, assuming a short grass
    surface.

    Based on FAO equation 47 in Allen et al (1998).

    :param ws: Measured wind speed [m s-1]
    :param z: Height of wind measurement above ground surface [m]
    :return: Wind speed at 2 m above the surface [m s-1]
    :rtype: float
    """
    return ws * (4.87 / math.log((67.8 * z) - 5.42))


def gdal_calc_func(outfile_name, gdal_calc):
    delete_file_w_path(outfile_name)
#    write_log("gdal_calc_func ", gdal_calc)
    os.system(gdal_calc)


# This functions clips a raster image (in_image) based on a shapefile (aoi_lyr)
def clip_img(in_image, out_image, aoi_lyr, clip_x1, clip_y1, clip_x2, clip_y2):
    clip_img_wo_align(in_image, out_image, aoi_lyr, clip_x1, clip_y1, clip_x2, clip_y2)
    align_rast(out_image)


# This functions clips a raster image (in_image) based on a shapefile (aoi_lyr)
def clip_img_wo_align(in_image, out_image, aoi_lyr, clip_x1, clip_y1, clip_x2, clip_y2):
    out_image1 = out_image[:-4] + "_ZZ" + out_image[-4:]
    basedir = os.getcwd()
    delete_file_w_path(basedir + "/" + out_image)
    delete_file_w_path(basedir + "/" + out_image1)
    # GENERATION A 0.0 BUFFER IS NEEDED TO FIX A GEOMETRY FATAL ERROR IN GDALWARP
    shp_buffer(aoi_lyr, 0.0, aoi_lyr[:-4] + "_buff.shp")
    print("basedir = ", basedir)

    gdal_warp = [
        "gdalwarp",
        "-dstnodata",
        "-9999",
        "-overwrite",
#        "-to",
#        "SRC_METHOD=NO_GEOTRANSFORM",
        "-tr",
        str(cellSize),
        str(cellSize),
        "-cutline",
        aoi_lyr[:-4] + "_buff.shp",
        in_image,
        basedir + "/" + out_image1,
    ]
    print(" ".join(gdal_warp))
    out = subprocess.check_output(gdal_warp)

    ds = gdal.Open(basedir + "/" + out_image1)
    ds = gdal.Translate(
        basedir + "/" + out_image, ds, projWin=[clip_x1, clip_y1, clip_x2, clip_y2]
    )
    ds = None


def delete_shp_file(shp_name):
    if os.path.exists(shp_name):
        driver = osgeo.ogr.GetDriverByName("ESRI Shapefile")
        driver.DeleteDataSource(shp_name)


def shp_buffer(input_shapefile, buffer_distance, output_shapefile):
    # Open the input shapefile
    input_ds = osgeo.ogr.Open(input_shapefile)
    input_layer = input_ds.GetLayer()

    # Get the spatial reference of the input shapefile
    spatial_ref = input_layer.GetSpatialRef()

    # Create a new shapefile
    driver = osgeo.ogr.GetDriverByName('ESRI Shapefile')
    output_ds = driver.CreateDataSource(output_shapefile)
    output_layer = output_ds.CreateLayer('buffered', spatial_ref, osgeo.ogr.wkbPolygon)

    # Add fields from the input shapefile to the output shapefile
    input_layer_defn = input_layer.GetLayerDefn()
    for i in range(input_layer_defn.GetFieldCount()):
        field_defn = input_layer_defn.GetFieldDefn(i)
        output_layer.CreateField(field_defn)

    # Create a buffer around each feature in the input shapefile
    for feature in input_layer:
        geom = feature.GetGeometryRef()
        buffer_geom = geom.Buffer(buffer_distance)

        # Create a new feature with the buffered geometry
        output_feature = osgeo.ogr.Feature(output_layer.GetLayerDefn())
        output_feature.SetGeometry(buffer_geom)

        # Copy the attribute values from the input feature to the output feature
        for i in range(output_feature.GetFieldCount()):
            output_feature.SetField(i, feature.GetField(i))

        # Add the feature to the output layer
        output_layer.CreateFeature(output_feature)

    # Close the shapefile data sources
    input_ds = None
    output_ds = None


# This function adds a buffer (buff) to a shapefile (in_shp) and returns a new shapefile (out_shp).
def shp_buffer_orig(in_shp, buff, out_shp):
    # LAG1    aoi_cold = 'aoi_cold6.shp'
    delete_shp_file(out_shp)
    shp = osgeo.ogr.Open(in_shp)
    drv = shp.GetDriver()
    drv.CopyDataSource(shp, out_shp)
    shp.Destroy()
    buf1 = osgeo.ogr.Open(out_shp, 1)
    lyr1 = buf1.GetLayer(0)
    for i in range(0, lyr1.GetFeatureCount()):
        feat = lyr1.GetFeature(i)
        lyr1.DeleteFeature(i)
        geom1 = feat.GetGeometryRef()
        feat.SetGeometry(geom1.Buffer(buff))
        lyr1.CreateFeature(feat)
    # This code adds an area attribute to the aoi buffered file.  Once you buffer
    # some of the polygons are empty and therefore have no area.  This code
    # determines if a polygon has no area and deletes it.  If we do not delete
    # empty polygons the code crashes later in gdalwarp.
    feature = lyr1.GetNextFeature()
    while feature is not None:
        geom = feature.GetGeometryRef()
        try:
            area = geom.GetArea()
            if area < cellSize * cellSize * 1.1:
                lyr1.DeleteFeature(feature.GetFID())
        except:
            lyr1.DeleteFeature(feature.GetFID())
        feature = lyr1.GetNextFeature()
    buf1.Destroy()


def match_projection(shp_path, raster_path):
    # reproject to AOI projection
    aoi_proj = get_projection_shapefile(shp_path)
    reproj_file_name = reproject(raster_path, aoi_proj.ExportToProj4())
    return reproj_file_name


def reproject(raster_path, projection, cell_size=None):
    basename, ext = os.path.splitext(raster_path)
    reproj_path = basename + "_reproj" + ext
    args = ["gdalwarp", "-t_srs", projection]
    if cell_size:
        args += ["-tr", str(cell_size), str(cell_size)]
    args += [raster_path, reproj_path]
    delete_file_w_path(reproj_path)
    check = subprocess.check_output(args)
    if b"Error" in check:
        raise Exception("Failed to reproject NLADS to aoi projection: {0}".format(check))
        exit(1)
    return reproj_path


# This function returns the projection of a shapefile
def get_projection_shapefile(file_path):
    source = osgeo.ogr.Open(file_path)
    layer = source.GetLayerByIndex(0)
    proj = layer.GetSpatialRef()
    return proj


# This function determines the four corners of the AOI extent (North, South, East, West).  From these points
# a rectangle can be created that will encompass the AOI
def aoi_extent(aoi):
    inDriver = osgeo.ogr.GetDriverByName("ESRI Shapefile")
    inDataSource = inDriver.Open(aoi, 0)
    inLayer = inDataSource.GetLayer()
    extent = inLayer.GetExtent()
    del inDataSource
    clip_x1 = extent[0]
    clip_y1 = extent[3]
    clip_x2 = extent[1]
    clip_y2 = extent[2]
    x_cells = round((clip_x2 - clip_x1) / cellSize)
    y_cells = round((clip_y1 - clip_y2) / cellSize)
    return (clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells)


def align_rast(rast_name):
    basedir = os.getcwd()
    orig_rast = rast_name[:-4] + "_orig." + rast_name[-3:]
    copyFile(basedir + "/" + rast_name, basedir + "/" + orig_rast)
    delete_file_w_path(basedir + "/" + rast_name)
    str_cellSize = str(cellSize)
    UL_x, UL_y, LL_x, LL_y, UR_x, UR_y, LR_x, LR_y = Read_Raster_Extent(
        basedir + '/dem.tif'
#        basedir + '/image/' + LS_name + '_B10_CLIP.TIF'
    )
    str_LL_x = str(LL_x)
    str_UR_x = str(UR_x)
    str_LL_y = str(LL_y)
    str_UR_y = str(UR_y)
    os.system(
        "gdalwarp -tr "
        + str_cellSize
        + " "
        + str_cellSize
        + " -te "
        + str_LL_x
        + " "
        + str_LL_y
        + " "
        + str_UR_x
        + " "
        + str_UR_y
        + " "
        + basedir
        + "/"
        + orig_rast
        + " "
        + basedir
        + "/"
        + rast_name
    )


def Read_Raster_Extent(in_image):
    basedir = os.getcwd()
    delete_file_w_path(basedir + "/" + "gdalinfo_output.txt")
    gdalinfo = (
        "gdalinfo -nomd -norat -noct "
        + in_image
        + " > "
        + basedir
        + "/gdalinfo_output.txt"
    )
    os.system(gdalinfo)
    fp = open(basedir + "/gdalinfo_output.txt")
    for line in fp:
        if "Upper Left " in line:
            line2 = line.split(" " or ")")
            UL_x = line2[5][:-1]
            UL_y = line2[6][:-1]
        if "Lower Left " in line:
            line2 = line.split(" " or ")")
            LL_x = line2[5][:-1]
            LL_y = line2[6][:-1]
        if "Upper Right " in line:
            line2 = line.split(" " or ")")
            UR_x = line2[4][:-1]
            UR_y = line2[5][:-1]
        if "Lower Right " in line:
            line2 = line.split(" " or ")")
            LR_x = line2[4][:-1]
            LR_y = line2[5][:-1]
            break
    fp.close()
    delete_file_w_path(basedir + "/" + "gdalinfo_output.txt")
    return (
        abs(float(UL_x)),
        abs(float(UL_y)),
        abs(float(LL_x)),
        abs(float(LL_y)),
        abs(float(UR_x)),
        abs(float(UR_y)),
        abs(float(LR_x)),
        abs(float(LR_y)),
    )


def read_ls_acq_date_time(fp_path,offset):
    re_date = re.compile("DATE_ACQUIRED = (.+)")
    re_time = re.compile("SCENE_CENTER_TIME = (.+)")
    fp = open(fp_path, "r")
    ln = fp.readline()
    while ln:
        s = re_date.search(ln)
        if s:
            dt_str0 = s.group(1)

        s = re_time.search(ln)
        if s:
            time_str = s.group(1)
        ln = fp.readline()
    Y, M, D = dt_str0.split("-")
    h2, m2, s = time_str.split(":")
    h1 = h2[1:3]
    h = int(h1)
    m = int(m2)
#    import pdb;pdb.set_trace()
    offset = int(offset * -1.0)
    begTime = dt_str0 + 'T'+str(offset).rjust(2,'0')+':00:00'
    dt_str1 = Y+'-'+M.rjust(2,'0')+'-'+str(int(D)+1).rjust(2,'0')
    endTime = dt_str1 + 'T'+str(offset-1).rjust(2,'0')+':00:00'
    return begTime, endTime, h, m
#begTime = '2020-07-28T00:00:00-08:00'
#endTime = '2020-07-29T00:00:00-08:00'

#    import pdb; pdb.set_trace()
#    Y, M, D = 111str0.split("-")
#    h, m, s = time_str.split(":")
#    h1 = h[1:3]
#    h = int(h1)
#    s = int(math.floor(float(s[:-2])))  # trim trailing 'Z'
#    mer = "AM"
#    if h > 12:
#        h -= 12
#        mer = "PM"
#    fp.close()
#    # Format is 7/7/2001 5:26:00 PM
#    return "{M}/{D}/{Y} {h}:{m}:{s} {mer}".format(M=M, D=D, Y=Y, h=h, m=m, s=s, mer=mer)

def raster_to_point_shapefile(input_raster, output_shapefile):
    # Open the raster dataset
    raster_ds = gdal.Open(input_raster)
    if raster_ds is None:
        print(f"Failed to open raster file: {input_raster}")
        return

    # Get the raster's spatial reference
    raster_srs = osr.SpatialReference()
    raster_srs.ImportFromWkt(raster_ds.GetProjection())

    # Get the raster band
    raster_band = raster_ds.GetRasterBand(1)

    # Create a new shapefile
    driver = osgeo.ogr.GetDriverByName('ESRI Shapefile')
    output_ds = driver.CreateDataSource(output_shapefile)
    output_layer = output_ds.CreateLayer('points', srs=raster_srs, geom_type=osgeo.ogr.wkbPoint)

    # Add a single field to store the raster values
    field_defn = osgeo.ogr.FieldDefn('value', osgeo.ogr.OFTReal)
    output_layer.CreateField(field_defn)

    # Get the raster geotransform
    geotransform = raster_ds.GetGeoTransform()
    cell_width = geotransform[1]
    cell_height = geotransform[5]

    # Iterate through the raster cells and create a point feature for every four pixels
    for y in range(0, raster_ds.RasterYSize, 2):
        for x in range(0, raster_ds.RasterXSize, 2):
            # Read the cell values for the four pixels
            values = raster_band.ReadAsArray(x, y, 2, 2)

            # Calculate the average value
            value = np.mean(values)

            # Calculate the cell center coordinates
            x_center = geotransform[0] + (x + 1) * cell_width
            y_center = geotransform[3] + (y + 1) * cell_height

            # Create the point geometry
            point = osgeo.ogr.Geometry(osgeo.ogr.wkbPoint)
            point.AddPoint(x_center, y_center)

            # Create a new feature and set the value attribute
            feature = osgeo.ogr.Feature(output_layer.GetLayerDefn())
            feature.SetGeometry(point)
            feature.SetField('value', float(value))

            # Add the feature to the output layer
            output_layer.CreateFeature(feature)

    # Close the shapefile data source
    output_ds = None


def raster_to_point_shapefile_centroid(input_raster, output_shapefile):
    # Open the raster dataset
    raster_ds = gdal.Open(input_raster)
    if raster_ds is None:
        print(f"Failed to open raster file: {input_raster}")
        return

    # Get the raster's spatial reference
    raster_srs = osr.SpatialReference()
    raster_srs.ImportFromWkt(raster_ds.GetProjection())

    # Get the raster band
    raster_band = raster_ds.GetRasterBand(1)

    # Create a new shapefile
    driver = osgeo.ogr.GetDriverByName('ESRI Shapefile')
    output_ds = driver.CreateDataSource(output_shapefile)
    output_layer = output_ds.CreateLayer('points', srs=raster_srs, geom_type=osgeo.ogr.wkbPoint)

    # Add a single field to store the raster values
    field_defn = osgeo.ogr.FieldDefn('value', osgeo.ogr.OFTReal)
    output_layer.CreateField(field_defn)

    # Get the raster geotransform
    geotransform = raster_ds.GetGeoTransform()
    cell_width = geotransform[1]
    cell_height = geotransform[5]

    # Iterate through the raster cells and create a point feature at the center of each cell
    for y in range(raster_ds.RasterYSize):
        for x in range(raster_ds.RasterXSize):
            # Read the cell value
            value = raster_band.ReadAsArray(x, y, 1, 1)[0, 0]

            # Calculate the cell center coordinates
            x_center = geotransform[0] + (x + 0.5) * cell_width
            y_center = geotransform[3] + (y + 0.5) * cell_height

            # Create the point geometry
            point = osgeo.ogr.Geometry(osgeo.ogr.wkbPoint)
            point.AddPoint(x_center, y_center)

            # Create a new feature and set the value attribute
            feature = osgeo.ogr.Feature(output_layer.GetLayerDefn())
            feature.SetGeometry(point)
            feature.SetField('value', float(value))

            # Add the feature to the output layer
            output_layer.CreateFeature(feature)

    # Close the shapefile data source
    output_ds = None


class Nlads(object):
    def __init__(self, log_file=None):
        """
        :param logger: optional logger
        """
        self.log_file = log_file

    def extract_nlads(self, LS_name):
        # STEP 4
        # Define the parameters for the data subset
        product = 'NLDAS_NOAH0125_H_2.0'
        varNames =['PotEvap','UGRD','VGRD']
        sumNames =['PotEvap_NLDAS','WGRD_NLDAS']
        #sum1 = "sum_" + str(varNames[1])
        #sum2 = "sum_" + str(varNames[2])
        sum3 = "climate_etr.tif"
        sum3a = "sumt_PotEvap_daily.tif"
        sum3b = "sum_PotEvap_daily_b.tif"
        sum4 = "sumt_WGRD_10m_daily.tif"
        sum4b = "sum_WGRD_10m_daily.tif"
        sum5 = "sumt_WGRD_2m_daily.tif"
        sum5b = "climate_windrun_mile_day.tif"
#        import pdb; pdb.set_trace()
        basedir = os.getcwd()
        shp_buffer(basedir + "/aoi.shp", 20000.0, basedir + "/aoi_nlads.shp")

        #Lat/Lon projection is 4326
        lat_long_proj = "EPSG:4326"
        (minlon, maxlon, minlat, maxlat) = get_extent_shapefile(basedir + "/aoi_nlads.shp", lat_long_proj)
        print("minlon",minlon,"maxlon",maxlon,"minlat",minlat,"maxlat",maxlat)

        basedir = os.getcwd()
        fp_path = basedir + "/image/" + LS_name + "_MTL.txt"
#        fp_path = "LC80390372023083_MTL.txt"
        #import pdb; pdb.set_trace()
        offset = int(minlon/15.0)
        begTime, endTime, imageHour, imageMinute = read_ls_acq_date_time(fp_path, offset)
        fracHour = imageMinute/60.0
        str_fracHour = str(fracHour)

        print("\n\n\n\nImage Hour and Minute are ", imageHour, imageMinute,"\n\n\n\n")

        #begTime = '2020-07-28T00:00:00-08:00'
        #endTime = '2020-07-29T00:00:00-08:00'
        # format =   'netCDF'
        # begHour = '00:00:00'
        # endHour = '23:59:59'

        # Subset only the mandatory pressure levels (units are hPa)
        # 1000 925 850 700 500 400 300 250 200 150 100 70 50 30 20 10 7 5 3 2 1
        # dimName = 'lev'
        # dimVals = [1,4,7,13,17,19,21,22,23,24,25,26,27,29,30,31,32,33,35,36,37]
        # Construct the list of dimension name:value pairs to specify the desired subset
        # dimSlice = []
        # for i in range(len(dimVals)):
        #     dimSlice.append({'dimensionId': dimName, 'dimensionValue': dimVals[i]})
        #varNames2 = []
        #for x in range(len(varNames2)):
        for x in range(len(varNames)):
            # STEP 5
            # Construct JSON WSP request for API method: subset
            subset_request = {
                'methodname': 'subset',
                'type': 'jsonwsp/request',
                'version': '1.0',
                'args': {
                    'role'  : 'subset',
                    'format': 'tif',
                    'start' : begTime,
                    'end'   : endTime,
                    'box'   : [minlon, minlat, maxlon, maxlat],
                    'crop'  : True,
                    # 'format':   'netCDF'
                    'data'  : [{'datasetId': product,
                              'variable' : varNames[x],
                             }]
                }
            }
        #    import pdb; pdb.set_trace()

            print("subset_request ",subset_request)

            # STEP 6
            # Submit the subset request to the GES DISC Server
            response = get_http_data(subset_request)
            # Report the JobID and initial status
            myJobId = response['result']['jobId']
            print('Job ID: '+myJobId)
            print('Job status: '+response['result']['Status'])

            # STEP 7
            # Construct JSON WSP request for API method: GetStatus
            status_request = {
                'methodname': 'GetStatus',
                'version': '1.0',
                'type': 'jsonwsp/request',
                'args': {'jobId': myJobId}
            }

            # Check on the job status after a brief nap
            while response['result']['Status'] in ['Accepted', 'Running']:
                sleep(5)
                response = get_http_data(status_request)
                status  = response['result']['Status']
                percent = response['result']['PercentCompleted']
                print ('Job status: %s (%d%c complete)' % (status,percent,'%'))
            if response['result']['Status'] == 'Succeeded' :
                print ('Job Finished:  %s' % response['result']['message'])
            else :
                print('Job Failed: %s' % response['fault']['code'])
                sys.exit(1)

            # STEP 8 (Plan A - preferred)
            # Construct JSON WSP request for API method: GetResult
            batchsize = 20
            results_request = {
                'methodname': 'GetResult',
                'version': '1.0',
                'type': 'jsonwsp/request',
                'args': {
                    'jobId': myJobId,
                    'sessionId': varNames[x],
                    'count': batchsize,
                    'startIndex': 0
                }
            }
            print(results_request)
            # Retrieve the results in JSON in multiple batches
            # Initialize variables, then submit the first GetResults request
            # Add the results from this batch to the list and increment the count
            results = []
            count = 0
            response = get_http_data(results_request)
            count = count + response['result']['itemsPerPage']
            results.extend(response['result']['items'])

            # Increment the startIndex and keep asking for more results until we have them all
            total = response['result']['totalResults']
            while count < total:
                results_request['args']['startIndex'] += batchsize
                response = get_http_data(results_request)
                count = count + response['result']['itemsPerPage']
                results.extend(response['result']['items'])

            # Check on the bookkeeping
            print('Retrieved %d out of %d expected items' % (len(results), total))


            # STEP 8 (Plan B)
            # Retrieve a plain-text list of results in a single shot using the saved JobID

            result = requests.get('https://disc.gsfc.nasa.gov/api/jobs/results/'+myJobId)
            try:
                result.raise_for_status()
                urls = result.text.split('\n')
                for i in urls : print('\n%s' % i)
            except:
                raise
                print('Request returned error code %d' % result.status_code)

             #Sort the results into documents and URLs

            docs = []
            urls = []
            for item in results:
                try:
                    if item['start'] and item['end']: urls.append(item)
                except:
                    docs.append(item)
            # Print out the documentation links, but do not download them
            # print('\nDocumentation:')
            # for item in docs : print(item['label']+': '+item['link'])


            # ATLERNATIVE STEP 10
            # Create a password manager to deal with the 401 response that is returned from
            # Earthdata Login
#            import numpy as np
            import fiona
            from rasterio.mask import mask

            # Create a password manager to deal with the 401 response that is returned from
            # Earthdata Login
            # Enter your Earthdata login credentials
            username = "idsgroup1"  # input('Enter your Earthdata username: ')
            password = "Bazezo99"  # getpass.getpass('Enter your Earthdata password: ')
            # username = input("Provide your EarthData userid: ")
            # password = getpass.getpass("Provide your EarthData password: ")

            password_manager = urllib.request.HTTPPasswordMgrWithDefaultRealm()
            password_manager.add_password(None, "https://urs.earthdata.nasa.gov", username, password)

            # Create a cookie jar for storing cookies. This is used to store and return the session cookie #given to use by the data server
            cookie_jar = CookieJar()

            # Install all the handlers.
            opener = urllib.request.build_opener(urllib.request.HTTPBasicAuthHandler(password_manager),
                                                 urllib.request.HTTPCookieProcessor(cookie_jar))
            urllib.request.install_opener(opener)

            # Open a request for the data, and download files
            print('\nHTTP_services output:')
            for item in urls:
                URL = item['link']
                DataRequest = urllib.request.Request(URL)
                DataResponse = urllib.request.urlopen(DataRequest)

                # Print out the result
                DataBody = DataResponse.read()

                # Save file to working directory
                try:
                    file_name = varNames[x] + '_' + item['label']
                    file_ = open(file_name, 'wb')
                    file_.write(DataBody)
                    file_.close()
                    print(file_name, "is downloaded")
                except requests.exceptions.HTTPError as e:
                    raise
                    print(e)

            print('Downloading is done and find the downloaded files in your current working directory')

        py_script_dir = "/usr/bin"
        if not os.path.exists(os.path.join(py_script_dir, "gdal_calc.py")):
            py_script_dir = "/usr/local/bin/"
        sum = []
        sum1 = []
        count = 0
        #First generate the Wind as the hypothenuse of the U and V components
        for t in os.listdir(basedir):
            if t.startswith(varNames[1]):
                t1 = "V" + t[1:]
                t2 = "W" + t[1:]
                gdal_calc = (
                    "python "
                    + py_script_dir
                    + "/gdal_calc.py -A "
                    + basedir
                    + "/"
                    + t
                    + " -B "
                    + basedir
                    + "/"
                    + t1
                    + " --outfile="
                    + basedir
                    + "/"
                    + t2
                    + " --calc="
                    + '"sqrt(A*A+B*B)" '
                    + "--type=Float64"
                    + " --overwrite --quiet --NoDataValue=-9999"
                )
                print(t, t1, t2, gdal_calc)
                gdal_calc_func(t2, gdal_calc)

        for x in range(0,2):
            count = 0
            for t in os.listdir(basedir):
                if t.startswith(sumNames[x]):
                    count = count + 1
                    if (count == 1):
                        sum.append("tmp_sum_" + sumNames[x] + '.tif')
                        sum1.append("1tmp_sum_" + sumNames[x] + '.tif')
        #               copy the first file with the correct variable name to be the initial sum and then add the otehr files with teh correct variable name to it
                        delete_file_w_path(basedir + "/" + sum[x])
                        copyFile(basedir + "/" + t, basedir + "/" + sum[x])
                    else:
                        print("count",count,"x", x, varNames[x], t, t.startswith(varNames[x]), sum, sum1)
                        gdal_calc = (
                            "python "
                            + py_script_dir
                            + "/gdal_calc.py -A "
                            + basedir
                            + "/"
                            + t
                            + " -B "
                            + basedir
                            + "/"
                            + sum[x]
                            + " --outfile="
                            + basedir
                            + "/"
                            + sum1[x]
                            + " --calc="
                            + '"(A+B)" '
                            + "--type=Float64"
                            + " --overwrite --quiet --NoDataValue=-9999"
                        )
            #            print(t, sum[x], sum1[x], gdal_calc)
                        gdal_calc_func(sum1[x], gdal_calc)
                        delete_file_w_path(basedir + "/" + sum[x])
                        copyFile(basedir + "/" + sum1[x], basedir + "/" + sum[x])

        #import pdb; pdb.set_trace()
        copyFile(basedir + "/" + sum[0], basedir + "/" + sum3)
        copyFile(basedir + "/" + sum[1], basedir + "/" + sum4)
        print("sum[0]",sum[0],"sum[1]",sum[1])

        # Convert from 10 meter wind speed to 2 meter wind speed
        #       (4.87 / LN((67.8 * 10) - 5.42)) = 0.747951
        gdal_calc = (
                "python "
                + py_script_dir
                + "/gdal_calc.py -A "
                + basedir
                + "/"
                + sum4
                + " --outfile="
                + basedir
                + "/"
                + sum5
                + " --calc="
                + '"((A*0.747951)/24)*53.6865" '
                + "--type=Float64"
                + " --overwrite --quiet --NoDataValue=-9999"
        )
        #import pdb; pdb.set_trace()
        print(sum4, sum5, gdal_calc)
        gdal_calc_func(sum5, gdal_calc)

        for t in os.listdir(basedir):
            if t.startswith("PotEvap"):
                if (str(imageHour*100) in t):
                    firstHour = t
                if (str((imageHour+1)*100) in t):
                    secondHour = t

        print("\n\n\n\nImage Hour and Image Hour + 1 ", firstHour, secondHour,"\n\n\n\n")

        # Calculate the PotEvap at the time of the image by interpolating between the two hours before and after
        PotEvapHour = "climate_etr_hourly.tif"
        PotEvapHour1 = "PotEvap_Hourly1.tif"
        PotEvapHour2 = "PotEvap_Hourly2.tif"
        gdal_calc = (
                "python "
                + py_script_dir
                + "/gdal_calc.py -A "
                + basedir
                + "/"
                + firstHour
                + " -B "
                + basedir
                + "/"
                + secondHour
                + " --outfile="
                + basedir
                + "/"
                + PotEvapHour2
                + " --calc="
                + '"((A+((B-A)*'
                + str_fracHour
                + ')*1.0))" '
# MULTIPLIED BY 1.2 IF GRASS PET AND CONVERTING TO ALFALFA.  IT IS NOT CLEAR WHAT THE PET IS.
#               + ')*1.2))" '
                + "--type=Float64"
                + " --overwrite --quiet --NoDataValue=-9999"
        )
        print(PotEvapHour2, gdal_calc)
        gdal_calc_func(PotEvapHour2, gdal_calc)

        # import pdb; pdb.set_trace()

        PotEvapHour1 = match_projection("aoi.shp", basedir + "/" + PotEvapHour2)
#        import pdb;pdb.set_trace()
        raster_to_point_shapefile_centroid(PotEvapHour1, basedir + "/ws.shp")
#        raster_to_point_shapefile(PotEvapHour1, basedir + "/ws.shp")
        sum3a = match_projection("aoi.shp", basedir + "/" + sum3)
        sum4a = match_projection("aoi.shp",basedir + "/" + sum4)
        sum5a = match_projection("aoi.shp", basedir + "/" + sum5)
        clip_x1, clip_y1, clip_x2, clip_y2, x_cells, y_cells = aoi_extent(
            "aoi_nlads.shp"
        )
        clip_img(PotEvapHour1, PotEvapHour, "aoi_nlads.shp",clip_x1, clip_y1, clip_x2, clip_y2)
        clip_img(sum3a, sum3, "aoi_nlads.shp",clip_x1, clip_y1, clip_x2, clip_y2)
#        clip_img(sum3a, sum3b, "aoi_nlads.shp", clip_x1, clip_y1, clip_x2, clip_y2)
#       convert hourly PET from grass to alfalfa reference multiply by 1.2
#        gdal_calc = (
#                "python "
#                + py_script_dir
#                + "/gdal_calc.py -A "
#                + basedir
#                + "/"
#                + sum3b
#                + " --outfile="
#                + basedir
#                + "/"
#                + sum3
#                + " --calc="
#                + '"(A*1.2)" '
#                + "--type=Float64"
#                + " --overwrite --quiet --NoDataValue=-9999"
#        )
#        print(sum3, gdal_calc)
#        gdal_calc_func(sum3, gdal_calc)

        clip_img(sum4a, sum4b, "aoi_nlads.shp",clip_x1, clip_y1, clip_x2, clip_y2)
        clip_img(sum5a, sum5b, "aoi_nlads.shp",clip_x1, clip_y1, clip_x2, clip_y2)
