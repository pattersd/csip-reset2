# import warnings
# warnings.simplefilter(action='ignore', category=FutureWarning)
import argparse
import csip
import datetime
import geopandas
import subprocess
import glob
import logging
import os
import pytz
import runpy
from shapely.geometry import shape
import shutil
import sys
import send_email
from imagery.main import Imagery
from climate.main import Climate
from nlads.nlads import Nlads
from era5.era5 import ERA5
import gdal_utils
from AgroET_library import write_log

DEBUGFLAG = 1

logging.basicConfig(
    filename="reset.log", level=os.environ.get("LOGLEVEL", "DEBUG").upper()
)
imagery_cache_dir = "/reset"


def run_automatic(
    scene_date_str,
    aoi,
    row,
    path,
    raster_prefix=None,
    test_climate=False,
    user_cloud_buffer=None,
    user_cold_buffer=None,
    cold_aoi=None,
    cold_pts=None,
    hot_aoi=None,
    hot_pts=None,
    ls7_disable_fill_gap=None,
    user_uncalib=None,
    user_conl=None,
    max_cloud_perc=None,
    weather_data=None,
    flat_dem=None,
    seasonal=None,
):
    imagery = Imagery(force_usgs=False)
    scene_date = datetime.datetime.strptime(scene_date_str, "%Y-%m-%d").date()
    df = geopandas.read_file(aoi)

    # Should be single feat
    df = df.to_crs({"init": "epsg:4326"})
    aoi_geom = df.loc[0].geometry
    centroid = [aoi_geom.centroid.x, aoi_geom.centroid.y]

    # Get image
    if DEBUGFLAG == 1:
        write_log("Getting imagery !!!")
    start_date = scene_date - datetime.timedelta(days=0)
    end_date = scene_date + datetime.timedelta(days=0)
    if row:
        prods = imagery.get_products_row_path(row, path, start_date, end_date)
    else:
        prods = imagery.get_products(centroid, start_date, end_date)
    #        prods = imagery.get_products(aoi_geom.centroid.x, aoi_geom.centroid.y, centroid, start_date, end_date)
    #    write_log("Finished imagery.get_products results are ", prods)

    if len(prods):
        image_data = prods[0]
        if DEBUGFLAG == 1:
            write_log(
                "Downloading imagery after get_products_centroid - image data[id] !! ",
                image_data["id"],
            )
        images = imagery.copy_to_workspace(image_data["id"])
        if DEBUGFLAG == 1:
            write_log("Done with imagery !!!! ", images, "\n")
        if (
            image_data["entityId"][2] == "9"
            or image_data["entityId"][2] == "8"
            or image_data["entityId"][2] == "7"
            or image_data["entityId"][2] == "5"
        ):
            bqa_path = [i for i in images if "QA_PIXEL" in i][0]
        else:
            bqa_path = [i for i in images if "BQA" in i][0]
        if DEBUGFLAG == 1:
            write_log("Checking the bqa_path ", bqa_path, "\n")
        bqa = gdal_utils.gdalinfo(bqa_path)
        if DEBUGFLAG == 1:
            write_log("Getting metadata")
        metadata = imagery.get_metadata(image_data["id"])
        if DEBUGFLAG == 1:
            write_log("Done with metadata")
        if not raster_prefix:
            raster_prefix = image_data["id"]
        if DEBUGFLAG == 1:
            write_log("Ready to reproject AOI to landsat")
        # Reproject AOI to landsat projection
        df_landsat_proj = df.to_crs(bqa["proj4"])
        df_landsat_proj.to_file(aoi)

        if DEBUGFLAG == 1:
            write_log("Done with the reproject AOI to landsat")

            write_log("Ready to extract DEM")
        #        extract_dem(aoi_geom.wkt, bqa["proj4"], cell_size=[30, -30])
        extract_dem_original(aoi_geom.wkt, aoi, bqa["proj4"], cell_size=[30, -30])
        if DEBUGFLAG == 1:
            write_log("Done extracting DEM")
        with open("/tmp/g_wkt.txt", "w") as fp:
            fp.write(aoi_geom.wkt)
        # LG        write_log("Done extracting DEM")

        climate = None

        if weather_data == 5:
            if DEBUGFLAG == 1:
                write_log("weather-data flag set to 5 - User provided weather data!!!!")
        elif weather_data == 3:
            nlads = None
            if DEBUGFLAG == 1:
                write_log(
                    "weather-data flag set to 3 - USING GRIDDED WEATHER DATA - NLADS!!!!"
                )
            ls_basedir = os.getcwd() + "/image"
            FileList = glob.glob(os.path.join(ls_basedir, "*B1.TIF"))
            len_ls_basedir = len(ls_basedir)
            LS_name = FileList[0][len_ls_basedir + 1 : -7]
            nlads = Nlads()
            nlads_extract = nlads.extract_nlads(LS_name)

        elif weather_data == 4:
            era5 = None
            if DEBUGFLAG == 1:
                write_log(
                    "weather-data flag set to 4 - USING GRIDDED WEATHER DATA - ERA5!!!!"
                )
            ls_basedir = os.getcwd() + "/image"
            FileList = glob.glob(os.path.join(ls_basedir, "*B1.TIF"))
            len_ls_basedir = len(ls_basedir)
            LS_name = FileList[0][len_ls_basedir + 1 : -7]
            era5 = ERA5()
            era5_extract = era5.extract_era5(LS_name, aoi)

        else:
            # Extract climate data for AOI
            if DEBUGFLAG == 1:
                write_log("Extracting climate")

            climate, count = extract_climate(aoi, metadata, bqa["proj4"])
            if count == 0:
                if DEBUGFLAG == 1:
                    write_log(
                        "climate extraction did not find any ET values - run is terminated"
                    )
                return 10
            if DEBUGFLAG == 1:
                write_log("climate extraction finished")
            if test_climate:
                if DEBUGFLAG == 1:
                    write_log("Exiting after climate test.")
                return

        if (
            climate is not None
            or weather_data == 3
            or weather_data == 4
            or weather_data == 5
        ):
            if weather_data == 5:
                generate_rasters_user_weather()
            elif weather_data != 3 and weather_data != 4:
                generate_rasters()
            # build agroet.txt and run.
            script = "AgroET_WEB_LS8.py"
            script_path = os.path.join("/tmp/csip/python", script)
            #            if not os.path.exists(script_path):
            #                # local development
            #                rundir = (
            #                    "/mnt/work/work_home/work/new/reset/csip-reset2/src/java/python"
            #                )
            #                script_path = os.path.join(rundir, script)
            #                shutil.copy(os.path.join(rundir, "AgroET_input.txt"), ".")
            #            else:
            #                shutil.copy("/tmp/csip/python/AgroET_input.txt", ".")

            delete_file_w_path("AgroET_input.txt")
            with open("AgroET_input.txt", "w") as fp:
                fp.write(f"DATA_DIRECTORY=.\n")
                fp.write(f"LS_IMAGE_DIR=image\n")
                fp.write(f"DEM_FILE=dem.tif\n")
                fp.write(f"climate_windrun_mile_day=climate_windrun_mile_day.tif\n")
                fp.write(f"climate_etr_hourly=climate_etr_hourly.tif\n")
                fp.write(f"climate_etr=climate_etr.tif\n")
                fp.write(f"WS_FILE=ws.shp\n")
                fp.write(f"AOI_FILE={aoi}\n")
                fp.write(f"LOCAL_NASS_DIR=/tmp/csip/NASS\n")
                fp.write(f"LOCAL_DEM_DIR=/tmp/csip/dem\n")
                if user_cloud_buffer:
                    fp.write("USER_CLOUD_BUFFER={0}\n".format(user_cloud_buffer))
                if user_cold_buffer:
                    fp.write("USER_COLD_BUFFER={0}\n".format(user_cold_buffer))
                if cold_aoi:
                    fp.write("USER_COLD_AOI_FILE={0}\n".format(cold_aoi))
                if cold_pts:
                    fp.write("USER_COLD_PTS_FILE={0}\n".format(cold_pts))
                if hot_aoi:
                    fp.write("USER_HOT_AOI_FILE={0}\n".format(hot_aoi))
                if hot_pts:
                    fp.write("USER_HOT_PTS_FILE={0}\n".format(hot_pts))
                if ls7_disable_fill_gap:
                    fp.write("LS7_DISABLE_FILL_GAP=1\n")
                if user_uncalib is not None:
                    fp.write("USER_UNCALIB={0}\n".format(1 if user_uncalib else 0))
                if user_conl is not None:
                    fp.write("USER_CONL={0}\n".format(user_conl))
                if seasonal:
                    fp.write("seasonal=1\n")
                if max_cloud_perc is not None:
                    fp.write(f"USER_MAX_CLOUD_PERC={max_cloud_perc}\n")
                #                if weather_data is not None:
                #                    fp.write(f"WEATHER_DATA={weather_data}\n")
                if flat_dem is not None:
                    fp.write(f"DEM_FLAT={1 if flat_dem else 0}\n")
                fp.write("#\n")

            if DEBUGFLAG == 1:
                write_log("Model run started")
            try:
                globals = runpy.run_path(script_path)
            except Exception as ex:
                if DEBUGFLAG == 1:
                    write_log("Run failed:")
                    write_log(str(ex))
                raise

            if DEBUGFLAG == 1:
                write_log("Run finished successfully")

            delete_files_w_pattern("/reset/reset_csip/", "*.tar.gz")

            if False and raster_prefix:
                to_rename = [
                    "et_24_raster.img",
                    "et_inst.img",
                    "evapo_fr.img",
                    "*_aoi.*",
                    "*_pts.*",
                    "*_Comp.*",
                ]
                for pattern in to_rename:
                    for fname in glob.glob(pattern):
                        if os.path.exists(fname):
                            os.rename(fname, "{0}_{1}".format(raster_prefix, fname))
        else:
            if DEBUGFLAG == 1:
                write_log("Sorry, no climate data available")
        if DEBUGFLAG == 1:
            write_log("Exit")
    else:
        raise Exception(
            "No images found on date {0} with row={1} and path={2}".format(
                scene_date, row, path
            )
        )
    delete_file_w_path("AgroET_input.txt")


def delete_file_w_path(filename):
    full_name = filename
    if os.path.isfile(full_name):
        try:
            os.remove(full_name)
        except OSError:
            # LAG 8-12            if DEBUGFLAG == 1:
            # LAG 8-12                print('!!!!!!!! could not remove file ', full_name)
            pass


def delete_files_w_pattern(dirpath, pattern):
    pf = open("progress1.log", "w+")  # LAGT
    # Get a list of all the file paths that ends with .txt from in specified directory
    if DEBUGFLAG == 1:
        write_log("Inside delete_files_w_pattern")
        write_log(dirpath)
        write_log(pattern)
    fileList = glob.glob(os.path.join(dirpath, pattern))

    # Iterate over the list of filepaths & remove each file.
    for filePath in fileList:
        try:
            if DEBUGFLAG == 1:
                write_log(
                    "It would delete the following file but currently it does not - AgroET_automatic 205 ",
                    filePath,
                )
        # LAG            os.remove(filePath)
        except:
            pass
    pf.close()  # LAGT


def extract_climate_prod(aoi, metadata, scene_proj):
    climate = Climate(aoi, logger=logging.getLogger())
    extraction_date = datetime.datetime.strptime(
        "{0} {1}".format(metadata["DATE_ACQUIRED"], metadata["SCENE_CENTER_TIME"][1:6]),
        "%Y-%m-%d %H:%M",
    )
    utc = pytz.utc
    extraction_date = utc.localize(extraction_date)
    station_features = climate.extract_climate(extraction_date)
    count = 0
    for istation, station in enumerate(station_features):
        count = count + 1
    if DEBUGFLAG == 1:
        write_log("count of extract_climate in AgroET_automatic.py ", count)
    if count > 0:
        gdf = geopandas.GeoDataFrame.from_features(station_features)
        gdf.geometry = gdf.geometry.apply(shape)
        gdf.crs = "epsg:4326"
        gdf.to_file("climate.shp")
        scene_climate_df = gdf.to_crs(scene_proj)
        scene_climate_df.to_file("ws.shp")
    return (station_features, count)


def extract_climate(aoi, metadata, scene_proj):
    climate = Climate(aoi, logger=logging.getLogger())
    extraction_date = datetime.datetime.strptime(
        "{0} {1}".format(metadata["DATE_ACQUIRED"], metadata["SCENE_CENTER_TIME"][1:6]),
        "%Y-%m-%d %H:%M",
    )
    utc = pytz.utc
    extraction_date = utc.localize(extraction_date)
    if DEBUGFLAG == 1:
        write_log("before extract_climate in AgroET_automatic.py 321")
    station_features = climate.extract_climate(extraction_date)
    if DEBUGFLAG == 1:
        write_log(
            "after extract_climate in AgroET_automatic.py 218",
            station_features,
            len(station_features["features"]),
        )
    count = len(station_features["features"])
    # WE ARE SETTING WIND RUN TO A MINIMUM OF 60.00 MILES PER DAY
    for istation, station in enumerate(station_features["features"]):
        if station["properties"]["wrun_mile_day"] < 60.00:
            #            print("for station ", station["properties"]["wrun_mile_day"]
            del station_features["features"][istation]
            count -= 1
    if DEBUGFLAG == 1:
        write_log(
            "count of extract_climate in AgroET_automatic.py ", count, station_features
        )
    if count > 0:
        gdf = geopandas.GeoDataFrame.from_features(station_features)
        if DEBUGFLAG == 1:
            write_log("inside extract_climate in AgroET_automatic.py 219")
        gdf.geometry = gdf.geometry.apply(shape)
        if DEBUGFLAG == 1:
            write_log("inside extract_climate in AgroET_automatic.py 221")
        gdf.crs = "epsg:4326"
        if DEBUGFLAG == 1:
            write_log("inside extract_climate 223")
        gdf.to_file("climate.shp")
        if DEBUGFLAG == 1:
            write_log("inside extract_climate 225")
        scene_climate_df = gdf.to_crs(scene_proj)
        if DEBUGFLAG == 1:
            write_log("inside extract_climate 227")
        scene_climate_df.to_file("ws.shp")
        if DEBUGFLAG == 1:
            write_log("inside extract_climate 229")
    return (station_features, count)


def extract_dem(feat, aoi, proj4, cell_size=None):
    #    boundary = get_wkt(aoi)
    dem_path = "dem.tif"
    if DEBUGFLAG == 1:
        write_log("before deleting dem.*")
    for p in glob.glob("dem.*"):
        if DEBUGFLAG == 1:
            write_log("deleting dem file", p)
        os.unlink(p)
    if DEBUGFLAG == 1:
        write_log("Before url")
    url = "http://csip.engr.colostate.edu:8088/csip-watershed/m/extract_DEM/1.0"
    c = csip.Client(url=url)
    c.add_data("bound_wkt", feat)
    if DEBUGFLAG == 1:
        write_log("after deleting dem.* and Before trying CSIP")
    resp = c.execute()
    if DEBUGFLAG == 1:
        write_log("after c.execute resp.get_error is ", resp.get_error())
    if resp.get_error():
        if DEBUGFLAG == 1:
            write_log("Extracting DEM Failed trying local version !!!!!!!!")
        local_dem_layer = "/tmp/csip/dem/US_DEM_all2.tif"
        #           check if the local dem layer exists
        file_exists = os.path.isfile(local_dem_layer)
        if DEBUGFLAG == 1:
            write_log("Checking if local DEM exists ", file_exists)
        if file_exists:
            dem_local_dir = os.getcwd()
            if DEBUGFLAG == 1:
                write_log("Running gdalwarp on local DEM version")
            check = subprocess.run(
                [
                    "gdalwarp",
                    "-overwrite",
                    "-ot",
                    "Float32",
                    "-of",
                    "GTiff",
                    "-cutline",
                    aoi,
                    "-crop_to_cutline",
                    "-dstalpha",
                    local_dem_layer,
                    dem_local_dir + "/" + dem_path,
                ]
            )
        else:
            if DEBUGFLAG == 1:
                write_log(dem.get_error())
            exit(7)

    if DEBUGFLAG == 1:
        write_log("Done extracting DEM - ready to reproject", dem_path)
    info = gdal_utils.gdalinfo(dem_path)
    if DEBUGFLAG == 1:
        write_log("dem_path info is ", info)
    gdal_utils.reproject_raster(dem_path, info["proj4"], proj4, cell_size=cell_size)
    if DEBUGFLAG == 1:
        write_log("Done re-projecting DEM returning to main program")
    return dem_path


def extract_dem_original(feat, aoi, proj4, cell_size=None):
    dem_path = "dem.tif"
    if DEBUGFLAG == 1:
        write_log("Before trying CSIP ")
    url = "http://csip.engr.colostate.edu:8088/csip-watershed/m/extract_DEM/1.0"
    c = csip.Client(url=url)
    c.add_data("bound_wkt", feat)
    resp = c.execute()
    if resp.get_error():
        if DEBUGFLAG == 1:
            write_log(resp.get_error())
    else:
        if DEBUGFLAG == 1:
            write_log("Trying to download from service")
        resp.download_data_files(["DEM_clip.tif"], os.getcwd())
        # rename DEM_clip.tif to dem.tif
        if DEBUGFLAG == 1:
            write_log("Renaming DEM_clip.tif to", dem_path)
        os.rename("DEM_clip.tif", dem_path)

    if DEBUGFLAG == 1:
        write_log("Done extracting DEM - ready to reproject")
    info = gdal_utils.gdalinfo(dem_path)
    gdal_utils.reproject_raster(dem_path, info["proj4"], proj4, cell_size=cell_size)
    if DEBUGFLAG == 1:
        write_log("Done re-projecting DEM returning to main program")
    return dem_path


def generate_rasters():
    """Prepare data for the ReSET model"""
    dem_layer = gdal_utils.gdalinfo("dem.tif")
    climate_gp = geopandas.read_file("ws.shp")
    climate_cols = list(climate_gp.columns.values)

    # First use IDW on climate layer
    for output_name, output_field in [
        ["climate_etr_hourly.tif", "etr_hourly_mm"],
        ["climate_etr.tif", "etr_mm"],
        ["climate_windrun_mile_day.tif", "wrun_mile_day"],
    ]:
        result_path = output_name
        basename, ext = os.path.splitext(result_path)
        for f in glob.glob(basename + ".*"):
            os.unlink(f)

        # get the possibly shortened field name from the shapefile
        output_field_shp = None
        for k in climate_cols:
            if k in output_field:
                output_field_shp = k
                break
        assert (
            output_field_shp
        ), "Cannot find field {0} in climate layer (cols = {1})".format(
            output_field, climate_cols
        )

        request_data = {
            "column": output_field_shp,
            "power": 2,
            "smoothing": 2,
            "max_points": 12,
            "radius1": 0,
            "radius2": 0,
            "angle": 0,
            "nodata": -999,
        }
        extent = [
            dem_layer["x_min"],
            dem_layer["y_min"],
            dem_layer["x_max"],
            dem_layer["y_max"],
        ]
        gdal_utils.gdal_grid(
            "invdist", extent, 30, "ws.shp", result_path, **request_data
        )


def generate_rasters_user_weather():
    """Prepare data for the ReSET model"""
    dem_layer = gdal_utils.gdalinfo("dem.tif")
    climate_gp = geopandas.read_file("ws.shp")
    #    climate_cols = ["etr_hourly_mm","etr_mm","wrun_mile_day"]
    #    climate_cols = [0.79,5.79,64.83437173]
    climate_cols = list(climate_gp.columns.values)

    # First use IDW on climate layer
    for output_name, output_field in [
        ["climate_etr_hourly.tif", "etr_hourly_mm"],
        ["climate_etr.tif", "etr_mm"],
        ["climate_windrun_mile_day.tif", "wrun_mile_day"],
    ]:
        result_path = output_name
        basename, ext = os.path.splitext(result_path)
        for f in glob.glob(basename + ".*"):
            os.unlink(f)

        # get the possibly shortened field name from the shapefile
        output_field_shp = None
        for k in climate_cols:
            if k in output_field:
                output_field_shp = k
                break
        assert (
            output_field_shp
        ), "Cannot find field {0} in climate layer (cols = {1})".format(
            output_field, climate_cols
        )

        request_data = {
            "column": output_field_shp,
            "power": 2,
            "smoothing": 2,
            "max_points": 12,
            "radius1": 0,
            "radius2": 0,
            "angle": 0,
            "nodata": -999,
        }
        extent = [
            dem_layer["x_min"],
            dem_layer["y_min"],
            dem_layer["x_max"],
            dem_layer["y_max"],
        ]
        gdal_utils.gdal_grid(
            "invdist", extent, 30, "ws.shp", result_path, **request_data
        )


if __name__ == "__main__":
    with open("progress.log", "w") as pf:
        parser = argparse.ArgumentParser(
            description="Run the AgroET model for a date and an AOI."
        )
        parser.add_argument(
            "scene_date",
            metavar="scene_date",
            type=str,
            help="Scene date in YYYY-MM-DD format",
        )
        parser.add_argument(
            "--aoi",
            type=str,
            #            default="aoi.shp",
            help="name of AOI shapefile",
        )
        parser.add_argument("--row", type=str, default=None, help="row in DDD format")
        parser.add_argument("--path", type=str, default=None, help="path in DDD format")
        parser.add_argument(
            "--prefix",
            type=str,
            default=None,
            help="string to prepend to output raster names",
        )
        parser.add_argument(
            "--user",
            type=str,
            default="<undefined user>",
            help="user who made the request",
        )
        parser.add_argument(
            "--cold-buffer", type=str, default=None, help="cold buffer value"
        )
        parser.add_argument(
            "--cloud-buffer", type=str, default=None, help="cloud buffer value"
        )
        parser.add_argument("--cold-aoi", type=str, default=None, help="cold aoi layer")
        parser.add_argument(
            "--cold-pts", type=str, default=None, help="cold points layer"
        )
        parser.add_argument("--hot-aoi", type=str, default=None, help="hot aoi layer")
        parser.add_argument(
            "--hot-pts", type=str, default=None, help="hot points layer"
        )
        parser.add_argument(
            "--test-climate", action="store_true", help="Test climate extraction."
        )
        #        parser.add_argument(
        #            "--use-gridded-data", action="store_true", help="Use NLDAS gridded data rather than weather data for wind and daily ET to calibrate."
        #        )
        parser.add_argument(
            "--ls7-disable-fill-gap",
            action="store_true",
            help="Do not use Landsat 7 filling algorithm.",
        )
        parser.add_argument(
            "--user-uncalib", action="store_true", help="Use uncalibrated mode."
        )
        parser.add_argument("--user-conl", type=str, help="Convergence value.")
        parser.add_argument(
            "--max-cloud-perc",
            type=float,
            help="Limit on what percentage of the scene can be clouds.",
        )
        parser.add_argument(
            "--weather-data",
            type=int,
            #            default=1,
            help="1. Use either weather station or gridded data (default); 2. Use weather station data ONLY; 3. Use gridded data ONLY",
        )
        parser.add_argument(
            "--no-flat-dem", action="store_true", help="Do not use a flat DEM."
        )
        parser.add_argument(
            "--seasonal", action="store_true", help="Run as part of seasonal model"
        )
        args = parser.parse_args()
        if DEBUGFLAG == 1:
            write_log("Automatic options:")
            for arg in vars(args):
                write_log(f"{arg} = {getattr(args, arg)}")
            write_log(" ".join(sys.argv))

        admin_site = os.getenv(
            "admin_url",
            "http://www.irrigator.co/?next=/reset_admin/&workspace={0}".format(
                os.getcwd()
            ),
        )
        try:
            run_automatic(
                args.scene_date,
                args.aoi,
                args.row,
                args.path,
                args.prefix,
                test_climate=args.test_climate,
                user_cold_buffer=args.cold_buffer,
                user_cloud_buffer=args.cloud_buffer,
                cold_aoi=args.cold_aoi,
                cold_pts=args.cold_pts,
                hot_aoi=args.hot_aoi,
                hot_pts=args.hot_pts,
                #                use_gridded_data=args.use_gridded_data,
                ls7_disable_fill_gap=args.ls7_disable_fill_gap,
                user_uncalib=args.user_uncalib,
                user_conl=args.user_conl,
                max_cloud_perc=args.max_cloud_perc,
                weather_data=args.weather_data,
                seasonal=args.seasonal,
                flat_dem=not args.no_flat_dem,
            )
            # all finished, send email
            try:
                send_email.send(
                    "finished run",
                    "Automatic run finished for user {0}. Name is {1}. Workspace is {2}".format(
                        "unset", args.prefix or "", os.getcwd()
                    ),
                )
            except Exception as e:
                if DEBUGFLAG == 1:
                    write_log(str(e))
        except Exception as e:
            try:
                send_email.send(
                    "Error in run",
                    "Automatic run failed for user {0}. Workspace is {1}. Error is {2}".format(
                        "unset", os.getcwd(), e
                    ),
                )
            except Exception as e:
                if DEBUGFLAG == 1:
                    write_log(str(e))
            raise
