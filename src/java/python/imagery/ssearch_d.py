import json
import logging
import os
import shutil
from satsearch import Search
import satstac
from .utils import date_fmt
from .gdal_utils import gdalinfo


class SatsearchBackend(object):
    def __init__(self, imagery_cache_dir="/reset/", log_file=None):
        self._imagery_cache_dir = imagery_cache_dir
        self._log_file = log_file
        self._filename_template = os.path.join(self._imagery_cache_dir, "${id}")

    def debug(self, msg):
        self._log_file.write(msg + "\n")

    @staticmethod
    def get_datetime(start_date, end_date):
        return "{0}/{1}".format(
            start_date.strftime(date_fmt), end_date.strftime(date_fmt)
        )

    @staticmethod
    def imagery(ls_type, centroid, start_date, end_date):
        search_feat = {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "Point",
                "coordinates": centroid,
            },
        }
        geom_json = json.dumps(search_feat)

        ret = []
        if ls_type == "LS8":
            search = Search.search(
                intersects=geom_json,
                property=["landsat:tier=T1", "eo:platform=landsat-8"],
                datetime=SatsearchBackend.get_datetime(start_date, end_date),
                limit=30,
            )
            items = search.items()
            # items.download(key='thumbnail', path=self.imagery_cache_dir)
            # convert scenes to dictionaries
            for s in items:
                # This adds scene metadata
                d = {k: s[k] for k in s.properties.keys()}
                # Add scene band info
                d["id"] = d["landsat:scene_id"]
                d["Collection Category"] = d["landsat:tier"]
                d.update(s.assets)
                ret.append(d)
        return ret

    @staticmethod
    def imagery_row_path(ls_type, row, path, start_date, end_date):
        search = Search.search(
            property=[  #'landsat:tier=T1',
                "eo:column={0:0>3}".format(path),
                "eo:row={0:0>3}".format(row),
                "eo:platform=landsat-8",
            ],
            datetime=SatsearchBackend.get_datetime(start_date, end_date),
            limit=30,
        )
        items = search.items()
        # items.download(key='thumbnail', path=self.imagery_cache_dir)
        # convert scenes to dictionaries
        ret = []
        for s in items:
            # This adds scene metadata
            d = {k: s[k] for k in s.properties.keys()}
            # Add scene band info
            d["id"] = d["landsat:scene_id"]
            d["Collection Category"] = d["landsat:tier"]
            d.update(s.assets)
            ret.append(d)
        return ret

    @staticmethod
    def get_products(geom, start_date, end_date, row=None, path=None):
        search_feat = {
            "type": "Feature",
            "properties": {},
            "geometry": geom,
        }
        geom_json = json.dumps(search_feat)
        property = ["landsat:tier=T1", "eo:platform=landsat-8"]
        if row:
            property.append("eo:row={0}".format(row))
        if path:
            property.append("eo:column={0}".format(path))
        search = Search.search(
            intersects=geom_json,
            property=property,
            datetime="{0}/{1}".format(start_date, end_date),
            limit=30,
        )
        items = search.items()

        # convert scenes to dictionaries
        ret = []
        for s in items:
            # This adds scene metadata
            d = {k: s[k] for k in s.properties.keys()}
            # Add scene band info
            d["id"] = d["landsat:scene_id"]
            d.update(s.assets)
            ret.append(d)
        return ret

    def get_metadata(self, scene_id):
        metadata = {}
        search = Search.search(
            query={"landsat:scene_id": {"eq": scene_id}},
            collections=["landsat-8-l1-c1"],
        )
        file_list = search.items().download(
            key="MTL", filename_template=self._filename_template
        )
        # TODO: fix this for non-landsat 8 images
        for file_path in file_list:
            with open(file_path) as fp:
                for r in fp:
                    if "=" in r:
                        k, v = [t.strip() for t in r.split("=")]
                        metadata[k] = v
        return metadata

    def get_scene(self, scene_id):
        metadata = {}
        bqa_layer = None
        # TODO: include collection as a user-specified argument.
        search = Search(property=["landsat:scene_id={0}".format(scene_id)])
        for s in search.items():
            logging.getLogger("er2").info(s.id)
            # TODO: fix this for non-landsat 8 images
            file_list = [
                s.download(key="MTL", filename_template=self._filename_template),
                s.download(key="BQA", filename_template=self._filename_template),
            ]

            for file_path in file_list:
                logging.getLogger("er2").debug(file_path)
                if file_path:
                    ext = os.path.splitext(file_path)[1].lower()
                    if ".txt" == ext:
                        # load metadata
                        with open(file_path) as fp:
                            for r in fp:
                                if "=" in r:
                                    k, v = [t.strip() for t in r.split("=")]
                                    metadata[k] = v
                    elif "BQA" in file_path:
                        bqa_layer = self.get_BQA_layer(file_path)
        self.set_state(self.state)
        return {"metadata": metadata, "BQA_layer": bqa_layer}

    def fix_image_path(self, file_path):
        # Check if the file path contains 'LGN00', in which case we strip it off
        #  (2019 file paths do not have this)
        if "LGN00" in file_path:
            fixed_file_path = file_path.replace("LGN00", "")
            shutil.move(file_path, fixed_file_path)
            file_path = fixed_file_path
        return file_path

    def get_band(self, scene_or_id, band="BQA"):
        scene_id = scene_or_id["id"] if isinstance(scene_or_id, dict) else scene_or_id
        bqa = {}
        url = "https://earth-search.aws.element84.com/v0"
        collection = "landsat-8-l1-c1"  # HLS
        search = Search.search(
            url=url,
            collections=[collection],
            query={"landsat:scene_id": {"eq": scene_id}},
        )
        # TODO: fix this for non-landsat 8 images
        logging.getLogger("er2").info(f"Found {search.found()} items")
        items = search.items()
        # file_list = items.download(key=band, path=self._imagery_cache_dir)
        file_list = items.download(
            key=band,
            filename_template=self._filename_template,
        )
        for file_path in file_list:
            file_path = self.fix_image_path(file_path)
            bqa = gdalinfo(file_path)

        # TODO: get this working for latest version of sat-search 0.3.0
        # search = Search.search(query={"landsat:scene_id": scene_id})
        # for item in search.items(limit=5):
        #     # TODO: fix this for non-landsat 18 images
        #     # file_list = [s.download(key=band, path=self._imagery_cache_dir)]
        #     item.download(key=band, filename_template=self._imagery_cache_dir),
        #     file_path = os.path.join(self._imagery_cache_dir, scene_id)
        #     file_path = self.fix_image_path(file_path)
        #     bqa = gdalinfo(file_path)
        return bqa

    def download_scene(self, scene_id):
        search = Search.search(property=["landsat:scene_id={0}".format(scene_id)])
        ret = []
        for s in search.items():
            keys = ["MTL", "BQA"] + ["B{0}".format(i) for i in range(1, 12)]
            for key in keys:
                # Download to cache
                ret.append(
                    self.fix_image_path(
                        s.download(key=key, filename_template=self._filename_template)
                    )
                )
        return ret
