# Manages imagery cache
from functools import partial
import glob
import gzip
import logging
import os
import pyproj
import shutil
from .ssearch import SatsearchBackend as Satsearch
from .agro_usgs import UsgsBackend as Usgs
from .usgs_api import UsgsApiException


def delete_files_w_pattern(dirpath, pattern):
    # Get a list of all the file paths that ends with .txt from in specified directory
    fileList = glob.glob(os.path.join(dirpath, pattern))

    # Iterate over the list of filepaths & remove each file.
    for filePath in fileList:
        try:
            os.remove(filePath)
        except:
            if DEBUGFLAG == 1:
                print("!!!!!!!! could not remove file ", filePath)
            pass


class Imagery(object):
    project_to_4326 = partial(
        pyproj.transform,
        pyproj.Proj("epsg:3857"),  # source coordinate system
        pyproj.Proj("epsg:4326"),
    )  # destination coordinate system

    ls_types = ["LS9", "LS8", "LS7", "LS5"]

    def __init__(self, log_file=None, force_usgs=False, imagery_cache_dir="/reset/"):
        self._force_usgs = force_usgs
        self._imagery_cache_dir = imagery_cache_dir
        self._satsearch = None
        self._usgs = None
        self._log_file = log_file

    def debug(self, msg, level=logging.INFO):
        if self._log_file:
            self._log_file.write(str(msg) + "\n")
        logging.log(level, msg)
        logging.debug(msg)

    @property
    def satsearch(self):
        if not self._satsearch:
            self._satsearch = Satsearch(self._imagery_cache_dir, self._log_file)
        return self._satsearch

    @property
    def usgs(self):
        if not self._usgs:
            self._usgs = Usgs(self._imagery_cache_dir, self._log_file)
        return self._usgs

    def get_products(self, centroid, start_date, end_date):
        products = []
        for ls_type in self.ls_types:
            if ls_type == "LS5" and start_date.year > 2012:
                self.debug("skipping LS5")
                continue
            try:
                ret = self.usgs.imagery(ls_type, centroid, start_date, end_date)
            except Exception as e:
                self.debug(f"Failed to get imagery for landsat type {ls_type}")
                self.debug(e)
                ret = None
            # if not ret:
            #    # TODO: satsearch might be broken.
            #    ret = self.satsearch.imagery(ls_type, centroid, start_date, end_date)
            if ret:
                products += ret
        return self.filter_products(products)

    def get_products_row_path(self, row, path, start_date, end_date):
        products = []
        for ls_type in self.ls_types:
            if ls_type == "LS5" and start_date.year > 2012:
                continue
            try:
                ret = self.usgs.imagery_row_path(
                    ls_type, row, path, start_date, end_date
                )
            except Exception as e:
                self.debug(f"Failed to get imagery for landsat type {ls_type}")
                self.debug(e)
                ret = None
            # sat-search fallback doesn't work, but it shouldn't be needed either.
            if not ret:
                ret = self.satsearch.imagery_row_path(
                    ls_type, row, path, start_date, end_date
                )
            if ret:
                products += ret
        return self.filter_products(products)

    def filter_products(self, products):
        ret = []
        for product in products:
            if (
                product["Collection Category"] == "T1"
                or product["Collection Category"] == "RT"
            ):
                ret.append(product)
        return ret

    def get_metadata(self, scene_id):
        metadata = False
        if not self._force_usgs:
            metadata = self.satsearch.get_metadata(scene_id)
        if not metadata:
            metadata = self.usgs.get_metadata(scene_id)
        return metadata

    def get_band_path(self, scene_id, band="BQA"):
        # the displayId contains the download entry that sat-search uses
        return os.path.join(
            self._imagery_cache_dir, "{0}_{1}.TIF".format(scene_id["displayId"], band)
        )

    def get_band(self, scene, band):
        ret = False

        if not self._force_usgs:
            try:
                ret = self.satsearch.get_band(scene, band)
            except Exception as e:
                logging.getLogger("er2").error(e)
                raise
                # fallback to usgs
        if not ret:
            ret = self.usgs.get_band(scene, band)
        return ret

    def download_scene(self, scene_id):
        ret = None
        if not self._force_usgs:
            ret = self.satsearch.download_scene(scene_id)
        if not ret or not ret[0]:
            ret = self.usgs.download_scene(scene_id)
        return ret

    def copy_to_workspace(self, scene_id, wdir="."):
        test_path = os.path.join(self._imagery_cache_dir, scene_id[:-5] + "_B1.TIF")
        if not os.path.exists(test_path):
            print("Downloading...")
            self.download_scene(scene_id)
        else:
            print("Copying imagery from cache.")
        imagedir = os.path.join(wdir, "image")
        if not os.path.exists(imagedir):
            os.makedirs(imagedir)
        delete_files_w_pattern(imagedir, "*.*")
        bands = (
            ["MTL", "BQA", "QA_PIXEL", "QA_RADSAT"]
            + ["B{0}".format(i) for i in range(1, 12)]
            + ["B6_VCID_1", "B6_VCID_2"]
        )
        for band in bands:
            for ext in ["TIF", "txt"]:
                band_path = os.path.join(
                    self._imagery_cache_dir,
                    scene_id[:-5] + "_{0}.{1}".format(band, ext),
                )
                if os.path.exists(band_path):
                    shutil.copy(band_path, imagedir)
        bands = ["B{0}".format(i) for i in range(1, 12)] + ["B6_VCID_1", "B6_VCID_2"]
        gap_dir = os.path.join(imagedir, "gap_mask")
        if not os.path.exists(gap_dir):
            os.makedirs(gap_dir)
        delete_files_w_pattern(gap_dir, "*.*")
        for band in bands:
            band_path = os.path.join(
                self._imagery_cache_dir,
                "gap_mask",
                scene_id[:-5] + "_{0}.TIF.gz".format(band),
            )
            if os.path.exists(band_path):
                shutil.copy(band_path, gap_dir)
                gz_files = glob.glob(gap_dir + "/*.gz")
                for gz_file in gz_files:
                    with gzip.open(gz_file, "rb") as gz:
                        with open(os.path.splitext(gz_file)[0], "wb") as gz_out:
                            shutil.copyfileobj(gz, gz_out)
                    os.unlink(gz_file)
                ret = glob.glob(os.path.join(imagedir, "*.TIF"))
            else:
                # New LS7 format that does not have a .gz for the gap_mask files
                band_path = os.path.join(
                    self._imagery_cache_dir,
                    "gap_mask",
                    scene_id[:-5] + "_{0}.TIF".format(band),
                )
                if os.path.exists(band_path):
                    shutil.copy(band_path, gap_dir)
                #                    gz_files = glob.glob(gap_dir + "/*")
                #                for gz_file in gz_files:
                #                    with gzip.open(gz_file, "rb") as gz:
                #                        with open(os.path.splitext(gz_file)[0], "wb") as gz_out:
                #                            shutil.copyfileobj(gz, gz_out)
                #                    os.unlink(gz_file)
                ret = glob.glob(os.path.join(imagedir, "*.TIF"))
        return ret
