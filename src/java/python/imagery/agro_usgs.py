import glob
import json
import logging
import os
import re
import requests
import shutil
import tarfile
import tempfile
import time
import usgs_api
from utils import date_fmt, Utils
from gdal_utils import gdalinfo


class UsgsBackend(Utils):
    types = {
        "LS9": ["landsat_ot_c2_l1", "EE"],
        "LS8": ["landsat_ot_c2_l1", "EE"],
        "LS7": ["landsat_etm_c2_l1", "EE"],
        "LS5": ["landsat_tm_c2_l1", "EE"],
        #        "LS8": ["LANDSAT_8_C1", "EE"],
        #        "LS7": ["LANDSAT_ETM_C1", "EE"],
        #        "LS5": ["LANDSAT_TM_C1", "EE"],
    }

    def __init__(self, imagery_cache_dir="/reset/", log_file=None):
        self._api_key = None
        self._imagery_cache_dir = imagery_cache_dir
        self._log_file = log_file

    def debug(self, msg):
        if self._log_file:
            self._log_file.write(msg)
            self._log_file.write("\n")
        logging.getLogger().info(msg)
        print(msg)

    @property
    def api_key(self):
        if not self._api_key or not self._api_key["data"]:
            start = time.process_time()
            self._api_key = usgs_api.login("idsgroup", "Bazezo99#")
            logging.getLogger("er2").debug(
                "Login took {0} seconds.".format(time.process_time() - start)
            )
        return self._api_key["data"]

    @staticmethod
    def return_imagery(search):
        items = search["data"]["results"]
        ret = []
        for d in items:
            # Add scene band info
            d["id"] = d["entityId"]
            d["datetime"] = d["publishDate"]
            d["thumbnail"] = {
                # LandsatLook Natural Color Preview Image
                "href": d["browse"][0]["browsePath"],
            }
            # load metadata
            metadata = d["metadata"]
            for item in metadata:
                k, v = item["fieldName"], item["value"]
                d[k] = v

            d["eo:cloud_cover"] = float(d.get("cloudCover", -1))
            if d["eo:cloud_cover"] > -1:
                ret.append(d)
        return ret

    def imagery(self, ls_type, centroid, start_date, end_date):
        dataset, node = self.types[ls_type]
        ret = []
        if self.api_key:
            search = usgs_api.search(
                dataset=dataset,
                node=node,
                lng=centroid[0],
                lat=centroid[1],
                start_date=start_date.strftime(date_fmt),
                end_date=end_date.strftime(date_fmt),
                api_key=self.api_key,
            )
            ret = self.return_imagery(search)
        return ret

    def ll_from_row_path(self, s_row, s_path, api_key):
        payload = {
            "row": s_row,
            "path": s_path,
            "gridType": "WRS2",
            "responseShape": "point",
        }
        grid2ll_out = usgs_api.submit("grid2ll", payload, api_key)
        lat = grid2ll_out["data"]["coordinates"][0]["latitude"]
        long = grid2ll_out["data"]["coordinates"][0]["longitude"]
        return (lat, long)

    def imagery_row_path(self, ls_type, row, path, start_date, end_date):
        dataset, node = self.types[ls_type]
        lat, long = self.ll_from_row_path(row, path, api_key=self.api_key)
        # http://kapadia.github.io/usgs/reference/api.html
        search = usgs_api.search(
            dataset,
            node,
            start_date=start_date.strftime(date_fmt),
            end_date=end_date.strftime(date_fmt),
            lat=lat,
            lng=long,
            api_key=self.api_key,
        )
        return self.return_imagery(search)

    def get_metadata(self, scene_id):
        metadata = {}
        # remove the LGN00
        file_path = os.path.join(self._imagery_cache_dir, scene_id[:-5] + "_MTL.txt")
        with open(file_path) as fp:
            for r in fp:
                if "=" in r:
                    k, v = [t.strip() for t in r.split("=")]
                    metadata[k] = v
        return metadata

    def find_tar_file(self, basefile):
        basedir, tarfile = os.path.split(basefile)
        itemList = os.listdir(basedir)
        for item in itemList:
            if tarfile in item:
                return os.path.join(basedir, item)
        raise Exception(basefile + " which is basefile not found in find_tar_file")

    def download_scene(self, scene):
        """Download to cache"""
        scene_id = scene
        # remove the LGN00
        test_path = os.path.join(self._imagery_cache_dir, scene_id[:-5] + "_B1.TIF")
        ret = []

        # Check cache
        if "LE9" in scene_id or "LC9" in scene_id:
            ls_type = "LS9"
        elif "LE8" in scene_id or "LC8" in scene_id:
            ls_type = "LS8"
        elif "LE7" in scene_id:
            ls_type = "LS7"
        elif "LT5" in scene_id:
            ls_type = "LS5"
        else:
            ls_type = None
        if ls_type:
            logging.getLogger().info(f"Checking path {test_path}")
            dataset, node = self.types[ls_type]
            if not os.path.exists(test_path):
                label = f"agroet_{scene_id}"
                ret = usgs_api.download(
                    dataset, node, entityIds=[scene_id], api_key=self.api_key
                )

                logging.getLogger().info(ret)

                if not ret["errorMessage"]:
                    path = os.path.join(self._imagery_cache_dir, scene_id)

                    for result in ret["data"]["availableDownloads"]:
                        self.debug(f"Downloading from url {result['url']}\n")
                        # There should only be one download
                        scene_tar_path = self.downloadFile(result["url"], path)
                        # seems that the file is being downloaded twice, here and in availableDownloads
                        count = 0
                        while not scene_tar_path and count < 5:
                            scene_tar_path = self.downloadFile(result["url"], path)
                        if not scene_tar_path:
                            raise Exception(f"Download failed for {scene_id}")

                    # should already be done
                    if False:
                        # LAG 9/13/22                    url = ret["data"]["preparingDownloads"][0]["url"]
                        # LAG 9/13/22                    basename = os.path.basename(url)
                        # basename = path
                        # checking if the url is for preparingDownloads or is already available
                        if ret["data"]["preparingDownloads"]:
                            url = ret["data"]["preparingDownloads"][0]["url"]
                        elif ret["data"]["availableDownloads"]:
                            url = ret["data"]["availableDownloads"][0]["url"]
                        else:
                            self.debug(f"ret is{json.dumps(ret['data'])}")
                            exit(7)
                        basename = os.path.basename(url)
                        s = basename.find("?")
                        print(ret)
                        scene_tar_path = os.path.join(
                            path, "download.tar"
                        )  # os.path.join(path, basename[:s])

                        with requests.get(url, stream=True) as r:
                            r.raise_for_status()
                            if not os.path.exists(path):
                                os.makedirs(path)
                            with open(scene_tar_path, "wb") as f:
                                for chunk in r.iter_content(chunk_size=81920):
                                    f.write(chunk)
                        self.debug(
                            f"Done writing request from {url} to {scene_tar_path}"
                        )
                    # scene_tar_path = self.find_tar_file(path)
                    #                    import pdb; pdb.set_trace()
                    # This is a variable to determine if the dataset is a Collection 2 Level 1 and does NOT have a gap_mask directory
                    GM_C2L1 = -1
                    targetgappath = os.path.join(self._imagery_cache_dir, "gap_mask")
                    if not os.path.exists(targetgappath):
                        os.makedirs(targetgappath)
                    with tarfile.open(scene_tar_path) as tf:
                        # Extract and rename to convention used by sat-search
                        tdir = tempfile.mkdtemp()
                        tf.extractall(tdir)
                        for name in glob.glob(tdir + "/*"):
                            if name.rfind("_GM_B") > 0:
                                GM_C2L1 = 1
                                self.copy_to_cache_gap(targetgappath, name, scene_id)
                            elif not os.path.isdir(name):
                                self.copy_to_cache(
                                    self._imagery_cache_dir, name, scene_id
                                )
                        self.debug("Done extracting imagery")

                        # If it DOES include gap_mask directory
                        if GM_C2L1 == -1:
                            tdir_gap = os.path.join(tdir, "gap_mask")
                            if os.path.exists(tdir_gap):
                                targetpath = os.path.join(
                                    self._imagery_cache_dir, "gap_mask"
                                )
                                if not os.path.exists(targetpath):
                                    os.makedirs(targetpath)
                                # If it DOES NOT includes gap_mask directory
                                for name in glob.glob(tdir_gap + "/*"):
                                    try:
                                        self.debug(f"Copying {name} to {targetpath}")
                                        self.copy_to_cache(targetpath, name, scene_id)
                                    except:
                                        # already exists
                                        raise
                            self.debug(f"Done copying {tdir_gap}")
                        shutil.rmtree(tdir)
                        self.debug(f"Done deleting {tdir}")
                    self.debug("Attempting to move on")

                    # Don't seem to need this anymore since the download exists.
                    # scene_downloads = usgs_api.download_prepared(
                    #     ret["data"]["preparingDownloads"],
                    #     label=label,
                    #     api_key=self.api_key,
                    # )
                    # self.debug(f"scene_downloads = ")
                    # self.debug(scene_downloads)
                    # for url in scene_downloads:
                    #     print(f"Scene download url: {url}\n")
                    #     downloadFile(url, path)

                    # url = ret["data"][0]["url"]
                    # # This is a tar file
                    # basename = os.path.basename(url)
                    # s = basename.find("?")
                    # scene_tar_path = os.path.join(path, basename[:s])
                    # if not os.path.exists(scene_tar_path):
                    #     with requests.get(url, stream=True) as r:
                    #         r.raise_for_status()
                    #         if not os.path.exists(path):
                    #             os.makedirs(path)
                    #         with open(scene_tar_path, "wb") as f:
                    #             for chunk in r.iter_content(chunk_size=8192):
                    #                 if chunk:  # filter out keep-alive new chunks
                    #                     f.write(chunk)
                    # with tarfile.open(scene_tar_path) as tf:
                    #     # Extract and rename to convention used by sat-search
                    #     tdir = tempfile.mkdtemp("ee")
                    #     tf.extractall(tdir)
                    #     for name in glob.glob(tdir + "/*"):
                    #         if not os.path.isdir(name):
                    #             self.copy_to_cache(
                    #                 self._imagery_cache_dir, name, scene_id
                    #             )
                    #     tdir_gap = os.path.join(tdir, "gap_mask")
                    #     if os.path.exists(tdir_gap):
                    #         targetpath = os.path.join(
                    #             self._imagery_cache_dir, "gap_mask"
                    #         )
                    #         if not os.path.exists(targetpath):
                    #             os.makedirs(targetpath)
                    #         for name in glob.glob(tdir_gap + "/*"):
                    #             try:
                    #                 self.copy_to_cache(targetpath, name, scene_id)
                    #             except:
                    #                 # already exists
                    #                 raise
                    #     shutil.rmtree(tdir)
                else:
                    raise Exception(ret["data"]["error"])
            self.debug("USGS download complete")
        return ret

    @staticmethod
    def copy_to_cache(tdir, name, scene_id):
        # copy original to new file
        iband = name.rfind("_B")
        if iband < 0:
            iband = name.rfind("_QA_PIXEL")
            if iband < 0:
                # try MTL
                iband = name.rfind("_MTL")
        band = name[iband:]
        # strip off the trailing 'LGN00'
        targetpath = os.path.join(tdir, scene_id[:-5] + band)
        shutil.move(name, targetpath)

    @staticmethod
    def copy_to_cache_gap(tdir, name, scene_id):
        # copy original to new file
        iband = name.rfind("_B")
        band = name[iband:]
        # strip off the trailing 'LGN00'
        targetpath = os.path.join(tdir, scene_id[:-5] + band)
        shutil.move(name, targetpath)

    def get_band(self, scene, band):
        self.download_scene(scene)
        band_path = self.get_band_path(band, scene)
        band = gdalinfo(band_path)
        return band

    def downloadFile(self, url, path):
        try:
            with requests.get(url, stream=True) as response:
                disposition = response.headers["content-disposition"]
                filename = re.findall("filename=(.+)", disposition)[0].strip('"')
                filepath = path + filename
                self.debug(f"Downloading {filename} ...\n")
                with open(filepath, "wb") as f:
                    for chunk in response.iter_content(chunk_size=512 * 1024):
                        if chunk:  # filter out keep-alive new chunks
                            f.write(chunk)
            logging.getLogger().info(f"Downloaded {filename} to {filepath}")
            return filepath

        except Exception as e:
            print(f"Failed to download from {url}:", e)
            return None
