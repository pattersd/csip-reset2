import datetime
import geopandas
import logging
import os
import sys
from imagery.main import Imagery

# SATUTILS_API_URL=https://hb57pr2zof.execute-api.us-east-1.amazonaws.com/stage/


def run_test(logger, centroid, start_date, end_date):
    imagery = Imagery(imagery_cache_dir=os.getcwd())
    prods = imagery.get_products(centroid, start_date, end_date)
    scene_id = prods[0]["id"]
    if not os.path.exists(scene_id):
        os.makedirs(scene_id)
    b1 = imagery.get_band(scene_id, "B1")
    b1_path = imagery.get_band_path(scene_id, "B1")
    print(b1_path)


if __name__ == "__main__":
    logging.basicConfig(level="INFO")
    if len(sys.argv) == 1:
        print("Arguments: boundary layer")
        sys.exit()
    aoi_path = sys.argv[1]

    aoi_df = geopandas.read_file(aoi_path)
    aoi_4326 = aoi_df.to_crs("epsg:4326")
    aoi_shape = aoi_df.unary_union
    aoi_centroid = [aoi_shape.centroid.x, aoi_shape.centroid.y]

    date_fmt = "%Y-%m-%d"
    start = datetime.datetime.strptime("2019-06-13", date_fmt)
    end = datetime.datetime.strptime("2019-07-13", date_fmt)
    run_test(logging.getLogger(), aoi_centroid, start, end)

    # Check results
