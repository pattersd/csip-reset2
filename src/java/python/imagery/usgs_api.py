"""Faster implmentation of usgs API using pycurl"""
from io import BytesIO
import json
import logging
import math
import requests
import pycurl
import time
from urllib.parse import urlencode
import xmltodict

url = "https://m2m.cr.usgs.gov/api/api/json/stable/"


def great_circle_dist(lat, lng, dist):
    lat = math.radians(lat)
    lng = math.radians(lng)
    brng = math.radians(45.0)
    ibrng = math.radians(225.0)

    earth_radius = 6371000.0
    dR = (dist / 2.0) / earth_radius

    lat1 = math.asin(
        math.sin(lat) * math.cos(dR) + math.cos(lat) * math.sin(dR) * math.cos(brng)
    )
    lng1 = lng + math.atan2(
        math.sin(brng) * math.sin(dR) * math.cos(lat),
        math.cos(dR) - math.sin(lat) * math.sin(lat1),
    )

    lat2 = math.asin(
        math.sin(lat) * math.cos(dR) + math.cos(lat) * math.sin(dR) * math.cos(ibrng)
    )
    lng2 = lng + math.atan2(
        math.sin(ibrng) * math.sin(dR) * math.cos(lat),
        math.cos(dR) - math.sin(lat) * math.sin(lat2),
    )

    return [math.degrees(lat1), math.degrees(lat2)], [
        math.degrees(lng1),
        math.degrees(lng2),
    ]


def submit_pycurl(method, data):
    c = pycurl.Curl()
    c.setopt(c.URL, url + method)

    payload = {
        "jsonRequest": json.dumps(data),
    }
    postfields = urlencode(payload)

    start_time = time.time()
    buffer = BytesIO()
    c.setopt(c.POSTFIELDS, postfields)
    c.setopt(c.WRITEDATA, buffer)
    c.perform()
    c.close()
    body = buffer.getvalue()
    ret = json.loads(body)
#    print(json.loads(body))
#    print("--- USGS: %s seconds ---" % (time.time() - start_time))
    if ret["errorMessage"]:
        raise UsgsApiException(ret["errorMessage"])
    return ret


def submit_requests(method, data, api_key=None):
    headers = {}
    if api_key:
        headers = {"x-auth-token": api_key}
#    print("data in submit_request in USGS_API",data)
#    print("url + method in submit_request in USGS_API",url+method, json.dumps(data),headers)
    req = requests.post(url + method, data=json.dumps(data), headers=headers)
    if req.ok:
        ret = req.json()
        return ret
    raise UsgsApiException(req.text)


submit = submit_requests


def login(username, password):
    payload = {
        "username": username,
        "password": password,
        "catalogId": "EE",
    }
    return submit("login", payload)


def search(
    dataset,
    node,
    lat=None,
    lng=None,
    distance=100,
    ll=None,
    ur=None,
    start_date=None,
    end_date=None,
    where=None,
    max_results=50000,
    starting_number=1,
    sort_order="DESC",
    extended=False,
    api_key=None,
):
    sceneFilter = {}
    payload = {
        "datasetName": dataset,
        "node": node,
        "metadataType": "full",
        "sceneFilter": sceneFilter,
    }

    if lat and lng:
        lats, lngs = great_circle_dist(lat, lng, distance / 2.0)

        ll = {"longitude": min(*lngs), "latitude": min(*lats)}
        ur = {"longitude": max(*lngs), "latitude": max(*lats)}

    if ll and ur:
        sceneFilter["spatialFilter"] = {
            "filterType": "mbr",
            "lowerLeft": ll,
            "upperRight": ur,
        }

    if start_date or end_date:
        sceneFilter["acquisitionFilter"] = {"dateField": "search_date"}

        if start_date:
            sceneFilter["acquisitionFilter"]["start"] = start_date
        if end_date:
            sceneFilter["acquisitionFilter"]["end"] = end_date

    if where:
        # TODO: Support more than AND key/value equality queries
        # usgs search --node EE LANDSAT_8_C1 --start-date 20170410 --end-date 20170411 --where wrs-row 032 | jq ""
        # LC81810322017101LGN00
        payload["additionalCriteria"] = {
            "filterType": "and",
            "childFilters": [
                {
                    "filterType": "value",
                    "fieldId": field_id,
                    "value": value,
                    "operand": "=",
                }
                for field_id, value in iter(where.items())
            ],
        }

    if max_results:
        payload["maxResults"] = max_results

    if starting_number:
        payload["startingNumber"] = starting_number

    if sort_order:
        payload["sortOrder"] = sort_order

    return submit("scene-search", payload, api_key=api_key)


def download(dataset, node, entityIds, api_key=None):
    payload = {"datasetName": dataset, "entityIds": entityIds}

    # download options
    ret = submit("download-options", payload, api_key)
    downloadOptions = ret["data"]

    downloads = []
#    import pdb;pdb.set_trace()
    for product in downloadOptions:
        # Make sure the product is available for this scene
        if (
#            product["available"] == True
            product["bulkAvailable"] == True
            and ("LC9" in entityIds[0] or "LC8" in entityIds[0] or "LE7" in entityIds[0] or
            "LT5" in entityIds[0]) and product["productName"] == "Landsat Collection 2 Level-1 Product Bundle"
#            and ("LC9" in entityIds[0] or "LC8" in entityIds[0] or "LE7" in entityIds[0] or
#            "LT5" in entityIds[0] or product["productName"] == "Level-1 GeoTIFF Data Product")
        ):
            downloads.append(
                {"entityId": product["entityId"], "productId": product["id"]}
            )
            # Did we find products?
            label = "download-agroet"
            payload = {"downloads": downloads, "label": label}
#            print(f"Executing download with payload: {payload}")
            return submit("download-request", payload, api_key)


def download_prepared(preparingDownloads, label, api_key):
    preparingDownloadCount = len(preparingDownloads)
    preparingDownloadIds = []
    downloads = []
    if preparingDownloadCount > 0:
        for result in preparingDownloads:
            preparingDownloadIds.append(result["downloadId"])

        payload = {"label": label}
        # Retrieve download urls
#        print("Retrieving download urls...\n")
        ret = submit("download-retrieve", payload, api_key)
        results = ret["data"]
        if results != False:
            for result in results["available"]:
                if result["downloadId"] in preparingDownloadIds:
                    preparingDownloadIds.remove(result["downloadId"])
#                    print(f"Get download url: {result['url']}\n")
                    downloads.append(result["url"])

            for result in results["requested"]:
                if result["downloadId"] in preparingDownloadIds:
                    preparingDownloadIds.remove(result["downloadId"])
#                    print(f"Get download url: {result['url']}\n")
                    downloads.append(result["url"])

        # Don't get all download urls, retrieve again after 30 seconds
        while len(preparingDownloadIds) > 0:
#            print(
#                f"{len(preparingDownloadIds)} downloads are not available yet. Waiting for 30s to retrieve again\n"
#            )
            time.sleep(30)
            ret = submit("download-retrieve", payload, api_key)
            results = ret["data"]
            if results != False:
                for result in results["available"]:
                    if result["downloadId"] in preparingDownloadIds:
                        preparingDownloadIds.remove(result["downloadId"])
#                        print(f"Get download url: {result['url']}\n")
                        downloads.append(result["url"])
    return downloads


def dataset_fields(dataset, node, **kwargs):
    payload = {
        "datasetName": dataset,
        "node": node,
    }
    return submit("datasetfields", payload, kwargs["api_key"])


def get_metadata(url):
    c = pycurl.Curl()
    c.setopt(c.URL, url)

    buffer = BytesIO()
    c.setopt(c.WRITEDATA, buffer)
    try:
        c.perform()
    except pycurl.error as e:
        logging.getLogger("er2").error(e)
        c.close()
        raise UsgsApiException(e)
    body = buffer.getvalue()
    metadata = xmltodict.parse(body)
    return metadata


class UsgsApiException(Exception):
    pass
