import datetime
import geopandas
import json
import logging
import os
import pytz
import requests
from shapely.geometry import shape


class Colorado(object):
    def __init__(self):
        self.local_timezone = pytz.timezone("US/Mountain")

    def get_stations(self, aoi_ll):
        data_dir = "/app/er2/apps/er2_reset/data"
        if not os.path.exists(data_dir):
            data_dir = "/tmp/csip/python"
        if not os.path.exists(data_dir):
            # local dev dir
            data_dir = "/home/dave/work/new/reset/er2_reset2/data"
        climate_layer = geopandas.read_file(os.path.join(data_dir, "climate_co.json"))
        station_rows = climate_layer.__geo_interface__["features"]
        # Remove stations that are not in the buffered AOI
        stations_filtered = []
        for station_row in station_rows:
            geom = shape(station_row["geometry"])
            if aoi_ll.contains(geom):
                # normalize
                station_row["properties"]["name"] = station_row["properties"][
                    "Station Name"
                ]
                station_row["properties"]["geometry"] = station_row["geometry"]
                stations_filtered.append(station_row["properties"])
        return stations_filtered

    def load_climate(self, aoi_ll, hourly_date):
        stations_filtered = self.get_stations(aoi_ll)

        # Get station ids. Format is ncwcd:xxx
        station_ids = [
            s["ID"] for s in stations_filtered if s["Station Type"] == "ncwcd"
        ]
        ncwcd_data_list = self.download_climate(
            "ncwcd",
            station_ids,
            hourly_date,
            "hourly",
            ["ETr_Alfalfa_Tot", "TemperatureAir_Ave"],
        )
        station_ids = [
            s["ID"] for s in stations_filtered if s["Station Type"] == "Coagmet"
        ]
        coagmet_data_list = self.download_climate(
            "coagmet", station_ids, hourly_date, "hourly", ["etr", "tmean"]
        )

        # Interpolate between hourly station data
        self.add_hourly_data_to_stations(
            coagmet_data_list, hourly_date, ncwcd_data_list, stations_filtered
        )
        # Daily values
        station_ids = [
            s["ID"] for s in stations_filtered if s["Station Type"] == "ncwcd"
        ]
        ncwcd_data_list = self.download_climate(
            "ncwcd",
            station_ids,
            hourly_date,
            "daily",
            ["ETr_Alfalfa_Tot", "WindTravel_Tot"],
        )
        station_ids = [
            s["ID"] for s in stations_filtered if s["Station Type"] == "Coagmet"
        ]
        coagmet_data_list = self.download_climate(
            "coagmet", station_ids, hourly_date, "daily", ["etr", "wrun"]
        )
        self.add_daily_data_to_stations(
            coagmet_data_list, ncwcd_data_list, stations_filtered
        )
        return stations_filtered

    def load_climate_range(self, aoi_ll, start_date, end_date):
        stations_filtered = self.get_stations(aoi_ll)
        # Get station ids. Format is ncwcd:xxx
        station_ids_ncwcd = [
            s["ID"] for s in stations_filtered if s["Station Type"] == "ncwcd"
        ]
        station_ids_coagmet = [
            s["ID"] for s in stations_filtered if s["Station Type"] == "Coagmet"
        ]

        # Daily values
        ndays = (start_date, end_date).days + 1
        for day in range(ndays):
            dt = start_date + datetime.timedelta(days=day)
            ncwcd_data_list = self.download_climate(
                "ncwcd",
                station_ids_ncwcd,
                dt,
                "daily",
                ["ETr_Alfalfa_Tot", "WindTravel_Tot"],
            )
            coagmet_data_list = self.download_climate(
                "coagmet", station_ids_coagmet, dt, "daily", ["etr", "wrun"]
            )

        self.add_daily_data_to_stations(
            coagmet_data_list, ncwcd_data_list, stations_filtered
        )
        return stations_filtered

    def add_hourly_data_to_stations(
        self, coagmet_data_list, extraction_date, ncwcd_data_list, stations_filtered
    ):
        for ctype, data_list in [
            ["ncwcd", ncwcd_data_list],
            ["Coagmet", coagmet_data_list],
        ]:
            for climate_data in data_list:
                station_name = climate_data["station"]
                hourly_rows = climate_data["data"]

                for i in range(1, len(hourly_rows)):
                    station_date_str, etr1, tave1 = hourly_rows[i]
                    station_date = datetime.datetime.strptime(
                        station_date_str, "%Y-%m-%d %H:%M:%S"
                    )
                    mountain = pytz.timezone("US/Mountain")
                    station_date = mountain.localize(station_date)
                    # Comvert to UTC.
                    station_date = station_date.astimezone(pytz.utc)

                    if station_date > extraction_date:
                        # Calculate linear average
                        prev_station_date = datetime.datetime.strptime(
                            hourly_rows[i - 1][0], "%Y-%m-%d %H:%M:%S"
                        )
                        prev_station_date = mountain.localize(prev_station_date)
                        # Convert to minutes
                        timediff_min = (
                            extraction_date - prev_station_date
                        ).seconds / 60.0
                        l = timediff_min / 60.0

                        etr2 = hourly_rows[i - 1][1]
                        tave2 = hourly_rows[i - 1][2]
                        if etr1 is not None and etr2 is not None:
                            etrdiff = etr2 - etr1
                            # Divide by 60 minutes to get the fraction to interpolate by.
                            etr = etr1 + etrdiff * l
                            tempdiff = tave2 - tave1
                            tave = tave1 + tempdiff * l
                        else:
                            etr = None
                            tave = None
                        # Find this station's geometry
                        station = next(
                            filter(
                                lambda x: x["ID"] == station_name
                                and x["Station Type"] == ctype,
                                stations_filtered,
                            )
                        )
                        station.update(
                            {
                                "etr_hourly_mm": etr,
                                "tave_hourly_c": tave,
                            }
                        )
                        # Found the record that corresponds to the date, so quit searching
                        break

    def add_daily_data_to_stations(
        self, coagmet_data_list, ncwcd_data_list, stations_filtered
    ):
        for ctype, data_list in [
            ["ncwcd", ncwcd_data_list],
            ["Coagmet", coagmet_data_list],
        ]:
            for climate_data in data_list:
                station_name = climate_data["station"]
                station = next(
                    filter(
                        lambda x: x["ID"] == station_name
                        and x["Station Type"] == ctype,
                        stations_filtered,
                    )
                )
                daily_rows = climate_data["data"]

                for i in range(1, len(daily_rows)):
                    station_date_str, etr, wrun = daily_rows[i]
                    # windrun is in m/s. Convert to miles/day
                    if wrun:
                        wrun = wrun * 24 * 60 * 60 / 1000 * 0.621371
                    station["etr_mm"] = etr
                    station["wrun_mile_day"] = wrun

    @staticmethod
    def download_climate(which, station_list, extract_date, timestep, climate_data):
        """
        CSIP server call.
        arguments:
        """
        start_date = extract_date.strftime("%Y-%m-%d")
        # Since daily is broken, I am just going to sum hourly values
        request_timestep = timestep
        if which == "ncwcd":
            request_timestep = "hourly"
        end_date = (extract_date + datetime.timedelta(days=1)).strftime("%Y-%m-%d")

        url = "http://csip.engr.colostate.edu:8083/csip-climate/m/{0}/1.0".format(
            which.lower()
        )
        data = {
            "parameter": [
                {
                    "name": "station_list",
                    "value": station_list,
                },
                {
                    "name": "climate_data",
                    "value": climate_data,
                },
                {"name": "units", "value": "metric"},
                {
                    "name": "timestep",
                    "value": request_timestep,
                },
                {"name": "start_date", "value": start_date},
                {"name": "end_date", "value": end_date},
            ]
        }

        req = requests.post(
            url,
            data=json.dumps(data),
            headers={"Content-Type": "application/json", "Accept": "application/json"},
        )
        resp = json.loads(req.text)
        ret = []
        if "error" in resp["metainfo"]:
            logging.getLogger("er2").error(resp["metainfo"]["error"])
        elif "result" in resp:
            for o in resp["result"]:
                # Getting a list with a single empty dictionary because NCWCD database is down.
                if o["name"] == "output" and o["value"] and o["value"][0]:
                    ret = o["value"]

        if timestep == "daily" and which == "ncwcd":
            daily_ret = []
            for station_data in ret:
                data = station_data["data"]
                vals = [0] * (len(data[0]) - 1)
                cnt = 0
                for hourly_val in data[1:]:
                    dt = hourly_val[0]
                    if start_date in dt:
                        cnt += 1
                        # always sum
                        for i, val in enumerate(hourly_val[1:]):
                            if val:
                                vals[i] += val
                # Average wind
                if cnt > 0:
                    vals[1] /= cnt
                daily_ret.append(
                    {
                        "station": station_data["station"],
                        "data": [ret[0], [start_date] + vals],
                    }
                )
            ret = daily_ret
        return ret
