import datetime
import geopandas
import json
import logging
import os
import pytz
import requests
#AE
from FAWN_1hr_PET_v1 import ETh

from shapely.geometry import shape

def write_log(pf, text):
    pf.write(text)
    pf.write("\n")
    pf.flush()

class FAWN(object):
    def __init__(self, logger=None, ignore_cache=False):
        self.logger = logger or logging.getLogger()
        self.local_timezone = pytz.timezone('US/Eastern')

#LG        pf = open("progress2.log", "w+") #LAGT
#LG        write_log(pf, "Inside of Azmet") 

    def load_climate(self, aoi, hourly_date):
        pf = open("progress4.log", "w") #LAGT
        write_log(pf, "Inside load climate in FAWN")
        pf.close()
        pf = open("progress5.log", "w") #LAGT
        write_log(pf, "hourly_date is ") 
        write_log(pf, str(hourly_date)) 

        data_dir = '/app/er2/apps/er2_reset/data'
        if not os.path.exists(data_dir):
            data_dir = '/tmp/csip/python'
        if not os.path.exists(data_dir):
            data_dir = '/home/dave/work/reset/er2_web/app/er2/apps/er2_reset/data'
        climate_layer = geopandas.read_file(os.path.join(data_dir, 'FAWN.json'))
        write_log(pf, "After geopandas read the json file") 
        station_rows = climate_layer.__geo_interface__['features']
        write_log(pf, "Number of station rows") 

        stations = []
        for station_row in station_rows:
            write_log(pf, "Inside AOI station loop") 
            geom = shape(station_row['geometry'])
            if aoi.contains(geom):
                write_log(pf, "Inside AOI that contains station") 
                # normalize
                station_row['properties']['name'] = station_row['properties']['Station Name']
                write_log(pf, "Station Name") 
                write_log(pf, "Before Station Geometry") 

                station_row['properties']['geometry'] = station_row['geometry']
                write_log(pf, "After Station Geometry") 
                stations.append(station_row['properties'])

        write_log(pf, "After finding the station before extraction") 
        for station in stations:
            write_log(pf, "Inside download climate loop") 
            eth_avg, eth_day, wrun, climate_data = self.download_climate(station['Site'], 'hourly', hourly_date)
            write_log(pf, "After climate_data is read") 
            etr = None
            if climate_data:
                write_log(pf, "INSIDE climate_data in hourly") 
                station_date, etr1 = climate_data[0]
                write_log(pf, "station_date") 
                write_log(pf, str(station_date)) 
                write_log(pf, "etr1") 
                write_log(pf, str(etr1)) 
                station_date, etr2 = climate_data[1]
                write_log(pf, "station_date") 
                write_log(pf, str(station_date)) 
                write_log(pf, "etr2") 
                write_log(pf, str(etr2)) 

                # Calculate linear average
                timediff_minutes = (hourly_date - station_date).seconds / 60.0

                if etr1 is not None and etr2 is not None:
                    # Divide by 60 minutes to get the fraction to interpolate by.
                    frac = timediff_minutes / 60.0
                    etrdiff = (etr2 - etr1)
                    etr = etr1 + etrdiff * frac

            # Find this station's geometry
            station.update({
                    'etr_hourly_mm': etr,
                    'etr_mm': eth_day,
                    'wrun_mile_day': wrun
            })

        # Daily values
#        for station in stations:
#            climate_data = self.download_climate(station['Site'], 'daily', #hourly_date)
#            etr, wrun = None, None
#            if climate_data:
#                station_date, wrun, etr = climate_data[0]
#                # windrun is in m/s. Convert to miles/day
#                if wrun:
#                    wrun = wrun * 24 * 60 * 60 / 1000 * 0.621371
#            station['etr_mm'] = etr
#            station['wrun_mile_day'] = wrun
        return stations

    @staticmethod
    def download_climate(climate_id, timestep, extraction_date):
        pf = open("progress2.log", "w") #LAGT
        write_log(pf, "Inside of florida download_climate") 
        write_log(pf, str(extraction_date)) 

        ret = []
        if timestep == 'hourly':
            url = 'https://fawn.ifas.ufl.edu/controller.php/today/obs//{0:02};csv?asText=1'.format(climate_id)
            write_log(pf, url) 
        else:
            url = 'https://fawn.ifas.ufl.edu/controller.php/today/obs//{0:02};csv?asText=1'.format(climate_id)

            write_log(pf, url) 

        r = requests.get(url)
        lines = r.text.split()
        prev_tokens = None
        prev_date = None
        data_dir = '/tmp/csip/python/climate/'
        file_name = '{0:02}.csv'.format(climate_id)
        file_name1 = data_dir + file_name
        fp1 = open(data_dir + file_name , "w") #LAGT
        for i, l in enumerate(lines):
            write_log(pf, l) 
            write_log(fp1, l) 
        fp1.close()
        write_log(pf, 'BEFORE calling ETh function') 
        FAWN_ET = ETh(file_name1, file_name)
        write_log(pf, 'AFTER calling ETh function') 
        file_name = '{0:02}_PET.csv'.format(climate_id)
        file_name1 = data_dir + file_name
        fp1 = open(data_dir + file_name , "r") #LAGT
        eth_day = 0.0
        ww_day = 0.0
        hours = 0

        year = int(extraction_date.strftime("%Y"))
        doy = int(extraction_date.strftime("%j"))

        for i, l in enumerate(fp1.readlines()[1:]):
            hours += 1
            tokens = l.split(',')
            if len(tokens) < 3:
                print('FAWN: No data for station {0}'.format(climate_id))
                break
            hour,ww,eth = int(tokens[0]), float(tokens[17]), float(tokens[-1])
            eth_day = eth_day + eth
            ww_day = ww_day + ww

            if hour == 10:
                eth10 = eth
            elif hour == 11:
                eth11 = eth

            if timestep == 'hourly':
                # hour 24 is 0 hour the next day.
                if hour == 24:
                    doy += 1
                    hour = 0
                station_date = datetime.datetime(year, 1, 1, hour=hour) + datetime.timedelta(days=doy - 1)
            else:
                station_date = datetime.datetime(year, 1, 1) + datetime.timedelta(days=doy - 1)

            eastern = pytz.timezone('US/Eastern')
            station_date = eastern.localize(station_date)
# Comvert to UTC.
            station_date = station_date.astimezone(pytz.utc)
            write_log(pf, "station date is ") 
            write_log(pf, str(station_date)) 
            write_log(pf, "extraction_date") 
            write_log(pf, str(extraction_date)) 

#
            if station_date > extraction_date:
                if timestep == 'hourly':
                    # Add previous hour and this hour.
#                    etr, tave = float(prev_tokens[15]), float(prev_tokens[3])
                    etr = float(prev_tokens[-1])
                    ret.append([prev_date, etr])
                    etr = float(tokens[-1])
                    ret.append([station_date, etr])
                else:
                    etr, wrun1 = float(prev_tokens[-1]), float(prev_tokens[17])
                    ret.append([station_date, etr])
                break
            prev_date = station_date
            prev_tokens = tokens
#       converting wind run from m/s to miles per day
        wrun = ww_day/hours * 24 * 60 * 60 / 1000 * 0.621371
#        wrun = (ww_day/hours) * 14.9129 
        if eth10 and eth11:
            eth_avg = (eth10 + eth11)/2.0

        write_log(pf, "eth10 is ") 
        write_log(pf, str(eth10)) 
        write_log(pf, "eth11 is ") 
        write_log(pf, str(eth11)) 
        write_log(pf, "etr is ") 
        write_log(pf, str(etr)) 
        write_log(pf, "eth_avg is ") 
        write_log(pf, str(eth_avg)) 
        write_log(pf, "eth_day is ") 
        write_log(pf, str(eth_day)) 
        write_log(pf, "wrun is ") 
        write_log(pf, str(wrun)) 
#        write_log(pf, "wrun2 is ") 
#        write_log(pf, str(wrun2)) 

        pf.close()

        return etr, eth_day, wrun, ret