import datetime
import geopandas
import json
import logging
import os
import pytz
import requests
from shapely.geometry import shape

def write_log(pf, text):
    pf.write(text)
    pf.write("\n")
    pf.flush()

class FAWN(object):
    def __init__(self, logger=None, ignore_cache=False):
        self.climate_data = {}
        self.logger = logger or logging.getLogger()
        self.local_timezone = pytz.timezone('US/Eastern')

#LG        pf = open("progress2.log", "w+") #LAGT
#LG        write_log(pf, "Inside of Azmet") 

    def load_climate(self, aoi, hourly_date):
        pf = open("progress4.log", "w") #LAGT
        write_log(pf, "Inside load climate in FAWN")
        pf.close()
        pf = open("progress5.log", "w") #LAGT


        data_dir = '/app/er2/apps/er2_reset/data'
        if not os.path.exists(data_dir):
            data_dir = '/tmp/csip/python'
        if not os.path.exists(data_dir):
            data_dir = '/home/dave/work/reset/er2_web/app/er2/apps/er2_reset/data'
        climate_layer = geopandas.read_file(os.path.join(data_dir, 'FAWN.json'))
        write_log(pf, "After geopandas read the json file") 
        station_rows = climate_layer.__geo_interface__['features']
        write_log(pf, "Number of station rows") 

        stations = []
        for station_row in station_rows:
            write_log(pf, "Inside AOI station loop") 
            geom = shape(station_row['geometry'])
            if aoi.contains(geom):
                write_log(pf, "Inside AOI that contains station") 
                # normalize
                station_row['properties']['name'] = station_row['properties']['Station Name']
                write_log(pf, "Station Name") 
                write_log(pf, "Before Station Geometry") 

                station_row['properties']['geometry'] = station_row['geometry']
                write_log(pf, "After Station Geometry") 
                stations.append(station_row['properties'])

        write_log(pf, "After finding the station before extraction") 
        for station in stations:
            write_log(pf, "Inside download climate loop") 
            climate_data = self.download_climate(station['Site'], 'hourly', hourly_date)
            etr, tave = None, None
            if climate_data:
                station_date, etr1, tave1 = climate_data[0]
                _, etr2, tave2 = climate_data[1]

                # Calculate linear average
                timediff_minutes = (hourly_date - station_date).seconds / 60.0

                if etr1 is not None and etr2 is not None:
                    # Divide by 60 minutes to get the fraction to interpolate by.
                    frac = timediff_minutes / 60.0
                    etrdiff = (etr2 - etr1)
                    etr = etr1 + etrdiff * frac
                    tempdiff = (tave2 - tave1)
                    tave = tave1 + tempdiff * frac

                # Find this station's geometry
                station.update({
                    'etr_hourly_mm': etr,
                    'tave_hourly_c': tave,
                })

        # Daily values
        for station in stations:
            climate_data = self.download_climate(station['Site'], 'daily', hourly_date)
            etr, wrun = None, None
            if climate_data:
                station_date, wrun, etr = climate_data[0]
                # windrun is in m/s. Convert to miles/day
                if wrun:
                    wrun = wrun * 24 * 60 * 60 / 1000 * 0.621371
            station['etr_mm'] = etr
            station['wrun_mile_day'] = wrun
        return stations

    @staticmethod
    def download_climate(climate_id, timestep, extraction_date):
        pf = open("progress2.log", "w") #LAGT
        write_log(pf, "Inside of florida download_climate") 

        ret = []
        if timestep == 'hourly':
            url = 'https://fawn.ifas.ufl.edu/controller.php/today/obs//{0:02};csv?asText=1'.format(climate_id)
            write_log(pf, url) 
        else:
            url = 'https://fawn.ifas.ufl.edu/controller.php/today/obs//{0:02};csv?asText=1'.format(climate_id)

            write_log(pf, url) 

        r = requests.get(url)
        lines = r.text.split()
        prev_tokens = None
        prev_date = None
        for i, l in enumerate(lines):
            write_log(pf, l) 

#            tokens = l.split(',')
#            if len(tokens) < 3:
#                print('AzMET: No data for station {0}'.format(climate_id))
#                break
#            year, doy, hour = int(tokens[0]), int(tokens[1]), int(tokens[2])
#            if timestep == 'hourly':
#                # hour 24 is 0 hour the next day.
#                if hour == 24:
#                    doy += 1
#                    hour = 0
#                station_date = datetime.datetime(year, 1, 1, hour=hour) + #datetime.timedelta(days=doy - 1)
#            else:
#                station_date = datetime.datetime(year, 1, 1) + #datetime.timedelta(days=doy - 1)

#            eastern = pytz.timezone('US/Eastern')
#            station_date = eastern.localize(station_date)
            # Comvert to UTC.
#            station_date = station_date.astimezone(pytz.utc)

#            if station_date > extraction_date:
#                if timestep == 'hourly':
#                    # Add previous hour and this hour.
#                    etr, tave = float(prev_tokens[15]), float(prev_tokens[3])
#                    ret.append([prev_date, etr, tave])
#                    etr, tave = float(tokens[15]), float(tokens[3])
#                    ret.append([station_date, etr, tave])
#                else:
#                    etr, wrun = float(prev_tokens[24]), float(prev_tokens[18])
#                    ret.append([station_date, wrun, etr])
#                break
#            prev_date = station_date
#            prev_tokens = tokens
#        return ret
        return
