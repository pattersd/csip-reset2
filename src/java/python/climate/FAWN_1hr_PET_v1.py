import pandas as pd
import numpy as np
import datetime
#LAGimport matplotlib.pyplot as mplot
#LAGimport matplotlib.pyplot as plt
import os
path = "/tmp/csip/python/climate/"
#LAGpath= r"C:\ref_et"
#LAGos.chdir(path)
#LAGfn = "260.csv"
class FAWN_1hr_PET_v1(object):
    def __init__(self, logger=None, ignore_cache=False):
        self.climate_data = {}
        self.logger = logger or logging.getLogger()
        self.local_timezone = pytz.timezone('US/Eastern')

#LG        pf = open("progress2.log", "w+") #LAGT
#LG        write_log(pf, "Inside of Azmet") 

def ETh(fn, fn1): #formate ETh(fn)  file name   etrh is the hourly PET

    dfo = pd.read_csv(fn, sep=',', error_bad_lines=False, index_col=False, dtype='unicode')

    dfo.columns = dfo.columns.str.replace(' ', '_')
    dfo["t2m_n"] = pd.to_numeric(dfo["t2m"], downcast="float")

    dfo["rfd_n"] = pd.to_numeric(dfo["rfd"], downcast="float")

    dfo["ws_n"] = pd.to_numeric(dfo["ws"], downcast="float")

    dfo["rh_n"] = pd.to_numeric(dfo["rh"], downcast="float")
    #
    dfo['hour'] = dfo.endTime.str[11:13]
    print(dfo.head(10))
    # df = dfo.groupby('hour').agg({'t2m': ['mean']})
    df=dfo.groupby('hour').mean()
    print(df.head(10))
    df.to_csv('test.csv')

    # dfo["t2m_n"] = pd.to_numeric(dfo["t2m"], downcast="float")
    # print(df.head(10))
    # print(df.columns)
    # df = pd.read_csv(fn, index_col='Period', parse_dates=['Period'])
    # print (df)
    # print(df.index)
    # df["TIMESTAMP_END"] = pd.to_datetime(df["TIMESTAMP_END"])
    Z = 100  # assumed elevation in meters
    df['tsmax'] = ((df['t2m_n']).astype(float) - 0)
    df['tsmin'] = ((df['t2m_n']).astype(float) - 0)
    df['tsav'] = ((df['t2m_n']).astype(float) - 0)
    df['esmin'] = 0.6108 * np.exp(((df.tsmin).astype(float) * 17.27) / ((df.tsmin).astype(
        float) + 237.3))  # Calculate min saturation vapor pressure '"0.6108*exp((17.27*A)/(A+237.3))" '
    df['esmax'] = 0.6108 * np.exp(((df.tsmax).astype(float) * 17.27) / (
                (df.tsmax).astype(float) + 237.3))  # Calculate max saturation vapor pressure
    df['es1'] = (df.esmin + df.esmax.astype(float)) * 0.5
    df['ea'] = df.es1 * (df['rh_n']).astype(float) * 0.01
    df['VPD1'] = df.es1 - df.ea  # kpa Calculate vapor pressure deficit (VPD)
    df['s1'] = 4098 * (0.6108 * np.exp((17.27 * (df.tsav).astype(float)) / ((df.tsav).astype(float) + 237.3)))
    df['s2'] = (((df.tsav).astype(float) + 237.3) * ((df.tsav).astype(float) + 237.3))
    df['s'] = df.s1 / df.s2
    # df['P1']=(df['BP_avg_(mb)']).astype(float)*0.1  # 101.3 - 0.0115 * Z + 5.44 * 10**7 * Z**2 #Calculate barometric pressure
    P1 =101.3 * ((293 - .0065 * Z) / 293) ** 5.26 # 101.3 - 0.0115 * Z + 5.44 * 10 ** 7 * Z ** 2  # Calculate barometric pressure '"101.3 * ((293 - .0065 * A) / 293) ** 5.26" '
    df['lhv'] = 2.501 - (2.361 * 10 ** -3) * df.tsav.astype(float)
    # df['GAM']=(df.P1.astype(float) * 0.001013 / (0.622 * df.lhv.astype(float)))
    df['GAM'] = (P1 * 0.001013 / (0.622 * df.lhv.astype(float)))
    df['tday'] = ((df.tsmax.astype(float) - df.tsav.astype(float)) * .45 + df.tsav.astype(float))
    # air emissivity
    df['em'] = 1 - 0.261 * np.exp(-7.77 * 10 ** -4 * (273 - (df.tsav.astype(float) + 237.15)) ** 2)  ## emisivity
    # net radiation  stefan_boltzman_w m2 = str(5.670373 * 10 ** -8)   #OK
    #                 stefan_boltzman_MJ = float(4.903 * (10) ** -9) #OK
    df['nr'] = .0036 * (((1 - 0.23) *((df['rfd_n']).astype(float)) + ((((df.em).astype(float)) - 0.97) * (4.903 * (10) ** -9) * (273.15 + (df.tsav).astype(float)) ** 4)))  # 0.0036*((1 - 0.23) * (SWGDN_Rs) + ((em) - 0.97) * (4.903 * (10) ** -9) * (273.15 + t) ** 4)got 15 min Rn  multiply by 4 to convert to houraly radiationdivided by 24 to convert it from daily to hourly and by 4 to go from 15 to hourly
    # converting wind from mph to m/s at 2 m
    df['ww'] = (df['ws_n']).astype(float) * 0.277778 * .75  # wind run converted from km/hr to m/s  and from 10 m to 2 m
    # calculating hourly ETr and ETo
    df['x1'] = df.s.astype(float) * df.nr.astype(float) * 0.408
    df['x2'] = df.VPD1.astype(float) * df.GAM.astype(float) * df.ww.astype(float) * (
                37.5 / (df.tday.astype(float) + 273))  # sort crop ET0 37/.24  and   tall crop ETr 66/.25
    df['x3'] = df.s
    df['x4'] = df.GAM.astype(float) * (1 + 0.34 * df.ww.astype(float))
    df['etrh'] = (df['x1'] + df['x2']) / (df['x3'] + df['x4'])
#    df['hour'] = df.endTime.str[11:13]
    # df['ETH']=""
    print(df.head(10))

    # df.ETH=df.loc[df['Period'] == dh]
#    df2 = df.groupby(['hour']).sum()
    fn2 = '/tmp/csip/python/climate/' + fn1[:-4]+'_PET.csv' 
    df.to_csv('PET_FAWAN_H6060.csv')
    return df.to_csv(fn2)
    # return df.to_csv('ET_HAll.csv')

#LAGETh(fn)