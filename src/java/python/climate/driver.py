import datetime
import json
import logging
import sys
from agroet_climate.main import Climate


def run_test(logger, aoi_path, year, jul_start, jul_end):
    climate = Climate(aoi_path, logger=logger)

    start_date = datetime.date(year, 1, 1)
    for t in range(jul_start, jul_end + 1):
        daily_date = start_date + datetime.timedelta(days=t)
        hourly_date = datetime.datetime(daily_date.year, daily_date.month, daily_date.day, hour=18,
                                        tzinfo=datetime.timezone.utc)
        weather_geojson = climate.extract_climate(daily_date, hourly_date)
        logger.info(f'Date is {daily_date}')
        logger.info(weather_geojson)
        with open(f'weather_{daily_date}.geojson', 'w') as fp:
            json.dump(weather_geojson, fp, sort_keys=True, indent=4)
        break


if __name__ == '__main__':
    logging.basicConfig(level='INFO')
    # Extract ten days of weather
    # aoi_path = 'aoi_4326.shp'
    if len(sys.argv) == 1:
        print('Arguments: boundary layer')
        sys.exit()
    aoi_path = sys.argv[1]
    run_test(logging.getLogger(), aoi_path, 2019, 100, 110)

    # Check results
    with open('weather_2019-04-11.geojson') as fp:
        j = json.load(fp)
    tests_passed = True
    for station in j['features']:
        if station['properties']['name'] == 'Calipatria/Mulberry':
            expected = 5.56 * 1.2
            if station['properties']['etr_mm'] != expected:
                logging.error(f"Got bad value for etr. Expected {expected} but got {station['properties']['etr_mm']}")
                tests_passed = False
            # 10:00 UTC == 2:00 PST
            expected = 24.5
            if station['properties']['tave_hourly_c'] != expected:
                logging.error(
                    f"Got bad value for etr. Expected {expected} but got {station['properties']['tave_hourly_c']}")
                tests_passed = False
    if tests_passed:
        logging.info(f"Tests passed")
