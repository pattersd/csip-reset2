import datetime
import json
import logging
import os
import pytz
import requests
from shapely.geometry import Point
import datetime
from datetime import date, timedelta
import math
import time
from cache_db import Cache
#from AgroET_library import write_log 


class AgroetException(Exception):
    pass


def date_to_mjd(yr_m_d):
    """
    Modified function to convert a date to Julian Day.  It sets Dec 31, 1979 as date 0 by subtracting 2444238.5 from absolute julian date
    Algorithm from 'Practical Astronomy with your Calculator or Spreadsheet',
        4th ed., Duffet-Smith and Zwart, 2011.
    Parameters
    ----------
    month : int
        Month as integer, Jan = 1, Feb. = 2, etc.
    day : int
        Day
    year : int
        Year as integer. Years preceding 1 A.D. should be 0 or negative.
        The year before 1 A.D. is 0, 10 B.C. is year -9.
    date_flag == 0 (means it is year and DOY) or date_flag == 1 (means it is Year-Month-Day)
    Returns
    -------
    jd : int
        Julian Day
    Examples
    --------
    February 17, 1985 to Julian Day
    >>> date_to_jd(1985,2,17)
    1875
    """

    month = int(yr_m_d.strftime("%m"))
    #    write_log("inside date_to_mjd date month is ", month)
    day = int(yr_m_d.strftime("%d"))
    #    write_log("inside date_to_mjd day is ", day)
    year = int(yr_m_d.strftime("%Y"))

    if month == 1 or month == 2:
        yearp = year - 1
        monthp = month + 12
    else:
        yearp = year
        monthp = month
    # this checks where we are in relation to October 15, 1582, the beginning
    # of the Gregorian calendar.
    if ((year < 1582) or
            (year == 1582 and month < 10) or
            (year == 1582 and month == 10 and day < 15)):
        # before start of Gregorian calendar
        B = 0
    else:
        # after start of Gregorian calendar
        A = math.trunc(yearp / 100.)
        B = 2 - A + math.trunc(A / 4.)
    if yearp < 0:
        C = math.trunc((365.25 * yearp) - 0.75)
    else:
        C = math.trunc(365.25 * yearp)
    D = math.trunc(30.6001 * (monthp + 1))
    jd = B + C + D + day + 1720994.5
    # Set julian date 1 as January 1st 1980 by subtracting 2444238.5
    i_jd = int(jd - 2444238.5)
    return i_jd


class Cimis(object):
    stations = []
# LAG 7-26-23    api_key = "89779982-b425-4ed1-915f-29521c4aa649"
    api_key = "86630001-8c53-4c0b-b06c-d77f38c5bac1"
    base_url = "https://et.water.ca.gov/api"
    headers = {"content-type": "application/json"}

    def __init__(self, logger=None, ignore_cache=False):
        self.climate_data = {}
        self.logger = logger or logging.getLogger()
        self.ignore_cache = ignore_cache
        self.cache = None
        if not ignore_cache:
            cache = Cache()
            if cache.conn:
                self.cache = cache

        # NOTE: this takes into account daylight savings. To get the real local time, use UTC-8
        #   extraction_date - datetime.timedelta(hours=8)))
        self.local_timezone = pytz.timezone("US/Pacific")

        self.logger.info("Using CIMIS!")

    def query_cache_7_11_22(self, climate_date):
        if self.cache:
            return self.cache.query(climate_date)
        return None


    def query_cache(self, climate_date, end_date):
        ret = None
        if self.cache:
            if end_date:
                ret = {}
#                write_log("inside query_cache before dt ", climate_date, end_date)
                dt = climate_date
#                write_log("inside query_cache after dt ", dt, end_date )
                while dt <= end_date:
#                    write_log("inside while dt, end_date, climate_date ", dt, end_date, climate_date)
                    if( climate_date == end_date ):
                        q = self.cache.query(dt, daily_only=False)
                    else:
                        q = self.cache.query(dt, daily_only=True)
#                    write_log("after cache.query")
                    for stationNbr, data in q.items():
#                        write_log("inside for loop stationNbr", stationNbr)
                        if not stationNbr in ret:
                            ret[stationNbr] = []
                        ret[stationNbr].extend(data)
                    dt += datetime.timedelta(days=1)
            else:
                ret = self.cache.query(climate_date)
        return ret


    def get(self, endpoint, params=None, restart_count=0):
        """Query CIMIS API"""
        max_restarts = 2
        timeout_duration = 5

        def restart():
            if restart_count < max_restarts:
                duration = timeout_duration * restart_count
                time.sleep(duration)
                return self.get(endpoint, params, restart_count + 1)
            raise AgroetException(
                "Climate extraction giving up after {0} restarts".format(max_restarts)
            )

        params = params or {}
        params["appKey"] = self.api_key
        try:
            req = requests.get(
                os.path.join(self.base_url, endpoint),
                params=params,
                headers=self.headers,
                timeout=600,
            )
        except Exception as e:
            self.logger.error("Got error making request")
            self.logger.error(e)
            raise
        if "ERR" in req.text or "Rejected" in req.text:
            self.logger.warning(f"Got CIMIS connection error: {req.text}")
            return restart()
        try:
            j = json.loads(req.text)
        except json.decoder.JSONDecodeError:
            self.logger.warning("Restarting request:")
            self.logger.warning(req.request.url)
            return restart()

        return j

    def get_stations(self, aoi_buffer_ll=None):
        """Get list of stations that are in the AOI
        :param aoi_buffer_ll Shapely geometry in 4326. If none, get all stations
        :return list of dictionaries with name and coordinates properties in lon, lat order.
        """
        # Remove stations not within 10 miles of the AOI
        ret = None
        if self.cache:
            ret = {
                "Stations": self.cache.query_stations(),
            }
        if not ret:
            ret = self.get("station")

        stations = []
        for station in ret.get("Stations", []):
            if station["IsActive"] == "True":
                x = float(station["HmsLongitude"].split()[2])
                y = float(station["HmsLatitude"].split()[2])
                coordinates = (float(x), float(y))
                geometry = Point(coordinates)
                station["geometry"] = geometry.__geo_interface__
                station["name"] = station["Name"]
                del station["Name"]
                del station["ZipCodes"]
                # removing these because of unicode issues
                del station["HmsLongitude"]
                del station["HmsLatitude"]
                # removing these because of date issues
                del station["ConnectDate"]
                del station["DisconnectDate"]
                if not aoi_buffer_ll or aoi_buffer_ll.contains(geometry):
                    stations.append(station)
        return stations

    @staticmethod
    def get_hourly_records(records):
        return [rec for rec in records if rec["Scope"] == "hourly"]


    def add_climate_data_to_stations_range(self, stations, station_data_by_id, hourly_date, end_date=None):
        """Iteratate over the station_data and modify it with the climate data.
        :param stations List of climate stations to populate.
        :param station_data_by_id Dictionary of climate date with key station ID
        :param hourly_date datetime.datetime Date to calculate hourly data on.
        """#
#        write_log("\n\ninside add_climate_data_to_stations stations hourly_date and end_date ", hourly_date, end_date ,"\n\n")
        stations_range = []
#        ind_stations_range = []
        stat_cnt = 0
        for s_id, records in station_data_by_id.items():
            stations_filter = [s for s in stations if s["StationNbr"] == s_id]
#            write_log("Inside add_climate_data_to_stations - len(stations_filter) ", stations_filter)
            if len(stations_filter):
                station = stations_filter[0]
#                write_log("inside add_climate_data_to_stations_range - station ", station, "\n\n")
                for irec, rec in enumerate(records):
#                    write_log("irec is ", irec)
                    if rec["Scope"] == "daily":
#                        write_log("rec is daily ", stat_cnt, rec)
#                        write_log("TESTING CODE TO ADD A LIST TO A LIST")
                        if rec["DayEto"]["Value"] and rec["DayWindRun"]["Value"]:
#                            write_log("before append([])")
                            stations_range.append([])
#                            write_log("after append and before stationNbr and name", int(rec["StationNbr"]), station["name"])
                            stations_range[stat_cnt].append(int(rec["StationNbr"]))
                            stations_range[stat_cnt].append(station["name"])
#                            ind_stations_range.append(int(rec["StationNbr"]))
#                            ind_stations_range.append(station["name"])
#                            write_log("after stationNbr and name", int(rec["StationNbr"]), station["name"])
                            date1 = datetime.datetime.strptime(rec["Date"], "%Y-%m-%d")
                            mjd_date = date_to_mjd(date1)
                            stations_range[stat_cnt].append(mjd_date)
#                            write_log("after date ",mjd_date)
#                            ind_stations_range.append(mjd_date)
                            etr = float(rec["DayEto"]["Value"])
                            etr *= 1.2
                            etr2 = round(etr,2)
                            stations_range[stat_cnt].append(etr2)
#                            ind_stations_range.append(etr2)
                            wind_run = float(rec["DayWindRun"]["Value"])
                            windrun1 = round(wind_run * 0.621371,1)
                            stations_range[stat_cnt].append(windrun1)
#                            ind_stations_range.append(windrun1)
#                            stations_range += ind_stations_range
#                            stations_range.append(ind_stations_range)
#                            write_log("ind_stations_range ",ind_stations_range,"\n")
#                            write_log("stations_range - cimis.py 266 ", stations_range,"\n")
 #                           ind_stations_range.clear()
#                            write_log("ind_stations_range after clear ", ind_stations_range, "\n")
                            stat_cnt +=1
#                        write_log("after stat_cnt+", stat_cnt)
#                    write_log("before appending station to stations_range", stations_range ,"\n")
#                    write_log("stations_range ", stations_range, "\n")
#        stations_range.append(station)
#        write_log("\n\n Returning from add_climate_data_to_stations_range in cimis.py ",len(stations_range), "   ",stations_range," \n\n" )
        return stations_range


    def add_climate_data_to_stations_for_LS(self, stations, station_data_by_id, hourly_date):
        """Iteratate over the station_data and modify it with the climate data.
        :param stations List of climate stations to populate.
        :param station_data_by_id Dictionary of climate date with key station ID
        :param hourly_date datetime.datetime Date to calculate hourly data on.
        """
        for s_id, records in station_data_by_id.items():
            stations_filter = [s for s in stations if s["StationNbr"] == s_id]
            if len(stations_filter):
                station = stations_filter[0]
                for irec, rec in enumerate(records):
                    if rec["Scope"] == "daily":
                        if rec["DayEto"]["Value"]:
                            etr = float(rec["DayEto"]["Value"])
                            assert rec["DayEto"]["Unit"] == "(mm)"
                            etr *= 1.2

                        if rec["DayWindRun"]["Value"]:
                            wind_run = float(rec["DayWindRun"]["Value"])
                            assert rec["DayWindRun"]["Unit"] == "(m/s)"
                            station["etr_mm"] = etr
                            # m/s to miles/day
                            # units are actually km/day?
                            station["wrun_mile_day"] = wind_run * 0.621371

                if hourly_date:
                    hourly_records = self.get_hourly_records(records)
                    for irec, rec in enumerate(hourly_records):
                        station_date = get_climate_date(rec["Date"], rec["Hour"])
                        if station_date > hourly_date:
                            # Calculate linear average
                            prev_rec = hourly_records[irec - 1]
                            prev_station_date = get_climate_date(
                                prev_rec["Date"], prev_rec["Hour"]
                            )

                            # Convert to minutes
                            timediff_min = (
                                hourly_date - prev_station_date
                            ).seconds / 60.0
                            if (
                                rec["HlyEto"]["Value"] is not None
                                and prev_rec["HlyEto"]["Value"] is not None
                            ):
                                try:
                                    etr1 = float(rec["HlyEto"]["Value"])
                                    etr2 = float(prev_rec["HlyEto"]["Value"])
                                    etrdiff = etr1 - etr2
                                    # Divide by 60 minutes to get the fraction to interpolate by.
                                    #   convert to mm from inches and apply eto from etr factor
                                    etr_mm = etr2 + etrdiff * (timediff_min / 60.0)
                                    station["etr_hourly_mm"] = round(etr_mm * 1.2, 3)
                                except Exception as e:
                                    self.logger.error(e)
                                    self.logger.error(
                                        f"failed station row: {rec} - {prev_rec}"
                                    )
                            if (
                                rec["HlyAirTmp"]["Value"] is not None
                                and prev_rec["HlyAirTmp"]["Value"] is not None
                            ):
                                try:
                                    tave1 = float(rec["HlyAirTmp"]["Value"])
                                    tave2 = float(prev_rec["HlyAirTmp"]["Value"])
                                    tempdiff = tave1 - tave2
                                    tave = tave2 + tempdiff * (timediff_min / 60.0)
                                    station["tave_hourly_c"] = round(tave, 2)
#                                    station["date"] = (
#                                        hourly_date.strftime("%Y-%m-%d"),
#                                    )
                                    break
                                except Exception as e:
                                    self.logger.error(e)
                                    self.logger.error(
                                        f"failed station row: {rec} - {prev_rec}"
                                    )
        return stations


    def load_climate(self, aoi, hourly_date, end_date=None):
        """
        Get climate geojson layer with data at daily_date and hour_date
        :param aoi: shapely geometry
        :param hourly_date datetime.datetime
        :return: list of stations
        """
#        write_log("start and end date", hourly_date, end_date)

        stations = self.get_stations(aoi)
#        write_log("inside load_climate - number of CIMIS stations ", len(stations))
#        if not end_date:
#            write_log("inside load_climate - no end date provided so setting end_date to hourly_date")
#LAG 7-12-22            end_date = (hourly_date + datetime.timedelta(days=0)).strftime("%Y-%m-%d")
#            end_date = hourly_date
#        import pdb; pdb.set_trace()
        station_data_by_id = {}
        if self.cache:
            self.logger.info("Trying cache")
#            write_log("CIMIS trying cache - end date", end_date)
            station_data_by_id = self.query_cache(hourly_date, end_date)
#LAG 7-11-22            station_data_by_id = self.query_cache(hourly_date)
            if station_data_by_id:
#               write_log("CIMIS Cache succeeded")
                self.logger.info("Cache succeeded")
#COMMENTED ON 7-11-22
#            has_data = False
#            for s_id, records in station_data_by_id.items():
#                write_log("Inside of station_data_by_id.items ",records)
#                for irec, rec in enumerate(records):
#                    write_log("Inside enumerate(records) ", rec)
#                    if rec["Scope"] == "daily":
#                        if rec["DayEto"]["Value"] and rec["DayWindRun"]["Value"]:
#                            has_data = True
#                        else:
#                            write_log("Station {",s_id,"} is missing data")
#                            self.logger.warning(f"Station {s_id} is missing data")

            has_data = False
            for s_id, records in station_data_by_id.items():
#                write_log("inside station_data_by_id loop - records, s_id ", s_id)
                for rec in records:
#                    write_log("for records loop - rec ", rec)
                    if rec["Scope"] == "daily":
                        if (
                            rec["DayEto"]["Value"] is not None
                            or rec["DayWindRun"]["Value"] is not None
                        ):
                            has_data = True
                        else:
#                            write_log("Station {",s_id,"} is missing daily data")
                            self.logger.warning(
                                f"Station {s_id} is missing daily data: ${rec}"
                            )
                    else:
                        if (
                            rec["HlyEto"]["Value"] is not None
                            or rec["HlyAirTmp"]["Value"] is not None
                        ):
                            has_data = True
                        else:
#                            write_log("Station {",s_id,"} is missing hourly data")
                            self.logger.warning(
                                f"Station {s_id} is missing hourly data: ${rec}"
                            )

            if not has_data:
                # Try to query CIMIS using API
#                write_log("CIMIS trying to extract data using API")
                station_data_by_id = {}

#        import pdb; pdb.set_trace()

        if not station_data_by_id:
            chunk_size = 30
            for lim in range(chunk_size, 10000, chunk_size):
                stations_chunk = stations[lim - chunk_size : lim]
                if not stations_chunk:
                    break
                params = {
                    "startDate": (hourly_date + datetime.timedelta(days=0)).strftime(
                        "%Y-%m-%d"
                    ),
                    "endDate": (hourly_date + datetime.timedelta(days=0)).strftime(
                        "%Y-%m-%d"
                    ),
#                    "endDate": end_date,
                    "targets": ",".join([s["StationNbr"] for s in stations_chunk]),
                    "dataItems": ",".join(
                        ["day-eto", "day-wind-run", "hly-eto", "hly-air-tmp"]
                    ),
                    "unitOfMeasure": "M",
                }
                climate = self.get("data", params)
                try:
                    for provider in climate["Data"]["Providers"]:
                        for rec in provider["Records"]:
                            station = [
                                s for s in stations if s["StationNbr"] == rec["Station"]
                            ][0]
                            s_id = station["StationNbr"]
                            # initialize station data
                            if s_id not in station_data_by_id:
                                station_data_by_id[s_id] = []
                            station_data_by_id[s_id].append(rec)
                except Exception as e:
                    raise Exception(
                        "Got an error retrieving CIMIS data ({0}). Please try again shortly".format(
                            e
                        )
                    )

        self.climate_data = station_data_by_id
#        write_log("end of load_data in cimis.py - climate data ", self.climate_data )
#        write_log("end of load_data in cimis.py - climate data " )

#8-10-22        if(hourly_date == end_date ):
        if end_date == None:
            return self.add_climate_data_to_stations_for_LS(
                stations, station_data_by_id, hourly_date
            )
        else:
            return self.add_climate_data_to_stations_range(
                stations, station_data_by_id, hourly_date, end_date
            )

    def load_climate_range(self, aoi, start_date, end_date):
#        write_log("inside CIMIS load_climate_range BEFORE calling load_climate ")
#        start = datetime.datetime.strptime(start_date, "%Y-%m-%d")
#        end = datetime.datetime.strptime(end_date, "%Y-%m-%d")
        ret = self.load_climate(aoi, start_date, end_date)
#        ret = self.load_climate(aoi, start, end)
#        write_log("inside CIMIS load_climate_range AFTER calling load_climate - this is the data daily data it retrieved len(ret) ", len(ret))
#        spreadsheet = [["Stn ID", "Stn Name", "Date", "ETo (mm)", "qc"]]
#        for feat in ret:
#            write_log("inside feat loop ",feat)
#            spreadsheet.append(
#                [
#                    stationNbr,
#                    feat["StationNbr"],
#                    feat["name"],
#                    feat["Date"],
#                    feat["etr_mm"],
#                    "",
#                ]
#            )
#        return spreadsheet
        return ret

    def test_cimis(self):
        try:
            self.get("station", restart_count=200)
            return True
        except AgroetException:
            return False


def get_climate_date(climate_date, hr=None, convert_to_utc=True):
    ret = datetime.datetime.strptime(climate_date, "%Y-%m-%d")
    if hr:
        if hr[:2] == "24":
            # flip to next day
            dt = datetime.datetime.strptime(climate_date, "%Y-%m-%d")
            ret = dt + datetime.timedelta(days=1)
        else:
            ret = datetime.datetime.strptime(
                "{0} {1}".format(climate_date, hr), "%Y-%m-%d %H%M"
            )
    if convert_to_utc:
        # Don't localize but just add 8 hours to get PST
        use_local_time = False
        utc = pytz.utc
        if use_local_time:
            pacific_tz = pytz.timezone("US/Pacific")
            ret = pacific_tz.localize(ret)
            ret = ret.astimezone(utc)
        else:
            ret += datetime.timedelta(hours=8)
            ret = utc.localize(ret)
    return ret
