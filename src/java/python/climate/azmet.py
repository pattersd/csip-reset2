import datetime
import json
import logging
import os
import pytz
import requests
from shapely.geometry import shape


class Azmet(object):
    def __init__(self, logger=None, ignore_cache=False):
        self.climate_data = {}
        self.logger = logger or logging.getLogger()
        self.local_timezone = pytz.timezone("US/Mountain")

    def load_climate(self, aoi, hourly_date, daily_only=False):
        data_dir = "/tmp/csip/python"
        if not os.path.exists(data_dir):
            data_dir = "/app/er2/apps/er2_reset/data"
        if not os.path.exists(data_dir):
            # local dev dir
            data_dir = "/mnt/work/work_home/work/new/reset/er2_reset2/data"
        climate_path = os.path.join(data_dir, "azmet.json")
        with open(climate_path) as fp:
            climate_layer = json.load(fp)

        stations = []
        for feat in climate_layer["features"]:
            if aoi.contains(shape(feat["geometry"])):
                feat["properties"]["name"] = feat["properties"]["Station Name"]
                del feat["properties"]["Station Name"]
                stations.append(feat["properties"])

        if not daily_only:
            for station in stations:
                climate_data = self.download_climate(
                    station["Site"], "hourly", hourly_date
                )
                etr, tave = None, None
                if climate_data:
                    station_date, etr1, tave1 = climate_data[0]
                    _, etr2, tave2 = climate_data[1]

                    # Calculate linear average
                    timediff_minutes = (hourly_date - station_date).seconds / 60.0

                    if etr1 is not None and etr2 is not None:
                        # Divide by 60 minutes to get the fraction to interpolate by.
                        frac = timediff_minutes / 60.0
                        etrdiff = etr2 - etr1
                        etr = etr1 + etrdiff * frac
                        tempdiff = tave2 - tave1
                        tave = tave1 + tempdiff * frac

                    # Find this station's geometry
                    station.update(
                        {
                            "etr_hourly_mm": etr,
                            "tave_hourly_c": tave,
                        }
                    )

        # Daily values
        for station in stations:
            climate_data = self.download_climate(station["Site"], "daily", hourly_date)
            etr, wrun = None, None
            if climate_data:
                station_date, wrun, etr = climate_data[0]
                # windrun is in m/s. Convert to miles/day
                if wrun:
                    wrun = wrun * 24 * 60 * 60 / 1000 * 0.621371
            station["etr_mm"] = etr
            station["wrun_mile_day"] = wrun
        return stations

    def load_climate_range(self, aoi, start_date, end_date):
        ndays = (end_date - start_date).days
        spreadsheet = [["Stn ID", "Stn Name", "Date", "ETo (mm)", "qc"]]
        for iday in range(ndays + 1):
            dt = start_date + datetime.timedelta(days=iday)
            stations = self.load_climate(aoi, dt, daily_only=True)
            for feat in stations:
                spreadsheet.append(
                    [
                        feat["Site"],
                        feat["name"],
                        dt.strftime("%Y-%m-%d"),
                        feat["etr_mm"],
                        "",
                    ]
                )

        return spreadsheet

    @staticmethod
    def download_climate(climate_id, timestep, extraction_date):
        ret = []
        if timestep == "hourly":
            # https://cals.arizona.edu/AZMET/raw2003.htm
            url = "https://cals.arizona.edu/AZMET/data/{0:02}{1}rh.txt".format(
                climate_id, str(extraction_date.year)[2:]
            )
        else:
            url = "https://cals.arizona.edu/AZMET/data/{0:02}{1}rd.txt".format(
                climate_id, str(extraction_date.year)[2:]
            )

        r = requests.get(url)
        lines = r.text.split()
        prev_tokens = None
        prev_date = None
        for i, l in enumerate(lines):
            tokens = l.split(",")
            if len(tokens) < 3:
                print("AzMET: No data for station {0}".format(climate_id))
                break
            year, doy, hour = int(tokens[0]), int(tokens[1]), int(tokens[2])
            if timestep == "hourly":
                # hour 24 is 0 hour the next day.
                if hour == 24:
                    doy += 1
                    hour = 0
                station_date = datetime.datetime(
                    year, 1, 1, hour=hour
                ) + datetime.timedelta(days=doy - 1)
            else:
                station_date = datetime.datetime(year, 1, 1) + datetime.timedelta(
                    days=doy - 1
                )

            mountain = pytz.timezone("US/Mountain")
            station_date = mountain.localize(station_date)
            # Comvert to UTC.
            station_date = station_date.astimezone(pytz.utc)

            if station_date > extraction_date:
                if timestep == "hourly":
                    # Add previous hour and this hour.
                    etr, tave = float(prev_tokens[15]), float(prev_tokens[3])
                    ret.append([prev_date, etr, tave])
                    etr, tave = float(tokens[15]), float(tokens[3])
                    ret.append([station_date, etr, tave])
                else:
                    etr, wrun = float(prev_tokens[24]), float(prev_tokens[18])
                    ret.append([station_date, wrun, etr])
                break
            prev_date = station_date
            prev_tokens = tokens
        return ret
