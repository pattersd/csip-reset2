import logging, os, shutil, sys
from imagery.main import Imagery
from AgroET_library import write_log

logging.basicConfig(filename="reset.log", level=logging.DEBUG)


def download_scene(scene_id, image_dir):
    logging.debug("Starting download of LANDSAT images")
    if not os.path.exists(image_dir):
        os.makedirs(image_dir)

    imagery = Imagery(pf)
    images = imagery.copy_to_workspace(scene_id, dest_dir)
    write_log("Downloaded {0} to workspace".format(scene_id))
    logging.debug("Downloaded {0} to workspace".format(scene_id))


if __name__ == "__main__":
    if sys.argv[1] == "test":
        # s_id = "LC80420362019173LGN00"
        s_id = "LE70410352011200EDC00"
        dest_dir = "/tmp/test_scene_copy"
    else:
        s_id, dest_dir = sys.argv[1:]
    download_scene(s_id, dest_dir)
