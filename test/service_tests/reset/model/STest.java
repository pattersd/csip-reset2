package service_tests.reset.model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor
 */
import csip.test.ServiceTest;
import java.io.File;
import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author od
 */
public class STest {

    @Test
    public void stest() throws Exception {
        ServiceTest.Results r = ServiceTest.run("test/" + new File(getClass().getCanonicalName().replace('.', '/')).getParent());
        Assert.assertTrue(r.getTotal() == r.getSucceeded());
    }
}
