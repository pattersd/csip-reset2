# How to build

Download the source code from here: https://bitbucket.org/pattersd/csip-reset2

Install version 11 of the java development kit [here](https://www.oracle.com/java/technologies/downloads/#java11-windows)
or directly from [here](https://www.oracle.com/java/technologies/downloads/#license-lightbox)

The csip code is compiled and built using Netbeans version 13. Install netbeans [here](https://www.apache.org/dyn/closer.cgi/netbeans/netbeans-installers/13/Apache-NetBeans-13-bin-windows-x64.exe)

You will also need to install mecurial from [here](https://www.mercurial-scm.org/release/windows/mercurial-6.1.2-x64.msi) and then download the CSIP development code from [here](https://alm.engr.colostate.edu/cb/repository/8266) if you have access to OMSLAB at CSU. Otherwise you can download the csip jar file [here](https://mavenlibs.com/jar/file/edu.colostate.omslab/csip-core).